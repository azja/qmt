#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../headers/qmtorbitals.h"
#include "gaussexp.h"
#include <math.h>
#include "../headers/qmtbform.h"
#include "../headers/qmteqsolver.h"



/////////////////////////////////////////////////////////////////////

double slater1soverlap(double R, double alfa) {
    return exp(-alfa * R)*(1 + alfa * R + alfa *alfa * R *R/3);
}

double slater1skinetic(double R, double alfa) {
    return alfa * alfa * exp(-alfa * R) * (1.0 + alfa * R - alfa*alfa *R*R/3);
}
///////////////////////////////////////////////////////////////////////////

/*
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "../../headers/qmtorbitals.h"
#include <iostream>
#include <math.h>

*/

//#define PI 3.14159265


int H2_molecule(double d, double alpha, std::vector<double>& betas) {



    Slater1S slater2(0,0,0, alpha);
    Slater1S slater1(0,d,0, alpha);
    Wannier<Slater1S> wannier1;
    Wannier<Slater1S> wannier2;

    wannier1.addOrbital(slater1,0);
    wannier2.addOrbital(slater2,0);

    wannier1.addOrbital(slater2,1);
    wannier2.addOrbital(slater1,1);


    double **M = new double*[2];

    for(int i = 0; i < 2; ++i) {
        M[i] = new double[2];
    }



    std::vector<BilinearForm> forms;

    Wannier<Slater1S>::multiply(wannier1,wannier2,M,2);
    forms.push_back(BilinearForm(M,2));

    Wannier<Slater1S>::multiply(wannier1,wannier1,M,2);
    forms.push_back(BilinearForm(M,2,-1));

    double in[2] = { 1.0, 0.0 };
    double out[2] = {0.0, 0.0 };

    NonLinearSystemSolve(forms,in,out,1.0e-8);

    //std::cout<<" "<<out[0]<<" "<<out[1];
    betas.clear();
    betas.push_back(out[0]);
    betas.push_back(out[1]);
    
    for(int i = 0; i < 2; ++i)
        delete [] M[i];
    delete [] M;


    return 0;
}


///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int main() {
    
    int N = 19;
    double alfa = 1.19378;
    double d = 1.43042;

    std::vector<double> coefs;
    std::vector<double> gammas;


    qmt::qmGtoNSlater1s(2.0,2.0 , 1.0e-5, 0.01, 0.01, coefs, gammas, N);
    
    
    std::vector<Gauss> atom1;
    std::vector<Gauss> atom2;

    

    for(int i=0; i < N; ++i) {
        atom1.push_back(Gauss(0,0,0,alfa*gammas[i]));
        atom2.push_back(Gauss(d,0,0,alfa*gammas[i]));
    }

    QmExpansion<Gauss> slater1(0,0,0,0);
    QmExpansion<Gauss> slater2(d,0,0,1);

    for(int i=0; i < N; ++i) {
        slater1.add_element(i, coefs[i], atom1[i]);
        slater2.add_element(i, coefs[i], atom2[i]);
    }

//////////////////////////////////////////////////////////////

for(int iterations = 0; iterations < 5000; ++iterations) {
   
    d = 1.0 + iterations * 0.01;
   
    for(int i=0; i < N; ++i) {
        atom1[i].set_gamma(alfa * gammas[i]);
        atom2[i].set_gamma(alfa * gammas[i]);
       
        atom1[i].set_r(0, 0, 0);
        atom2[i].set_r(d, 0, 0);
    }

    slater2.set_r(d,0,0);

/*

    std::cout<<" <slater1|slater1> ="<<QmExpansion<Gauss>::overlap(slater1,slater1)<<" real = "<<slater1soverlap(0,alfa)<<std::endl;;
    std::cout<<" <slater1|slater2> ="<<QmExpansion<Gauss>::overlap(slater1,slater2)<<" real = "<<slater1soverlap(d,alfa)<<std::endl;;
    std::cout<<" <slater2|slater1> ="<<QmExpansion<Gauss>::overlap(slater2,slater1)<<" real = "<<slater1soverlap(d,alfa)<<std::endl;;

    std::cout<<" <slater1|-nabla^2|slater1> ="<<QmExpansion<Gauss>::kinetic_integral(slater1,slater1)<<" real = "<<slater1skinetic(0,alfa)<<std::endl;;
    std::cout<<" <slater1|-nabla^2|slater2> ="<<QmExpansion<Gauss>::kinetic_integral(slater1,slater2)<<" real = "<<slater1skinetic(d,alfa)<<std::endl;;
    std::cout<<" <slater2|-nabla^2|slater1> ="<<QmExpansion<Gauss>::kinetic_integral(slater2,slater1)<<" real = "<<slater1skinetic(d,alfa)<<std::endl;;

    std::cout<<" <slater1|sum 1/r|slater1> ="<<QmExpansion<Gauss>::attractive_integral(slater1,slater1,0,0,0)<<" real = "<<-2*alfa * exp(-alfa * 0)*(alfa*0 +1)<<std::endl;;
    std::cout<<" <slater1|sum 1/r|slater2> ="<<QmExpansion<Gauss>::attractive_integral(slater1,slater2,0,0,0)<<" real = "<<-2*alfa * exp(-alfa * d)*(alfa*d +1)<<std::endl;;
    std::cout<<" <slater2|sum 1/r|slater1> ="<<QmExpansion<Gauss>::attractive_integral(slater2,slater1,d,0,0)<<" real = "<<-2*alfa * exp(-alfa * d)*(alfa*d +1)<<std::endl;;

*/

    std::vector<double> betas;
    H2_molecule( d, alfa, betas); 
//    for(int i = 0; i < 2; ++i)
//     std::cout<<"beta["<<i<<"] = "<<betas[i]<<std::endl; 

     
    QmExpansion< QmExpansion<Gauss> > wannier1(0,0,0);
    wannier1.add_element(0, betas[0], slater1);
    wannier1.add_element(1, betas[1], slater2);

    QmExpansion< QmExpansion<Gauss> > wannier2(d,0,0);
    wannier2.add_element(0, betas[0], slater2);
    wannier2.add_element(1, betas[1], slater1);
    
    double kinetic = QmExpansion< QmExpansion<Gauss> >::kinetic_integral(wannier1,wannier1);
    double attractive = QmExpansion< QmExpansion<Gauss> >::attractive_integral(wannier1,wannier1,0,0,0) + QmExpansion< QmExpansion<Gauss> >::attractive_integral(wannier1,wannier1,d,0,0);
    
    double kinetic1 = QmExpansion< QmExpansion<Gauss> >::kinetic_integral(wannier1,wannier2);
    double attractive1 = QmExpansion< QmExpansion<Gauss> >::attractive_integral(wannier1,wannier2,0,0,0) + QmExpansion< QmExpansion<Gauss> >::attractive_integral(wannier1,wannier2,d,0,0);
    
    std::cout<<d<<"  "<< kinetic + attractive<<" "<<kinetic1 + attractive1<<std::endl;; 

 }
    return 0;
}
