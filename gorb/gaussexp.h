#ifndef gaussexp_h__
#define gaussexp_h__
#include <vector>
namespace qmt {
 typedef typename std::vector<double>  d_vector;
 extern  int qmGtoNSlater1s(double q, double g0, double eps, double dq, double dg, d_vector& c,d_vector& g, int n);
}

#endif // gaussexp h