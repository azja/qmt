//============================================================================
// Name        : gaussexpansion
// Author      : Andrzej Biborski
// Version     : 1.0
// Copyright   : General Public Licence
// Description : s-like GTO expansion for s-Slater wave function of hydrogenium
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "headers/gslwrapper.h"
#include "headers/geignslv.h"
#include "headers/gelements.h"
#include "headers/gelements.h"
#include "headers/gminimizer.h"
#include "gaussexp.h"
#include <iostream>

namespace qmt {


int qmGtoNSlater1s(double q, double g0, double eps, double dq, double dg,d_vector& c ,d_vector& g, int n) {

        if( n%2 == 0 || n <=0) {
         std::cerr<<"gorbexpansion:: n = "<<n<<" should >=0 and n = 2k+1"<<std::endl;
        return EXIT_FAILURE;
        }

        GMinimizer minimizer(n);                  /*minimizer*/
        gsl_vector *eigen = gsl_vector_alloc(n);
        const gsl_vector  *result =  &minimizer.solve(q, g0, eps, dq, dg, eigen);
        
        double q_0 = gsl_vector_get(result,0);
        double g_0 = gsl_vector_get(result, 1);
        
        
        for(int k = 0; k< n; ++k) {
         c.push_back(gsl_vector_get(eigen,k));
         
         //gsl_vector_set(c, k, gsl_vector_get(eigen,k));
//         gsl_vector_set(g, k, g_0*pow(q_0, k - (n+1)/2 +1));
         g.push_back(g_0*pow(q_0, k - (n+1)/2 +1));
        }
        gsl_vector_free(eigen);

    return EXIT_SUCCESS;
}

}

