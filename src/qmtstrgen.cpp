#include "../headers/qmtstrgen.h"


std::string qmt::structure::convertFromAtomicNumber(size_t n) {
 switch(n) {
   case 1 : return "H";
   case 2 : return "He";
   case 3 : return "Li";
 }
 return "H";
}

std::string qmt::structure::printTwoBodyTerm(size_t i, size_t j, size_t k, size_t l, double factor, size_t id,
				 const qmt::QmtVector& v1,const qmt::QmtVector& v2,const qmt::QmtVector& v3,const qmt::QmtVector& v4,qmt::QmtSpin s1, qmt::QmtSpin s2){
  
  
  std::string out;
  
  if(s1 == qmt::QmtSpin::up && s2 ==qmt::QmtSpin::up)
    out =   std::to_string(i) + "cup, " +std::to_string(j) + "cup, " +std::to_string(l) + "aup, " +  std::to_string(k) + "aup,  ";
  if(s1 == qmt::QmtSpin::up && s2 ==qmt::QmtSpin::down)
    out =   std::to_string(i) + "cup, " +std::to_string(j) + "cdown, " +std::to_string(l) + "adown, " +  std::to_string(k) + "aup,  ";
  if(s1 == qmt::QmtSpin::down && s2 ==qmt::QmtSpin::down)
    out =   std::to_string(i) + "cdown, " +std::to_string(j) + "cdown, " +std::to_string(l) + "adown, " +  std::to_string(k) + "adown,  ";
  if(s1 == qmt::QmtSpin::down && s2 ==qmt::QmtSpin::up)
    out =   std::to_string(i) + "cdown, " +std::to_string(j) + "cup, " +std::to_string(l) + "aup, " +  std::to_string(k) + "adown,  ";
  out += std::to_string(factor) + ", " + std::to_string(id) + ", ";
  out += "(" + std::to_string(v1.get_x()) +", "  + std::to_string(v1.get_y()) + ", " +  std::to_string(v1.get_z()) +", " + std::to_string(i) +"), ";
  out += "(" + std::to_string(v2.get_x()) +", "  + std::to_string(v2.get_y()) + ", " +  std::to_string(v2.get_z()) +", " + std::to_string(j) +"), ";
  out += "(" + std::to_string(v4.get_x()) +", "  + std::to_string(v4.get_y()) + ", " +  std::to_string(v4.get_z()) +", " + std::to_string(l) +"), ";
  out += "(" + std::to_string(v3.get_x()) +", "  + std::to_string(v3.get_y()) + ", " +  std::to_string(v3.get_z()) +", " + std::to_string(k) +") \n";
  
  return out;
}

std::string qmt::structure::printOneBodyTerm(size_t i, size_t j, size_t id, const qmt::QmtVector& v1, const qmt::QmtVector& v2, qmt::QmtSpin s){
 
  
  std::string out;
  
  if(s == qmt::QmtSpin::up)
    out =   std::to_string(i) + "cup, " +std::to_string(j) + "aup, ";
  if(s == qmt::QmtSpin::down)
    out =   std::to_string(i) + "cdown, " +std::to_string(j) + "adown, ";
  
  out += std::to_string(id) + ", ";
  out += "(" + std::to_string(v1.get_x()) +", "  + std::to_string(v1.get_y()) + ", " +  std::to_string(v1.get_z()) +", " + std::to_string(i) +"), ";
  out += "(" + std::to_string(v2.get_x()) +", "  + std::to_string(v2.get_y()) + ", " +  std::to_string(v2.get_z()) +", " + std::to_string(j) +") \n ";
  
  return out;
}
/*
 * QmtSite implementation
 */


qmt::structure::QmtSite(const std::string& definition) {
  auto s_sites =  qmt::parser::get_delimited_words(",",definition);
  
   id_number = static_cast<size_t>std::stoi(s_sites[0]);
   double x = std::stod(s_sites[1]);
   double y = std::stod(s_sites[2]);
   double z = std::stod(s_sites[3]);
   position = qmt::QmtVector(x,y,z);
  
}


std::string qmt::structure::QmtSite::toXYZ() const{
  std::string site = std::to_string(id_number) + "	" + std::to_string(position.get_x()) + 
	  ", " + std::to_string(position.get_y()) + ", " + std::to_string(position.get_z());
  return site;
}

void qmt::structure::QmtSite::showXYZ() const {
  std::cout<<id_number<<"	"<<position.get_x()<<", "<<position.get_y()<<", "<<position.get_z();
}


/*
 * QmtBase implementation
 */

size_t qmt::structure::QmtBase::size() const {
  return sites.size();
}  

const qmt::structure::QmtSite&  qmt::structure::QmtBase::operator[](int i) const {
 return sites[i]; 
}

const qmt::structure::QmtSite& qmt::structure::QmtBase::get_site(size_t i)  {
 return sites[i];
}
  
qmt::structure::QmtBase(const std::string& definition) {
  auto sites_defs = qmt::parser::get_bracketed_words(definition,"site_definition","site_definition_end");
  for(const auto &def : sites_defs)
    sites.push_back(QmtSite(def)); 
}


/*
 * QmtBaseStructure implementation
 */

qmt::structure:: QmtBaseStructure(std::string& definition){
  //Base
  auto base_definition = qmt::parser::get_bracketed_words(definition,"base_definition","base_definition_end");
  base.reset(new qmt::structure::QmtBase(base_definition[0]);
}


std::string qmt::structure::QmtBaseStructure::toXYZ() const {
  return base->toXYZ();
}

void qmt::structure::QmtBaseStructure::showXYZ() const {
  base->showXYZ();
}

std::string qmt::structure::QmtBaseStructure::get_hamiltonian() const{

  std::string hamiltonian;
  size_t id = 0;
  //one body
  for(size_t i = 0; i < base->size(); ++i){
    for(size_t j = 0; j < base->size(); ++j){
      hamiltonian += printOneBodyTerm(i,j,id,base->get_site(i).position, base->get_site(j).position, qmt::QmtSpin::up);
      hamiltonian += printOneBodyTerm(i,j,id,base->get_site(i).position, base->get_site(j).position, qmt::QmtSpin::down);
      id++;      
    }
  }
    
  
  
}