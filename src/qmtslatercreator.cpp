#include "../headers/qmtslatercreator.h"
#include "../headers/qmtparsertools.h"
namespace qmt {

QmtSlaterCreator::QmtSlaterCreator(std::string input) {

    std::vector<std::string> parameters = qmt::parser::get_delimited_words(",",input);
    orbital_type = qmt::parser::stringToOrbitalType(parameters[0]);
    position = qmt::QmtVector(std::stod(parameters[1]),std::stod(parameters[2]), std::stod(parameters[3]));
    alpha_id=std::stoi(parameters[4]);
    if(parameters.size() > 5)
        num_gaussians=std::stoi(parameters[5]);
    else
        num_gaussians=9;
}

SlaterOrbital* QmtSlaterCreator::Create(double alpha, const qmt::QmtVector& scale) const
{
    qmt::QmtVector actual_position=qmt::QmtVector(position.get_x()*scale.get_x(),position.get_y()*scale.get_y(),position.get_z()*scale.get_z());
    switch (orbital_type) {
    case qmt::OrbitalType::_1s:
        return new qmt::SltOrb_1s(actual_position,alpha,num_gaussians);
    case qmt::OrbitalType::_2s:
        return new qmt::SltOrb_2s(actual_position,alpha);
    case qmt::OrbitalType::_2p_x:
        return new qmt::SltOrb_2pX(actual_position,alpha);
    case qmt::OrbitalType::_2p_xp:
        return new qmt::SltOrb_2pX(actual_position,alpha,1);
    case qmt::OrbitalType::_2p_xm:
        return new qmt::SltOrb_2pX(actual_position,alpha,-1);
    case qmt::OrbitalType::_2p_y:
        return new qmt::SltOrb_2pY(actual_position,alpha);
    case qmt::OrbitalType::_2p_yp:
        return new qmt::SltOrb_2pY(actual_position,alpha,1);
    case qmt::OrbitalType::_2p_ym:
        return new qmt::SltOrb_2pY(actual_position,alpha,-1);
    case qmt::OrbitalType::_2p_z:
        return new qmt::SltOrb_2pZ(actual_position,alpha);
    case qmt::OrbitalType::_2p_zp:
        return new qmt::SltOrb_2pZ(actual_position,alpha,1);
    case qmt::OrbitalType::_2p_zm:
        return new qmt::SltOrb_2pZ(actual_position,alpha,-1);
    case qmt::OrbitalType::_3s:
        return new qmt::SltOrb_3s(actual_position,alpha);
    case qmt::OrbitalType::_3p_x:
        return new qmt::SltOrb_3pX(actual_position,alpha);
    case qmt::OrbitalType::_3p_y:
        return new qmt::SltOrb_3pY(actual_position,alpha);
    case qmt::OrbitalType::_3p_z:
        return new qmt::SltOrb_3pZ(actual_position,alpha);
    default:
        return NULL;
    }
}

int QmtSlaterCreator::get_alpha_id() const{
    return alpha_id;
}

std::ostream& operator<<(std::ostream& stream, const qmt::QmtSlaterCreator& creator) {
    stream<<"["<<qmt::parser::orbitalTypeToString(creator.orbital_type)<<", "<<creator.position<<", "<<creator.alpha_id<<"]";
    return stream;
}

} //end of namespace qmt
