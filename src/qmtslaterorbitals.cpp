/*
 * qmtslaterorbitals.cpp
 *
 *  Created on: 22 kwi 2015
 *      Author: akadzielawa
 */

#include "../headers/qmtslaterorbitals.h"

/*
 *
 *	Main Slater Orbital Class
 *
 */

double qmt::SlaterOrbital::overlap (const SlaterOrbital& O1,const SlaterOrbital& O2) {
    return g_omp::overlap(O1.STO_nG, O2.STO_nG);
}

double qmt::SlaterOrbital::overlap (const SlaterOrbital& O1,const SlaterOrbital& O2, const qmt_vector t1, const qmt_vector t2) {
    return g_omp::overlap(O1.STO_nG, O2.STO_nG, t1, t2);
}

double qmt::SlaterOrbital::attractive_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, double rx, double ry, double rz) {
    return g_omp::attractive_integral (O1.STO_nG, O2.STO_nG,rx,ry,rz);
}

double qmt::SlaterOrbital::attractive_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const qmt_vector t1, const qmt_vector t2, double rx, double ry, double rz) {
    return g_omp::attractive_integral (O1.STO_nG, O2.STO_nG, t1, t2,rx,ry,rz);
}

double qmt::SlaterOrbital::kinetic_integral (const SlaterOrbital& O1,const SlaterOrbital& O2) {
    return g_omp::kinetic_integral (O1.STO_nG, O2.STO_nG);
}

double qmt::SlaterOrbital::kinetic_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const qmt_vector t1, const qmt_vector t2) {
    return g_omp::kinetic_integral (O1.STO_nG, O2.STO_nG, t1, t2);
}

double qmt::SlaterOrbital::v_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const SlaterOrbital& O3,const SlaterOrbital& O4) {
    return g_omp::v_integral (O1.STO_nG, O2.STO_nG, O3.STO_nG, O4.STO_nG);
}

double qmt::SlaterOrbital::v_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const SlaterOrbital& O3,const SlaterOrbital& O4, const qmt_vector t1, const qmt_vector t2, const qmt_vector t3, const qmt_vector t4) {
    return g_omp::v_integral (O1.STO_nG, O2.STO_nG, O3.STO_nG, O4.STO_nG, t1, t2, t3, t4);
}


double qmt::SlaterOrbital::get_value(const  SlaterOrbital& O,const qmt_vector& position,const qmt_vector& translation) {
    return g_omp::get_value(O.STO_nG,position, translation);
}


/*
 *
 * 1s STO
 *
 */


qmt::SltOrb_1s::SltOrb_1s(qmt::QmtVector r, double alpha, unsigned int Num_Gaussians) {
    std::vector<double> coefs;
    std::vector<double> gammas;

    qmt::qmGtoNSlater1s(2.0, 2.0, 1.0e-12, 1.0e-06, 1.0e-06, coefs, gammas, Num_Gaussians);

    gaussians.clear();
    gaussians.reserve(Num_Gaussians);

    STO_nG.clear_elements();

    for(unsigned int i=0; i<Num_Gaussians; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z(),alpha*gammas[i]));
        STO_nG.add_element(i,coefs[i],gaussians[i]);
    }
}

qmt::SltOrb_1s::SltOrb_1s(qmt::QmtVector r, double alpha, std::vector<double> coefs, std::vector<double> gammas, unsigned int Num_Gaussians) {

    gaussians.clear();
    gaussians.reserve(Num_Gaussians);

    STO_nG.clear_elements();

    for(unsigned int i=0; i<Num_Gaussians; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z(),alpha*gammas[i]));
        STO_nG.add_element(i,coefs[i],gaussians[i]);
    }
}

qmt::SltOrb_1s::SltOrb_1s(const qmt::SltOrb_1s& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);

    STO_nG=orbital.STO_nG;
}

/*
 *
 * 2s STO
 *
 */

qmt::SltOrb_2s::SltOrb_2s(qmt::QmtVector r, double alpha) {

    std::vector<double> Gammas2s({0.324774, 2.1969, 0.306742, 7.49669, 0.0100032});
    std::vector<double> Coefs2s({0.720285, -0.0380166, 0.283096, -0.00212869, 0.00183898});
    gaussians.clear();
    gaussians.reserve(5); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z(),alpha*Gammas2s[i]));
        STO_nG.add_element(i,Coefs2s[i],gaussians[i]);
    }


}

qmt::SltOrb_2s::SltOrb_2s(const qmt::SltOrb_2s& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}

/*
 *
 * 2pX STO
 *
 */

qmt::SltOrb_2pX::SltOrb_2pX(qmt::QmtVector r, double alpha, int phase) {

    std::vector<double> Gammas2p({0.32903, 0.329026, 0.694091, 0.328841, 0.329304});
    std::vector<double> Coefs2p({0.837515, 0.200614, 0.424416, 0.0000340649, 0.0000123895});
    gaussians.clear();
    gaussians.reserve(10); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x()+1.0/alpha,r.get_y(),r.get_z(),alpha*Gammas2p[i]));
        STO_nG.add_element(i,phase * Coefs2p[i],gaussians[i]);
    }
    for(unsigned int i=5; i<10; ++i) {
        gaussians.push_back(Gauss(r.get_x()-1.0/alpha,r.get_y(),r.get_z(),alpha*Gammas2p[i-5]));
        STO_nG.add_element(i,-phase*Coefs2p[i-5],gaussians[i]);
    }
}

qmt::SltOrb_2pX::SltOrb_2pX(const qmt::SltOrb_2pX& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}
/*
 *
 * 2pY STO
 *
 */

qmt::SltOrb_2pY::SltOrb_2pY(qmt::QmtVector r, double alpha, int phase) {

    std::vector<double> Gammas2p({0.32903, 0.329026, 0.694091, 0.328841, 0.329304});
    std::vector<double> Coefs2p({0.837515, 0.200614, 0.424416, 0.0000340649, 0.0000123895});
    gaussians.clear();
    gaussians.reserve(10); // only NG=5 is now possible

    STO_nG.clear_elements();


    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y()+1.0/alpha,r.get_z(),alpha*Gammas2p[i]));
        STO_nG.add_element(i,phase * Coefs2p[i],gaussians[i]);
    }
    for(unsigned int i=5; i<10; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y()-1.0/alpha,r.get_z(),alpha*Gammas2p[i-5]));
        STO_nG.add_element(i,-phase*Coefs2p[i-5],gaussians[i]);
    }

}

qmt::SltOrb_2pY::SltOrb_2pY(const qmt::SltOrb_2pY& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}
/*
 *
 * 2pZ STO
 *
 */

qmt::SltOrb_2pZ::SltOrb_2pZ(qmt::QmtVector r, double alpha, int phase) {

    std::vector<double> Gammas2p({0.32903, 0.329026, 0.694091, 0.328841, 0.329304});
    std::vector<double> Coefs2p({0.837515, 0.200614, 0.424416, 0.0000340649, 0.0000123895});
    gaussians.clear();
    gaussians.reserve(10); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z()+1.0/alpha,alpha*Gammas2p[i]));
        STO_nG.add_element(i,phase * Coefs2p[i],gaussians[i]);
    }
    for(unsigned int i=5; i<10; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z()-1.0/alpha,alpha*Gammas2p[i-5]));
        STO_nG.add_element(i,-phase * Coefs2p[i-5],gaussians[i]);
    }
}

qmt::SltOrb_2pZ::SltOrb_2pZ(const qmt::SltOrb_2pZ& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}

/*
 *
 * 3s STO
 *
 */

qmt::SltOrb_3s::SltOrb_3s(qmt::QmtVector r, double alpha) {

    std::vector<double> Gammas3s({0.2417800024746346, 0.8231141480402558, 0.6274184436008862, 0.9024835811277047, 0.024719898323713952});
    std::vector<double> Coefs3s({1.051724981022243, -0.14232528396408514, -0.007000557042354074, -0.004559869152580718, 0.004102693742485604});
    gaussians.clear();
    gaussians.reserve(5); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z(),alpha*Gammas3s[i]));
        STO_nG.add_element(i,Coefs3s[i],gaussians[i]);
    }


}

qmt::SltOrb_3s::SltOrb_3s(const qmt::SltOrb_3s& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}

/*
 *
 * 3pX STO
 *
 */

qmt::SltOrb_3pX::SltOrb_3pX(qmt::QmtVector r, double alpha, int phase) {

    std::vector<double> Gammas3p({0.392549, 0.256936, 0.482985, 0.169962, 0.0355175});
    std::vector<double> Coefs3p({0.581015, 1.0859, 0.0616235, 0.156355, 0.00108255});
    gaussians.clear();
    gaussians.reserve(10); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x()+1.0/alpha,r.get_y(),r.get_z(),alpha*Gammas3p[i]));
        STO_nG.add_element(i,phase * Coefs3p[i],gaussians[i]);
    }
    for(unsigned int i=5; i<10; ++i) {
        gaussians.push_back(Gauss(r.get_x()-1.0/alpha,r.get_y(),r.get_z(),alpha*Gammas3p[i-5]));
        STO_nG.add_element(i,-phase*Coefs3p[i-5],gaussians[i]);
    }
}

qmt::SltOrb_3pX::SltOrb_3pX(const qmt::SltOrb_3pX& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}
/*
 *
 * 3pY STO
 *
 */

qmt::SltOrb_3pY::SltOrb_3pY(qmt::QmtVector r, double alpha,int  phase) {

    std::vector<double> Gammas3p({0.392549, 0.256936, 0.482985, 0.169962, 0.0355175});
    std::vector<double> Coefs3p({0.581015, 1.0859, 0.0616235, 0.156355, 0.00108255});
    gaussians.clear();
    gaussians.reserve(10); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y()+1.0/alpha,r.get_z(),alpha*Gammas3p[i]));
        STO_nG.add_element(i,phase * Coefs3p[i],gaussians[i]);
    }
    for(unsigned int i=5; i<10; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y()-1.0/alpha,r.get_z(),alpha*Gammas3p[i-5]));
        STO_nG.add_element(i,-phase * Coefs3p[i-5],gaussians[i]);
    }

}

qmt::SltOrb_3pY::SltOrb_3pY(const qmt::SltOrb_3pY& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}
/*
 *
 * 3pZ STO
 *
 */

qmt::SltOrb_3pZ::SltOrb_3pZ(qmt::QmtVector r, double alpha,int phase) {

    std::vector<double> Gammas3p({0.392549, 0.256936, 0.482985, 0.169962, 0.0355175});
    std::vector<double> Coefs3p({0.581015, 1.0859, 0.0616235, 0.156355, 0.00108255});
    gaussians.clear();
    gaussians.reserve(10); // only NG=5 is now possible

    STO_nG.clear_elements();

    for(unsigned int i=0; i<5; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z()+1.0/alpha,alpha*Gammas3p[i]));
        STO_nG.add_element(i,phase * Coefs3p[i],gaussians[i]);
    }
    for(unsigned int i=5; i<10; ++i) {
        gaussians.push_back(Gauss(r.get_x(),r.get_y(),r.get_z()-1.0/alpha,alpha*Gammas3p[i-5]));
        STO_nG.add_element(i,-phase * Coefs3p[i-5],gaussians[i]);
    }
}

qmt::SltOrb_3pZ::SltOrb_3pZ(const qmt::SltOrb_3pZ& orbital) {
    for(auto &gaussian : orbital.gaussians)
        gaussians.push_back(gaussian);
    STO_nG=orbital.STO_nG;
}
