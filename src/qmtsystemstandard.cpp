#include "../headers/qmtsystemstandard.h"


qmt::QmtSystemStandard::QmtSystemStandard(const char* filename) {
 std::ifstream cfile;       
 cfile.open(filename); 
  std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
   std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
   std::string definitions_fn = qmt::parser::get_bracketed_words(content,"wfs_definitions","wfs_definitions_end")[0];
   std::string megacell_fn = qmt::parser::get_bracketed_words(content,"megacell","megacell_end")[0];;
   std::string supercell_fn =qmt::parser::get_bracketed_words(content,"supercell","supercell_end")[0];
   int alphas_number  =std::stoi(qmt::parser::get_bracketed_words(content,"alphas_number","alphas_number_end")[0]);
   auto mode = qmt::parser::get_bracketed_words(content,"mode_definition","mode_definition_end");
  
     
   
   std::vector<double> params;
   for(auto i = 0;i < alphas_number;i++)
     params.push_back(1);
   
   cfile.close();
   if(mode.size() == 0 || (mode.size() > 0 && mode[0] != "periodic"))
     microscopic = new qmt_standard_system( hamiltonian_fn,definitions_fn,supercell_fn,megacell_fn,params); 
   else if(mode.size() > 0 && mode[0] == "move")
     microscopic = new qmt_standard_system_move( hamiltonian_fn,definitions_fn,supercell_fn,megacell_fn,params); 
   else if(mode.size() > 0 && mode[0] == "periodic") {
     std::string geometry_fn = qmt::parser::get_bracketed_words(content,"geometry_file_name","geometry_file_name_end")[0];
     microscopic = new qmt_invariant_system(hamiltonian_fn,definitions_fn,supercell_fn,geometry_fn,params);
   }
   else{
     std::cerr<<"Error in config file!"<<std::endl;
     exit(-127);
   }
}


qmt::QmtSystemStandard::~QmtSystemStandard() {
  if(microscopic != nullptr)
    delete microscopic;
}



int qmt::QmtSystemStandard::get_one_body_number(){
  return microscopic->number_of_one_body() + 1;  // "0" is external field - here, Coulomb interaction for the system 
}


int qmt::QmtSystemStandard::get_two_body_number(){
  return microscopic->number_of_two_body();
}


double  qmt::QmtSystemStandard::get_one_body_integral(size_t index){
  if(index == 0) return microscopic->get_coulomb_repulsion();
  return microscopic->get_one_body(index - 1);
}


double  qmt::QmtSystemStandard::get_two_body_integral(size_t index){
  return microscopic->get_two_body(index);
}

void qmt::QmtSystemStandard::set_parameters(std::vector<double> params,const qmt::QmtVector& scale) {
  microscopic ->set_parameters(params,scale);
}

double qmt::QmtSystemStandard::get_overlap(int i, int j,const qmt::QmtVector& first, const qmt::QmtVector& second) {
  return microscopic ->get_overlap(i,j,first,second);
}

double qmt::QmtSystemStandard::get_wfs_products_sum(const qmt::QmtVector& r,const std::vector<double>& averages, const std::vector<int>& index_map) const {
  return  microscopic->get_wfs_products_sum(r, averages,index_map);
}

double qmt::QmtSystemStandard::get_wfs_value(size_t id,const qmt::QmtVector& r) const {
  return microscopic->get_wfs_value(id,r);
}

qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int>>>>* qmt::QmtSystemStandard::get_microscopic(){
  return microscopic;
}
