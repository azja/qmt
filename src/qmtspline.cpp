#include "../headers/qmtspline.h"

#include <iostream>
#ifdef GSL_SUPPORT
//////////////////////////////:QmtSpline1dGsl///////////////////////////////////////

qmt::QmtSpline1dGsl::QmtSpline1dGsl(double *x, double *y, size_t size) {
	acc= gsl_interp_accel_alloc ();
	spline = gsl_spline_alloc (gsl_interp_cspline, size);
	gsl_spline_init (spline, x, y, size);
}

double qmt::QmtSpline1dGsl::get_value(double x) {
        return gsl_spline_eval (spline, x, acc);
}


qmt::QmtSpline1dGsl::~QmtSpline1dGsl() {
	gsl_spline_free (spline);
	gsl_interp_accel_free (acc);
}

#endif
//////////////////////////////QmtTableFunction1d///////////////////////////////////////

qmt::QmtTableFunction1d::QmtTableFunction1d(double *x, double *y, size_t size){
  
  for(int i = 0; i < int(size);++i) {
    args.push_back(x[i]);
    values.push_back(y[i]);
  }
}

bool qmt::QmtTableFunction1d::comparer(const double t1,const double t2) {
  return t1 < t2;
}

double qmt::QmtTableFunction1d::get_value(double x) {
  auto it = std::lower_bound(args.begin(),args.end(),x);
  auto id = static_cast<int>(it - args.begin());
  return values[id];
}