#include "../headers/qmtlncdiag.h"


 qmt::QmtLanczosDiagonalizer::sq_operator qmt::QmtLanczosDiagonalizer::Op(qmt::QmtLanczosOperators o, unsigned int i){
  switch(o){
    case AUp: return sq_operator::get_up_anihilator(i);
    case CUp: return sq_operator::get_up_creator(i);
    case ADo: return sq_operator::get_down_anihilator(i);
    case CDo: return sq_operator::get_down_creator(i);
    default:  return sq_operator::get_up_anihilator(0);
  }
 }

qmt::QmtLanczosDiagonalizer::QmtLanczosDiagonalizer(const char* file_name, int option): _lanczos_steps(500), _lanczos_eps(1.0e-9) {
  
  std::ifstream cfile;       
  cfile.open(file_name); 
  
  std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
   std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
   problem_size = std::stoi(qmt::parser::get_bracketed_words(content,"problem_size","problem_size_end")[0]);
   number_of_centers=problem_size;
   electron_number = std::stoi(qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0]);
   hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(hamiltonian_fn);
   generator.generate(electron_number,problem_size*2,0,states);
//   for(auto item : states)
  // std::cout<<item<<std::endl;
   formula.set_hamiltonian(*hamiltonian);
   formula.set_StatesOptional(states,option);  
   problem_size=states.size();
   LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);
   LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);
   LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size,_lanczos_steps, _lanczos_eps); 
   cfile.close();
}

double qmt::QmtLanczosDiagonalizer::Diagonalize(const std::vector<double>& one_body_integrals, 
						const std::vector<double>& two_body_integrals) {
  double external_energy = one_body_integrals[0];
  
  #ifdef _QMT_DEBUG
  std::cout<<"Coulomb = "<<external_energy<<std::endl;
  #endif
  
  for(auto i = 1U; i < one_body_integrals.size() ;++i) {
     formula.set_microscopic_parameter(1,i-1,one_body_integrals[i]);
     
     #ifdef _QMT_DEBUG
     std::cout<<"I1["<<i-1<<"]= "<<one_body_integrals[i]<<std::endl;
     #endif
  }
  
  for(auto i = 0U; i < two_body_integrals.size() ;++i) {
     formula.set_microscopic_parameter(2,i,two_body_integrals[i]);
     #ifdef _QMT_DEBUG
     std::cout<<"I2["<<i<<"]= "<<two_body_integrals[i]<<std::endl;
     #endif
  }
  
  
  formula.get_Hamiltonian_Matrix(hamiltonian_M);
  
  LanczosSolver->solve(hamiltonian_M);
  
  LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);
 // for(int i = 0; i < problem_size;i++){
//  std::cout<<states[i]<<" "<<LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,i)<<std::endl;
 // }
 
  return LanczosSolver->get_minimal_eigenvalue() + external_energy;
}


  void qmt::QmtLanczosDiagonalizer::NWaveFunctionEssentials(std::vector<double>& averages){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);
for(int i=0; i<number_of_centers; i++){
        for(int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l])));
                }
            }
            averages.push_back(average);
        }
    }
    for(int i=0; i<number_of_centers; i++){
        for(int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l])));
                }
            }
            averages.push_back(average);
        }
    }

}

/*
/Double occupancy
*/

double qmt::QmtLanczosDiagonalizer::DoubleOccupancy(){

LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);


double d2=0.0;

for(int i=0; i<number_of_centers; i++){
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    d2+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l])))));
                }
            }
    }

d2/=number_of_centers;

return d2;
}

  double qmt::QmtLanczosDiagonalizer::AvCUpCUp(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_up_creator(i)(sq_operator::get_up_creator(j)(states[l]))));

    return result;
  }

  double qmt::QmtLanczosDiagonalizer::AvCUpCDo(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_up_creator(i)(sq_operator::get_down_creator(j)(states[l]))));

    return result;
  }

  double qmt::QmtLanczosDiagonalizer::AvCDoCDo(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_down_creator(i)(sq_operator::get_down_creator(j)(states[l]))));

    return result;
  }

  double qmt::QmtLanczosDiagonalizer::AvCUpAUp(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l]))));

    return result;
  }

  double qmt::QmtLanczosDiagonalizer::AvCUpADo(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(j)(states[l]))));

    return result;
  }

  double qmt::QmtLanczosDiagonalizer::AvCDoAUp(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(j)(states[l]))));

    return result;
  }

  double qmt::QmtLanczosDiagonalizer::AvCDoADo(unsigned int i, unsigned int j){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l]))));

    return result;
  }


double qmt::QmtLanczosDiagonalizer::Average_2(qmt::QmtLanczosOperators Op1, unsigned int s1, qmt::QmtLanczosOperators Op2, unsigned int s2){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

    auto o1 = Op(Op1,s1);
    auto o2 = Op(Op2,s2);

       for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(o1(o2(states[l]))));

    return result;
}

double qmt::QmtLanczosDiagonalizer::Average_4(qmt::QmtLanczosOperators Op1, unsigned int s1, qmt::QmtLanczosOperators Op2, unsigned int s2, qmt::QmtLanczosOperators Op3, unsigned int s3, qmt::QmtLanczosOperators Op4, unsigned int s4){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

    auto o1 = Op(Op1,s1);
    auto o2 = Op(Op2,s2);
    auto o3 = Op(Op3,s3);
    auto o4 = Op(Op4,s4);

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(o1(o2(o3(o4(states[l]))))));

    return result;
}

double qmt::QmtLanczosDiagonalizer::Average_6(qmt::QmtLanczosOperators Op1, unsigned int s1, qmt::QmtLanczosOperators Op2, unsigned int s2, qmt::QmtLanczosOperators Op3, unsigned int s3, qmt::QmtLanczosOperators Op4, unsigned int s4, qmt::QmtLanczosOperators Op5, unsigned int s5, qmt::QmtLanczosOperators Op6, unsigned int s6){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

    auto o1 = Op(Op1,s1);
    auto o2 = Op(Op2,s2);
    auto o3 = Op(Op3,s3);
    auto o4 = Op(Op4,s4);
    auto o5 = Op(Op5,s5);
    auto o6 = Op(Op6,s6);

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(o1(o2(o3(o4(o5(o6(states[l]))))))));

    return result;
}

double qmt::QmtLanczosDiagonalizer::Average_8(qmt::QmtLanczosOperators Op1, unsigned int s1, qmt::QmtLanczosOperators Op2, unsigned int s2, qmt::QmtLanczosOperators Op3, unsigned int s3, qmt::QmtLanczosOperators Op4, unsigned int s4, qmt::QmtLanczosOperators Op5, unsigned int s5, qmt::QmtLanczosOperators Op6, unsigned int s6, qmt::QmtLanczosOperators Op7, unsigned int s7, qmt::QmtLanczosOperators Op8, unsigned int s8){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);

    double result=0.0;

    auto o1 = Op(Op1,s1);
    auto o2 = Op(Op2,s2);
    auto o3 = Op(Op3,s3);
    auto o4 = Op(Op4,s4);
    auto o5 = Op(Op5,s5);
    auto o6 = Op(Op6,s6);
    auto o7 = Op(Op7,s7);
    auto o8 = Op(Op8,s8);

      for(int k=0; k<problem_size; k++)
        for(int l=0; l<problem_size; l++)
          result+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],(o1(o2(o3(o4(o5(o6(o7(o8(states[l]))))))))));

    return result;
}
/*
/Dimer-dimer
*/


double qmt::QmtLanczosDiagonalizer::DimerDimerAvs(size_t i, size_t j){

LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);


double uuuu = 0.0;
double uuud = 0.0;
double uudu = 0.0;
double uduu = 0.0;
double duuu = 0.0;

double uudd = 0.0;
double udud = 0.0;
double duud = 0.0;
double dudu = 0.0;
double dduu = 0.0;
double uddu = 0.0;

double dddd = 0.0;
double dddu = 0.0;
double ddud = 0.0;
double dudd = 0.0;
double uddd = 0.0;
 




            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){

uuuu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

uuud -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


uudu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


uduu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


duuu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

 
uudd += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


udud += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));

duud += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

dudu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

dduu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

uddu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


dddd += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


dddu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


ddud -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));

dudd -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


uddd -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));
               }
            }
  


return  0.5*(uuuu + uuud + uudu + uduu + duuu + uudd + udud + duud + dudu + dduu + uddu + dddd + dddu + ddud + dudd + uddd);


}


qmt::QmtLanczosDiagonalizer::~QmtLanczosDiagonalizer() {
  delete LanczosSolver;
}
