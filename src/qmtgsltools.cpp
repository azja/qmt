/*
 * qmtgsltools.cpp
 *
 *  Created on: 1 lip 2014
 *      Author: abiborski
 */

#include "../headers/qmtgsltools.h"


void qmt::qmt_gsl_matrix_print(const gsl_matrix* matrix) {
    for(unsigned int i=0; i<matrix->size1; i++) {
        for(unsigned int j=0; j<matrix->size2; j++)
            std::cout<<gsl_matrix_get(matrix,i,j)<<" ";
        std::cout<<std::endl;
    }
}

void qmt::qmt_gsl_vector_print(const gsl_vector* vector) {
    std::cout<<"( ";
    for(unsigned int i=0; i<vector->size; i++) {
        std::cout<<gsl_vector_get(vector,i)<<" ";
        std::cout<<")"<<std::endl;
    }
}


void qmt::qmt_gsl_vector_print_perp(const gsl_vector* vector) {

    for(unsigned int i=0; i<vector->size; i++)
        std::cout<<gsl_vector_get(vector,i)<<std::endl;

}
