#include "../headers/qmtwanniercreator.h"


namespace qmt {

//[id] [x,y,z] [slater0] [slater1] ....


qmt::QmtWannierSlaterBasedCreator::QmtWannierSlaterBasedCreator(const std::string& input) {
    std::vector<std::string> slater_defs =
        qmt::parser::get_bracketed_words(input,'[',']',true);

    std::vector<std::string> beta_ind =
        qmt::parser::get_bracketed_words(input,'(',')',true);


    id = std::stoi(slater_defs[0]);
    std::vector<std::string> pos = qmt::parser::get_delimited_words(",",slater_defs[1]);
    position = qmt::QmtVector(std::stod(pos[0]), std::stod(pos[1]), std::stod(pos[2]));
    for(unsigned int i = 2; i < slater_defs.size(); ++i)
        slater_creators.push_back(std::make_tuple(std::stoi(beta_ind[i-2]), new qmt::QmtSlaterCreator(slater_defs[i])));

}

qmt:: QmtWannierSlaterBasedCreator:: ~QmtWannierSlaterBasedCreator() {
    for(auto def : slater_creators)
        delete std::get<1>(def);
}


typename  qmt:: QmtWannierSlaterBasedCreator::Wannier* qmt::QmtWannierSlaterBasedCreator::Create(double alpha, const qmt::QmtVector& scale) const {
    qmt::QmtVector actual_position=qmt::QmtVector(position.get_x()*scale.get_x(),position.get_y()*scale.get_y(),position.get_z()*scale.get_z());
    Wannier *wannier = new Wannier(actual_position,id);
    for(const auto &def : slater_creators)
        wannier->add_element_with_cindex(std::get<0>(def),1.0, std::get<1>(def)->Create(alpha, scale));
    return wannier;
}

typename  qmt:: QmtWannierSlaterBasedCreator::Wannier* qmt::QmtWannierSlaterBasedCreator::Create(const std::vector<double> &alphas, const qmt::QmtVector& scale) const {
    qmt::QmtVector actual_position=qmt::QmtVector(position.get_x()*scale.get_x(),position.get_y()*scale.get_y(),position.get_z()*scale.get_z());
    Wannier *wannier = new Wannier(actual_position,id);
    for(const auto &def : slater_creators)
        wannier->add_element_with_cindex(std::get<0>(def),1.0, std::get<1>(def)->Create(alphas[ std::get<1>(def)->get_alpha_id()], scale));
    return wannier;
}

std::ostream& operator<<(std::ostream& stream, const qmt::QmtWannierSlaterBasedCreator& creator) {
    stream<<"["<<creator.id<<"], ["<<creator.position<<"]";
    for(const auto &def : creator.slater_creators)
        stream<<" ("<<std::get<0>(def)<<") "<<*std::get<1>(def);
    return stream;
}

int qmt:: QmtWannierSlaterBasedCreator::get_max_beta_index() const{
	int max_beta_index=0;	
	for (const auto& slater_cr : slater_creators){
		if(std::get<0>(slater_cr) > max_beta_index)
			max_beta_index=std::get<0>(slater_cr);
	}

	return max_beta_index;
}


}
