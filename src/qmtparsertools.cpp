#include <vector>
#include "../headers/qmtparsertools.h"

namespace qmt {

namespace parser {



std::vector<std::string> get_delimited_words(const std::string& delimiters, const std::string& phrase) {
    std::vector<std::string> output;
    std::string word;
    for(const auto &letter : phrase) {

        if(std::any_of(delimiters.begin(),delimiters.end(),[&](const char c)->bool { return c==letter;}) || letter == phrase[phrase.size() - 1]) {
            if(word.size() > 0 && letter != phrase[phrase.size() - 1] )
                output.push_back(word);
            else {
                if(!std::any_of(delimiters.begin(),delimiters.end(),[&](const char c)->bool { return c==letter;}))
                    word = word + letter;
                if(word.size() > 0)
                    output.push_back(word);
            }

            word.clear();
            continue;
        }
        word = word + letter;
    }
    return output;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<std::string> get_delimited_words(const std::vector<std::string>& delimiters, const std::string& phrase) {
    std::vector<std::string> output;
    std::string word;

    std::vector<std::size_t> found(delimiters.size(),0);

    for(unsigned int i=0; i<delimiters.size(); ++i)
      found[i]=phrase.find(delimiters[i],found[i]);

    auto it = std::min_element(std::begin(found),std::end(found));
    auto idx= std::distance(found.begin(), it);
    auto foundB = *it;
    auto len    = delimiters[idx].size();

    output.push_back(phrase.substr(0,foundB));

    auto check = [&found](){bool IS_END=true; for(const auto& f : found) if(f!=std::string::npos){IS_END=false; break;} return IS_END;};

    while(!check()){
      for(unsigned int i=0; i<delimiters.size(); ++i)
        found[i]=phrase.find(delimiters[i],(foundB)+1);

      it = std::min_element(std::begin(found),std::end(found));

      output.push_back(phrase.substr(foundB+len,(*it)-foundB-len));

//      std::cout<<foundB<<" "<<*it<<" "<<output.back()<<std::endl;

      if(check())  break;

      foundB = *it;
      idx= std::distance(found.begin(), it);
      len    = delimiters[idx].size();
    }
    return output;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<std::string> get_bracketed_words(const std::string& input, const char& open, const char& close, bool CLEAR_WHITE_SPACES) {
    if (open == close) {
        throw std::logic_error("qmt::parser::get_bracketed_words: \'open\' and \'close\' marks can not be the same!");
    }
    if (isspace(open)) { // function is ignoring strings consiting only of white spaces
        throw std::logic_error("qmt::parser::get_bracketed_words: \'open\' mark can not be a white space!");
    }
    if (isspace(close)) {
        throw std::logic_error("qmt::parser::get_bracketed_words: \'close\' mark can not be a white space!");
    }
    std::vector<std::string> output;

    bool IF_IN_BRACKETS=false;
    std::string tmp_str;

    for (auto iter=input.begin(); iter!=input.end(); iter++) {
        if (CLEAR_WHITE_SPACES && isspace(*iter)) continue; //if CLEAR_WHITE_SPACES is switched, function will ignore white spaces
        if (*iter == open && IF_IN_BRACKETS) continue; // ignore double opening

        if (*iter == close && IF_IN_BRACKETS) { // closing brackets; note: all open, but not closed brackets will not contribute to the resultant output
            IF_IN_BRACKETS=false;
            if (tmp_str.length()) {
                bool IS_EMPTY=true; // checking if string is not empty (is not consisted of white spaces)
                for (auto iter2=tmp_str.begin(); iter2!=tmp_str.end(); iter2++) {
                    if (!isspace(*iter2))
                        IS_EMPTY=false ;
                }
                if(!IS_EMPTY) output.push_back(tmp_str);
            }
            tmp_str.clear();
        }

        if (IF_IN_BRACKETS) {
            tmp_str+=*iter;
        }

        if (*iter == open && !IF_IN_BRACKETS) { // opening brackets
            IF_IN_BRACKETS=true;
        }
    }
    return output;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> get_bracketed_words(const std::string& input, const std::string& open, const std::string& close, bool CLEAR_WHITE_SPACES) {
    
  if (open == close) {
        throw std::logic_error("qmt::parser::get_bracketed_words: \'open\' and \'close\' marks can not be the same!");
    }
    
    std::vector<std::string> output;
    std::string current = input;
    if(CLEAR_WHITE_SPACES) remove_whitespace(current); 
    size_t open_size = open.size();
    size_t close_size = close.size();
    
    while(1) {
      size_t begin = current.find(open);
      size_t end = current.find(close);
      if(end <=begin || end == std::string::npos || begin == std::string::npos)
	break;
      output.push_back(current.substr(begin + open_size,end - begin - open_size));
      current = current.substr(end + close_size);    
    }
    return output;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

qmt::OrbitalType stringToOrbitalType(const std::string& orbital) {
    if(orbital == "1s") return qmt::OrbitalType::_1s;
    else if (orbital == "2s")  return qmt::OrbitalType::_2s;
    else if (orbital == "2p_x")  return qmt::OrbitalType::_2p_x;
    else if (orbital == "2p_xp")  return qmt::OrbitalType::_2p_xp;
    else if (orbital == "2p_xm")  return qmt::OrbitalType::_2p_xm;
    else if (orbital == "2p_y")  return qmt::OrbitalType::_2p_y;
    else if (orbital == "2p_yp")  return qmt::OrbitalType::_2p_yp;
    else if (orbital == "2p_ym")  return qmt::OrbitalType::_2p_ym;
    else if (orbital == "2p_z")  return qmt::OrbitalType::_2p_z;
    else if (orbital == "2p_zp")  return qmt::OrbitalType::_2p_zp;
    else if (orbital == "2p_zm")  return qmt::OrbitalType::_2p_zm;
    else if (orbital == "3s")    return qmt::OrbitalType::_3s;
    else if (orbital == "3p_x")  return qmt::OrbitalType::_3p_x;
    else if (orbital == "3p_y")  return qmt::OrbitalType::_3p_y;
    else if (orbital == "3p_z")  return qmt::OrbitalType::_3p_z;
    else if (orbital == "3d_z2") return qmt::OrbitalType::_3d_z2;
    else if (orbital == "3d_xz") return qmt::OrbitalType::_3d_xz;
    else if (orbital == "3d_yz")  return qmt::OrbitalType::_3d_yz;
    else if (orbital == "3d_xy")  return qmt::OrbitalType::_3d_xy;
    else if (orbital == "3d_x2y2")return qmt::OrbitalType::_3d_x2y2;
    else
        throw std::logic_error("qmt::parser::stringToOrbitalType: unknown OrbitalType: " + std::string(orbital));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

std::string orbitalTypeToString(qmt::OrbitalType orbital) {
    switch (orbital) {
    case qmt::OrbitalType::_1s:
        return std::string("1s");
    case qmt::OrbitalType::_2s:
        return std::string("2s");

    case qmt::OrbitalType::_2p_x:
        return std::string("2p_x");
    case qmt::OrbitalType::_2p_xm:
        return std::string("2p_xm");
    case qmt::OrbitalType::_2p_xp:
        return std::string("2p_xp");
    

    case qmt::OrbitalType::_2p_y:
        return std::string("2p_y");
    case qmt::OrbitalType::_2p_ym:
        return std::string("2p_ym");
    case qmt::OrbitalType::_2p_yp:
        return std::string("2p_yp");
        
    case qmt::OrbitalType::_2p_z:
        return std::string("2p_z");
    case qmt::OrbitalType::_2p_zm:
        return std::string("2p_zm");
    case qmt::OrbitalType::_2p_zp:
        return std::string("2p_zp");
        
    case qmt::OrbitalType::_3s:
        return std::string("3s");
    case qmt::OrbitalType::_3p_x:
        return std::string("3p_x");
    case qmt::OrbitalType::_3p_y:
        return std::string("3p_y");
    case qmt::OrbitalType::_3p_z:
        return std::string("3p_z");
    case qmt::OrbitalType::_3d_z2:
        return std::string("3d_z2");
    case qmt::OrbitalType::_3d_xz:
        return std::string("3d_xz");
    case qmt::OrbitalType::_3d_yz:
        return std::string("3d_yz");
    case qmt::OrbitalType::_3d_xy:
        return std::string("3d_xy");
    case qmt::OrbitalType::_3d_x2y2:
        return std::string("3d_x2y2");
    }
    throw std::logic_error("qmt::orbitalTypeToString: unknown OrbitalType");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<int> get_atomic_numbers(const std::string& file_name){
  
   std::ifstream input_file(file_name.c_str());
      std::string   content( (std::istreambuf_iterator<char>(input_file) ),
                     (std::istreambuf_iterator<char>()    ) );
      input_file.close();
      
   auto words_to_parse = get_bracketed_words(content,"open_atomic_numbers","close_atomic_numbers");
   
   auto words_numbers = get_delimited_words(",",words_to_parse[0]);
   std::vector<int> values;
   
   for(const auto& word : words_numbers) {
     auto lword = word;
     lword.erase( std::remove_if( lword.begin(), lword.end(), ::isspace ), lword.end() ); //remove blanks
     values.push_back(std::stoi(lword));
   }
   
   return values;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
QmtVector get_QmtVector_from_line(const std::string& line){
 auto bracketed = get_bracketed_words(line,"(",")");
 
 auto myvector = get_delimited_words(",",bracketed[0]);
 
 if(myvector.size()!=3)
   throw std::logic_error("qmt::parser::get_QmtVector_from_line : expression in brackets is not a 3D-vector!");
 
 return QmtVector(std::stod(myvector[0]),std::stod(myvector[1]),std::stod(myvector[2]));
   
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<qmt::QmtVector> get_QmtVector_from_file(const std::string& file_name){

    std::vector<qmt::QmtVector> result;
  
    std::ifstream input_file(file_name.c_str());
       
    std::string line;
    while (std::getline(input_file, line)){
      // removing comments
      std::size_t pos = line.find("#");
      if (pos<line.length())
	line.erase(line.begin()+pos,line.end());
      
      //removing white-spaces if line was not erased
      if(line.length()){
	std::size_t found = line.find_first_of(" \t");
	while (found!=std::string::npos)
	{
	  line.erase(line.begin()+found);
	  found=line.find_first_of(" \t",found+1);
	}
      }
      if(line.find("(")==0) //if line starts with bracket
	result.push_back(get_QmtVector_from_line(line));
    }
    
      input_file.close();
      
      return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void remove_comments(std::string& content){
//remove comments
  std::size_t found = 0;
  while((found = content.find("#",found))!=std::string::npos){
    std::size_t f2 = content.find("\n",found);
    content.erase(found,f2-found+1); // +1 to erase "\n" as well!
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void remove_whitespace(std::string& content){
  content.erase(std::remove_if(content.begin(), content.end(), ::isspace), content.end());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
namespace hamiltonian{

//////////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int get_num_states(const std::string& ham_file){
  std::set<unsigned int> indices;

  std::ifstream input_file(ham_file.c_str());
       
  std::string line;

  std::vector<std::string> operators;
  operators.push_back(std::string("cup,"));
  operators.push_back(std::string("aup,"));
  operators.push_back(std::string("cdown,"));
  operators.push_back(std::string("adown,"));

  while (std::getline(input_file, line)){
    auto line_elements = get_delimited_words(operators,line);
    line_elements.pop_back(); //remove integral definitions !
    for(auto& i : line_elements){
      remove_whitespace(i);
      indices.insert(std::stoul(i));
    }
  }

  return indices.size();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int get_num_one_body(const std::string& ham_file){
  std::set<unsigned int> one_body_idx;

  std::ifstream input_file(ham_file.c_str());
       
  std::string line;

  std::string comma(",");
  std::string open("(");
  std::string close(")");

  while (std::getline(input_file, line)){
    auto line_elements = get_delimited_words(comma,line);
    auto atoms = get_bracketed_words(line,open,close,false);

    if(atoms.size()!=2U)
      continue;

   one_body_idx.insert(std::stoul(line_elements[3])); // index of the integral is the 4th element
  }

  return one_body_idx.size();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int get_num_two_body(const std::string& ham_file){
  std::set<unsigned int> two_body_idx;

  std::ifstream input_file(ham_file.c_str());
       
  std::string line;

  std::string comma(",");
  std::string open("(");
  std::string close(")");

  while (std::getline(input_file, line)){
    auto line_elements = get_delimited_words(comma,line);
    auto atoms = get_bracketed_words(line,open,close,false);

    if(atoms.size()!=4U)
      continue;

   two_body_idx.insert(std::stoul(line_elements[5])); // index of the integral is the 6th element
  }

  return two_body_idx.size();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void compare_supercell(const std::string& ham_file, const std::string& supercell_file, QmtAtomMap& map_supercell){

  if(!map_supercell.empty())
    map_supercell.clear();

  std::unordered_map<std::string,qmt::QmtVectorizedAtom> SuperCell;

  std::ifstream hamiltonian_input(ham_file.c_str());
  std::ifstream supercell_input(supercell_file.c_str());

  std::string line;

  unsigned int num_states = qmt::parser::hamiltonian::get_num_states(ham_file);
  
  while (std::getline(supercell_input, line)){
    auto atom_string = get_delimited_words("();",line)[0];
    auto atom_elements = get_delimited_words("(,;) ",line);

    if(atom_elements.size()!=4U)
      continue;
    
    qmt::QmtVector position(std::stod(atom_elements[0]),std::stod(atom_elements[1]),std::stod(atom_elements[2]));
    qmt::QmtVectorizedAtom atom(position,std::stoi(atom_elements[3]));

    SuperCell.insert(std::pair<std::string,qmt::QmtVectorizedAtom>(atom_string,atom));
  }

  if(SuperCell.size()!=num_states){
    std::cerr<<"qmt::parser::hamiltonian::compare_supercell: ";
    std::cerr<<ham_file<<" (hamiltonian file) and "<<supercell_file<<" (supercell file)";
    std::cerr<<" are not compatible"<<std::endl;
    exit(-128);
  }
  
  std::string delimiters(",");
  std::string open("(");
  std::string close(")");

  while (std::getline(hamiltonian_input, line)){
    auto line_elements = get_delimited_words(delimiters,line);
    auto atoms = get_bracketed_words(line,open,close,false);

    std::vector<unsigned int> operators;

    for (auto& el : line_elements){
      remove_whitespace(el);
      if(el.back()=='p' || el.back()=='n') //it means they are operators
        operators.push_back(std::stoul(el));  //std::stoul ignores everything after first non-digit (nice!)
    }

  if(atoms.size() != operators.size()){
      std::cerr<<"qmt::parser::hamiltonian::compare_supercell: ";
      std::cerr<<ham_file<<" (hamiltonian file) corrupted"<<std::endl;
      exit(-129);
    }

    for(unsigned int i=0; i<atoms.size(); ++i)
      if(SuperCell.find(atoms[i]) != SuperCell.end())
        map_supercell.insert(std::pair<qmt::QmtVectorizedAtom,unsigned int>(SuperCell.at(atoms[i]),operators[i]));

   if(map_supercell.size()==SuperCell.size())
      return;
  }

  std::cerr<<"qmt::parser::hamiltonian::compare_supercell: "; //if you're here, it's bad!
  std::cerr<<ham_file<<" (hamiltonian file) corrupted"<<std::endl;
  exit(-129);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void compare_megacell(const std::string& ham_file, const std::string& megacell_file, QmtAtomMap& map_megacell){

  if(!map_megacell.empty())
    map_megacell.clear();

  std::unordered_map<std::string,qmt::QmtVectorizedAtom> MegaCell;

  std::ifstream hamiltonian_input(ham_file.c_str());
  std::ifstream megacell_input(megacell_file.c_str());

  std::string line;
  
  while (std::getline(megacell_input, line)){
    auto atom_string = get_delimited_words("();",line)[0];

    auto atom_elements = get_delimited_words("(,;) ",line);

    if(atom_elements.size()!=4U)
      continue;
    
    qmt::QmtVector position(std::stod(atom_elements[0]),std::stod(atom_elements[1]),std::stod(atom_elements[2]));
    qmt::QmtVectorizedAtom atom(position,std::stoi(atom_elements[3]));

    MegaCell.insert(std::pair<std::string,qmt::QmtVectorizedAtom>(atom_string,atom));
  }

  std::string delimiters(",");
  std::string open("(");
  std::string close(")");

  while (std::getline(hamiltonian_input, line)){
    auto line_elements = get_delimited_words(delimiters,line);
    auto atoms = get_bracketed_words(line,open,close,false);

    std::vector<unsigned int> operators;

    if(atoms.size()!=4U)
      continue;

    for (auto& el : line_elements){
      remove_whitespace(el);
      if(el.back()=='p' || el.back()=='n'){ //it means they are operators
        operators.push_back(std::stoul(el));  //std::stoul ignores everything after first non-digit (nice!)
      }
    }

    if(operators[0]!=operators[1] || operators[2]!=operators[3]) // take only K into account
      continue;

    auto tmp_operator = operators[2];
    operators[2] = operators[1];
    operators[1] = tmp_operator;

    if(atoms.size() != operators.size()){
      std::cerr<<"qmt::parser::hamiltonian::compare_megacell: ";
      std::cerr<<ham_file<<" (hamiltonian file) corrupted"<<std::endl;
      exit(-129);
    }

    for(unsigned int i=0; i<atoms.size(); ++i){
      if(MegaCell.find(atoms[i]) != MegaCell.end())
       map_megacell.insert(std::pair<qmt::QmtVectorizedAtom,unsigned int>(MegaCell.at(atoms[i]),operators[i]));
    }

   if(map_megacell.size()==MegaCell.size())
      return;
  }

  std::cerr<<"qmt::parser::hamiltonian::compare_megacell: "; //if you're here, it's bad!
  std::cerr<<ham_file<<" (hamiltonian file) corrupted"<<std::endl;
  exit(-129);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void compare_one_body(const std::string& ham_file, QmtOneBodyMap& map_one_body){

  if(!map_one_body.empty())
    map_one_body.clear();

  std::ifstream hamiltonian_input(ham_file.c_str());

  std::string line;

  unsigned int num_one_body = qmt::parser::hamiltonian::get_num_one_body(ham_file);

  std::string delimiters(",");
  std::string open("(");
  std::string close(")");

  while (std::getline(hamiltonian_input, line)){
    auto line_elements = get_delimited_words(delimiters,line);
    auto atoms = get_bracketed_words(line,open,close,false);

    if(atoms.size()!=2U) //only one-body interactions
      continue;


    auto atom_parts = get_delimited_words(delimiters,atoms[0]);
    qmt::QmtVectorizedAtom atom1(qmt::QmtVector(std::stod(atom_parts[0]), std::stod(atom_parts[1]), std::stod(atom_parts[2])), std::stoi(atom_parts[3]));
         atom_parts = get_delimited_words(delimiters,atoms[1]);
    qmt::QmtVectorizedAtom atom2(qmt::QmtVector(std::stod(atom_parts[0]), std::stod(atom_parts[1]), std::stod(atom_parts[2])), std::stoi(atom_parts[3]));

    map_one_body.insert(std::pair<qmt::integrals::one_body,unsigned int>(qmt::integrals::one_body(atom1,atom2),std::stoul(line_elements[3]))); //fourth elemnt is the integral index

 }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void compare_two_body(const std::string& ham_file, QmtTwoBodyMap& map_two_body){

  if(!map_two_body.empty())
    map_two_body.clear();

  std::ifstream hamiltonian_input(ham_file.c_str());

  std::string line;

  unsigned int num_two_body = qmt::parser::hamiltonian::get_num_two_body(ham_file);

  std::string delimiters(",");
  std::string open("(");
  std::string close(")");

  while (std::getline(hamiltonian_input, line)){
    auto line_elements = get_delimited_words(delimiters,line);
    auto atoms = get_bracketed_words(line,open,close,false);

    if(atoms.size()!=4U) //only one-body interactions
      continue;


    auto atom_parts = get_delimited_words(delimiters,atoms[0]);
    qmt::QmtVectorizedAtom atom1(qmt::QmtVector(std::stod(atom_parts[0]), std::stod(atom_parts[1]), std::stod(atom_parts[2])), std::stoi(atom_parts[3]));

         atom_parts = get_delimited_words(delimiters,atoms[1]);
    qmt::QmtVectorizedAtom atom2(qmt::QmtVector(std::stod(atom_parts[0]), std::stod(atom_parts[1]), std::stod(atom_parts[2])), std::stoi(atom_parts[3]));

         atom_parts = get_delimited_words(delimiters,atoms[2]);
    qmt::QmtVectorizedAtom atom3(qmt::QmtVector(std::stod(atom_parts[0]), std::stod(atom_parts[1]), std::stod(atom_parts[2])), std::stoi(atom_parts[3]));

         atom_parts = get_delimited_words(delimiters,atoms[3]);
    qmt::QmtVectorizedAtom atom4(qmt::QmtVector(std::stod(atom_parts[0]), std::stod(atom_parts[1]), std::stod(atom_parts[2])), std::stoi(atom_parts[3]));

    map_two_body.insert(std::pair<qmt::integrals::two_body,unsigned int>(qmt::integrals::two_body(atom1,atom2,atom3,atom4),std::stoul(line_elements[5]))); //sixth elemnt is the integral index
  }  
}

} //end namespace hamiltonian
} //end namespace parser
} //end namespace qmt
