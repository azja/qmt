/*
 * SolverSGA.cpp
 *
 *  Created on: 21-12-2011
 *      Author: andrzej kadzielawa
 *
 *  Edited on:  21-03-2012
 *  	Author: andrzej kadzielawa
 *
 *  Edited on:  06-04-2014
 *  	Author: andrzej kadzielawa
 *
 *  Edited on: 19-08-2014
 *  	Author: andrzej kadzielawa
 */

#include "../headers/SolverSGA.h"
#define DEBUG false
#define DEB_GUTZ false
#define DENSITY_N 1


void Solver_SGA::Solver_SGA_Constructor(double _beta, double _t, double _U, double _R, double _h, FILE *_error_file,FILE *_exit_file, bool _DCONST)
{
    CZY=true;

    beta = _beta;
    t = _t;
    U = _U;
    R = _R;
    h = _h;

    n = DENSITY_N;

    L = 128;
    L3 = L*L*L;

    err=_error_file;
    ext=_exit_file;

    Checker=false;
    Checher_which_approach=0;

    DCONST=_DCONST;
}

Solver_SGA::Solver_SGA(double _beta, double _t, double _U, double _R, double _h, FILE *_error_file,FILE *_exit_file, bool _DCONST)
{
    Solver_SGA_Constructor(_beta,_t,_U,_R,_h,_error_file,_exit_file,_DCONST);
}
Solver_SGA::Solver_SGA(double _beta, double _t, double _U, bool _DCONST)
{
    Solver_SGA_Constructor(_beta,_t,_U,4,0,NULL,NULL,_DCONST);
}
Solver_SGA::Solver_SGA(bool _DCONST)
{
    Solver_SGA_Constructor(500,-1.0,1.6,4,0,NULL,NULL,_DCONST);
}


Solver_SGA::~Solver_SGA()
{
    (void)0;
}

void Solver_SGA::epsilon(double kx, double ky, double kz)
{
    e_k = (-2.0)* t* (cos(2.0* M_PIl* kx)+cos(2.0* M_PIl* ky)+cos(2.0* M_PIl* kz));
}

void Solver_SGA::energy_s(double *p)
{
    double *test_EnU, *test_EnD; //test pointer to determine which energy are we calculating;

    test_EnU = &EnU;
    test_EnD = &EnD;

    if(test_EnU == p)
    {
        EnU = (g_up * e_k) - h - lmb_m - mu - lmb_n;
        if(DEBUG)if(isnan(EnU))printf("g_up = %e\te_k = %e\th = %e\tlmb_m = %e\tmu = %e\tlmb_n = %e\tEnU = %e\n",g_up,e_k,h,lmb_m,mu,lmb_n,EnU);
        iferr=0;
    }
    else if(test_EnD == p)
    {
        EnD = (g_down * e_k) + h + lmb_m - mu - lmb_n;
        if(DEBUG)if(isnan(EnD))printf("g_down = %e\te_k = %e\th = %e\tlmb_m = %e\tmu = %e\tlmb_n = %e\tEnD = %e\n",g_down,e_k,h,lmb_m,mu,lmb_n,EnD);
        iferr=0;
    }
    else
    {
        if (err != NULL) fprintf(err,"Wrong variable in energy_s input\n");
        iferr=-1;
    }

    if(DEBUG)if(isnan(*p))printf("g_up = %.20e, g_down = %.20e, e_k = %.20e, p = %.20e\n",g_up,g_down,e_k,*p);
}

double Solver_SGA::GutzFactor(double s)
{
    double twodsquare = 2.0*d*d;
    double nminum = n - m;
    double nplusm = n + m;
    double result;

    if(d==0.0) result = (2.0*(n-1.0))/(nplusm - 2.0);

    if (s>0.0) result=2.0*(nplusm*(1.0-n)-twodsquare*(twodsquare+1.0-2.0*n)+2.0*d*sqrt((nplusm-twodsquare)*(nminum-twodsquare)*(1.0-n+d*d)))/((2.0-nplusm)*(nplusm));
    else result=2.0*(nminum*(1.0-n)-twodsquare*(twodsquare+1.0-2.0*n)+2.0*d*sqrt((nplusm-twodsquare)*(nminum-twodsquare)*(1.0-n+d*d)))/((2.0-nminum)*(nminum));

    return result;
}

void Solver_SGA::FerDirStat(double *p)
{
    double *test_FDSU, *test_FDSD; //test pointer to determine what are we calculating;

    test_FDSU = &FDSU;
    test_FDSD = &FDSD;

    if(test_FDSU == p)
    {
        if(beta*EnU < -1000.0)			FDSU = 1.0;
        else if(beta*EnU > 1000.0)	FDSU = 0.0;
        else						FDSU = 0.5*(1.0 + tanh(-0.5* beta* EnU));
        iferr=0;
    }
    else if(test_FDSD == p)
    {
        if(beta*EnD < -1000.0)			FDSD = 1.0;
        else if(beta*EnD > 1000.0)	FDSD = 0.0;
        else						FDSD = 0.5*(1.0 + tanh(-0.5* beta* EnD));
        iferr=0;

    }
    else
    {
        if (err != NULL) fprintf(err,"Wrong variable in FerDirStat input\n");
        iferr=-1;
    }

    if(DEBUG)if(isnan(*p))printf("EnU = %.20e, EnD = %.20e, beta = %.20e, p = %.20e\n",EnU,EnD,beta,*p);
}

void Solver_SGA::innerlogarithm()
{
    double Up, Down; //temporary variables defined as below:

    // For energies much smaller than 0 one gets exp(large number) = inf which gave us nan
    // To solve this one rewrite log(1.0+exp(x)) = x + log(1.0+exp(-x))

    if(EnU<0.0) Up=	-beta*EnU + log(1.0+exp(beta*EnU));
    else		Up=	log(1.0+exp(-beta*EnU));

    if(EnD<0.0) Down=	-beta*EnD + log(1.0+exp(beta*EnD));
    else		Down=	log(1.0+exp(-beta*EnD));

    LogEn = Up + Down;
}

void Solver_SGA::stepset(double mul)
{
    energy_s(&EnU);
    energy_s(&EnD);
    FerDirStat(&FDSU);
    FerDirStat(&FDSD);

//	if(FDSU != 1 || FDSD != 1)
//		printf("O!");
    if(!DCONST)d_d += mul* ((FDSU*g_up_d_d+FDSD*g_down_d_d)) *e_k;
    d_lmb_n -= mul*(FDSU + FDSD);
    d_n += mul*(FDSU*g_up_d_n+FDSD*g_down_d_n)*e_k;
    d_m += mul* (FDSU*g_up_d_m+FDSD*g_down_d_m) *e_k;
    d_lmb_m -= mul*(FDSU - FDSD);
}

inline double Solver_SGA::der_m(double s)
{
    double result;
    if(d==0.0) result = 0.0;

    if (s>0) result=(2*(d*sqrt(-2*pow(d,2) - m + n) +
                            sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n))))*
                         (-2*(-2 + m + n)*(m + n)*(-d/(2.*sqrt(-2*pow(d,2) - m + n)) +
                                 (sqrt(1 + pow(d,2) - n))/
                                 (2.*sqrt(((-2*pow(d,2) + m + n))))) +
                          (-2 + m + n)*(d*sqrt(-2*pow(d,2) - m + n) +
                                        sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n)))) +
                          (m + n)*(d*sqrt(-2*pow(d,2) - m + n) +
                                   sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n))))))/
                        (pow(-2 + m + n,2)*pow(m + n,2));
    else result=(2*(sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) + d*sqrt(-2*pow(d,2) + m + n))*
                     ((2 + m - n)*(-m + n)*(-(sqrt(1 + pow(d,2) - n)/sqrt(-2*pow(d,2) - m + n)) +
                                            d/sqrt(-2*pow(d,2) + m + n)) + (m - n)*
                      (sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) + d*sqrt(-2*pow(d,2) + m + n)) +
                      (2 + m - n)*(sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) + d*sqrt(-2*pow(d,2) + m + n))))/
                    (pow(m - n,2)*pow(2 + m - n,2));

    return result;
}
inline double Solver_SGA::der_d(double s)
{
    double result;
    if(fabs(d)>1e-10) {
        if (s>0.0) result=(-4*(d*sqrt(-2*pow(d,2) - m + n) +
                                   sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n))))*
                               ((-2*pow(d,2))/sqrt(-2*pow(d,2) - m + n) +
                                sqrt(-2*pow(d,2) - m + n) +
                                (d*(-2 - 4*pow(d,2) + m + 3*n))/
                                sqrt(-((1 + pow(d,2) - n)*(2*pow(d,2) - m - n)))))/
                              ((-2 + m + n)*(m + n));
        else result=(-4*(-((d*(2 + 4*pow(d,2) + m - 3*n))/
                               sqrt(-((1 + pow(d,2) - n)*(2*pow(d,2) + m - n)))) -
                             (2*pow(d,2))/sqrt(-2*pow(d,2) + m + n) +
                             sqrt(-2*pow(d,2) + m + n))*
                         (sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) +
                          d*sqrt(-2*pow(d,2) + m + n)))/((m - n)*(2 + m - n));
    }
    else if(n>=1.0) result=0.0;
    else
    {
        double a = n + m;
        double b = n - m;
        if(s>0.0) result=(4.0*sqrt(a*b*(1.0-n)))/(b*(2.0-b));
        else	result=(4.0*sqrt(a*b*(1.0-n)))/(a*(2.0-a));
    }
    return result;

}

inline double Solver_SGA::der_n(double s)
{
    double result;

    if (fabs(d)>1e-10) {
        if (s>0.0) result=(2*(d*sqrt(-2*pow(d,2) - m + n) +
                                  sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n))))*
                               (-2*(-2 + m + n)*(m + n)*(d/(2.*sqrt(-2*pow(d,2) - m + n)) +
                                       (1 + 3*pow(d,2) - m - 2*n)/
                                       (2.*sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n))))) +
                                (-2 + m + n)*(d*sqrt(-2*pow(d,2) - m + n) +
                                              sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n)))) +
                                (m + n)*(d*sqrt(-2*pow(d,2) - m + n) +
                                         sqrt(-((-1 - pow(d,2) + n)*(-2*pow(d,2) + m + n))))))/
                              (pow(-2 + m + n,2)*pow(m + n,2));
        else result=(2*(sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) +
                            d*sqrt(-2*pow(d,2) + m + n))*
                         (-((m - n)*(2 + m - n)*((1 + 3*pow(d,2) + m - 2*n)/
                                                 sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) +
                                                 d/sqrt(-2*pow(d,2) + m + n))) -
                          (2 + m - n)*(sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) +
                                       d*sqrt(-2*pow(d,2) + m + n)) +
                          (-m + n)*(sqrt((2*pow(d,2) + m - n)*(-1 - pow(d,2) + n)) +
                                    d*sqrt(-2*pow(d,2) + m + n))))/
                        (pow(m - n,2)*pow(2 + m - n,2));
    }
    else result = 0.0;
    return result;

}

void Solver_SGA::energy_step(double mul)
{
    energy_s(&EnU);
    energy_s(&EnD);
    innerlogarithm();

    F_SGA -= (mul/beta)*LogEn;
}

void Solver_SGA::epsset(int x, int y, int z)
{
    epsilon((double)x/((double)L),(double)y/((double)L),(double)z/((double)L));
}

void Solver_SGA::derivatives()
{
    if(!DCONST)d_d = ((double)L3)* d*(2.0* U);
    d_m = ((double)L3)*lmb_m;
    d_lmb_m = ((double)L3)*m;
    d_n = ((double)L3)*lmb_n;
    d_lmb_n = (double)L3*n;

    g_up = GutzFactor(1.0);
    if(DEB_GUTZ || DEBUG) if(isnan(g_up))printf("g_up is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_down = GutzFactor(-1.0);
    if(DEB_GUTZ || DEBUG) if(isnan(g_down))printf("g_down is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_up_d_d = der_d(1.0);
    if(DEBUG) if(isnan(g_up_d_d))printf("g_up_d_d is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_down_d_d = der_d(-1.0);
    if(DEBUG) if(isnan(g_down_d_d))printf("g_down_d_d is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_up_d_n = der_n(1.0);
    if(DEBUG) if(isnan(g_up_d_n))printf("g_up_d_n is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_down_d_n = der_n(-1.0);
    if(DEBUG) if(isnan(g_down_d_n))printf("g_down_d_n is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_up_d_m = der_m(1.0);
    if(DEBUG) if(isnan(g_up_d_m))printf("g_up_d_m is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_down_d_m = der_m(-1.0);
    if(DEBUG) if(isnan(g_down_d_m))printf("g_down_d_m is nan for d=%e\tm=%e\tn=%e\n",d,m,n);

    if(Checker && Checher_which_approach==7) {
        printf("d=%e\tm=%e\tn=%e\ng_up=%e\tg_down=%e\ng_up_d_d=%e\tg_down_d_d=%e\ng_up_d_n=%e\tg_down_d_n=%e\ng_up_d_m=%e\tg_down_d_m=%e\n",d,m,n,g_up,g_down,g_up_d_d,g_down_d_d,g_up_d_n,g_down_d_n,g_up_d_m,g_down_d_m);
        Checker=false;
    }

    Checher_which_approach++;


    //printf("%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\n",d,m,lmb_m, d_d, d_m,d_lmb_m);

    for(int i=1; i<L/2; i++)
        for(int j=1; j<i; j++)
            for(int k=1; k<j; k++)
            {
                epsset(i,j,k);		// points inside the pyramid (48x)
                stepset(48.0);
            }
    for(int i=1; i<L/2; i++)
        for(int j=1; j<i; j++)
        {
            epsset(i,j,0);			// points on the surface of the pyramid (24x)
            stepset(24.0);
            epsset(i,j,L/2);
            stepset(24.0);
            epsset(i,i,j);
            stepset(24.0);
            epsset(j,j,i);
            stepset(24.0);
        }
    for(int i=1; i<L/2; i++)
    {
        epsset(i,0,0);				// points on the edges (6x)
        stepset(6.0);
        epsset(i,L/2,L/2);
        stepset(6.0);
    }
    for(int i=1; i<L/2; i++)
    {
        epsset(i,0,L/2);			// points on the edges (12x)
        stepset(12.0);
        epsset(i,i,L/2);
        stepset(12.0);
        epsset(i,i,0);
        stepset(12.0);
    }
    for(int i=1; i<L/2; i++)
    {
        epsset(i,i,i);				// points on the last edge (8x)
        stepset(8.0);
    }
    epsset(0,L/2,L/2);				// missing points (3x)
    stepset(3.0);
    epsset(0,0,L/2);
    stepset(3.0);

    epsset(L/2,L/2,L/2);			// missing points (1x)
    stepset(1.0);
    epsset(0,0,0);
    stepset(1.0);

}

void Solver_SGA::energy()
{
    F_SGA = (double)L3* (U*d*d + lmb_m*m + (lmb_n + mu)*n);
    //printf("%.20e\t%.20e\t%.20e\t%.20e\t%.20e\n",d,m,lmb_m,lmb_n,F_SGA);

    g_up = GutzFactor(1.0);
    if(DEBUG) if(isnan(g_up))printf("g_up is nan for d=%e\tm=%e\tn=%e\n",d,m,n);
    g_down = GutzFactor(-1.0);
    if(DEBUG) if(isnan(g_down))printf("g_down is nan for d=%e\tm=%e\tn=%e\n",d,m,n);

    if(Checker) {
        printf("d=%e\tm=%e\tn=%e\ng_up=%e\tg_down=%e\ng_up_d_d=%e\tg_down_d_d=%e\ng_up_d_n=%e\tg_down_d_n=%e\ng_up_d_m=%e\tg_down_d_m=%e\n",d,m,n,g_up,g_down,g_up_d_d,g_down_d_d,g_up_d_n,g_down_d_n,g_up_d_m,g_down_d_m);
        Checker=false;
    }

    for(int i=1; i<L/2; i++)
        for(int j=1; j<i; j++)
            for(int k=1; k<j; k++)
            {
                epsset(i,j,k);		// points inside the pyramid (48x)
                energy_step(48.0);
            }
    for(int i=1; i<L/2; i++)
        for(int j=1; j<i; j++)
        {
            epsset(i,j,0);			// points on the surface of the pyramid (24x)
            energy_step(24.0);
            epsset(i,j,L/2);
            energy_step(24.0);
            epsset(i,i,j);
            energy_step(24.0);
            epsset(j,j,i);
            energy_step(24.0);
        }
    for(int i=1; i<L/2; i++)
    {
        epsset(i,0,0);				// points on the edges (6x)
        stepset(6.0);
        epsset(i,L/2,L/2);
        energy_step(6.0);
    }
    for(int i=1; i<L/2; i++)
    {
        epsset(i,0,L/2);			// points on the edges (12x)
        energy_step(12.0);
        epsset(i,i,L/2);
        energy_step(12.0);
        epsset(i,i,0);
        energy_step(12.0);
    }
    for(int i=1; i<L/2; i++)
    {
        epsset(i,i,i);				// points on the last edge (8x)
        energy_step(8.0);
    }
    epsset(0,L/2,L/2);				// missing points (3x)
    energy_step(3.0);
    epsset(0,0,L/2);
    energy_step(3.0);

    epsset(L/2,L/2,L/2);			// missing points (1x)
    energy_step(1.0);
    epsset(0,0,0);
    energy_step(1.0);
}

int Solver_SGA::to_opt (const gsl_vector * x, void *params, gsl_vector * f)
{
    m = gsl_vector_get (x, 0);
    lmb_m = gsl_vector_get (x, 1);
    mu = gsl_vector_get (x, 2);
    lmb_n = gsl_vector_get (x, 3);
    if(!DCONST)	d = gsl_vector_get (x, 4);
    else d = 0.0;

    // **************************************************************************************
    // In January 2012 I had a problem with GSL library - the gsl_solver was having
    // issues with the equations given, and was proposing not-a-number as a next
    // step in finding roots of equations. To solve this annoyance I erased "const"
    // from all files attached to this project by first adding a preprocessor directive
    // in file SolverSGA.h: "#define const", then supplying optimisation for root finding
    // by slightly decreasing (90% of)the parameters. For testing purpose the file
    // handling was introduce. Deleting "const" in all files was necessary because of
    // the structure of GSL library, which enforce working on "const gsl_vector*"
    // structures.
    /*
    FILE *wsk2 = fopen("dane.txt","a");
    fprintf(wsk2,"*%.20e\t%.20e\t%.20e\t%.20e\n",d,lam_n,d_d,d_lam_n);

    if(isnan(d) || isnan(lam_n))
    {
      d = 0.9*t_d;
      lam_n = 0.9*t_lam_n;

      gsl_vector_set(x,0,d);
      gsl_vector_set(x,1,lam_n);
    }
    // **************************************************************************************
    */
    if(-2.0*d*d-m + n <0.0 || -2.0*d*d +m + n <0.0) {
        gsl_vector_set (f, 0, 0.0);
        gsl_vector_set (f, 1, 0.0);
        gsl_vector_set (f, 2, 0.0);
        gsl_vector_set (f, 3, 0.0);
        if(!DCONST) gsl_vector_set (f, 4, (double)L3);

    }
    else {
        derivatives();

        if(!DCONST) d_d /= (double)L3;
        d_m /= (double)L3;
        d_lmb_m /= (double)L3;
        d_n /= (double)L3;
        d_lmb_n /= (double)L3;

        if(DEBUG) {
            if(CZY) {
                printf("d\t\t\t\tm\t\t\t\tlmb_m\t\t\t\tmu\t\t\t\tlmb_n\t\t\t\td_d\t\t\t\td_m\t\t\t\td_lmb_m\t\t\t\td_lmb_n\t\t\t\td_n\n");
                CZY=false;
            }
            printf("%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\n",d,m,lmb_m,mu,lmb_n, d_d, d_m,d_lmb_m,d_lmb_n,d_n);
        }



        gsl_vector_set (f, 0, d_m);
        gsl_vector_set (f, 1, d_lmb_m);
        gsl_vector_set (f, 2, d_n);
        gsl_vector_set (f, 3, d_lmb_n);
        if(!DCONST) gsl_vector_set (f, 4, d_d);

    }

    /*
      gsl_vector_set (f, 0, sin((d-1.0)*M_PI));
      gsl_vector_set (f, 1, cos(m*M_PI));
      gsl_vector_set (f, 2, sin((lmb_m-2.0)*M_PI));
    */
    // **************************************************************************************
// printf("%.20e\t%.20e\t%.20e\t%.20e\t%.20e\t%.20e\n",d,m,lmb_m, d_d, d_m,d_lmb_m);
// fclose(wsk2);
    // **************************************************************************************

    return GSL_SUCCESS;
}

double Solver_SGA::get(char c)
{
    switch(c)
    {
    case 't':
        return t;
        break;
    case 'U':
        return U;
        break;
    case 'b':
        return beta;
        break;
    case 'R':
        return R;
        break;
    case 'D':
        return d_d;
        break;
    case 'd':
        return d;
        break;
    case 'h':
        return h;
        break;
    default:
        iferr = -1;
        if (err != NULL) fprintf(err,"Wrong variable in \"get\" input\n");
        return 0.0;
    }
}

void Solver_SGA::set(char c,double value)
{
    switch(c)
    {
    case 't':
        t = value;
        break;
    case 'U':
        U = value;
        break;
    case 'b':
        beta = value;
        break;
    case 'R':
        R = value;
        break;
    case 'D':
        d_d = value;
        break;
    case 'd':
        d = value;
        break;
    case 'h':
        h = value;
        break;
    default:
        iferr = -1;
        if (err != NULL) fprintf(err,"Wrong variable in \"set\" input\n");
    }
}

void Solver_SGA::set_all(double _t, double _U, double _h) {
    t = _t;
    U = _U;
    h = _h;
}


void Solver_SGA::set_beta(double _beta) {
    beta=_beta;
}


void Solver_SGA::print() {
    energy();

    printf("t= %e\tU= %e\td= %e\tm= %e\tlmb_m= %e\n",t,U,d,m,lmb_m);
    printf("E_G = %e\n",F_SGA/(double)L3);
}

double Solver_SGA::get_energy() {
    energy();

    return F_SGA/(double)L3;
}


s_SGA_out Solver_SGA::end()
{
    energy();

    F_SGA/=(double)L3;
    s_SGA_out value;
    value.d = d;
    value.m = m;
    value.lmb_m = lmb_m;
    value.lmb_n = lmb_n;
    value.mu = mu;
    value.EG = F_SGA;

    if(DEBUG)fprintf(stdout,"%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n",t,beta,U,R,d,m,lmb_m,F_SGA);

    /*FILE *nowy;
    double *handle;

    //d=0.2999;
    //m=0.79;

    handle = &m;
    nowy = fopen("E_SGA.txt","w+");
    int N = 1000;
    double step = (4.0*fabs(*handle))/1000.0;
    *handle-=(N/2)*step;

    for(int i=0; i<N; i++)
    {
    	energy();
    	F_SGA/=(double)L3;
    	fprintf(nowy,"%.20e\t%.20e\n",*handle,F_SGA);
    	*handle+=step;
    }
    fclose(nowy);

    system("gnuplot plot.plt");
    system("evince En_m.eps &");*/

    return value;
}

s_SGA_out Solver_SGA::solve(double test_d, double test_m, double test_lmb_m, double test_mu, double test_lmb_n)
{

    s_SGA_out value; //value returned
    const gsl_multiroot_fsolver_type *T;
    gsl_multiroot_fsolver *s;

    int status;
    size_t iter = 0;

    const size_t n = 5;

    rparams p(t,beta,U,R,h);

    p.pnt = this;

    gsl_multiroot_function f= {&optme,n,&p};

    double x_init[5] = {test_m,test_lmb_m,test_mu,test_lmb_n,test_d};
    if(DCONST) x_init[4]=0.0;

    gsl_vector *x = gsl_vector_alloc (n);

    gsl_vector_set (x, 0, x_init[0]);
    gsl_vector_set (x, 1, x_init[1]);
    gsl_vector_set (x, 2, x_init[2]);
    gsl_vector_set (x, 3, x_init[3]);
    gsl_vector_set (x, 4, x_init[4]);

    T = gsl_multiroot_fsolver_hybrid;
    s = gsl_multiroot_fsolver_alloc (T, n);
    gsl_multiroot_fsolver_set (s, &f, x);


    do
    {
        iter++;

        if (iter > 200)
        {
            status = GSL_EMAXITER;
            break;
        }

        status = gsl_multiroot_fsolver_iterate (s);

        if (isnan(d))
        {
            status = GSL_ESANITY;
            break;
        }

        if (status) /* check if solver is stuck */
        {
            break;
        }


        status =
            gsl_multiroot_test_residual (s->f, 1e-13);
        //printf("status: %d step: %d\n",status,(int)iter);

        /* if(status == GSL_SUCCESS)
         {
         	status = tosolve.boostmax();
         }*/
    }
    while (status == GSL_CONTINUE);

    //printf ("status = %s\n", gsl_strerror (status));

    value = end();

    gsl_multiroot_fsolver_free (s);
    gsl_vector_free (x);

    return value;
}


s_SGA_out Solver_SGA::solve(double test_m, double test_lmb_m, double test_mu, double test_lmb_n)
{
    s_SGA_out value; //value returned
    const gsl_multiroot_fsolver_type *T;
    gsl_multiroot_fsolver *s;

    int status;
    size_t iter = 0;

    const size_t n = 4;

    rparams p(t,beta,U,R,h);

    p.pnt = this;

    gsl_multiroot_function f= {&optme,n,&p};

    double x_init[4] = {test_m,test_lmb_m,test_mu,test_lmb_n};

    gsl_vector *x = gsl_vector_alloc (n);

    gsl_vector_set (x, 0, x_init[0]);
    gsl_vector_set (x, 1, x_init[1]);
    gsl_vector_set (x, 2, x_init[2]);
    gsl_vector_set (x, 3, x_init[3]);

    T = gsl_multiroot_fsolver_hybrid;
    s = gsl_multiroot_fsolver_alloc (T, n);
    gsl_multiroot_fsolver_set (s, &f, x);


    do
    {
        iter++;

        if (iter > 200)
        {
            status = GSL_EMAXITER;
            break;
        }

        status = gsl_multiroot_fsolver_iterate (s);

        if (isnan(d))
        {
            status = GSL_ESANITY;
            break;
        }

        if (status) /* check if solver is stuck */
        {
            break;
        }


        status =
            gsl_multiroot_test_residual (s->f, 1e-13);
        //printf("status: %d step: %d\n",status,(int)iter);

        /* if(status == GSL_SUCCESS)
         {
         	status = tosolve.boostmax();
         }*/
    }
    while (status == GSL_CONTINUE);

    //printf ("status = %s\n", gsl_strerror (status));

    value = end();

    gsl_multiroot_fsolver_free (s);
    gsl_vector_free (x);

    return value;
}

s_SGA_out Solver_SGA::solve() {
    double magnetic_field_sgn = fabs(h) < 1e-07 ? 0.0 : h/fabs(h);
    s_SGA_out result;

    if(DCONST) result = solve(0.1*magnetic_field_sgn,0.001*magnetic_field_sgn,0.0,0.0);
    else result = solve(0.25,0.1*magnetic_field_sgn,0.001*magnetic_field_sgn,0.0,0.0);

    return result;
}


int optme (const gsl_vector * x, void *params, gsl_vector * f)
{
    Solver_SGA *loc;
    int stat;

    loc = ((class rparams*)params)->pnt;
    stat = (*loc).to_opt(x,&params,f);
    return stat;
}
