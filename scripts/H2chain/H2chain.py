#!/usr/bin/python

import math
import numpy
import sys

# lattice constant
a=4.0
lattice=numpy.array([a,0,0])

# displacement
delta = numpy.array([0.1,0.1,0.1])

# molecular basis (vector)
theta = math.pi/2.0
R=1.0
Mbasis = numpy.array([math.cos(theta)*R,math.sin(theta)*R,0])

# max Coordination Radius
# must be the same as in main program!
CoordRadMul=7
maxCoordRad=CoordRadMul*a

# super cell
SuperCell = [
[0,numpy.array([0,0,0]),0,-1,0,-1],
[1,Mbasis,0,1,0,2],
[2,lattice,1,-1,0,1],
[3,Mbasis+lattice,1,1,0,2],
[4,-lattice,-1,-1,0,1],
[5,Mbasis-lattice,-1,1,0,2]
]

# border of the super cell 
BorderSuperCell = [
[4,2.0*lattice,2,-1,0,1],
[5,Mbasis+2.0*lattice,2,1,0,2],
[2,-2.0*lattice,-2,-1,0,1],
[3,Mbasis-2.0*lattice,-2,1,0,2]
]




#distance

def distance(atom1,atom2):
	return math.sqrt(math.pow(atom1[1][0]-atom2[1][0],2)+math.pow(atom1[1][1]-atom2[1][1],2)+math.pow(atom1[1][2]-atom2[1][2],2))


# mega cell

#translation
def CellTrans(translation,Cell):
	newCell=[]
	for atom in Cell:
		newatom=[atom[0],atom[1]+numpy.array([translation*a,0,0]),atom[2]+translation,atom[3],atom[4],atom[5]]
		if(distance(Cell[0],newatom) < math.sqrt(maxCoordRad*maxCoordRad+a*a) or distance(Cell[1],newatom) < math.sqrt(maxCoordRad*maxCoordRad+a*a)):
			newCell.append(newatom)
	return newCell
#end of function


#mega cell construction
MegaCell=[]

for i in range(-CoordRadMul/2 - 1,CoordRadMul + 1): #+/- 1 to catch the last ion
	MegaCell+=CellTrans(3*i,SuperCell)


# Mega/Super Cell saved in 'mega(/super)cell.dat' -> important to calculate <w_i|H_1|w_j>

MegaCellFile = open('megacell.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
MegaCellFile.close()

SuperCellFile = open('supercell.dat', 'w+')
for atom in SuperCell:
	SuperCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
SuperCellFile.close()

############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################

# anihilator
def an(N,s):
	return str(N)+"a"+str(s)
# creator
def cr(N,s):
	return str(N)+"c"+str(s)
# number operator
def num(N,s):
	return cr(N,s)+",	"+an(N,s)

# spins
spin = ["up","down"]

# actual microscopic parameter flag

one_body_flag = 0
two_body_flag = 0

# epsilon_a
print ""
print "one_body"
for itemS in SuperCell:
	for s in spin:
		print 	cr(itemS[0],s)+",	"+an(itemS[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"
	one_body_flag+=1
print "end_one_body"



# t in SuperCell
print ""
print "one_body"
for i in range(0,len(SuperCell)):
	for j in range(i+1,len(SuperCell)):
		dij=distance(SuperCell[i],SuperCell[j])
		if (i==j or dij > math.sqrt(R*R+a*a+2*a*R*math.fabs(math.cos(theta)))):
			continue
		for s in spin:
			print cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		";"
			print cr(SuperCell[j][0],s)+",	"+an(SuperCell[i][0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		";"
		one_body_flag+=1
print "end_one_body"


# t with border
print ""
print "one_body"
for itemS in SuperCell:
	for itemB in BorderSuperCell:
		dij=distance(itemS,itemB)
		if (dij<1e-16 or dij > math.sqrt(R*R+a*a+2*a*R*math.fabs(math.cos(theta)))):
			continue
		for s in spin:
			print cr(itemS[0],s)+",	"+an(itemB[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemB[2])+",  "+str(itemB[3])+",  "+str(itemB[4])+",  "+str(itemB[5])+")"\
		+		";"
			print cr(itemB[0],s)+",	"+an(itemS[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemB[2])+",  "+str(itemB[3])+",  "+str(itemB[4])+",  "+str(itemB[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		";"
		one_body_flag+=1
print "end_one_body"


# U
print ""
print "two_body"
for itemS in SuperCell:
	print num(itemS[0],spin[0])+",	"+num(itemS[0],spin[1])+",	"+" +,	"+str(two_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"
	two_body_flag+=1
print "end_two_body"


# K
print ""
print "two_body"
for itemS in SuperCell:
	for itemM in MegaCell:
		dij=distance(itemS,itemB)
		if (itemS[2]==itemM[2] and itemS[3]==itemM[3] and itemS[4]==itemM[4]):
			continue
		for s1 in spin:
			for s2 in spin:
				print num(itemS[0],s1)+",	"+num(itemM[0],s2)+",	"+" +,	"+str(two_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemM[2])+",  "+str(itemM[3])+",  "+str(itemM[4])+",  "+str(itemM[5])+")"\
		+		",	"\
		+		"("+str(itemM[2])+",  "+str(itemM[3])+",  "+str(itemM[4])+",  "+str(itemM[5])+")"\
		+       ";"
		two_body_flag+=1
print "end_two_body"

print one_body_flag
print two_body_flag
print two_body_flag+one_body_flag-2
sys.exit()
