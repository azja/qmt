#!/bin/bash

date

startdate=$(date +"%s")

a=$(qstat | grep -c $USER)

while [ 1 ]
do
    if [ $a -eq 0 ] ; then break ; else (qstat | grep $USER) ; fi
    sleep 2
    a=$(qstat | grep -c $USER)
done

enddate=$(date +"%s")
diff=$(($enddate-$startdate))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed."

date

