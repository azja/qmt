#ifndef QMTHUBBARD_H_INCLUDED
#define QMTHUBBARD_H_INCLUDED

#include <vector>
#include <list>
#include <utility>
#include <algorithm>
#include <math.h>
#include <tuple>
#include "qmvars.h"
#include <omp.h>
#include <queue>
#include <set>
#include <map>

#ifdef GSL_LIBRARY	// to use GSL
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

#include "qmtmatrixoperations.h"
#endif

namespace qmt {

template<typename Operator>
class QmtHubbard {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template<typename OperatorIn>
    class OneBody {

        std::vector<std::tuple<OperatorIn, OperatorIn> > _operators;
        double prefix;
        typedef typename OperatorIn::SqOperatorNState NState;

    public:

        OneBody<OperatorIn>():prefix(1.0) {

        }

        OneBody<OperatorIn>(double p):prefix(p) {

        }

	void set_prefix(double p) {
	prefix = p;
	}


        void add(const OperatorIn& first, const OperatorIn& second) {
            _operators.push_back(
                std::make_tuple(OperatorIn(first), OperatorIn(second)));
        }

        double action(const NState& bra, const NState& ket) {
            typename std::vector<std::tuple<OperatorIn, OperatorIn> >::iterator i;
            double result = 0;
            for (i = _operators.begin(); i != _operators.end(); ++i) {

                result += NState::dot_product(bra,
                                              std::get<0>(*i)((std::get<1>(*i))(ket)));
            }
            return prefix * result;
        }

        void action(std::vector<NState> &out, const NState& ket) {
            typename std::vector<std::tuple<OperatorIn, OperatorIn> >::iterator i;
            for (i = _operators.begin(); i != _operators.end(); ++i) {
                NState state = std::get<0>(*i)((std::get<1>(*i))(ket));
                state.setPhase(state.getPhase()*prefix);
                if( state.getPhase() != 0 ) out.push_back(state);
            }
        }



    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//

    template<typename OperatorIn>
    class TwoBody {
        double prefix;
        std::vector<std::tuple<OperatorIn, OperatorIn, OperatorIn, OperatorIn> > _operators;
        typedef typename OperatorIn::SqOperatorNState NState;

    public:

        TwoBody<OperatorIn>():prefix(1.0) {
        }



        TwoBody<OperatorIn>(double p):prefix(p) {
        }


	void set_prefix(double p) {
	prefix = p;
	}


        void add(const OperatorIn& first, const OperatorIn& second,
                 const OperatorIn& third, const OperatorIn& fourth) {
            _operators.push_back(
                std::make_tuple(OperatorIn(first), OperatorIn(second),
                                OperatorIn(third), OperatorIn(fourth)));
        }



        double action(const NState& bra, const NState& ket) {
            typename std::vector<
            std::tuple<OperatorIn, OperatorIn, OperatorIn, OperatorIn> >::iterator i;
            double result = 0.0;
            for (i = _operators.begin(); i != _operators.end(); ++i) {
                result += NState::dot_product(bra,
                                              std::get<0>(*i)(
                                                  std::get<1>(*i)(
                                                      std::get<2>(*i)(
                                                              std::get<3>(*i)(ket)))));
                ;
            }
            return prefix * result;
        }

        void action(std::vector<NState> &out, const NState& ket) {
            typename std::vector<
            std::tuple<OperatorIn, OperatorIn, OperatorIn, OperatorIn> >::iterator i;
            for (i = _operators.begin(); i != _operators.end(); ++i) {
                NState state = std::get<0>(*i)(std::get<1>(*i)(
                                                   std::get<2>(*i)(
                                                       std::get<3>(*i)(ket))));
                state.setPhase(state.getPhase() * prefix);
                if(state.getPhase() != 0) out.push_back(state);
            }
        }

    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::vector<std::pair<double, OneBody<Operator> > > _one_body_list;
    std::vector<std::pair<double, TwoBody<Operator> > > _two_body_list;

public:
    typedef typename Operator::SqOperatorNState NState;
    typedef Operator HamiltonianOperator;
private:


class mycomparison
{
public:
  mycomparison(){}
  bool operator() (const NState& lhs, const NState& rhs) const
  {
    return NState::lt_configuration(lhs,rhs);
  }
} comp;

public:

    QmtHubbard<Operator>() {
    }

    void addOneBody(unsigned int term, const Operator& first,
                    const Operator& second) {
        if (term >= 0 && term < _one_body_list.size())
            std::get<1>(_one_body_list[term]).add(first, second);

    }

    void addOneBodyTerm(double parameter = 1.0) {
        _one_body_list.push_back(
            std::make_pair(parameter, OneBody<Operator>(parameter)));
    }

    void setMicroscopicParameterOne(unsigned int term, double parameter) {
        if (term >= 0 && term < _one_body_list.size()){
            std::get<0>(_one_body_list[term]) = parameter;
//            std::get<1>(_one_body_list[term]).set_prefix(parameter);
            }
            
    }

    void addTwoBody(unsigned int term, const Operator& first,
                    const Operator& second, const Operator& third,
                    const Operator& fourth) {
        if (term >= 0 && term < _two_body_list.size())
            std::get<1>(_two_body_list[term]).add(first, second, third, fourth);
    }

    void addTwoBodyTerm(double parameter = 1.0) {
        _two_body_list.push_back(
            std::make_pair(parameter, TwoBody<Operator>(parameter)));
    }

    void setMicroscopicParameterTwo(unsigned int term, double parameter) {
        if (term >= 0 && term < _two_body_list.size())
            std::get<0>(_two_body_list[term]) = parameter;
//            std::get<1>(_two_body_list[term]).set_prefix(parameter);
    }


    bool termExistsOne(unsigned int term) {
        return term >= 0 && term < _one_body_list.size();
    }

    bool termExistsTwo(unsigned int term) {
        return term >= 0 && term < _two_body_list.size();
    }

    double action(const NState& bra, const NState& ket) {
//     NState temp_state = ket;    //weak point.....

        double result;

        typename std::vector<std::pair<double, OneBody<Operator> > >::iterator i;
        for (i = _one_body_list.begin(); i != _one_body_list.end(); ++i) {
            result += (std::get<0>(*i) * ((std::get<1>(*i)).action(bra, ket)));
        }

        typename std::vector<std::pair<double, TwoBody<Operator> > >::iterator j;
        for (j = _two_body_list.begin(); j != _two_body_list.end(); ++j) {

            result += std::get<0>(*j) * ((std::get<1>(*j)).action(bra, ket));
        }

        return result;
    }

    /*
     * NOTE: This version is NOT ass effective (fast&robust) as it could be
     * it rather serves as a prototype... (yes, I know, forever...)
     */
    
    void action(std::vector<NState>& output,const NState& ket) {

        output.clear();

        std::vector<NState> states;
//	double t = omp_get_wtime();
	
        for (auto i = _one_body_list.begin(); i != _one_body_list.end(); ++i) {
                    std::vector<NState> one_local_states;
                     ((std::get<1>(*i)).action(one_local_states, ket));
//            for(auto& state : one_local_states)
//                  state.setPhase(state.getPhase());     //set microscopic parameter
            states.insert(states.end(),one_local_states.begin(),one_local_states.end());		
        }
        
//        std::cout<<"one body time:"<<omp_get_wtime() - t<<"s"<<std::endl;

//	t = omp_get_wtime();

        for (auto i = _two_body_list.begin(); i != _two_body_list.end(); ++i) {
                    std::vector<NState> two_local_states;
                     ((std::get<1>(*i)).action(two_local_states, ket));
   //         for(auto& state : two_local_states)
   //               state.setPhase(state.getPhase());     //set microscopic parameter
            states.insert(states.end(),two_local_states.begin(),two_local_states.end());		
        }

//	std::cout<<"two body time:"<<omp_get_wtime() - t<<"s"<<std::endl;
 	
//	t = omp_get_wtime();
 	std::vector<bool> indicator(states.size());
 	std::fill(indicator.begin(),indicator.end(),true);
	
//	std::cout<<"filling  time:"<<omp_get_wtime() - t<<"s"<<std::endl; 	
	

//	t = omp_get_wtime();
	for(int i = 0;i < states.size();++i) {
	  if(!indicator[i]) continue;
	  output.push_back(states[i]);
	  for(int j = i + 1;j < states.size();++j) {
	    if(!indicator[j]) continue;
	    if( NState::compare_configuration(states[i],states[j])){ 
	      output[i].setPhase(output[i].getPhase() + states[j].getPhase());
	      indicator[j] = false;  
	    }
	  }
	 }
	 	
//	std::cout<<"Final merging  time:"<<omp_get_wtime() - t<<"s"<<std::endl; 	
    }

    
    
    // It didn't. Here's a red-black tree version
    void action(std::map<NState,double,mycomparison>& output,const NState& ket) {

 	typedef std::map<NState,double,mycomparison> NMap;
        output.clear();

      for (auto i = _one_body_list.begin(); i != _one_body_list.end(); ++i) {
                    std::vector<NState> one_local_states;
                     ((std::get<1>(*i)).action(one_local_states, ket));

		for(const auto& state : one_local_states){
		        typename NMap::iterator handle = output.find(state);
			if (handle!=output.end())
				handle->second +=  (std::get<0>(*i))*state.getPhase();
			else
				output.insert(std::pair<NState,double>(state,(std::get<0>(*i))*state.getPhase()));		
		}
        }

      for (auto i = _two_body_list.begin(); i != _two_body_list.end(); ++i) {
                    std::vector<NState> two_local_states;
                     ((std::get<1>(*i)).action(two_local_states, ket));

		for(const auto& state : two_local_states){
		        typename NMap::iterator handle = output.find(state);
		
			if (handle!=output.end())
				handle->second += (std::get<0>(*i))*state.getPhase();
			else
				output.insert(std::pair<NState,double>(state,(std::get<0>(*i))*state.getPhase()));		
		}
        }


    }

    void action(std::vector<NState>& output, const std::vector<NState>& input) {

 	typedef std::map<NState,double,mycomparison> NMap; // for attempt No 7
	NMap output_map; // for attempt No 7

#ifdef _QMT_DEBUG
    double tall = omp_get_wtime();
    double t = omp_get_wtime();
#endif

output.reserve(2800000);
    for(int i = 0; i < input.size();++i) {
      NMap local;

#ifdef _QMT_DEBUG
      double t = omp_get_wtime();
#endif
      action(local,input[i]);

#ifdef _QMT_DEBUG
     if(!( i % 100000))
      std::cout<<"Action time:"<<omp_get_wtime() - t<<"s"<<std::endl;

      t = omp_get_wtime();
#endif

      for(const auto& mypair : local) {
// Attempt No 7
	typename NMap::iterator handle = output_map.find(mypair.first);;

	if (handle!=output_map.end()){

		handle->second += mypair.second;
		}
	else
		output_map.insert(mypair);


	}

#ifdef _QMT_DEBUG
     if(!( i % 100000))
      std::cout<<"Merging and pushing time: "<<omp_get_wtime() - t<<"s"<<" i:"<<i<<std::endl;
#endif  
     }

#ifdef _QMT_DEBUG
      t = omp_get_wtime();
#endif

	for (const auto& item : output_map){
		auto state = item.first;
		state.setPhase(item.second);
		output.push_back(state);
	}
#ifdef _QMT_DEBUG
      std::cout<<"Rewriting time: "<<(omp_get_wtime() - t)<<" s"<<std::endl;

      std::cout<<"Lanczos step' time: "<<(omp_get_wtime() - tall)/60.0<<" min"<<std::endl;

	std::cout<<"Output size: "<<output.size()<<std::endl; 
	std::cout<<"Output size: "<<output_map.size()<<std::endl; 
#endif
//	for(const auto& x : output )
//		std::cout<<x<<std::endl;  


//	for(const auto& x : output_map)
//		std::cout<<x.first<<" "<<x.second<<std::endl;  

    }

    unsigned int number_of_one_body() {
        return _one_body_list.size();
    }

    unsigned int number_of_two_body() {
        return _two_body_list.size();
    }

    double get_value(int which_term, unsigned int which_number) { // which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
        typename std::vector<std::pair<double, OneBody<Operator> > >::iterator iter1;
        typename std::vector<std::pair<double, TwoBody<Operator> > >::iterator iter2;

        switch (which_term) {
        case 1:

            iter1 = _one_body_list.begin();
            return std::get<0>(*(iter1 + which_number));
            break;
        case 2:
            iter2 = _two_body_list.begin();
            return std::get<0>(*(iter2 + which_number));
            break;
        }
        std::cerr << "QmtHubbard: Wrong body indicator!\n";
        return -1.0;
    }

    double matrix_el(const NState& bra, const NState& ket, int which_term,
                     int which_number) { // which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
        typename std::vector<std::pair<double, OneBody<Operator> > >::iterator iter1;
        typename std::vector<std::pair<double, TwoBody<Operator> > >::iterator iter2;

        switch (which_term) {
        case 1:

            iter1 = _one_body_list.begin();
            return ((std::get<1>(*(iter1 + which_number)).action(bra, ket)));
//            break;
        case 2:
            iter2 = _two_body_list.begin();
            return ((std::get<1>(*(iter2 + which_number)).action(bra, ket)));
//            break;
        }
        std::cerr << "QmtHubbard: Wrong body indicator!\n";
        return -1.0;
    }

    void vector_set(std::vector<NState> &out, const NState& ket, int which_term,
                    int which_number) { // which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
        typename std::vector<std::pair<double, OneBody<Operator> > >::iterator iter1;
        typename std::vector<std::pair<double, TwoBody<Operator> > >::iterator iter2;

        switch (which_term) {
        case 1:
            iter1 = _one_body_list.begin();
            ((std::get<1>(*(iter1 + which_number)).action(out, ket)));
            break;
        case 2:
            iter2 = _two_body_list.begin();
            ((std::get<1>(*(iter2 + which_number)).action(out, ket)));
            break;
        case 3:
            std::cerr << "QmtHubbard: Wrong body indicator!\n";
            break;
        }
    }

};

/*
 * Hamiltonian matrix generator
 *
 * Requires class Hamiltonian to have public methods:
 * 		[*]	int number_of_one_body()
 * 		[*]	int number_of_two_body()
 * 		[*]	double get_value(int which_term, int which_number)
 * 		[*]	double matrix_el(const NState& bra, const NState& ket, int which_term, int which_number)
 *
 * 		where which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody

 */

template<class Hamiltonian>
class QmtMatrixFormula {

    typedef typename Hamiltonian::NState State;
private:
    class microscopic_parameter_in_hamiltonian {
    public:
        microscopic_parameter_in_hamiltonian() {
            size_of_matrix = 0;
            value = 0.0;
            matrix = NULL;
        }

        ~microscopic_parameter_in_hamiltonian() {
            if (size_of_matrix > 0) {
                for (unsigned int i = 0; i < size_of_matrix; ++i)
                    delete[] matrix[i];

                delete[] matrix;
            }
        }

    public:
        void set_value(double _value) {
            value = _value;
        }

        void set_matrix_size(int msize) {
            if (size_of_matrix > 0) {
                for (unsigned int i = 0; i < size_of_matrix; ++i)
                    delete[] matrix[i];

                delete[] matrix;
            }

            size_of_matrix = msize;

            matrix = new double*[msize];
            for (int i = 0; i < msize; ++i)
                matrix[i] = new double[msize];
        }

        void set_matrix_el(double _value, unsigned int i, unsigned int j) {
            if (i >= 0 && j >= 0 && i < size_of_matrix && j < size_of_matrix)
                matrix[i][j] = _value;
        }

        double get_value() {
            return value;
        }
        double get_matrix_el(unsigned int i, unsigned int j) {
            if (i >= 0 && j >= 0 && i < size_of_matrix && j < size_of_matrix)
                return matrix[i][j];
            else
                return 0.0;
        }
        double get_hamiltonian_matrix(int i, int j) {
            return get_value() * get_matrix_el(i, j);
        }

#ifdef GSL_LIBRARY
        void get_matrix(gsl_matrix* pointer) {
            if( (pointer->size1 != size_of_matrix) || (pointer->size2 != size_of_matrix)) {
                std::cerr<<"QmtMatrixFormula: Wrong matrix size!"<<std::endl;

            }
            else if(pointer == NULL) {
                std::cerr<<"QmtMatrixFormula: No matrix given!"<<std::endl;

            }
            else {
                for(unsigned int i=0; i<size_of_matrix; i++)
                    for(unsigned int j=0; j<size_of_matrix; j++)
                        gsl_matrix_set(pointer,i,j,matrix[i][j]);
            }
        }

        void set_matrix(const gsl_matrix* pointer) {
            if((pointer != NULL) && (pointer->size1 == size_of_matrix) && (pointer->size2 == size_of_matrix) ) {

                for(unsigned int i=0; i<size_of_matrix; i++)
                    for(unsigned int j=0; j<size_of_matrix; j++)
                        matrix[i][j] = gsl_matrix_get(pointer,i,j);
            }
        }
#endif

    private:
        unsigned int size_of_matrix;
        double value;
        double **matrix;
    };

public:
    QmtMatrixFormula<Hamiltonian>() {
        one_body_microscopic_parameter = NULL;
        two_body_microscopic_parameter = NULL;
        hamiltonian_values = NULL;
        IF_PAREPARED = false;
    }
    QmtMatrixFormula<Hamiltonian>(Hamiltonian _hamiltonian) {
        getHamiltonian(_hamiltonian);
        IF_PAREPARED = false;
        hamiltonian_values = NULL;
    }

    ~QmtMatrixFormula<Hamiltonian>() {
        if(one_body_microscopic_parameter!=NULL) delete[] one_body_microscopic_parameter;
        if(two_body_microscopic_parameter!=NULL) delete[] two_body_microscopic_parameter;

        test_states.clear();

        if(one_body_microscopic_parameter!=NULL) {
            for (unsigned int i = 0; i < test_states.size(); ++i)
                delete[] hamiltonian_values[i];

            delete[] hamiltonian_values;
        }
    }

    void getHamiltonian(Hamiltonian _hamiltonian) {
        hamiltonian = _hamiltonian;

        one_body_microscopic_parameter =
            new microscopic_parameter_in_hamiltonian[hamiltonian.number_of_one_body()];
        two_body_microscopic_parameter =
            new microscopic_parameter_in_hamiltonian[hamiltonian.number_of_two_body()];
    }

    void set_States(std::vector<State> _test_states) {
        test_states = _test_states;

        IF_PAREPARED = prepareFormula();
    }

    void set_microscopic_parameter(int which_term, unsigned int which_number,
                                   double value) { //where which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
        switch (which_term) {
        case 1:
            if (which_number < hamiltonian.number_of_one_body()) {
                one_body_microscopic_parameter[which_number].set_value(value);
            } else
                std::cerr << "QmtHamiltonianFormula: OneBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        case 2:
            if (which_number < hamiltonian.number_of_two_body()) {
                two_body_microscopic_parameter[which_number].set_value(value);
            } else
                std::cerr << "QmtHamiltonianFormula: TwoBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        default:
            std::cerr << "QmtHamiltonianFormula: Wrong body indicator!\n";
            break;
        }
    }

    void print() {
        fill_outcome();
        for (unsigned int j = 0; j < test_states.size(); ++j) {
            for (unsigned int k = 0; k < test_states.size(); ++k) {
                std::cout << hamiltonian_values[j][k] << " ";
            }
            std::cout << std::endl;
        }
    }

    void print(int which_term, unsigned int which_number) { //where which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
        switch (which_term) {
        case 1:
            if (which_number < hamiltonian.number_of_one_body()) {
                for (unsigned int j = 0; j < test_states.size(); ++j) {
                    for (unsigned int k = 0; k < test_states.size(); ++k) {
                        std::cout
                                << one_body_microscopic_parameter[which_number].get_matrix_el(
                                    j, k) << " ";
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            } else
                std::cerr << "QmtHamiltonianFormula: OneBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        case 2:
            if (which_number < hamiltonian.number_of_two_body()) {
                for (unsigned int j = 0; j < test_states.size(); ++j) {
                    for (unsigned int k = 0; k < test_states.size(); ++k) {
                        std::cout
                                << two_body_microscopic_parameter[which_number].get_matrix_el(
                                    j, k) << " ";
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            } else
                std::cerr << "QmtHamiltonianFormula: TwoBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        default:
            std::cerr << "QmtHamiltonianFormula: Wrong body indicator!\n";
            break;
        }

    }

#ifdef GSL_LIBRARY
    void changeBase(gsl_matrix *beta) {
        if (beta != NULL) { //if matrix is allocated
            int loc_size=test_states.size();

            gsl_matrix *tmp_matrix;
            tmp_matrix = gsl_matrix_alloc (loc_size, loc_size);

            QmtBaseChanger transition(beta);

            for(unsigned int k=0; k<hamiltonian.number_of_one_body(); k++) {

                one_body_microscopic_parameter[k].get_matrix(tmp_matrix);

                transition.change_of_basis(tmp_matrix);

                one_body_microscopic_parameter[k].set_matrix(tmp_matrix);

            }

            //two body

            for(unsigned int k=0; k<hamiltonian.number_of_two_body(); k++) {

                two_body_microscopic_parameter[k].get_matrix(tmp_matrix);

                transition.change_of_basis(tmp_matrix);

                two_body_microscopic_parameter[k].set_matrix(tmp_matrix);

            }

            gsl_matrix_free(tmp_matrix);

        } else std::cerr<<"QmtHamiltonianFormula: Matrix not allocated!\n";
    }

    void getMatrix (gsl_matrix * m) {
        fill_outcome();
        if (m != NULL) { //if matrix is allocated
            unsigned int proper_size = test_states.size();
            if (m->size1 == proper_size && m->size2 == proper_size)
            {
                for(unsigned int i = 0; i < proper_size; ++i) {
                    for(unsigned int j = 0; j < proper_size; ++j) {
                        gsl_matrix_set(m,i,j,hamiltonian_values[i][j]);
                    }
                }
            }
            else std::cerr<<"QmtHamiltonianFormula: Wrong size of given matrix!\n";
        }
        else std::cerr<<"QmtHamiltonianFormula: Matrix not allocated!\n";
    }

#endif

    void getMatrix(double **m) {
        fill_outcome();
        if (m != NULL) { //if matrix is allocated
            for (int i = 0; i < test_states.size(); i++) {
                for (int j = 0; j < test_states.size(); j++) {
                    m[i][j] = hamiltonian_elem_value(i, j);
                }
            }
        } else
            std::cerr << "QmtHamiltonianFormula: Matrix not allocated!\n";
    }

    int getSize() {
        return test_states.size();
    }

private:
    bool prepareFormula() {
        //Getting one-body terms
        for (unsigned int i = 0; i < hamiltonian.number_of_one_body(); i++) {
            one_body_microscopic_parameter[i].set_value(
                hamiltonian.get_value(1, i));

            one_body_microscopic_parameter[i].set_matrix_size(
                test_states.size());
            for (unsigned int j = 0; j < test_states.size(); ++j) {
                for (unsigned int k = 0; k < test_states.size(); ++k) {
                    one_body_microscopic_parameter[i].set_matrix_el(
                        hamiltonian.matrix_el(test_states[j],
                                              test_states[k], 1, i), j, k);
                }
            }
        }

        //Getting two-body terms
        for (unsigned int i = 0; i < hamiltonian.number_of_two_body(); i++) {
            two_body_microscopic_parameter[i].set_value(
                hamiltonian.get_value(2, i));
            two_body_microscopic_parameter[i].set_matrix_size(
                test_states.size());

            for (unsigned int j = 0; j < test_states.size(); ++j) {
                for (unsigned int k = 0; k < test_states.size(); ++k) {
                    two_body_microscopic_parameter[i].set_matrix_el(
                        hamiltonian.matrix_el(test_states[j],
                                              test_states[k], 2, i), j, k);
                }
            }
        }
        return true;
    }

    void fill_outcome() {
        // Filling the outcome - can be changed if we change microscopic parameters
        int size_of_matrix = test_states.size();
        hamiltonian_values = new double*[size_of_matrix];
        for (int i = 0; i < size_of_matrix; i++) {
            hamiltonian_values[i] = new double[size_of_matrix];
        }

        for (int i = 0; i < size_of_matrix; i++) {
            for (int j = 0; j < size_of_matrix; j++) {
                hamiltonian_values[i][j] = hamiltonian_elem_value(i, j);
            }
        }
    }

    double hamiltonian_elem_value(int i, int j) {
        double matrix_element = 0.0;
        for (unsigned int k = 0; k < hamiltonian.number_of_one_body(); ++k) {
            matrix_element +=
                one_body_microscopic_parameter[k].get_hamiltonian_matrix(i,
                        j);
        }
        for (unsigned int k = 0; k < hamiltonian.number_of_two_body(); ++k) {
            matrix_element +=
                two_body_microscopic_parameter[k].get_hamiltonian_matrix(i,
                        j);
        }
        return matrix_element;
    }
private:
    Hamiltonian hamiltonian;
    microscopic_parameter_in_hamiltonian* one_body_microscopic_parameter;
    microscopic_parameter_in_hamiltonian* two_body_microscopic_parameter;
    std::vector<State> test_states;
    bool IF_PAREPARED;
    double **hamiltonian_values;
};

} // end of namespace qmt
#endif // QMTHUBBARD_H_INCLUDED
