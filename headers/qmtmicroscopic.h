#ifndef QMT_MICROSCOPIC_H_INCLUDED
#define QMT_MICROSCOPIC_H_INCLUDED

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include "qmthbuilder.h"
#include "qmtwannierprovider.h"
#include "qmtwannierbasis.h"
#include "qmtparsertools.h"


namespace qmt {

template <typename Creator, typename Hamiltonian>
class QmtMicroscopic{
protected:
  
    std::vector<qmt::QmtVectorizedAtom> megacell;
    std::vector<qmt::QmtVectorizedAtom> local_megacell;
    std::vector<qmt::QmtVectorizedAtom> supercell;
    std::vector<qmt::QmtVectorizedAtom> local_supercell;
    std::vector<qmt::one_body_integral_v> one_body_intgs;
    std::vector<qmt::two_body_integral_v> two_body_intgs;
    std::vector<qmt::one_body_integral_v> local_one_body_intgs;
    std::vector<qmt::two_body_integral_v> local_two_body_intgs;
    std::vector<int> atomic_numbers;
    qmt:: QmtWannierBasisProvider<Creator> wannier_provider;
    qmt:: QmtWannierBasis<Creator> *wannier_basis;


  
    
public: 
	QmtMicroscopic(const std::string& hamiltonian_fn, const std::string& basis_fn, 
		       const std::string& supercell_fn, const std::string& background_fn, double param) :
		       wannier_provider(basis_fn.c_str()){
		one_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getOneBodyIntegralsV(
					hamiltonian_fn);                
		two_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getTwoBodyIntegralsV(
					 hamiltonian_fn);
		megacell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					 background_fn);
		supercell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					supercell_fn);



		local_one_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getOneBodyIntegralsV(
					hamiltonian_fn);
		local_two_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getTwoBodyIntegralsV(
					 hamiltonian_fn);
		local_megacell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					 background_fn);
		local_supercell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					supercell_fn);

		atomic_numbers = qmt::parser::get_atomic_numbers(basis_fn);
#ifdef _QMT_DEBUG
		for(auto i : atomic_numbers)
		  std::cout<<"atomic_number:"<<i;
#endif
                
		wannier_basis = new qmt::QmtWannierBasis<Creator>(wannier_provider.get_definitions(),
								  wannier_provider.get_equations(),
								  wannier_provider.get_betas(),param);
								  
		
        }



        QmtMicroscopic(const std::string& hamiltonian_fn, const std::string& basis_fn, 
		       const std::string& supercell_fn, const std::string& background_fn,const std::vector<double> &params): wannier_provider(basis_fn.c_str()){
		one_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getOneBodyIntegralsV(
					hamiltonian_fn);
		two_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getTwoBodyIntegralsV(
					 hamiltonian_fn);
		megacell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					 background_fn);
		supercell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					supercell_fn);

                
								  
	 	local_one_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getOneBodyIntegralsV(
					hamiltonian_fn);
		local_two_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getTwoBodyIntegralsV(
					 hamiltonian_fn);
		local_megacell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					 background_fn);
		local_supercell =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getAllAtoms(
					supercell_fn);
		atomic_numbers = qmt::parser::get_atomic_numbers(basis_fn);
#ifdef _QMT_DEBUG
		for(auto i : atomic_numbers)
		  std::cout<<"atomic_number:"<<i;
#endif
                
			
                wannier_basis = new qmt::QmtWannierBasis<Creator>(wannier_provider.get_definitions(),
								  wannier_provider.get_equations(),
								  wannier_provider.get_betas(),params);

        }



protected:
	void scale(const qmt::QmtVector& s_vector) {

	    
	   for(int i = 0; i < one_body_intgs.size();++i){
	      std::get<0>(local_one_body_intgs[i]).position = 
	        qmt::QmtVector(std::get<0>(one_body_intgs[i]).position.get_x() * s_vector.get_x(),
	        std::get<0>(one_body_intgs[i]).position.get_y() * s_vector.get_y(),std::get<0>(one_body_intgs[i]).position.get_z() * s_vector.get_z());
	      std::get<1>(local_one_body_intgs[i]).position = 
	        qmt::QmtVector(std::get<1>(one_body_intgs[i]).position.get_x() * s_vector.get_x(),
	        std::get<1>(one_body_intgs[i]).position.get_y() * s_vector.get_y(),std::get<1>(one_body_intgs[i]).position.get_z() * s_vector.get_z());	        
	    }


	  for(int i = 0; i < two_body_intgs.size();++i){
	      std::get<0>(local_two_body_intgs[i]).position = 
	        qmt::QmtVector(std::get<0>(two_body_intgs[i]).position.get_x() * s_vector.get_x(),
	        std::get<0>(two_body_intgs[i]).position.get_y() * s_vector.get_y(),std::get<0>(two_body_intgs[i]).position.get_z() * s_vector.get_z());
	
	      std::get<1>(local_two_body_intgs[i]).position = 
	        qmt::QmtVector(std::get<1>(two_body_intgs[i]).position.get_x() * s_vector.get_x(),
	        std::get<1>(two_body_intgs[i]).position.get_y() * s_vector.get_y(),std::get<1>(two_body_intgs[i]).position.get_z() * s_vector.get_z());        

	      std::get<2>(local_two_body_intgs[i]).position = 
	        qmt::QmtVector(std::get<2>(two_body_intgs[i]).position.get_x() * s_vector.get_x(),
	        std::get<2>(two_body_intgs[i]).position.get_y() * s_vector.get_y(),std::get<2>(two_body_intgs[i]).position.get_z() * s_vector.get_z());

	      std::get<3>(local_two_body_intgs[i]).position = 
	        qmt::QmtVector(std::get<3>(two_body_intgs[i]).position.get_x() * s_vector.get_x(),
	        std::get<3>(two_body_intgs[i]).position.get_y() * s_vector.get_y(),std::get<3>(two_body_intgs[i]).position.get_z() * s_vector.get_z());	        

	    }

   	  for(int i = 0; i< megacell.size();++i)
             local_megacell[i].position = qmt::QmtVector( megacell[i].position.get_x() * s_vector.get_x(),megacell[i].position.get_y() * s_vector.get_y(),megacell[i].position.get_z() * s_vector.get_z());

   	  for(int i = 0; i < supercell.size(); ++i)
             local_supercell[i].position = qmt::QmtVector(supercell[i].position.get_x() * s_vector.get_x(),supercell[i].position.get_y() * s_vector.get_y(),supercell[i].position.get_z() * s_vector.get_z());

	}

public:
	/*
 	/ ONE BODY INTEGRALS
	*/

virtual	double get_one_body(size_t index) const {
          
		if (index >= local_one_body_intgs.size()) {
		std::cerr
				<< "MicroscopicH2chain::get_one_body: index exceeding the number of one-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 2 > (local_one_body_intgs[index])) {
		std::cerr
				<< "MicroscopicH2chain::get_one_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

	qmt::QmtVector trans1 =
			(std::get < 0 > (local_one_body_intgs[index])).position;
					
	qmt::QmtVector trans2 =
			(std::get < 1 > (local_one_body_intgs[index])).position;
					
	 double result = 0;

	 result += wannier_basis->kinetic_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id ,trans1,trans2);
       
	 for(auto iter=local_megacell.begin(); iter!=local_megacell.end(); iter++) {
		 result +=  atomic_numbers[(iter->id)]*(wannier_basis->attractive_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id, iter->position, trans1 ,trans2));
	 }
         return result;
        }
 	
	/*
 	/ TWO BODY INTEGRALS
	*/

virtual	double get_two_body(size_t index) const {
//if(index == 0) return 1.5;
        if (index >= local_two_body_intgs.size()) {
		std::cerr
				<< "MicroscopicH2chain::get_two_body: index exceeding the number of two-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 4 > (local_two_body_intgs[index])) {
		std::cerr
				<< "MicroscopicH2chain::get_two_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

	qmt::QmtVector trans0 =
			(std::get < 0 > (local_two_body_intgs[index])).position;
					
	qmt::QmtVector trans1 =
			(std::get < 1 > (local_two_body_intgs[index])).position;
					
	qmt::QmtVector trans2 =
			(std::get < 2 > (local_two_body_intgs[index])).position;
					
	qmt::QmtVector trans3 =
			(std::get < 3 > (local_two_body_intgs[index])).position;

         return wannier_basis->v_integral(std::get < 0 > (local_two_body_intgs[index]).id ,std::get < 1 > (local_two_body_intgs[index]).id,
				          std::get < 2 > (local_two_body_intgs[index]).id ,std::get < 3 > (local_two_body_intgs[index]).id,
					  trans0, trans1, trans2, trans3);
        }
       
virtual	double get_coulomb_repulsion() const {
        	double CoulombEnergy = 0.0;
		for (auto i = local_supercell.begin(); i != local_supercell.end(); i++) {
			for (auto j = local_megacell.begin(); j != local_megacell.end(); j++) {
				double distance = qmt::QmtVector::norm(i->position - j->position);
				if (distance > 1e-5) {
					CoulombEnergy += 2.0 / distance;
			}
		}
	}
	 return CoulombEnergy / 2;
	}

virtual	size_t number_of_one_body() const {
		return local_one_body_intgs.size();
        }

virtual	size_t number_of_two_body() const {
		return local_two_body_intgs.size();
        }

virtual	void set_parameters(const std::vector<double>& params, const  qmt::QmtVector& v = qmt::QmtVector(1,1,1)) {
	   delete wannier_basis;
           scale(v);
           wannier_basis = new qmt:: QmtWannierBasis<Creator>(wannier_provider.get_definitions(),

								  wannier_provider.get_equations(),
								  wannier_provider.get_betas(),params, v);
	}

virtual	void set_parameters(double param, const  qmt::QmtVector& v = qmt::QmtVector(1,1,1)) {
	  delete wannier_basis;
           scale(v);
	   
           wannier_basis = new qmt:: QmtWannierBasis<Creator>(wannier_provider.get_definitions(),

								  wannier_provider.get_equations(),
								  wannier_provider.get_betas(),param, v);
        
	}
	
	//(i,j) Wannier Basis indices, R_i, R_j - coordinates 
	double get_overlap(int i, int j,const qmt::QmtVector& R_i,const qmt::QmtVector& R_j) {
		return wannier_basis->overlap(i,j,R_i,R_j);
        }

        double get_wfs_products_sum(const qmt::QmtVector& r,const std::vector<double>& averages, const std::vector<int>& index_map) const {
          double result = 0.0;
          int i = 0,j = 0;
           for(const auto &item1 : local_megacell){
             for(const auto &item2 : local_megacell){
               double w1 = 0, w2 = 0;
              if( wannier_basis->get_position(item1.id) == item1.position)
                w1= wannier_basis->get_value(item1.id,r);
              else 
                w1= wannier_basis->get_value(item1.id,r,item1.position - wannier_basis->get_position(item1.id));
              if(wannier_basis->get_position(item2.id) ==item2.position)     
                w2= wannier_basis->get_value(item2.id,r);
              else 
                w2= wannier_basis->get_value(item2.id,r,item2.position - wannier_basis->get_position(item2.id));


             int ind1 = index_map[i];
             int ind2 = index_map[j];
//             std::cout<<item1.position<<" "<<item2.position<<" "<<ind1<<" "<<ind2<<std::endl;
             result+= w1*w2*(averages[ind1*local_supercell.size() + ind2] + 
                             averages[local_supercell.size() * local_supercell.size() + ind1*local_supercell.size() + ind2]); //spin up + spin down
                j++;
               }
              i++;
              j=0;
             }
          return result;
        }
        
double get_wfs_value(size_t id, const qmt::QmtVector& r) const {
return wannier_basis->get_value(id,r);
}

virtual	~QmtMicroscopic() {
         delete wannier_basis;
        }

};

}

#endif // QMT_MICROSCOPIC_H_INCLUDED

