#ifndef QMTGSLTOOLS_H
#define QMTGSLTOOLS_H


#include <iostream>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

namespace qmt {

void qmt_gsl_matrix_print(const gsl_matrix* matrix);
void qmt_gsl_vector_print(const gsl_vector* vector);
void qmt_gsl_vector_print_perp(const gsl_vector* vector);


} //end of namespace qmt
#endif
