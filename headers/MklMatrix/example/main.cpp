//#include "../qmtmklsparse.h"
#include "../../qmtsimplesparse.h"
#include <stdlib.h>
#include "omp.h"
int main() {

unsigned int N = 2500000;

 std::vector<double> values(N);
 std::vector<double> voutput(N);
 qmt::QmtSparseMatrix<double> matrixqmt(N,N);
 qmt::QmtSparseMatrix<double> matrixqmt1(N,N); 

/*Sorted*/ 
 qmt::QmtSparseMatrix<double,1> matrixqmts(N,N);
 qmt::QmtSparseMatrix<double,1> matrixqmt1s(N,N); 
 
 
// matrixqmt1.set_value(1000,0,3);
// matrixqmt1s.set_value(1000,0,3);


for(int i = 0; i < N; ++i) {
values.push_back(i);
for(int j = i; j <= i+3 ;++j) {
double value = (double)(rand())/RAND_MAX;
if(j > 0 && j < N) {
// matrixqmt.set_value(value,i,j);
//matrixqmt1.set_value(value,i,j);

// matrixqmts.set_value(value,i,j);
 matrixqmt1s.set_value(value,i,j);
  matrixqmts.set_value(value,i,j);
}
//if(i == 0){
//values.push_back((double)(rand())/RAND_MAX);
//}
 } 
}

// matrixqmts.set_value(1000,600,3);
// matrixqmts.set_value(1000,0,3);



std::cout<<"matricies generated"<<std::endl;
double t_qmt = omp_get_wtime();
matrixqmts.sort();
matrixqmt1s.sort();
for(int i = 0; i < 1; ++i)
 qmt::QmtSparseMatrix<double,1>::Add(matrixqmts,matrixqmt1s,1);
// qmt::QmtSparseMatrix<double,1>::Multiply(matrixqmts,values,voutput,1,0);
// qmt::QmtSparseMatrix<double>::Add(matrixqmt,matrixqmt1,1);
 std::cout<<"QmtSparseMatrix M1["<<N<<"]["<<N<<"] + M["<<N<<"]["<<N<<"]  elapsed time:"<<omp_get_wtime() - t_qmt<<"s"<<voutput[0]<<std::endl;
/*
std::cout<<matrixqmts<<std::endl;
std::cout<<"-----------------------------"<<std::endl;
std::cout<<matrixqmt1s<<std::endl;*/
/*
matrixqmts.show_sparse();
std::cout<<"-----------------------------"<<std::endl;
matrixqmts.show_sparse();

std::cout<<"-----------------------------"<<std::endl;
matrixqmt.show_sparse();
*/

std::vector<double> out(N);
/*
t_qmt = omp_get_wtime();
 qmt::QmtSparseMatrix<double,1>::Multiply(matrixqmts,values,out,1,1);

for(auto x : out) std::cout<<x<<" ";

out.clear();

std::cout<<matrixqmt<<std::endl;
 qmt::QmtSparseMatrix<double,1>::Multiply(matrixqmt1s,values,out,1,1);
std::cout<<std::endl;
for(int i = 0; i < N; i++) std::cout<<out[i]<<" ";


std::cout<<"QmtSparseMatrix M1["<<N<<"]["<<N<<"] * V["<<N<<"]  elapsed time:"<<omp_get_wtime() - t_qmt<<"s"<<std::endl;
//std::cout<<matrixqmts<<std::endl;

//std::cout<<u[1]<<std::endl;
*/
return 0;
}
