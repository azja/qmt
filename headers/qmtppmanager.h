#define GSL_LIBRARY
#include "../../../headers/qmtmpipool.h"
#include <iostream>
#include <iomanip>
#include <iterator>
#include "../include/microscopic.h"
//#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "../../../headers/qmtbasisgenerator.h"
#include <gsl/gsl_matrix.h>
#include "../include/hamiltonianH2sc.h"
#include "../../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../../headers/qmtmatrixformula.h"
#include "../../../headers/qmtreallanczos.h"
#include <stdlib.h>
#include <fstream>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


unsigned int  NSIZE;
typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

template <typename Microscopic>
struct Body {

    virtual double integral(const Microscopic &model) = 0;

    virtual ~Body() {}

};

template <typename Microscopic>
struct TwoBody:public Body {

    uint id;

    TwoBody(uint t):
        id(t) {}

    double integral(const Microscopic &model) {
        return model.get_two_body(id);
    }

};

template  <typename Microscopic>
struct OneBody:public Body {

    uint id;

    OneBody(uint t):  id(t) {}

    double integral(const Microscopic &model) {
        return model.get_one_body(id);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

template <typename Microscopic>
class PoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    std::vector< Body*> bodies;
    double *integrals;
    Microscopic& model;

public:
    PoolJob(Microscopic& Model): model(Model) {
            model.set_all(a, R, 1, theta);
        for(unsigned int i = 0; i < model.number_of_one_body(); ++i)
           bodies.push_back(new OneBody(i));

        for(unsigned int i = 0; i < model.number_of_two_body(); ++i)
            bodies.push_back(new TwoBody(i));

        integrals = new double[size];

        std::fill(integrals, integrals + size, 0.0);

    }


    ~PoolJob() {
        for(unsigned int i = 0; i < bodies.size(); ++i)
            delete bodies[i];
        delete [] integrals;
    }


     Microscopic& get_model()  {
        return model;
    }

private:  //Here result is returned by params!!! result = 0 is ok it can by anything
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_parameters(arg);

        double *array = static_cast<double*>(params);

        for(unsigned int i = 0; i < NSIZE; ++i) {
            if(i + id *NSIZE < bodies.size()) {
                array[i] =  bodies [id * NSIZE +i]->integral(model);
            }
            else
                array[i] = 0.0;
        }

        return result;
    }
    
     double Calculate(int n, double* arg,int id,void * params)  {
        double result  = 0;
	std::vector<double> params(arg[0],arg[0] +n);
        model.set_parameters(params);

        double *array = static_cast<double*>(params);

        for(unsigned int i = 0; i < NSIZE; ++i) {
            if(i + id *NSIZE < bodies.size()) {
                array[i] =  bodies [id * NSIZE +i]->integral(model);
            }
            else
                array[i] = 0.0;
        }

        return result;
    }
};


/*
 *
 */


template <typename Microscopic, typename Diagonalization>
class InternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {

  Microscopic& model;
  Diagonalization& diagonalization;
  

public:
    InternalJob(Microscopic &microscopic, Diagonalization& diag):model(microscopic), diagonalization(diag){
      
    }

    ~InternalJob() {
    
    }

private:
    double Calculate(double* arg,int size,void * params) {

        std::vector<double> intgs;
        std::copy(&arg[0], &arg[0] + model.number_of_one_body() + model.number_of_two_body(),
               std::back_inserter(intgs));
        
        return diagonalization.getEnergy(model,intgs);
    }

};

/*
 * Minimization - final result
 */

template <typename Optimizer>

class FinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {

  
    typedef double(*internal_func_one)(double,void*);
    typedef double(*internal_func_two)(int, double*,void*);
    Optimizer& optimizer;
     
public:
    
     FinalJob(Optimizer& opt):optimizer(opt){
       
       optimizer.Function = FinalProcessJob<double>::OutFunc;
       
    }
    
    std::tuple<double,double> getResult() {
      result optimizer.getResult();
    }

private:
    
    double Calculate(void * params) {
       
      optimizer.Params = params;
      
      return optimizer.result();
          
    }

};



template <typename Microscopic, typename Diagonalization, typename Optimizer, typename T = double>
class QmtPPManager {
 
  Microscopic& microscopic;
  Diagonalization& diagonalization;
  Optimizer& optimizer;
  
   qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE>* gatherer;
  
public:
   QmtPPManager(Microscopic& m, Diagonalization& d, Optimizer& o):microscopic(m),Diagonalization(d), optimizer(o) {
  
    int nproc;
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);
    MPI_Comm comm;		
    MPI_Comm_dup( MPI_COMM_WORLD, &comm);
    
    auto   NSIZE = (m.number_of_one_body() + m. number_of_two_body())/(nproc - 1) +1;
    
    qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;					//Diagonalization
    qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob(n_centers);			//number of centers 
    qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a, R, theta,nproc - 1, ng);	
    
    gatherer = new  qmt::QmtMPIGatheredFunction<T, MPI_DOUBLE>(comm, finalJob, internalJob, processJob, NSIZE);
     
  }
  
  std::tuple<T,T> getResult() const {
    gatherer.run();
    return optimizer.getResult();              //Root will do that
  }
  
  ~QmtPPManager() {
    delete gatherer;
  }
  
  
};



