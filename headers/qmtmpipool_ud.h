#UNDER DEVELOPMENT DON'T USE IT#

#include <mpi.h>
#include <pthread.h>
#include <iostream>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <stdexcept>
#ifndef QMTMPIPOOL_H_INCLUDED
#define QMTMPIPOOL_H_INCLUDED


namespace qmt {



/*
 * This class provides following functionality:
 * There are N+1 processes, one of process is selected to be local communiactor root the
 * rest N are workers filling as an output N-sized matrix.
 *
 * It is responsible for synchronization. The root also perform actions defined by:
 * - InternalProcessJob - data  gathered from N processes is used to perform some
 *   calculations
 * - FinalProcessJob - it may use gsl-like interface and perform some actions on
 *   result obtained from InternalProcessJob.
 *
 * PoolProcessJob is a job for each of N workers - the output matrix in QmtMPIGatheredFunction
 * is filled by the gathered result
 */



template <typename T = double,int MPI_TYPE = MPI_DOUBLE>
class QmtMPIGatheredFunction {
public:
///////////////////////////////////////////////////////////////////////////////////////////

    /*
     *Work classes
     */

    /*
     * Final job - output we want to receive - it is to be done by ROOT process only
     */

    template <typename U = double >
    class FinalProcessJob {

    protected:
        static  U OutFunc(U arg, void* params) {
            return QmtMPIGatheredFunction<T,MPI_TYPE>::Function(arg,params);
        }

        /*
        *TO BE IMPLEMENTED - MULTIVARIABLE CASE // 24.02.2015
        */

        static  U OutFunc(int n, U* arg, void* params) {
            return QmtMPIGatheredFunction<T,MPI_TYPE>::Function(n,arg,params);
        }


        virtual U Calculate( void* params) = 0;
    public:

        U Function(void *params = NULL) {
            return Calculate(params);
        }

        virtual ~FinalProcessJob() {}
    };

    /*
     * Work done on gathered table as an input
     */

    template <typename U = double >
    class InternalProcessJob {

        virtual U Calculate(U* arg, int size, void* params) = 0;
    public:

        U Function(U* arg,int size, void *params = NULL) {
            return Calculate(arg, size, params);
        }
        virtual ~InternalProcessJob() {}
    };

    /*
     * Work for the process pool
     */

    template <typename U = double >
    class PoolProcessJob {

        virtual U Calculate(U arg, int id, void* params) = 0;
	virtual U Calculate(int n, U arg, int id, void* params) = 0;
    public:

        T Function(U arg, int id, void *params = NULL) {
            return Calculate(arg, id, params);
        }
        
         T Function(int n, U* arg, int id, void *params = NULL) {
            return Calculate(n, arg, id, params);
        }

        virtual ~PoolProcessJob() {}
    };

    /*
    * end of work classes
    */

//////////////////////////////////////////////////////////////////////////////////


    friend class FinalProcessJob<T>;
    friend class InternalProcessJob<T>;
    friend class PoolProcessJob<T>;

private:
    MPI_Comm _globalComm;   //Communicator in which object context is
    int n_rank;
    int n_wps; //Number of worker processes: n_wps = n_tot(in communicator) - 1
    int *new_ranks_1;
    int new_ranks_2[1];
    int root;
    T *output;

    FinalProcessJob<T>    *finalProcessJob;
    InternalProcessJob<T> *internalProcessJob;
    PoolProcessJob<T>     *poolProcessJob;
    int NUM_SIZE;



    static T Function(int n, T *arg,void* params) { //It is a trigger as well!

        QmtMPIGatheredFunction* me = NULL;

        if(params != NULL)
            me = static_cast<QmtMPIGatheredFunction*>(params);

        MPI_Request request;
        MPI_Status  status;

        int trigger = 1;

//Triggers to do some job by workers since they are waiting...
        for(int j = 0; j < me->n_wps; ++j) {

            MPI_Isend(&trigger,1, MPI_TYPE, j, 0,
                      me->_globalComm, &request);
            MPI_Wait(&request,&status);

            MPI_Isend(&arg,n, MPI_TYPE, j, 0,
                      me->_globalComm, &request);
            MPI_Wait(&request,&status);


        }

//Now we wait for the result...
        MPI_Irecv(me->output,me->n_wps * me->NUM_SIZE, MPI_TYPE,0,0, me->_globalComm,&request);
        MPI_Wait(&request, &status);



        return (me->internalProcessJob)->Function(me->output, me->n_wps, me);

    }
    
    static T Function(T arg,void* params) { //It is a trigger as well!

        QmtMPIGatheredFunction* me = NULL;

        if(params != NULL)
            me = static_cast<QmtMPIGatheredFunction*>(params);

        MPI_Request request;
        MPI_Status  status;

        int trigger = 1;

//Triggers to do some job by workers since they are waiting...
        for(int j = 0; j < me->n_wps; ++j) {

            MPI_Isend(&trigger,1, MPI_TYPE, j, 0,
                      me->_globalComm, &request);
            MPI_Wait(&request,&status);

            MPI_Isend(&arg,1, MPI_TYPE, j, 0,
                      me->_globalComm, &request);
            MPI_Wait(&request,&status);


        }

//Now we wait for the result...
        MPI_Irecv(me->output,me->n_wps * me->NUM_SIZE, MPI_TYPE,0,0, me->_globalComm,&request);
        MPI_Wait(&request, &status);



        return (me->internalProcessJob)->Function(me->output, me->n_wps, me);

    }


    T Function(T arg) {
        return QmtMPIGatheredFunction::Function(arg, this);
    }

    /*
    *TO BE IMPLEMENTED - MULTIVARIABLE CASE // 24.02.2015
    */
    T Function(int n, T* args,void* params) {

        return QmtMPIGatheredFunction::Function(n, args, this);
    }


    void work(MPI_Comm &comm, int root,int n = 1) {


        int newcomm_taskid;
        MPI_Comm_rank(comm,&newcomm_taskid);
        T result = T();
        T* result_table = new T[NUM_SIZE];
        int do_flag = 0;

        MPI_Request request;
        MPI_Status  status;

        T arg;

        while(1) {
//   result = 0;
            MPI_Irecv(&do_flag, 1, MPI_TYPE, n_wps, 0, _globalComm, &request);
            MPI_Wait(&request, &status);
            if(do_flag == 2) break;

            if(do_flag) {
                MPI_Irecv(&arg, n, MPI_TYPE, n_wps, 0, _globalComm, &request);
                MPI_Wait(&request, &status);
		if(1 == n)
                 result = poolProcessJob->Function(arg, newcomm_taskid, result_table);
		if(1 < n)
                 result = poolProcessJob->Function(n, arg, newcomm_taskid, result_table);
            }
            do_flag = 0;

            if(NUM_SIZE == 1)
                MPI_Gather(&result,NUM_SIZE, MPI_TYPE,
                           output, NUM_SIZE, MPI_TYPE,0,
                           comm);
            else

                MPI_Gather(result_table,NUM_SIZE, MPI_TYPE,
                           output, NUM_SIZE, MPI_TYPE,0,
                           comm);
            /*
             * Data is also sent to the root process
             */

            if(newcomm_taskid == 0) {
                MPI_Isend(output, n_wps * NUM_SIZE, MPI_TYPE, root, 0, _globalComm, &request);
                MPI_Wait(&request, &status);
            }

            MPI_Barrier(comm);
        }
        delete result_table;
    }

    
    
    
public:

    T run(int n = 1) {

        MPI_Barrier(_globalComm);

        int rank;
        int taskid;
        MPI_Status status;
        MPI_Group world_group;
        MPI_Group new_group;
        MPI_Comm new_comm;
        MPI_Comm_rank(_globalComm,&taskid);
        MPI_Comm_size(_globalComm,&rank);


        MPI_Comm_group(_globalComm, &world_group);

        T result;

        if(taskid == root)
            MPI_Group_incl(world_group, 1, new_ranks_2, &new_group);
        else
            MPI_Group_incl(world_group, n_wps, new_ranks_1, &new_group);

        MPI_Comm_create(_globalComm,new_group,&new_comm);

//    int comm_task_id;


//    int doit = 1;
        int do_foo = 0;

        MPI_Request request;
        //ROOT

        if(taskid == root) {
            do_foo = 1;
            if( 1 == n)
              result  = finalProcessJob->Function(this);
            else
	       result  = finalProcessJob->Function(this);
            do_foo = 2;

            //  finalJob

            for(int j = 0; j < n_wps ; ++j) {
                MPI_Isend(&do_foo,1, MPI_TYPE, j, 0,
                          _globalComm, &request);
                MPI_Wait(&request,&status);
            }


        }

//WORKERS
        else {
            work(new_comm, root,n);
        }

        MPI_Bcast(&result,1,MPI_DOUBLE,root, MPI_COMM_WORLD);
        return result;
    }

    QmtMPIGatheredFunction(MPI_Comm &comm, FinalProcessJob<T>* finalJob,
                           InternalProcessJob<T>* internalJob,
                           PoolProcessJob<T>* processJob, int NSIZE = 1):
        _globalComm(comm),finalProcessJob(finalJob),
        internalProcessJob(internalJob),
        poolProcessJob(processJob), NUM_SIZE(NSIZE) {

        MPI_Comm_size(_globalComm,&n_rank);
        n_wps = n_rank - 1;
        new_ranks_1 = new int [n_wps];
        new_ranks_2[0] = {n_wps};
        root = n_wps;

        for(int i = 0; i < n_rank-1; ++i)
            new_ranks_1[i] = i;

        if(NUM_SIZE == 1)
            output = new T [n_wps ];
        else
            output = new T [n_wps * NUM_SIZE];


    }

    void get_output(T* array, int size) const {
        if(size != n_wps * NUM_SIZE)  throw std::length_error("QmtMPIGatheredFunction::get_output(T* array, int size)::invalid length");
        for(int i = 0; i < size; ++i)
            array[i] = output[i];
    }

    void get_output(std::vector<T> &array) const {
        array.clear();
        for(int i = 0; i < n_wps; ++i)
            array.push_back(output[i]);
    }

    ~QmtMPIGatheredFunction() {
        delete [] new_ranks_1;
        delete [] output;
    }


};



template<typename T, int MPI_TYPE/*, int NUM_SIZE  = 1*/>
using FinalProcessJob = QmtMPIGatheredFunction<T, MPI_TYPE/*,NUM_SIZE*/>::FinalProcessJob<T>;

template<typename T, int MPI_TYPE/*, int NUM_SIZE  = 1*/>
using InternalProcessJob = QmtMPIGatheredFunction<T, MPI_TYPE/*,NUM_SIZE*/>::InternalProcessJob<T>;

template<typename T, int MPI_TYPE/*, int NUM_SIZE  = 1*/>
using PoolProcessJob = QmtMPIGatheredFunction<T, MPI_TYPE /*,NUM_SIZE*/>::PoolProcessJob<T>;


///////////EXAMPLES///////////////

class SimpleFinalJob:public FinalProcessJob<double, MPI_DOUBLE/*,2*/> {
    double Calculate(void * params) {
        return FinalProcessJob<double>::OutFunc(1.0, params);
    }
};

class SimpleInternalJob:public InternalProcessJob<double, MPI_DOUBLE/*,2*/> {
    double Calculate(double* arg,int size,void * params) {
        double result = 0.0;
        std::cout<<"I am doing smth"<<std::endl;
        for(int i = 0; i < size * 2; ++i) {
            std::cout<<"result#"<<i<<" = "<<arg[i]<<std::endl;

            result += arg[i];
        }
        return result;
    }
};

class SimplePoolJob:public PoolProcessJob<double, MPI_DOUBLE/*,2*/> {

    double Calculate(double arg,int id,void * params) {
        double *array = static_cast<double*>(params);
        array[0] = 1.0;
        array[1] = id;
        return id;
    }
};

}

#endif //QMTMPIPOOL_H_INCLUDED
/*
int main() {
MPI_Init(0,0);
int taskid;
MPI_Comm comm;
MPI_Comm_dup( MPI_COMM_WORLD, &comm);
MPI_Comm_rank(MPI_COMM_WORLD,&taskid);



qmt::FinalProcessJob<double, MPI_DOUBLE,2>* finalJob = new qmt::SimpleFinalJob;
qmt::InternalProcessJob<double, MPI_DOUBLE,2>* internalJob = new qmt::SimpleInternalJob;
qmt::PoolProcessJob<double, MPI_DOUBLE,2>* processJob = new qmt::SimplePoolJob;


qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE,2> gatherer(comm, finalJob, internalJob, processJob);

double result;

result = gatherer.run();

if(taskid == 0)
std::cout<<result<<std::endl;
MPI_Finalize();
return 0;
}

*/
