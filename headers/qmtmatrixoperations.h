#ifndef QMTMATRIXOPERATIONS_H
#define QMTMATRIXOPERATIONS_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_cblas.h>

namespace qmt {

class QmtBaseChanger {

public:
    QmtBaseChanger(gsl_matrix* _beta) {
        if (_beta == NULL) {

            beta = NULL;
            inverse_beta = NULL;
            size_of_matrix = 0.0;

            throw("qmt::change_of_basis: No transition matrix!");
        }
        if (_beta->size1 != _beta->size2) {
            beta = NULL;
            inverse_beta = NULL;
            size_of_matrix = 0.0;

            throw("qmt::change_of_basis: Transition matrix is not square!");
        }

        size_of_matrix = _beta->size1;
        beta = gsl_matrix_alloc(size_of_matrix, size_of_matrix);
        gsl_matrix_memcpy(beta, _beta);

        inverse_beta = gsl_matrix_alloc(size_of_matrix, size_of_matrix);

        createInverse();

    }

    ~QmtBaseChanger() {
        gsl_matrix_free(beta);
        gsl_matrix_free(inverse_beta);
    }

    void newBase(gsl_matrix* _beta) {
        gsl_matrix_memcpy(beta, _beta);

        createInverse();

    }

    int change_of_basis(gsl_matrix* matrix) {
        if (matrix == NULL) {
            std::cerr << "qmt::change_of_basis: No matrix to be changed!"
                      << std::endl;
            return -1;
        }
        if (matrix->size1 != matrix->size2) {
            std::cerr << "qmt::change_of_basis: Matrix is not square!"
                      << std::endl;
            return -2;
        }
        if ((matrix->size1 != size_of_matrix)
                || (matrix->size2 != size_of_matrix)) {
            std::cerr
                    << "qmt::change_of_basis: Matrix is not the same size as the transition matrix!"
                    << std::endl;
            return -3;
        }

        gsl_matrix * tmp_matrix = gsl_matrix_alloc(size_of_matrix,
                                  size_of_matrix);

        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, inverse_beta, matrix,
                       0.0, tmp_matrix);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmp_matrix, beta, 0.0,
                       matrix);

        gsl_matrix_free(tmp_matrix);
        return 1;
    }

private:
    void createInverse() {		// Calculating the inverse beta

        gsl_matrix * tmp_matrix = gsl_matrix_alloc(size_of_matrix,
                                  size_of_matrix);
        gsl_permutation * permutiation = gsl_permutation_alloc(size_of_matrix);
        int sgn;

        gsl_matrix_memcpy(tmp_matrix, beta);

        // Make LU decomposition of matrix tmp_matrix1=beta
        gsl_linalg_LU_decomp(tmp_matrix, permutiation, &sgn);

        // Invert the matrix beta
        gsl_linalg_LU_invert(tmp_matrix, permutiation, inverse_beta);

        gsl_matrix_free(tmp_matrix);
        gsl_permutation_free(permutiation);
    }

    gsl_matrix *beta;
    gsl_matrix *inverse_beta;
    unsigned int size_of_matrix;

};
}
#endif
