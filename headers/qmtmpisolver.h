#ifndef QMT_MPI_SOLVER_H
#define QMT_MPI_SOLVER_H

#include<mpi.h>
#include<vector>
#include<functional>    // std::function
#include <algorithm>    // std::copy
#include "qmtsystem.h"
#include "qmtenginedata.h"

struct Qmt_MPI {
  const MPI_Comm communicator;
  int root;
  int stop_job_signal;
  int do_job_signal;
  int wait_signal;
  
  
  Qmt_MPI():communicator(MPI_COMM_WORLD), root(0),stop_job_signal(0),do_job_signal(1),wait_signal(2) {}
  
  virtual int get_rank() const {
    int rank;
    MPI_Comm_rank( communicator, &rank); 
   return rank;
  }
    
  virtual int listen() const{
    int signal;
    MPI_Status status;
    MPI_Recv(&signal, 1, MPI_INT, root, 0, communicator, &status);

    return signal; 
  }
  
  virtual std::vector<double> get_vector() const {
    int size;
    MPI_Status status;
    
    MPI_Recv(&size, 1, MPI_INT, root, 0, communicator, &status);
    double* args = new double[size];
    
    MPI_Recv(args, size, MPI_DOUBLE, root, 0, communicator, &status);
    
    std::vector<double> result(args,args + size);
    
    delete [] args;
    return result;
    
  }
  
   
  virtual void send_do_job_signal() const {
    int dsg = do_job_signal;
    for(int i = 0; i < get_comm_size();++i) {   
     if( i != root && get_rank() == root)
      MPI_Send(&dsg,1, MPI_INT, i, 0,communicator);
    }
  }
  

  
  void send_stop_job_signal() const  {
    int ssg = stop_job_signal;
    for(int i = 0; i < get_comm_size();++i) {   
     if( i != root && get_rank() == root)
      MPI_Send(&ssg,1, MPI_INT, i, 0,communicator);
    }
    
  }
  
  void send_vector(std::vector<double> args) const {
    int size = args.size();

    for(int i = 0; i < get_comm_size();++i) {   
     if( i != root && get_rank() == root)
      MPI_Send(&size,1, MPI_INT, i, 0,communicator);
    }
    
    double* largs = new double[size];
    
    
    std::copy(args.begin(),args.end(),largs);
    
    for(int i = 0; i < get_comm_size();++i) {   
     if( i != root && get_rank() == root)
      MPI_Send(largs,size, MPI_DOUBLE, i, 0,communicator);
    }
   
    
  }
  
  virtual int get_comm_size() const {
    int size;
    MPI_Comm_size( communicator, &size );
    return size;
  };
  
  virtual ~Qmt_MPI() {}
};





double QmtEnergyEngineMPIIntegrals(std::vector<double> args,const qmt::QmtVector& scaler,const qmt::QmtEngineData& data,const Qmt_MPI& settings) {
  /*
   * Get some MPI info
   */
   
  int rank = settings.get_rank();

  int nproc = settings.get_comm_size();
  if(rank == settings.root) {settings.send_do_job_signal();
   settings.send_vector(args); 
   settings.send_vector(std::vector<double>{scaler.get_x(),scaler.get_y(),scaler.get_z()}); 

  }
 
  /*
   * Compute one-body indicies for each process
   */
  
  int number_of_one_body = data.microscopic->get_one_body_number();
  int number_of_two_body = data.microscopic->get_two_body_number();
  
  
  int block_size_one;
  
  if(number_of_one_body <= nproc)
     block_size_one = 1;
  else
     block_size_one = number_of_one_body/nproc + 1;
    
   
  int start_index_one_body = block_size_one * rank;
  int end_index_one_body = start_index_one_body + block_size_one -1;
  
  /*
   * Compute two-body indicies for each process
   */
  
  
  int block_size_two;
  
  if(number_of_two_body <= nproc)
     block_size_two = 1;
  else
     block_size_two = number_of_two_body/nproc + 1;
  
   
  int start_index_two_body = block_size_two * rank;
  int end_index_two_body = start_index_two_body + block_size_two -1;
  

  
  double *local_one_body = new double [block_size_one];
  double *local_two_body = new double [block_size_two];
  
  /*
   * Set QmtMicroscopic parameters
   */
  
  data.microscopic->set_parameters(args,scaler);
  
  
  
  /*
   * Compute Integrals
   */
  
  for(auto i = start_index_one_body;i <= end_index_one_body; ++i){
    if(i < number_of_one_body)
      local_one_body[i - start_index_one_body] = data.microscopic->get_one_body_integral(i);
    else
      local_one_body[i - start_index_one_body] = 0.0f;
  }
   
  for(auto i = start_index_two_body;i <= end_index_two_body; ++i){
    if(i < number_of_two_body)
      local_two_body[i - start_index_two_body] = data.microscopic->get_two_body_integral(i);
    else
      local_two_body[i - start_index_two_body] = 0.0f;
  }
  
  /*
   * Gather data
   */
  
    
   double* global_one_body = new double[block_size_one * nproc ];
   double* global_two_body = new double[block_size_two * nproc ];
 

   MPI_Allgather(local_one_body, block_size_one,MPI_DOUBLE,global_one_body,block_size_one,MPI_DOUBLE,settings.communicator);
   MPI_Allgather(local_two_body, block_size_two,MPI_DOUBLE,global_two_body,block_size_two,MPI_DOUBLE,settings.communicator);
   
   /*
    * Diagonalization by root process
    */
   
   double result;
   std::vector<double> one(global_one_body, global_one_body + number_of_one_body);
   std::vector<double> two(global_two_body, global_two_body + number_of_two_body);

   
   if(rank == settings.root)
     result = data.diagonalizer->Diagonalize(one,two);
   
  
   MPI_Bcast(&result,1, MPI_DOUBLE, settings.root, settings.communicator);
  
   delete [] local_one_body;
   delete [] local_two_body;
   delete [] global_one_body;
   delete [] global_two_body;
 
 return result; 
  
}


typedef typename  std::function <double(std::vector<double> args,const qmt::QmtVector& scaler,const qmt::QmtEngineData& data,const Qmt_MPI& settings)>  QmtMPIEnergyFunction;


void Qmt_MPI_Engine( void* params, std::function <void(void*)> F,
		              QmtMPIEnergyFunction Energy,
		              const Qmt_MPI& settings,
	                      const qmt::QmtEngineData& data) {
  
  
  if(settings.get_rank() == settings.root) {
    F(params);
    settings.send_stop_job_signal();
  }
  else {
    while(1) {
      int signal = settings.listen();
      if(signal == settings.stop_job_signal) break;
      if(signal == settings.do_job_signal) {
       auto args = settings.get_vector();
       auto scale = settings.get_vector();
       qmt::QmtVector scale_v(scale[0],scale[1],scale[2]);
       Energy(args, scale_v, data, settings);
      }
	     
    }
    
  }

 }

QmtMPIEnergyFunction Qmt_Get_Predefined_MPI_Energy_Function(size_t n) {
  switch(n) {
    case 0 : return QmtMPIEnergyFunction(QmtEnergyEngineMPIIntegrals);
  }
  return QmtMPIEnergyFunction(QmtEnergyEngineMPIIntegrals);
}

#endif //_QMT_MPI_SOLVER_H