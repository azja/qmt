#ifndef QMTMATRIXFORMULA_H
#define QMTMATRIXFORMULA_H

#ifndef _QMT_NO_MPI
#include <mpi.h>
#endif
#include <vector>
#include <list>
#include <utility>
#include <algorithm>
#include <math.h>
#include "qmvars.h"
#include "omp.h"
#ifdef GSL_LIBRARY	// to use GSL
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <thread>         // std::thread
#include <mutex>          // std::mutex

#include "qmtmatrixoperations.h"
#endif

namespace qmt {

template<class Hamiltonian, class MatrixAlgebra>
class QmtGeneralMatrixFormula {

    typedef typename Hamiltonian::NState State;
    typedef typename MatrixAlgebra::Matrix Matrix;
    typedef typename MatrixAlgebra::Vector Vector;

private:
    Hamiltonian hamiltonian;
    std::vector< std::pair<double,Matrix> > one_body_microscopic_parameter;
    std::vector< std::pair<double,Matrix> > two_body_microscopic_parameter;
    std::vector<State> test_states;
    bool IF_PAREPARED;
    bool IF_SET_STATES;
    bool IF_SET_HAMILTONIAN;
    bool IF_FILLED;

    Matrix Hamiltonian_Matrix;
    std::mutex mtx;

public:
    QmtGeneralMatrixFormula<Hamiltonian,MatrixAlgebra>() {
        IF_PAREPARED = false;
        IF_SET_HAMILTONIAN = false;
        IF_SET_STATES = false;
        IF_FILLED = false;

    }

    ~QmtGeneralMatrixFormula<Hamiltonian,MatrixAlgebra>() {
        for(auto iter = one_body_microscopic_parameter.begin(); iter!=one_body_microscopic_parameter.end(); iter++) {
            MatrixAlgebra::DeinitializeMatrix(std::get<1>(*iter));
        }
        for(auto iter = two_body_microscopic_parameter.begin(); iter!=two_body_microscopic_parameter.end(); iter++) {
            MatrixAlgebra::DeinitializeMatrix(std::get<1>(*iter));
        }
        MatrixAlgebra::DeinitializeMatrix(Hamiltonian_Matrix);
    }

    void set_hamiltonian(Hamiltonian _hamiltonian) {
        hamiltonian = _hamiltonian;
        IF_SET_HAMILTONIAN = true;

        // std::cout<<hamiltonian.number_of_one_body()<<" "<<hamiltonian.number_of_two_body()<<std::endl;
    }

    void set_States(std::vector<State> _test_states) {
        test_states = _test_states;

        IF_PAREPARED = prepareFormula();

        MatrixAlgebra::InitializeMatrix(Hamiltonian_Matrix,test_states.size(),test_states.size());
    }

    void set_StatesOptional(std::vector<State> _test_states, int OPTION = 0) {
        test_states = _test_states;

        IF_PAREPARED = prepareFormulaOptional(OPTION);

        MatrixAlgebra::InitializeMatrix(Hamiltonian_Matrix,test_states.size(),test_states.size());
    }


#ifndef _QMT_NO_MPI
    void set_StatesMPI(std::vector<State> _test_states) {
        test_states = _test_states;

        IF_PAREPARED = prepareFormulaMPI();

        MatrixAlgebra::InitializeMatrix(Hamiltonian_Matrix,test_states.size(),test_states.size());
    }
#endif
private:
    bool prepareFormula() {
        if(!IF_SET_HAMILTONIAN) {
            std::cerr<<"QmtGeneralMatrixFormula: prepareFormula(): Hamiltonian is not set yet!"<<std::endl;

            return IF_SET_HAMILTONIAN;
        }

        //Getting one-body terms
        unsigned int i;
        unsigned int j;
        unsigned int k;
       

        Hamiltonian *h = &hamiltonian;
        State* t_states = new State [test_states.size()] ;
        std::copy(test_states.begin(), test_states.end(),t_states);

        std::vector<std::tuple<int,int,double>> entries;

        for (i = 0; i <  hamiltonian.number_of_one_body(); i++) {
            std::pair<double,Matrix> Hamiltonian_brick;
            std::get<0>(Hamiltonian_brick) =  hamiltonian.get_value(1, i);
            MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
            #pragma omp parallel for  private(j,k) shared(t_states,h, Hamiltonian_brick, entries) collapse(2)
            for ( j = 0; j < test_states.size(); ++j) {
                for ( k = 0; k < test_states.size(); ++k) {

                    double value =  h->matrix_el(t_states[j], t_states[k], 1, i);
#ifdef _QMT_DEBUG
			std::cout<<"QmtMatrixFormula: ";
			std::cout<<t_states[j]<<" . "<<t_states[k]<<" = "<<value<<std::endl; 
#endif
                    if (fabs(value)>1e-16)  MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),j,k,value);

                }
            }
           //MatrixAlgebra::PrintMatrix(std::get<1>(Hamiltonian_brick));
            one_body_microscopic_parameter.push_back(Hamiltonian_brick);
        }
        //Getting two-body terms
        for (unsigned int i = 0; i < hamiltonian.number_of_two_body(); i++) {
            std::pair<double,Matrix> Hamiltonian_brick;
            std::get<0>(Hamiltonian_brick) =  hamiltonian.get_value(2, i);
            MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
            #pragma omp parallel for  private(j,k) shared(t_states,h, Hamiltonian_brick, entries) collapse(2)
            for  (j = 0; j < test_states.size(); ++j) {
                for ( k = 0; k < test_states.size(); ++k) {
                    double value = h->matrix_el(t_states[j], t_states[k], 2, i);
                    if (fabs(value)>1e-16) MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),j,k,value);
                }
            }

            //MatrixAlgebra::PrintMatrix(std::get<1>(Hamiltonian_brick));
            two_body_microscopic_parameter.push_back(Hamiltonian_brick);

        }
        delete [] t_states;
        return true;
    }
////////////////////////////////////////////////////////////////////////////////////////////
    bool prepareFormulaOptional(int OPTION = 0) { /*0 - calculate all, 1 - calculate all one body and diagonal two-body*/
        if(!IF_SET_HAMILTONIAN) {
            std::cerr<<"QmtGeneralMatrixFormula: prepareFormula(): Hamiltonian is not set yet!"<<std::endl;

            return IF_SET_HAMILTONIAN;
        }

        //Getting one-body terms
        unsigned int i;
        unsigned int j;
        unsigned int k;

        Hamiltonian *h = &hamiltonian;
        State* t_states = new State [test_states.size()] ;
        std::copy(test_states.begin(), test_states.end(),t_states);


        std::vector<State> *MV = new std::vector<State> [test_states.size()];
        for (i = 0; i <  hamiltonian.number_of_one_body(); i++) {
            std::pair<double,Matrix> Hamiltonian_brick;
            std::get<0>(Hamiltonian_brick) =  hamiltonian.get_value(1, i);
            MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
            #pragma omp parallel for  private(j) shared(t_states, h,MV)
            for( j = 0; j < test_states.size(); ++j) {
                std::vector<State> combination;
                h->vector_set(combination,t_states[j],1,i);
                MV[j].assign(combination.begin(),combination.end());
            }

            #pragma omp parallel for  private(j,k) shared(t_states, h, MV, Hamiltonian_brick) collapse(2)
            for( j= 0; j < test_states.size(); ++j) {
                for ( k = 0; k < test_states.size(); ++k) {
                    double value = 0.0;
                    for(auto  vec = MV[j].begin(); vec != MV[j].end(); ++vec) {
                        double temp = State::dot_product(*vec,test_states[k]);
                        if(fabs(temp)>1e-16 ) value+=temp;
                    }
                    if (fabs(value)>1e-16)  MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),j,k,value);
                }

            }

            one_body_microscopic_parameter.push_back( Hamiltonian_brick);

        }
        /*TWO BODY*/

        delete [] MV;

        MV = new std::vector<State> [test_states.size()];


        for (i = 0; i <  hamiltonian.number_of_two_body(); i++) {
            std::pair<double,Matrix> Hamiltonian_brick;
            std::get<0>(Hamiltonian_brick) =  hamiltonian.get_value(2, i);
            MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
            #pragma omp parallel for  private(j) shared(t_states, h)
            for( j = 0; j < test_states.size(); ++j) {
                std::vector<State> combination;
                h->vector_set(combination,t_states[j],2,i);
                MV[j].assign(combination.begin(),combination.end());
            }
            
            if(OPTION == 0) {
	      double value = 0.0;
             //   #pragma omp parallel for  private(j,value) shared(t_states, h, MV, Hamiltonian_brick)
                for( j= 0; j < test_states.size(); ++j) {
                    for ( k = 0; k < test_states.size(); ++k) {
                         value = 0.0;
                        
			 for(auto  vec = MV[j].begin(); vec != MV[j].end(); ++vec) {
                        			  double temp = State::dot_product(*vec,test_states[k]);
                         
			    if(fabs(temp)>1e-16 ) value+=temp;
			
                        }
                        
                        if (fabs(value)>1e-16)  MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),j,k,value);
			
		
                    }

                }
            }


            if(OPTION == 1) {
                #pragma omp parallel for  private(j) shared(t_states, h, MV, Hamiltonian_brick)
                for( j= 0; j < test_states.size(); ++j) {
                    double value = 0.0;
                    for(auto  vec = MV[j].begin(); vec != MV[j].end(); ++vec) {
                        double temp = State::dot_product(*vec,test_states[j]);
                        if(fabs(temp)>1e-16 ) value+=temp;
                    }
                    if (fabs(value)>1e-16)  MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),j,j,value);
                }
            }


            two_body_microscopic_parameter.push_back( Hamiltonian_brick);
        }




        delete [] MV;


        delete [] t_states;
        return true;
    }




/////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _QMT_NO_MPI
    bool prepareFormulaMPI() {

//It is assumed that MPI is initialized
        if(!IF_SET_HAMILTONIAN) {
            std::cerr<<"QmtGeneralMatrixFormula: prepareFormulaMPI(): Hamiltonian is not set yet!"<<std::endl;

            return IF_SET_HAMILTONIAN;
        }

        //Getting one-body terms
        unsigned int i;
        unsigned int j;
        unsigned int k;
        unsigned int m_size = test_states.size();


        Hamiltonian *h = &hamiltonian;
        State* t_states = new State [m_size] ;
        std::copy(test_states.begin(), test_states.end(),t_states);


        std::vector<std::tuple<int,int,double>> entries;


        int taskid;					//Task id
        int nproc;					//Number of processes

        MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
        MPI_Comm_size(MPI_COMM_WORLD,&nproc);

        unsigned int chunk_one_body = hamiltonian.number_of_one_body()/nproc + 1;
        unsigned int chunk_two_body = hamiltonian.number_of_two_body()/nproc + 1;


        int one_cntr = 0;
        int two_cntr = 0;

        std::vector<int> strides_one;
        std::vector<int> strides_two;


        std::vector<int> cols_one;
        std::vector<int> rows_one;
        std::vector<double> vals_one;

        std::vector<int> cols_two;
        std::vector<int> rows_two;
        std::vector<double> vals_two;

        std::vector<double> integrals_one;
        std::vector<double> integrals_two;

        for (i = taskid *chunk_one_body; i <  taskid *chunk_one_body + chunk_one_body && i < hamiltonian.number_of_one_body(); i++) {
            //std::pair<double,Matrix> Hamiltonian_brick;
            //std::get<0>(Hamiltonian_brick) =  hamiltonian.get_value(1, i);
            //MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
            integrals_one.push_back(hamiltonian.get_value(1, i));
            int cntr = 0;
            #pragma omp parallel for  private(j,k) shared(t_states,h, rows_one, cols_one, vals_one) reduction(+:cntr) collapse(2)

            for ( j = 0; j < test_states.size(); ++j) {
                for ( k = 0; k < test_states.size(); ++k) {
                    double value = h->matrix_el(t_states[j], t_states[k], 1, i);
                    if (fabs(value)>1e-16)  {
                        mtx.lock();
                        rows_one.push_back(j);
                        cols_one.push_back(k);
                        vals_one.push_back(value);
                        mtx.unlock();
                        cntr += 1;
                    };
                    //std::cout<<" i = "<<i<<" j = "<<j<<std::endl;
                }
            }
            if(cntr > 0 )
                strides_one.push_back(cntr);
            if(cntr > one_cntr) one_cntr = cntr;

            //MatrixAlgebra::PrintMatrix(std::get<1>(Hamiltonian_brick));
            // one_body_microscopic_parameter.push_back(Hamiltonian_brick);
        }

        //Getting two-body terms
        for (i = taskid *chunk_two_body; i <  taskid *chunk_two_body + chunk_two_body && i < hamiltonian.number_of_two_body(); i++) {
            //std::pair<double,Matrix> Hamiltonian_brick;
            //std::get<0>(Hamiltonian_brick) =  hamiltonian.get_value(2, i);
            //MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
            int cntr = 0;
            integrals_two.push_back(hamiltonian.get_value(2, i));
            #pragma omp parallel for  private(j,k) shared(t_states,h, rows_two, cols_two, vals_two) reduction(+:cntr) collapse(2)
            for ( j = 0; j < test_states.size(); ++j) {
                for ( k = 0; k < test_states.size(); ++k) {
                    double value = h->matrix_el(t_states[j], t_states[k], 2, i);
                    if (fabs(value)>1e-16) {
                        mtx.lock();
                        rows_two.push_back(j);
                        cols_two.push_back(k);
                        vals_two.push_back(value);
                        mtx.unlock();
                        cntr +=1;
                    }

                }
            }
            if(cntr > 0)
                strides_two.push_back(cntr);
            if(cntr > two_cntr) two_cntr = cntr;
            //MatrixAlgebra::PrintMatrix(std::get<1>(Hamiltonian_brick));
            // two_body_microscopic_parameter.push_back(Hamiltonian_brick);

        }
        delete [] t_states;


        int root = nproc - 1;

//std::cout<<" root = "<<root<<" taskid = "<<taskid<<std::endl;

        if( root == taskid ) {
//std::cout<<"Entering MPI comunications..."<<std::endl;
        }

//Now find the the limit for strides
        int one_stride_size = strides_one.size();
        int two_stride_size = strides_two.size();

        int one_stride_max = 0;
        int two_stride_max = 0;

        MPI_Allreduce(&one_stride_size,&one_stride_max,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
        MPI_Allreduce(&two_stride_size,&two_stride_max,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);



        int *strides_one_p = NULL;
        int *strides_two_p = NULL;


        strides_one_p = new int[one_stride_max];
        strides_two_p = new int[two_stride_max];

        std::fill(strides_one_p,strides_one_p + one_stride_max, -1);
        std::fill(strides_two_p,strides_two_p + two_stride_max, -1);


        std::copy(strides_one.begin(), strides_one.end(),strides_one_p);
        std::copy(strides_two.begin(), strides_two.end(),strides_two_p);


        int *gathered_strides_one = NULL;
        int *gathered_strides_two = NULL;

        if( root == taskid) {
//  std::cout<<"one_stride_max = "<<one_stride_max<<std::endl;
            gathered_strides_one = new int[nproc * one_stride_max];
            gathered_strides_two = new int[nproc * two_stride_max];
        }

        MPI_Gather(strides_one_p,one_stride_max, MPI_INT, gathered_strides_one,one_stride_max,MPI_INT,root,MPI_COMM_WORLD);
        MPI_Gather(strides_two_p,two_stride_max, MPI_INT, gathered_strides_two,two_stride_max,MPI_INT,root,MPI_COMM_WORLD);


//int* strides_p = new

        int m_size_one_in = vals_one.size();
        int m_size_two_in = vals_two.size();


        int m_size_one_out;
        int m_size_two_out;

        MPI_Allreduce(&m_size_one_in,&m_size_one_out,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
        MPI_Allreduce(&m_size_two_in,&m_size_two_out,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);

        int *pcolumns_one = new int[m_size_one_out];
        int *pcolumns_two = new int[m_size_two_out];

        int *prows_one = new int[m_size_one_out];
        int *prows_two = new int[m_size_two_out];

        double *pvalues_one = new double[m_size_one_out];
        double *pvalues_two = new double[m_size_two_out];



        std::fill(pcolumns_one, pcolumns_one + m_size_one_out, -1);
        std::fill(pcolumns_two, pcolumns_two + m_size_two_out, -1);

        std::fill(prows_one, prows_one + m_size_one_out, -1);
        std::fill(prows_two, prows_two + m_size_two_out, -1);

        std::fill(pvalues_one, pvalues_one + m_size_one_out, 0);
        std::fill(pvalues_two, pvalues_two + m_size_two_out, 0);

        std::copy(cols_one.begin(), cols_one.end(),pcolumns_one);
        std::copy(cols_two.begin(), cols_two.end(),pcolumns_two);

        std::copy(rows_one.begin(), rows_one.end(),prows_one);
        std::copy(rows_two.begin(), rows_two.end(),prows_two);

        std::copy(vals_one.begin(), vals_one.end(),pvalues_one);
        std::copy(vals_two.begin(), vals_two.end(),pvalues_two);


        int *columns_one_gathered = NULL;
        int *columns_two_gathered = NULL;

        int *rows_one_gathered = NULL;
        int *rows_two_gathered = NULL;

        double *vals_one_gathered = NULL;
        double *vals_two_gathered = NULL;


        if(taskid == root) {
            columns_one_gathered = new int [m_size_one_out * nproc];
            columns_two_gathered = new int [m_size_two_out * nproc];

            rows_one_gathered = new int [m_size_one_out * nproc];
            rows_two_gathered = new int [m_size_two_out * nproc];

            vals_one_gathered = new double [m_size_one_out * nproc];
            vals_two_gathered = new double [m_size_two_out * nproc];

        }


        MPI_Gather(pcolumns_one,m_size_one_out, MPI_INT, columns_one_gathered,m_size_one_out,MPI_INT,root,MPI_COMM_WORLD);
        MPI_Gather(pcolumns_two,m_size_two_out, MPI_INT, columns_two_gathered,m_size_two_out,MPI_INT,root,MPI_COMM_WORLD);

        MPI_Gather(prows_one,m_size_one_out, MPI_INT, rows_one_gathered,m_size_one_out,MPI_INT,root,MPI_COMM_WORLD);
        MPI_Gather(prows_two,m_size_two_out, MPI_INT, rows_two_gathered,m_size_two_out,MPI_INT,root,MPI_COMM_WORLD);

        MPI_Gather(pvalues_one,m_size_one_out, MPI_DOUBLE, vals_one_gathered,m_size_one_out,MPI_DOUBLE,root,MPI_COMM_WORLD);
        MPI_Gather(pvalues_two,m_size_two_out, MPI_DOUBLE, vals_two_gathered,m_size_two_out,MPI_DOUBLE,root,MPI_COMM_WORLD);

        int integrals_one_size = integrals_one.size();
        int integrals_two_size = integrals_two.size();

        int integrals_one_size_max;
        int integrals_two_size_max;

        MPI_Allreduce(&integrals_one_size,&integrals_one_size_max,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
        MPI_Allreduce(&integrals_two_size,&integrals_two_size_max,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);


        double *pintegrals_one = new double [integrals_one_size_max];
        double *pintegrals_two = new double [integrals_two_size_max];

        std::fill(pintegrals_one, pintegrals_one + integrals_one_size_max,0);
        std::fill(pintegrals_two, pintegrals_two + integrals_two_size_max,0);

        std::copy(integrals_one.begin(), integrals_one.end(), pintegrals_one);
        std::copy(integrals_two.begin(), integrals_two.end(), pintegrals_two);

        double *gathered_integrals_one = NULL;
        double *gathered_integrals_two = NULL;

        if(taskid == root) {
//   std::cout<<"integrals_one_size_max = "<<integrals_one_size_max<<std::endl;
            gathered_integrals_one = new double [integrals_one_size_max * nproc];
            gathered_integrals_two = new double [integrals_two_size_max * nproc];
        }

        MPI_Gather(pintegrals_one,integrals_one_size_max, MPI_DOUBLE, gathered_integrals_one,integrals_one_size_max,MPI_DOUBLE,root,MPI_COMM_WORLD);
        MPI_Gather(pintegrals_two,integrals_two_size_max, MPI_DOUBLE, gathered_integrals_two,integrals_two_size_max,MPI_DOUBLE,root,MPI_COMM_WORLD);

        delete [] strides_one_p;
        delete [] strides_two_p;

        delete [] pcolumns_one;
        delete [] pcolumns_two;

        delete [] prows_one;
        delete [] prows_two;

        delete [] pvalues_one;
        delete [] pvalues_two;

        delete [] pintegrals_one;
        delete [] pintegrals_two;


        if( root == taskid )  {

            int cumulant = 0;
            for(int i = 0; i < nproc * one_stride_max; ++i) {

                if(  gathered_strides_one[i] > 0 ) {
//       if( i > 0 && gathered_strides_one[i-1] < 0) { lcntr++; cumulant = 0;}
                    std::pair<double,Matrix> Hamiltonian_brick;
                    std::get<0>(Hamiltonian_brick) =   gathered_integrals_one[i];
                    MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());

                    int j = 0;
                    while( j != gathered_strides_one[i])
                    {
                        int k =  rows_one_gathered[cumulant];
                        int l =  columns_one_gathered[cumulant];
                        double value = vals_one_gathered[cumulant];
                        cumulant++;
                        if( k >= 0  && l >= 0) {
                            MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),k,l,value);
                            j++;
                        }
                    }
//      std::cout<<" i ="<<i<<std::endl;
                    one_body_microscopic_parameter.push_back(Hamiltonian_brick);
//            cumulant += gathered_strides_one[i];
                }

            }
//std::cout<<"One matrix number:"<<lcntr<<std::endl;
//std::cout<<"Val 0,0 = "<<std::get<1>(one_body_microscopic_parameter[0])->get_value(1,1)<<std::endl;

            cumulant = 0;
            for(int i = 0; i < nproc * two_stride_max; ++i) {
//   std::cout<<"g["<<i<<"] = "<<gathered_strides_two[i]<<"  ";
                if(  gathered_strides_two[i] > 0 ) {
//             if( i > 0 && gathered_strides_two[i-1] < 0) {lcntr++; cumulant = 0;}
                    std::pair<double,Matrix> Hamiltonian_brick;
                    std::get<0>(Hamiltonian_brick) =   gathered_integrals_two[i];
                    MatrixAlgebra::InitializeMatrix(std::get<1>(Hamiltonian_brick),test_states.size(),test_states.size());
                    int j = 0;
                    while(j !=   gathered_strides_two[i] ) {
                        int k =  rows_two_gathered[cumulant];
                        int l =  columns_two_gathered[cumulant];
                        double value = vals_two_gathered[cumulant];
//          std::cout<<"value_two ["<<k<<"]["<<l<<"] = "<<value<<std::endl;
                        cumulant++;
                        if( k >= 0  && l >= 0) {
                            MatrixAlgebra::SetMatrix(std::get<1>(Hamiltonian_brick),k,l,value);
                            j++;
                        }

                    }
                    two_body_microscopic_parameter.push_back(Hamiltonian_brick);
//                  cumulant += gathered_strides_two[i];
                }
            }
//std::cout<<"Two matrix number:"<<lcntr<<std::endl;



        }



        return true;

    }
#endif

public:

    void set_microscopic_parameter(int which_term, unsigned int which_number,
                                   double value) { //where which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
	IF_FILLED = false;
        switch (which_term) {
        case 1:
            if (which_number < hamiltonian.number_of_one_body()) {
                std::get<0>(one_body_microscopic_parameter[which_number])=value;
            } else
                std::cerr << "QmtHamiltonianFormula: OneBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        case 2:
            if (which_number < hamiltonian.number_of_two_body()) {
                std::get<0>(two_body_microscopic_parameter[which_number])=value;
            } else
                std::cerr << "QmtHamiltonianFormula: TwoBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        default:
            std::cerr << "QmtHamiltonianFormula: Wrong body indicator!\n";
            break;
        }
    }

    void print() {
        fill_outcome(Hamiltonian_Matrix);
        MatrixAlgebra::PrintMatrix(Hamiltonian_Matrix);
        MatrixAlgebra::ClearMatrix(Hamiltonian_Matrix);
    }

    void get_Hamiltonian_Matrix(Matrix& output) {
        fill_outcome(output);
    }



    void print(int which_term, unsigned int which_number) { //where which_term = 1 <=> OneBody, which_term = 2 <=> TwoBody
        switch (which_term) {
        case 1:
            if (which_number < hamiltonian.number_of_one_body()) {
                MatrixAlgebra::PrintMatrix(std::get<1>(one_body_microscopic_parameter[which_number]));
            } else
                std::cerr << "QmtHamiltonianFormula: OneBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        case 2:
            if (which_number < hamiltonian.number_of_two_body()) {
                MatrixAlgebra::PrintMatrix(std::get<1>(two_body_microscopic_parameter[which_number]));

            } else
                std::cerr << "QmtHamiltonianFormula: TwoBody[" << which_number
                          << "] doesn't exist!\n";
            break;
        default:
            std::cerr << "QmtHamiltonianFormula: Wrong body indicator!\n";
            break;
        }

    }

private:
//NOTE:: It is important to update two_body first because of possible matrix-matrix addition implementation
//which for sparse format behaves as O(N1 + N2)
    void fill_outcome(Matrix Hamiltonian_Matrix) {
        MatrixAlgebra::ClearMatrix(Hamiltonian_Matrix);
        IF_FILLED = true;
        // Filling the outcome - can be changed if we change microscopic parameters
        for(auto iter = two_body_microscopic_parameter.begin(); iter!=two_body_microscopic_parameter.end(); iter++) {
//            std::cout<<*std::get<1>(*iter)<<" ";
            MatrixAlgebra::AddMM(Hamiltonian_Matrix,std::get<1>(*iter),std::get<0>(*iter));
        }

        for(auto iter = one_body_microscopic_parameter.begin(); iter!=one_body_microscopic_parameter.end(); iter++) {
            //std::cout<<std::get<0>(*iter)<<" ";
            MatrixAlgebra::AddMM(Hamiltonian_Matrix,std::get<1>(*iter),std::get<0>(*iter));
        }

    }


};


} //end of namespace qmt

#endif

