#ifndef QMTINTEGRALS_H
#define QMTINTEGRALS_H

#include <iostream>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include "qmtvector.h"
#include "qmthbuilder.h"

namespace qmt{
namespace integrals{

using one_body =  std::tuple<qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom>;
using two_body =  std::tuple<qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom>;

inline void hash_combine_atoms(std::size_t & seed, const qmt::QmtVectorizedAtom& atom){
  qmt::QmtVectorizedAtomHash hasher;
  seed ^= hasher(atom) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct one_body_Hash{
  std::size_t operator()(const one_body& integral) const{
    std::size_t seed = 0;
    hash_combine_atoms(seed,std::get<0>(integral));
    hash_combine_atoms(seed,std::get<1>(integral));
    return seed;
  }
};

struct one_body_Eq{
  bool operator()(const one_body& i1, const one_body& i2) const{
    return std::get<0>(i1) == std::get<0>(i2) &&
           std::get<1>(i1) == std::get<1>(i2);
  }
};


struct two_body_Hash{
  std::size_t operator()(const two_body& integral) const{
    std::size_t seed = 0;
    hash_combine_atoms(seed,std::get<0>(integral));
    hash_combine_atoms(seed,std::get<1>(integral));
    hash_combine_atoms(seed,std::get<2>(integral));
    hash_combine_atoms(seed,std::get<3>(integral));
    return seed;
  }
};

struct two_body_Eq{
  bool operator()(const two_body& i1, const two_body& i2) const{
    return std::get<0>(i1) == std::get<0>(i2) &&
           std::get<1>(i1) == std::get<1>(i2) &&
           std::get<2>(i1) == std::get<2>(i2) &&
           std::get<3>(i1) == std::get<3>(i2);
  }
};

template<typename T>
using unordered_map_one_body = std::unordered_map<one_body,T,one_body_Hash,one_body_Eq>;
using unordered_set_one_body = std::unordered_set<one_body,one_body_Hash,one_body_Eq>;
template<typename T>
using unordered_map_two_body = std::unordered_map<two_body,T,two_body_Hash,two_body_Eq>;
using unordered_set_two_body = std::unordered_set<two_body,two_body_Hash,two_body_Eq>;

} //end of namespace integrals
} //end of namespace qmt

#endif
