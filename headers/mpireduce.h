/*Andrzej Biborski 11.02.2014*/

#ifndef MPIREDUCE_H_INCLUDED
#define MPIREDUCE_H_INCLUDED
#include "mpi.h"
#include <math.h>
#include <iostream>


#define MASTER 0

template <typename Functor, int mpi_reduction = MPI_SUM, int mpi_type = MPI_DOUBLE>
class QmtMPIReduce {

    /* General functionality */

    typedef  typename Functor::returned_value returned_value;
    const Functor& functor;

    QmtMPIReduce(const QmtMPIReduce&);
    QmtMPIReduce operator=(const QmtMPIReduce&);

    /* MPI governinig */

    returned_value	task_value;              /* tasks (process) local value  */
    returned_value	outcome;	             /* reduction result gathered from all tasks*/

    int	taskid;                              /* task ID */
    int numtasks;                            /* number of tasks */
    int	rc;                                  /* return code */


public:

    /*************************************************************************************/

    QmtMPIReduce(const Functor &func):functor(func) {}

    /*************************************************************************************/


    returned_value get_result( ) const {
        return outcome;
    }

    /*************************************************************************************/

    returned_value get_local_result( ) const {
        return task_value;
    }


    /*************************************************************************************/

    void reduce() {
        task_value=functor();

        rc = MPI_Reduce(&task_value, &outcome, 1, mpi_type, mpi_reduction,
                        MASTER, MPI_COMM_WORLD);

        if (rc != MPI_SUCCESS)
            std::cerr<<"Failure on mpc_reduce:" <<taskid<<std::endl;

    }
    /*************************************************************************************/


    void reduce(const returned_value& value) {
        task_value=functor(value);

        rc = MPI_Reduce(&task_value, &outcome, 1, mpi_type, mpi_reduction,
                        MASTER, MPI_COMM_WORLD);

        if (rc != MPI_SUCCESS)
            std::cerr<<"Failure on mpc_reduce:" <<taskid<<std::endl;

    }

    /*************************************************************************************/

};

#endif // MPIREDUCE_H_INCLUDED
