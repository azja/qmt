#ifndef QMT_MC_LANCZOS_H
#define QMT_MC_LANCZOS_H

#include <iostream>
#include <random>
#include "qmtdiagonalizer.h"
#include "qmthubbard.h"
#include "qmthbuilder.h"
#include "qmtbasisgenerator.h"
#include "qmtparsertools.h"
#include <algorithm>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>

namespace qmt{

template<class hamiltonian, class NState, unsigned int N>
class QmtMCLanczosDiagonalizer : public QmtDiagonalizer {

	typedef qmt::SqOperator<NState> sq_operator;

	hamiltonian* lanczos_hamiltonian; // pointer to hamiltonian

	qmt::QmtBasisGenerator generator; // basis generator
	std::vector<NState> states;	// vector of states
	std::vector<NState> states2;	// vector of states
	std::vector<NState> output;	// output vector

	std::vector<double> alphas;	// alphas (form Lanczos algorithm)
	std::vector<double> betas;	// betas (form Lanczos algorithm)

	unsigned int num_of_mchf;	// maximal number of states
	unsigned int lanczos_steps;	// number of lanczos steps

	double eps;			// zero accuracy
	int problem_size;
	int number_of_centers;
	int electron_number;
	int alphas_number;

	static bool my_compare(const NState& one, const NState& two){
		return fabs(one.getPhase())>fabs(two.getPhase());
	}
	static bool my_compare2(const NState& one, const NState& two){
		return NState::lt_configuration(one,two);
	}

	class mycomparison
	{
	public:
		mycomparison(){}
		bool operator() (const NState& lhs, const NState& rhs) const
		{
			return NState::lt_configuration(lhs,rhs);
		}
	} comp;

	public:

	QmtMCLanczosDiagonalizer(const char* file_name, unsigned int _num_of_mchf, unsigned int _lanczos_steps=500, double _eps=1e-09) : num_of_mchf(_num_of_mchf), lanczos_steps(_lanczos_steps), eps(_eps){
std::ifstream cfile;       
	cfile.open(file_name); 
  
	std::string content( (std::istreambuf_iterator<char>(cfile) ),
				(std::istreambuf_iterator<char>()    ) );
  
	std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
	problem_size = std::stoi(qmt::parser::get_bracketed_words(content,"problem_size","problem_size_end")[0]);
	number_of_centers=problem_size;
	electron_number = std::stoi(qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0]);
	lanczos_hamiltonian = qmt::QmtHamiltonianBuilder<hamiltonian>::getHamiltonian(hamiltonian_fn);
//	generator.generate(electron_number,problem_size*2,states);
//	problem_size=states.size();
	cfile.close();

//	for(const auto & state:states)
//	    std::cout<<state<<std::endl;
}


	QmtMCLanczosDiagonalizer(hamiltonian& my_hamiltonian, int num_of_sites, int num_of_electrons, unsigned int _num_of_mchf, unsigned int _lanczos_steps=500, double _eps=1e-09) : num_of_mchf(_num_of_mchf), lanczos_steps(_lanczos_steps), eps(_eps){
	number_of_centers=num_of_sites;
	electron_number = num_of_electrons;
	lanczos_hamiltonian = &my_hamiltonian;
	generator.generate(electron_number,num_of_sites*2,states);
	problem_size=states.size();
}

	QmtMCLanczosDiagonalizer(hamiltonian& my_hamiltonian,std::vector<NState>& _states, int num_of_sites, int num_of_electrons, unsigned int _num_of_mchf, unsigned int _lanczos_steps=500, double _eps=1e-09) : num_of_mchf(_num_of_mchf), lanczos_steps(_lanczos_steps), eps(_eps){
	number_of_centers=num_of_sites;
	electron_number = num_of_electrons;
	lanczos_hamiltonian = &my_hamiltonian;

	states=_states;
	std::sort(states.begin(),states.end(),my_compare2);
	double norm = 1.0/sqrt(NState::dot_product(states,states));
	for (auto& state : states)
		state.setPhase(state.getPhase()*norm);

	problem_size=states.size();
}

	virtual double Diagonalize(const std::vector<double>& one_body_integrals, const std::vector<double>& two_body_integrals){
	alphas.clear();
	betas.clear();

	states.clear();
	states2.clear();
	output.clear();

	generator.generate<N>(electron_number,problem_size*2,states);
	problem_size=states.size();

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0, 1);

	for (auto& state : states)
		state.setPhase(state.getPhase()*dis(gen));

	double norm = 1.0/sqrt(NState::dot_product(states,states));
	for (auto& state : states)
		state.setPhase(state.getPhase()*norm);

	std::sort(states.begin(),states.end(),my_compare2);

	double Coulomb_repulsion=one_body_integrals[0];
//	std::cout<<"CouRep= "<<Coulomb_repulsion<<std::endl;


	for (unsigned int i=1; i<one_body_integrals.size(); i++){
		lanczos_hamiltonian->setMicroscopicParameterOne(i-1,one_body_integrals[i]);
//		std::cout<<"I1m["<<i-1<<"]= "<<one_body_integrals[i]<<std::endl;
//		std::cout<<"I1h["<<i-1<<"]= "<<lanczos_hamiltonian->get_value(1,i-1)<<std::endl;
	}

	for (unsigned int i=0; i<two_body_integrals.size(); i++){
		lanczos_hamiltonian->setMicroscopicParameterTwo(i,two_body_integrals[i]);
//		std::cout<<"I2m["<<i<<"]= "<<two_body_integrals[i]<<std::endl;
//		std::cout<<"I2h["<<i<<"]= "<<lanczos_hamiltonian->get_value(2,i)<<std::endl;
	}

	double alpha = 0.0;
	double beta = 0.0;

	unsigned int steps_done = lanczos_steps;

	for(int i=0; i<lanczos_steps; i++){
	betas.push_back(beta);

	output.clear();
  	lanczos_hamiltonian->action(output,states);
  	
  
	std::sort(output.begin(),output.end(),my_compare);

	if(output.size()>num_of_mchf)
		output.erase(output.begin()+num_of_mchf,output.end());

	std::sort(output.begin(),output.end(),my_compare2);


	alpha = NState::dot_product(output,states);

#ifdef _QMT_DEBUG
	std::cout<<"MCLanczos: alpha"<<i<<"= "<<alpha<<std::endl;
#endif

	alphas.push_back(alpha);

	for (auto & state : states2){
		auto handle = lower_bound(output.begin(),output.end(),state,comp);

		if (NState::compare_configuration(*handle,state))
			handle->setPhase(handle->getPhase() - beta*state.getPhase());
		else{
			state.setPhase(state.getPhase()*(-beta));
			output.insert(handle,state);
		}
	}

//	for (const auto state: output)
//	    std::cout<<state<<" "<<state.getPhase()<<std::endl;

//	std::sort(output.begin(),output.end(),my_compare2); // uneccessery - vector is already sorted

	states2.clear();
	states2=states;

	for (auto & state : states){
		auto handle = lower_bound(output.begin(),output.end(),state,comp);

		if (NState::compare_configuration(*handle,state))
			handle->setPhase(handle->getPhase() - alpha*state.getPhase());
		else{
			state.setPhase(-alpha*state.getPhase());
			output.insert(handle,state);
		}
	}

//	std::sort(output.begin(),output.end(),my_compare2); // uneccessery - vector is already sorted

	states.clear();
	states=output;

	beta=sqrt(NState::dot_product(output,output));

#ifdef _QMT_DEBUG
	std::cout<<"MCLanczos: beta"<<i<<"= "<<beta<<std::endl;
#endif
	if(fabs(beta)<1e-16){
		steps_done=i+1;
		break;
	}
	norm=1.0/beta;

	for (auto& state : states)
		state.setPhase(state.getPhase()*norm);
}
	lanczos_steps=steps_done;

	gsl_matrix* Tridiagonal;

	Tridiagonal = gsl_matrix_calloc(lanczos_steps, lanczos_steps);
	gsl_matrix_set_zero(Tridiagonal);

	gsl_matrix_set(Tridiagonal, 0, 0, alphas[0]);

	for (unsigned int i = 1; i < lanczos_steps; i++) {
		gsl_matrix_set(Tridiagonal, i, i, alphas[i]);
		gsl_matrix_set(Tridiagonal, i - 1, i, betas[i]);
		gsl_matrix_set(Tridiagonal, i, i - 1, betas[i]);
	}

	
	gsl_vector* eigenvalues=gsl_vector_calloc(lanczos_steps);
	
	gsl_matrix* eigenvectors = gsl_matrix_calloc(lanczos_steps,lanczos_steps);


	gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(lanczos_steps);

	gsl_eigen_symmv(Tridiagonal, eigenvalues, eigenvectors, workspace);



	//Sorting
	gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);

	
//	for(int i=0; i<(lanczos_steps>10 ? 10 : lanczos_steps); i++)
//		std::cout<<gsl_vector_get(eigenvalues,i)+Coulomb_repulsion<<std::endl;

	double result = gsl_vector_get(eigenvalues,0)+Coulomb_repulsion;

	gsl_vector_free(eigenvalues);
	gsl_matrix_free(eigenvectors);
	gsl_eigen_symmv_free(workspace);

	return result;
	}

}; //end of class QmtMCLanczosDiagonalizer

} //end of namespace qmt

#endif //QMT_MC_LANCZOS_H
