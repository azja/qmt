#ifndef QMT_PARSER_TOOLS_H_INCLUDED
#define QMT_PARSER_TOOLS_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <cctype>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <utility>
#include <stdexcept> // std::logic_error()
#include <cctype> // isspace()
#include "qmtorbtypes.h"
#include "qmtvector.h"
#include "qmthbuilder.h"
#include "qmtintegrals.h"

namespace qmt {
namespace parser {

std::vector<std::string> get_delimited_words(const std::string& delimiters, const std::string& phrase);
std::vector<std::string> get_delimited_words(const std::vector<std::string>& delimiters, const std::string& phrase);
std::vector<std::string> get_bracketed_words(const std::string& input, const char& open, const char& close, bool CLEAR_WHITE_SPACES = false);
std::vector<std::string> get_bracketed_words(const std::string& input, const std::string& open, const std::string& close, bool CLEAR_WHITE_SPACES = true);
std::string orbitalTypeToString(qmt::OrbitalType orbital);
qmt::OrbitalType stringToOrbitalType(const std::string& orbital);
std::vector<int> get_atomic_numbers(const std::string& file_name);
qmt::QmtVector get_QmtVector_from_line(const std::string& line);
std::vector<qmt::QmtVector> get_QmtVector_from_file(const std::string& file_name);
void remove_comments(std::string& content);
void remove_whitespace(std::string& content);

namespace hamiltonian{

using QmtAtomMap = std::unordered_map< qmt::QmtVectorizedAtom, unsigned int, qmt::QmtVectorizedAtomHash>;
using QmtOneBodyMap = qmt::integrals::unordered_map_one_body<unsigned int>; 
using QmtTwoBodyMap = qmt::integrals::unordered_map_two_body<unsigned int>; 

unsigned int get_num_states(const std::string& ham_file);
unsigned int get_num_one_body(const std::string& ham_file);
unsigned int get_num_two_body(const std::string& ham_file);
void compare_supercell(const std::string& ham_file, const std::string& supercell_file, QmtAtomMap& map_supercell);
void compare_megacell(const std::string& ham_file, const std::string& megacell_file, QmtAtomMap& map_megacell);
void compare_one_body(const std::string& ham_file, QmtOneBodyMap& map_one_body);
void compare_two_body(const std::string& ham_file, QmtTwoBodyMap& map_two_body);


} // end of namespace hamiltonian
} // end of namespace parser
} // end of namespace qmt
#endif //QMT_PARSER_TOOLS_H_INCLUDED
