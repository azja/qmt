#ifndef QMT_SYSTEM_H
#define QMT_SYSTEM_H

#include<vector>
#include"qmtvector.h"

namespace qmt  {
  
  class QmtSystem {
    
  public:
    virtual int  get_one_body_number() = 0; //Should be +1 for "external field" energy
    virtual int  get_two_body_number() = 0;
    
    virtual double  get_one_body_integral(size_t index) = 0; // 0 is "external field"
    virtual double  get_two_body_integral(size_t index) = 0;
    
    virtual  double get_overlap(int i, int j,const qmt::QmtVector& first, const qmt::QmtVector& second) = 0;
    
    virtual void set_parameters(std::vector<double> params,const qmt::QmtVector& scale) = 0;
    
    virtual ~QmtSystem() {};
    
  };
    
}


#endif //class QmtSystem



