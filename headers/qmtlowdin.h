#ifndef _QMT_LOWDIN_INCLUDED
#define _QMT_LOWDIN_INCLUDED

#include <iostream>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>


#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

namespace qmt {


void qmtLowdinOrthogonalization(double **M, size_t N);



}


#endif //_QMT_LOWDIN_INCLUDED
