/*Andrzej Biborski 11.02.2014*/

#ifndef MPIMIN_H_INCLUDED
#define MPIMIN_H_INCLUDED
#include <mpi.h>
#include <vector>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <iostream>

template < typename Type, int mpi_type>
int qmt_MPI_min(Type* send_data, Type* receive_data, int count,  int root = 0, unsigned int* winner = NULL, int mpi_comm = MPI_COMM_WORLD) {

    std::vector<Type> data;

    int size;
    int taskid;

    MPI_Comm_size(mpi_comm, &size);
    MPI_Comm_rank(mpi_comm, &taskid);

    int mpi_status;
    MPI_Status status;

    if(taskid == root) {
        data.resize(size);
        data[root] = *send_data;
        for(int i = 0; i < size; ++i) {
            if( i!= root) {
                mpi_status = MPI_Recv((void*)receive_data,  count, mpi_type, i, 0, mpi_comm, &status);
                if(mpi_status != MPI_SUCCESS)
                    return mpi_status;
                data[i] = *receive_data;
            }
        }
    }
    else {
        mpi_status = MPI_Send((void*)send_data, count,  mpi_type, root, 0, mpi_comm);
        if(mpi_status != MPI_SUCCESS)
            return mpi_status;


    }
    if(taskid == root) {
        *winner = std::distance(data.begin(),std::min_element(data.begin(),data.end()));
        *receive_data = data[*winner];
    }

    return MPI_SUCCESS;
}



#endif // MPIMIN_H_INCLUDED
