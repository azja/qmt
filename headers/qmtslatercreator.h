#ifndef QMTSLATERCREATOR_H_
#define QMTSLATERCREATOR_H_

#include <iostream>
#include <vector>
#include "qmtslaterorbitals.h"
#include "qmtorbtypes.h"
#include "../gorb/gaussexp.h"
#include "qmtparsertools.h"
#include "qmtvector.h"
#include <string>



namespace qmt {

class QmtSlaterCreator {

    int alpha_id;
    int num_gaussians;
    qmt::QmtVector position;
    qmt::OrbitalType orbital_type;

    QmtSlaterCreator(const QmtSlaterCreator&);
    QmtSlaterCreator& operator=(const QmtSlaterCreator&);

public:


    QmtSlaterCreator(std::string input);

    qmt::SlaterOrbital* Create(double alpha, const qmt::QmtVector& scale=qmt::QmtVector(1,1,1)) const;

    int get_alpha_id() const;

    friend std::ostream& operator<<(std::ostream& stream, const QmtSlaterCreator& creator);

};

} //end of namespace qmt


#endif
