#ifndef SPARSEALGEBRA_H
#define SPARSEALGEBRA_H
#include "../../headers/qmtsimplesparse.h"
#include <cmath>

// Only for Tridiagonal - to be changed
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#ifndef _QMT_NO_MKL
#include "mkl_lapacke.h"
#endif

namespace LocalAlgebras {
class SparseAlgebra{
public:
	/*
	 * MatrixAlgebra must contain:
	 * types:
	 * 		(*) Vector
	 * 		(*) Matrix
	 * 		(*) Tridiagonal
	 *
	 * static functions:
	 * 		(*) Matrix-Matrix Algebra:
	 * 				- void AddMM(Matrix& A, const Matrix& B, double alpha)
	 *		(*) Matrix-Vector Algebra:
	 *				- void MultiplyMV(const Matrix& A, const Vector& b, Vector& c)
	 *				- void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, double alpha, double beta)
	 *		(*) Vector Algebra:
	 *				- void MultiplyVV(const Vector& a, Vector& b, double* result)
	 *				- void CopyVV(const Vector& a, Vector& b)
	 *				- void CopyVV(const Vector& a, Vector& b, double alpha)
	 *				- void ScaleV(Vector& a, double alpha)
	 *				- void SwapVV(Vector& a, Vector& b)
	 *				- double NormV(const Vector& a)
	 *				- void NormalizeV(Vector& a)
	 *		(*) (De)Inicialization:
	 *				- void InitializeMatrix(Matrix& A, unsigned int size1, unsigned int size2)
	 *				- void DeinitializeMatrix(Matrix& A)
	 *				- void InitializeVector(Vector& A, unsigned int size)
	 *				- void DeinitializeVector(Vector& A)
	 *				- void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double> Alphas,std::vector<double> Betas)
	 *				- void DeinitializeTridiagonal(Tridiagonal& A)
	 *		(*) Object Handling:
	 *				- void PrintMatrix(const Matrix& A)
	 *				- void PrintVector(const Vector& a)
	 *				- void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value)
	 *				- void SetVector(Vector& A, unsigned int i, double value)
	 *				- double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j)
	 *				- void GetMatrixC(Vector& v, const Matrix& A, unsigned int i)
	 *				- void GetMatrixR(Vector& v, const Matrix& A, unsigned int j)
	 *				- double GetVectorE(const Vector& A, unsigned int i)
	 *				- void SolveTridiagonal(Tridiagonal& TMatrix, Vector& eigenvalues, Matrix& eigenvectors)
	 *				- void ClearMatrix(Matrix& A)
	 *
	 */

	typedef qmt::QmtSparseMatrix<double,1>* Matrix;
	typedef std::vector<double>* Vector;
	typedef gsl_matrix* Tridiagonal;

	/*
	 * 		(*) Matrix-Matrix Algebra:
	 * 				- void AddMM(Matrix& A, const Matrix& B, double alpha)
	 */

	static void AddMM(Matrix& A, const Matrix& B, double alpha=1.0){
		qmt::QmtSparseMatrix<double,1>::Add(*A,*B,alpha);
	}

	/*
	 * 		(*) Matrix-Vector Algebra:
	 *				- void MultiplyMV(const Matrix& A, const Vector& b, Vector& c)
	 *				- void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, double alpha, double beta)
	 */

	static void MultiplyMV(const Matrix& A, const Vector& b, Vector& c){
		 qmt::QmtSparseMatrix<double,1>::Multiply(*A,*b,*c,1.0,0.0);
	}

	static void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, double alpha, double beta){
		 qmt::QmtSparseMatrix<double,1>::Multiply(*A,*b,*c,alpha,beta);
	}
	/*
	 *		(*) Vector Algebra:
	 *				- void MultiplyVV(const Vector& a, const Vector& b, double* result)
	 *				- void CopyVV(const Vector& a, Vector& b)
	 *				- void CopyVV(const Vector& a, Vector& b, double alpha)
	 *				- void ScaleV(Vector& a, double alpha)
	 *				- void SwapVV(Vector& a, Vector& b)
	 *				- double NormV(const Vector& a)
	 *				- void NormalizeV(Vector& a)
	 */

	static void MultiplyVV(const Vector& a, const Vector& b, double* result){
		if(a->size() == b->size()){
			double scalar_product=0.0;

		for(unsigned int i=0; i<a->size(); i++){
			scalar_product += (*a)[i]*(*b)[i];
		}

		(*result) = scalar_product;
		}
		else{
			std::cerr<<"SparseAlgebra.h: MultiplyVV -> vectors are not the same size!"<<std::endl;
			(*result)=0.0;
		}
	}

	static void CopyVV(const Vector& a, Vector& b){
		if(a->size() == b->size()){

		for(unsigned int i=0; i<a->size(); i++){
			(*b)[i]=(*a)[i];
		}

		}
		else{
			std::cerr<<"SparseAlgebra.h: CopyVV -> vectors are not the same size!"<<std::endl;
		}
	}

	static void CopyVV(const Vector& a, Vector& b,double alpha){
		if(a->size() == b->size()){

		for(unsigned int i=0; i<a->size(); i++){
			(*b)[i]+=alpha*(*a)[i];
		}

		}
		else{
			std::cerr<<"SparseAlgebra.h: CopyVV -> vectors are not the same size!"<<std::endl;
		}
	}

	static void ScaleV(Vector& a, double alpha){
		for(auto iter=a->begin(); iter!=a->end(); iter++){
			(*iter)*=alpha;
		}
	}

	static void SwapVV(Vector& a, Vector& b){
		Vector tmp;
		tmp=a;
		a=b;
		b=tmp;
	}

	static double NormV(const Vector& a){
		double sq_norm;
		MultiplyVV(a,a,&sq_norm);
		return sqrt(sq_norm);
	}

	static void NormalizeV(Vector& a){
		double norm=NormV(a);
		ScaleV(a,pow(norm,-1));
	}

	/*		(*) (De)Inicialization:
	 *				- void InitializeMatrix(Matrix& A, unsigned int size1, unsigned int size2)
	 *				- void DeinitializeMatrix(Matrix& A)
	 *				- void InitializeVector(Vector& A, unsigned int size)
	 *				- void DeinitializeVector(Vector& A)
	 *				- void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double> Alphas,std::vector<double> Betas)
	 *				- void DeinitializeTridiagonal(Tridiagonal& A)
	 */

	static void InitializeMatrix(Matrix& M, unsigned int rows,
			unsigned int cols) {
		M = new qmt::QmtSparseMatrix<double,1>(rows, cols);
	}

	static void DeinitializeMatrix(Matrix& M) {
		delete M;
	}

	static void InitializeVector(Vector& v, unsigned int size) {
		v = new std::vector<double>(size, 0.0);
	}

	static void DeinitializeVector(Vector& v) {
		delete v;
	}

	static void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double> Alphas,
			std::vector<double> Betas) {
		Tridiagonal_matrix = gsl_matrix_calloc(size, size);
		gsl_matrix_set_zero(Tridiagonal_matrix);

		gsl_matrix_set(Tridiagonal_matrix, 0, 0, Alphas[0]);

		for (unsigned int i = 1; i < size; i++) {
			gsl_matrix_set(Tridiagonal_matrix, i, i, Alphas[i]);
			gsl_matrix_set(Tridiagonal_matrix, i - 1, i, Betas[i]);
			gsl_matrix_set(Tridiagonal_matrix, i, i - 1, Betas[i]);
		}

	}

	static void DeinitializeTridiagonal(Tridiagonal& A) {
		gsl_matrix_free(A);
	}

		/*
		 *		(*) Object Handling:
		 *				- void PrintMatrix(const Matrix& A)
		 *				- void PrintVector(const Vector& a)
		 *				- void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value)
		 *				- void SetVector(Vector& A, unsigned int i, double value)
		 *				- double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j)
		 *				- void GetMatrixC(Vector& v, const Matrix& A, unsigned int i)
		 *				- void GetMatrixR(Vector& v, const Matrix& A, unsigned int j)
		 *				- double GetVectorE(const Vector& A, unsigned int i)
		 *				- void SolveTridiagonal(Tridiagonal& TMatrix, Vector& eigenvalues, Matrix& eigenvectors)
		 *				- void ClearMatrix(Matrix& A)
		 */

		static void PrintMatrix(Matrix& A){
			std::cout<<*A<<std::endl;
		}

		static void PrintVector(Vector& v){
			for(auto iter=v->begin(); iter!=v->end(); iter++){
				std::cout<<*iter<<" ";
			}
			std::cout<<std::endl;
		}

		static void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value){
			A->set_value(value, i, j);
		}

		static void SetVector(Vector& v, unsigned int i, double value){
			(*v)[i]=value;
		}

		static double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j){
			return A->get_value(i,j);
		}

		static void GetMatrixR(Vector& v, const Matrix& A, unsigned int i){
			for(unsigned int j=0; j<v->size(); j++){
				(*v)[j]=A->get_value(i,j);
			}
		}

		static void GetMatrixC(Vector& v, const Matrix& A, unsigned int j){
			for(unsigned int i=0; i<v->size(); i++){
				(*v)[i]=A->get_value(i,j);
			}
		}

		static double GetVectorE(const Vector& v, unsigned int i){
			return (*v)[i];
		}

		static void SolveTridiagonal(Tridiagonal& TMatrix, Vector& _eigenvalues, Matrix& _eigenvectors) {
			gsl_vector* eigenvalues=gsl_vector_calloc(_eigenvalues->size());
			gsl_matrix* eigenvectors = gsl_matrix_calloc(_eigenvalues->size(),_eigenvalues->size());


			gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
					TMatrix->size1);

			gsl_eigen_symmv(TMatrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);

	//Sorting
			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);

			for(unsigned int i=0; i<_eigenvalues->size(); i++){
				(*_eigenvalues)[i]=gsl_vector_get(eigenvalues,i);
				for(unsigned int j=0; j<_eigenvalues->size(); j++){
					double tmp=gsl_matrix_get(eigenvectors,i,j);
					if(fabs(tmp)>1e-15){
						_eigenvectors->set_value(tmp,i,j);
					}
				}
			}


			gsl_vector_free(eigenvalues);
			gsl_matrix_free(eigenvectors);
		}

		static void ClearMatrix(Matrix& A){
			A->Reset();
		}

};
#ifndef _QMT_NO_MKL
class SparseAlgebraMklTridiag{
public:
        
        struct TridiagonalSymmetricMatrix {
         unsigned int size;
         double *maindiagonal;
         double *subdiagonal;
         
         TridiagonalSymmetricMatrix():size(0) {
           maindiagonal = NULL;
           subdiagonal = NULL;
         }

         void InitializeMatrix(unsigned int n, double* main, double *sub) {
           size = n;
           maindiagonal = new double [n];
           subdiagonal =  new double [n-1];
           std::copy(main, main + n, maindiagonal);
           std::copy(sub + 1 , sub + n , subdiagonal);
         }
       
          void InitializeMatrix(std::vector<double>& main, std::vector<double>& sub){
           size = main.size();
           maindiagonal = new double [size];
           subdiagonal =  new double [size - 1];
           std::copy(main.begin(), main.end(), maindiagonal);
           std::copy(sub.begin()+1, sub.end(), subdiagonal);
         }
         
         void DeinitializeMatrix() {
          if(maindiagonal != NULL)
           delete [] maindiagonal;
          if(subdiagonal != NULL)
           delete [] subdiagonal; 
          maindiagonal = NULL;
          subdiagonal = NULL;
          size = 0;
         }
         
        TridiagonalSymmetricMatrix(unsigned int n, double* main, double *sub):size(n) {
           InitializeMatrix(n, main, sub);
         }

         TridiagonalSymmetricMatrix(std::vector<double>& main, std::vector<double>& sub):size(main.size()) {
           InitializeMatrix( main, sub);
         }
         
         

         ~TridiagonalSymmetricMatrix() {
          if(maindiagonal != NULL)
           delete [] maindiagonal;
          if(subdiagonal != NULL)
           delete [] subdiagonal;
        }          
      
        };	


	typedef qmt::QmtSparseMatrix<double,1>* Matrix;
	typedef std::vector<double>* Vector;
	typedef TridiagonalSymmetricMatrix Tridiagonal;

	/*
	 * 		(*) Matrix-Matrix Algebra:
	 * 				- void AddMM(Matrix& A, const Matrix& B, double alpha)
	 */

	static void AddMM(Matrix& A, const Matrix& B, double alpha=1.0){
		qmt::QmtSparseMatrix<double,1>::Add(*A,*B,alpha);
	}

	/*
	 * 		(*) Matrix-Vector Algebra:
	 *				- void MultiplyMV(const Matrix& A, const Vector& b, Vector& c)
	 *				- void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, double alpha, double beta)
	 */

	static void MultiplyMV(const Matrix& A, const Vector& b, Vector& c){
		 qmt::QmtSparseMatrix<double,1>::Multiply(*A,*b,*c,1.0,0.0);
	}

	static void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, double alpha, double beta){
		 qmt::QmtSparseMatrix<double,1>::Multiply(*A,*b,*c,alpha,beta);
	}
	/*
	 *		(*) Vector Algebra:
	 *				- void MultiplyVV(const Vector& a, const Vector& b, double* result)
	 *				- void CopyVV(const Vector& a, Vector& b)
	 *				- void CopyVV(const Vector& a, Vector& b, double alpha)
	 *				- void ScaleV(Vector& a, double alpha)
	 *				- void SwapVV(Vector& a, Vector& b)
	 *				- double NormV(const Vector& a)
	 *				- void NormalizeV(Vector& a)
	 */

	static void MultiplyVV(const Vector& a, const Vector& b, double* result){
		if(a->size() == b->size()){
			double scalar_product=0.0;

		for(unsigned int i=0; i<a->size(); i++){
			scalar_product += (*a)[i]*(*b)[i];
		}

		(*result) = scalar_product;
		}
		else{
			std::cerr<<"SparseAlgebra.h: MultiplyVV -> vectors are not the same size!"<<std::endl;
			(*result)=0.0;
		}
	}

	static void CopyVV(const Vector& a, Vector& b){
		if(a->size() == b->size()){

		for(unsigned int i=0; i<a->size(); i++){
			(*b)[i]=(*a)[i];
		}

		}
		else{
			std::cerr<<"SparseAlgebra.h: CopyVV -> vectors are not the same size!"<<std::endl;
		}
	}

	static void CopyVV(const Vector& a, Vector& b,double alpha){
		if(a->size() == b->size()){

		for(unsigned int i=0; i<a->size(); i++){
			(*b)[i]+=alpha*(*a)[i];
		}

		}
		else{
			std::cerr<<"SparseAlgebra.h: CopyVV -> vectors are not the same size!"<<std::endl;
		}
	}

	static void ScaleV(Vector& a, double alpha){
		for(auto iter=a->begin(); iter!=a->end(); iter++){
			(*iter)*=alpha;
		}
	}

	static void SwapVV(Vector& a, Vector& b){
		Vector tmp;
		tmp=a;
		a=b;
		b=tmp;
	}

	static double NormV(const Vector& a){
		double sq_norm;
		MultiplyVV(a,a,&sq_norm);
		return sqrt(sq_norm);
	}

	static void NormalizeV(Vector& a){
		double norm=NormV(a);
		ScaleV(a,pow(norm,-1));
	}

	/*		(*) (De)Inicialization:
	 *				- void InitializeMatrix(Matrix& A, unsigned int size1, unsigned int size2)
	 *				- void DeinitializeMatrix(Matrix& A)
	 *				- void InitializeVector(Vector& A, unsigned int size)
	 *				- void DeinitializeVector(Vector& A)
	 *				- void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double> Alphas,std::vector<double> Betas)
	 *				- void DeinitializeTridiagonal(Tridiagonal& A)
	 */

	static void InitializeMatrix(Matrix& M, unsigned int rows,
			unsigned int cols) {
		M = new qmt::QmtSparseMatrix<double,1>(rows, cols);
	}

	static void DeinitializeMatrix(Matrix& M) {
		delete M;
	}

	static void InitializeVector(Vector& v, unsigned int size) {
		v = new std::vector<double>(size, 0.0);
	}

	static void DeinitializeVector(Vector& v) {
		delete v;
	}

	static void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double>& Alphas,
			std::vector<double>& Betas) {
                Tridiagonal_matrix.DeinitializeMatrix();
		Tridiagonal_matrix.InitializeMatrix(Alphas, Betas);
	}

	static void DeinitializeTridiagonal(Tridiagonal& A) {
		A.DeinitializeMatrix();
	}

		/*
		 *		(*) Object Handling:
		 *				- void PrintMatrix(const Matrix& A)
		 *				- void PrintVector(const Vector& a)
		 *				- void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value)
		 *				- void SetVector(Vector& A, unsigned int i, double value)
		 *				- double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j)
		 *				- void GetMatrixC(Vector& v, const Matrix& A, unsigned int i)
		 *				- void GetMatrixR(Vector& v, const Matrix& A, unsigned int j)
		 *				- double GetVectorE(const Vector& A, unsigned int i)
		 *				- void SolveTridiagonal(Tridiagonal& TMatrix, Vector& eigenvalues, Matrix& eigenvectors)
		 *				- void ClearMatrix(Matrix& A)
		 */

		static void PrintMatrix(Matrix& A){
			std::cout<<*A<<std::endl;
		}

		static void PrintVector(Vector& v){
			for(auto iter=v->begin(); iter!=v->end(); iter++){
				std::cout<<*iter<<" ";
			}
			std::cout<<std::endl;
		}

		static void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value){
			A->set_value(value, i, j);
		}

		static void SetVector(Vector& v, unsigned int i, double value){
			(*v)[i]=value;
		}

		static double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j){
			return A->get_value(i,j);
		}

		static void GetMatrixR(Vector& v, const Matrix& A, unsigned int i){
			for(unsigned int j=0; j<v->size(); j++){
				(*v)[j]=A->get_value(i,j);
			}
		}

		static void GetMatrixC(Vector& v, const Matrix& A, unsigned int j){
			for(unsigned int i=0; i<v->size(); i++){
				(*v)[i]=A->get_value(i,j);
			}
		}

		static double GetVectorE(const Vector& v, unsigned int i){
			return (*v)[i];
		}

		static void SolveTridiagonal(Tridiagonal& TMatrix, Vector& _eigenvalues, Matrix& _eigenvectors) {
                      int tm_size = TMatrix.size;
		      double* z = new double [tm_size * tm_size ];
                       
                       LAPACKE_dstedc( LAPACK_ROW_MAJOR,'I', tm_size,  TMatrix.maindiagonal, TMatrix.subdiagonal,  z, tm_size);
                        std::copy(TMatrix.maindiagonal, TMatrix.maindiagonal + tm_size, _eigenvalues->begin());
			for(unsigned int i=0; i<_eigenvalues->size(); i++){
			         (*_eigenvalues)[i] = TMatrix.maindiagonal[i];
				for(unsigned int j=0; j<_eigenvalues->size(); j++){
					double tmp= z[ i * tm_size + j];
					if(fabs(tmp)>1e-15){
						_eigenvectors->set_value(tmp,i,j);
					}
				}
			}
                     delete [] z;
		}

		static void ClearMatrix(Matrix& A){
			A->Reset();
		}

};
#endif


} //end of namespace LocalAlgebras

#endif
