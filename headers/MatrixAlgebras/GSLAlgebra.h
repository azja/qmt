#ifndef ALGEBRAS_H
#define ALGEBRAS_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <vector>
#include <tuple>
#include <iostream>
#include <cmath>

namespace LocalAlgebras {

/*
 * gsl+cblas matrix algebra
 *
 *
 *
 *
 *
 */

class GSLalgebra {
public:
	typedef gsl_matrix* Matrix;
	typedef gsl_matrix* Tridiagonal;
	typedef gsl_vector* Vector;

	/*
	 * Matrix - Matrix Algebra
	 */

	static void AddMM(Matrix& A, const Matrix& B, double alpha=1.0){
		if(fabs(alpha)<1e-16) return;
		if(alpha!=1.0) gsl_matrix_scale(A,pow(alpha,-1));
		gsl_matrix_add(A,B);
		if(alpha!=1.0) gsl_matrix_scale(A,alpha);
	}

	/*
	 * Matrix - Vector Algebra
	 */

	// c = A*b
	static void MultiplyMV(const Matrix& A, const Vector& b, Vector& c) {
		gsl_blas_dgemv(CblasNoTrans, 1.0, A, b, 0.0, c);
	}

	// c = alpha* A*b + beta*c
	static void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, const double alpha, const double beta) {
		gsl_blas_dgemv(CblasNoTrans, alpha, A, b, beta, c);
	}

	/*
	 * Vector Algebra
	 */

	// a*b
	static void MultiplyVV(const Vector& a, const Vector& b, double* result) {
		gsl_blas_ddot(a, b, result);
	}

	// b= a
	static void CopyVV(const Vector& a, Vector& b) {
		gsl_blas_dcopy(a, b);
	}

	// b= alpha* a
	static void CopyVV(const Vector& a, Vector& b, double alpha) {
		gsl_blas_daxpy(alpha, a, b);
	}

	// a-> alpha*a
	static void ScaleV(Vector& a, double alpha) {
		gsl_vector_scale(a, alpha);
	}

	// a <-> b
	static void SwapVV(Vector& a, Vector& b) {
		gsl_blas_dswap(a, b);
	}

	// ||a||
	static double NormV(const Vector& a) {
		return gsl_blas_dnrm2(a);
	}

	// a -> a/||a||
	static void NormalizeV(Vector& a) {
		gsl_vector_scale(a, pow(gsl_blas_dnrm2(a), -1));
	}

	/*
	 * (De)Initialization of objects
	 */
	static void InitializeMatrix(Matrix& A, unsigned int size1, unsigned int size2) {
		A = gsl_matrix_calloc(size1, size2);
		gsl_matrix_set_zero(A);
	}

	static void DeinitializeMatrix(Matrix& A) {
		gsl_matrix_free(A);
	}

	static void InitializeVector(Vector& A, unsigned int size) {
		A = gsl_vector_calloc(size);
		gsl_vector_set_basis(A, 1);
	}

	static void DeinitializeVector(Vector& A) {
		gsl_vector_free(A);
	}

	static void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double> Alphas,
			std::vector<double> Betas) {
		Tridiagonal_matrix = gsl_matrix_calloc(size, size);
		gsl_matrix_set_zero(Tridiagonal_matrix);

		gsl_matrix_set(Tridiagonal_matrix, 0, 0, Alphas[0]);

		for (unsigned int i = 1; i < size; i++) {
			gsl_matrix_set(Tridiagonal_matrix, i, i, Alphas[i]);
			gsl_matrix_set(Tridiagonal_matrix, i - 1, i, Betas[i]);
			gsl_matrix_set(Tridiagonal_matrix, i, i - 1, Betas[i]);
		}

	}

	static void DeinitializeTridiagonal(Tridiagonal& A) {
		gsl_matrix_free(A);
	}
	/*
	 * Handling objects
	 */

	static void PrintMatrix(const Matrix& A) {
		for (unsigned int i = 0; i < A->size1; i++) {
			for (unsigned int j = 0; j < A->size2; j++) {
				std::cout << gsl_matrix_get(A, i, j) << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}

	static void PrintVector(const Vector& a) {
		for (unsigned int i = 0; i < a->size; i++) {
			std::cout << gsl_vector_get(a, i) << " ";
		}
		std::cout << std::endl;
	}
	static void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value) {
		gsl_matrix_set(A, i, j, value);
	}

	static void SetVector(Vector& A, unsigned int i, double value) {
		gsl_vector_set(A, i, value);
	}

	static double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j) {
		return gsl_matrix_get(A, i, j);
	}

	static void GetMatrixC(Vector& v, const Matrix& A, unsigned int i) {
		gsl_matrix_get_col(v, A, i);
	}

	static void GetMatrixR(Vector& v, const Matrix& A, unsigned int j) {
		gsl_matrix_get_row(v, A, j);
	}

	static double GetVectorE(const Vector& A, unsigned int i) {
		return gsl_vector_get(A, i);
	}

	static void SolveTridiagonal(Tridiagonal& TMatrix, Vector& eigenvalues, Matrix& eigenvectors) {
		gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
				TMatrix->size1);

		gsl_eigen_symmv(TMatrix, eigenvalues, eigenvectors, workspace);

		gsl_eigen_symmv_free(workspace);

//Sorting
		gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
	}

	static void ClearMatrix(Matrix& A){
		gsl_matrix_set_zero(A);
	}


};

} //end namespace LocalAlgebras

#endif
