#ifndef QMT_STRUCTURE_GENERATOR_H_INCLUDED
#define QMT_STRUCTURE_GENERATOR_H_INCLUDED

#include "qmtparsertools.h"
#include "qmtvector.h"
#include "qmtstate.h"
#include <string>
#include <vector>
#include <iostream>
#include <memory>

namespace qmt {

  namespace structure {
  
    std::string convertFromAtomicNumber(size_t n);
    
    std::string printTwoBodyTerm(size_t i, size_t j, size_t k, size_t l, double factor, size_t id, const qmt::QmtVector& v1, const qmt::QmtVector& v2, const qmt::QmtVector& v3, const qmt::QmtVector& v4, qmt::QmtSpin s1, qmt::QmtSpin s2);
    std::string printOneBodyTerm(size_t i, size_t j, size_t id, const qmt::QmtVector& v1,const qmt::QmtVector& v2, qmt::QmtSpin s);
    
  /*
   * XYZFormatable interface
   */
  
    struct XYZFormatable {
      virtual std::string toXYZ() const = 0;
      virtual void showXYZ() const = 0;
    };
  
    
  /*
   * QmtHamiltonianDefinitionProvider
   */
  
    
    struct QmtHamiltonianDefinitionProvider{
      virtual std::string get_hamiltonian() const = 0;
      virtual void show_hamiltonian() const = 0;
    };
 
    struct QmtSite: public XYZFormatable {
    size_t id_number;
    qmt::QmtVector position;    
   
    
    QmtSite() = default;
    QmtSite(const std::string& definition);
    
    std::string toXYZ() const;
    void showXYZ() const;
    
    void set_id_number(size_t);
    void set_coordinates(double x, double y, double z);
    void set_coordinates(const qmt::QmtVector &v);
    
    const qmt::QmtVector& get_position() const;
    size_t get_id_number() const;
    
    
  };
    
  /*
   * QmtBase
   */
  
  struct QmtBase: public XYZFormatable {
    std::vector<QmtSite> sites;

    
    std::string toXYZ() const;
    void showXYZ() const;
    
    size_t size() const;
    const qmt::structure::QmtSite& operator[](int i) const;
    const qmt::structure::QmtSite& get_site(int i) const;
    QmtBase() = default;
    QmtBase(std::string& definition);
    friend std::ostream& operator<<(std::ostream& stream, const QmtBase& base); 

    
    };
  
  /*
   * QmtBaseStructure
   */
    
    
  struct QmtBaseStructure: public XYZFormatable, public QmtHamiltonianDefinitionProvider {
    std::shared_ptr<QmtBase> base;
   
   std::string toXYZ() const;
   void showXYZ() const;
    
   QmtBaseStructure() = default;
   QmtBaseStructure(std::string& definition);
    
   std::string get_hamiltonian() const;
   void show_hamiltonian() const;
       
  };
   
/*  
  struct QmtPeriodicStructure: public XYZFormatable, public QmtHamiltonianDefinitionProvider {
    QmtBase base;
        
   bool periodicity1;
   bool periodicity2;    
   bool periodicity3;
   
   double cut_off_radius;
   
   qmt::QmtVector r1;
   qmt::QmtVector r2;
   qmt::QmtVector r3;
    
   
   std::string toXYZ() const;
   void showXYZ() const;
    
   QmtPeriodicStructure() = default;
   QmtPeriodicStructure(std::string& definition);
    
   std::string get_hamiltonian() const;
   void show_hamiltonian() const;
       
  };*/
  }
}
#endif