#ifndef QMT_WANNIER_PROVIDER_H_INCLUDED
#define QMT_WANNIER_PROVIDER_H_INCLUDED

#include <iostream>
#include <fstream>
#include "qmtparsertools.h"
#include <string>    


namespace qmt {

template <typename Creator>
//Creator must implement Creator(const std::string&  definition) constructor
class QmtWannierBasisProvider {

std::vector<Creator*> wannier_definitions;
std::vector<std::tuple<int,int,int>> equations;
std::vector<double> betas;
public:
	QmtWannierBasisProvider(const char* filename){
          std::ifstream input_file;
          //Open file
 	  input_file.open(filename);
          //Get its content
	  std::string   content( (std::istreambuf_iterator<char>(input_file) ),
                     (std::istreambuf_iterator<char>()    ) );
          //Parser for wanniers
   	  std::vector<std::string>  wannier_strings = qmt::parser::get_bracketed_words(content,'{', '}', true);
          //Generate definitions
	  for(const auto &wannier_str : wannier_strings)
		wannier_definitions.push_back(new Creator(wannier_str));
	  //Parser for equations
   	  std::vector<std::string>  equation_strings = qmt::parser::get_bracketed_words(content,'<', '>', true);
          //Generate equations 
	  for(const auto &eq_str : equation_strings){
            std::vector<std::string> values = qmt::parser::get_delimited_words(",",eq_str);
		equations.push_back(std::tuple<int,int,int>(std::stoi(values[0]),std::stoi(values[1]),std::stoi(values[2])));
           }

    	  std::vector<std::string>  beta_strings_p = qmt::parser::get_bracketed_words(content,"open_betas", "close_betas");
          std::vector<std::string>  beta_strings = qmt::parser::get_delimited_words(",",beta_strings_p[0]);
          //Generate equations 
	  for(const auto &beta_str : beta_strings){
		betas.push_back(std::stod(beta_str));
           }


        }

	std::vector<Creator*> get_definitions(){return wannier_definitions;}
        std::vector<std::tuple<int,int,int>> get_equations(){return equations; }
	std::vector<double> get_betas(){return betas;}

	~QmtWannierBasisProvider(){
         for(auto definition : wannier_definitions)
           delete definition;
        }

};
}

#endif //QMT_WANNIER_PROVIDER_H_INCLUDED
