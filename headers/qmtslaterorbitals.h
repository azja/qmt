#ifndef QMTSLATERORBITALS_H_
#define QMTSLATERORBITALS_H_

/*
 * qmtslaterorbitals.h
 *
 *  Created on: 22 kwi 2015
 *      Author: akadzielawa
 */

#include <iostream>
#include <vector>
#include "qmtorbitals.h"
#include "../gorb/gaussexp.h"
#include "qmtvector.h"


namespace qmt {

class SlaterOrbital {
public:
    virtual ~SlaterOrbital() {};

protected:
    SlaterOrbital() {}; //=default;
    SlaterOrbital& operator=( const SlaterOrbital& )=delete;

    typedef QmExpansion<Gauss, QmVariables::QmParallel::omp> g_omp;
    typedef qmt::QmtVector qmt_vector;

    std::vector<Gauss> gaussians;
    g_omp STO_nG;

public:

    static double overlap (const SlaterOrbital& O1,const SlaterOrbital& O2);
    static double overlap (const SlaterOrbital& O1,const SlaterOrbital& O2, const qmt_vector t1, const qmt_vector t2);
    static double attractive_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, double rx, double ry, double rz);
    static double attractive_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const qmt_vector t1, const qmt_vector t2, double rx, double ry, double rz);
    static double kinetic_integral (const SlaterOrbital& O1,const SlaterOrbital& O2);
    static double kinetic_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const qmt_vector t1, const qmt_vector t2);
    static double v_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const SlaterOrbital& O3,const SlaterOrbital& O4);
    static double v_integral (const SlaterOrbital& O1,const SlaterOrbital& O2, const SlaterOrbital& O3,const SlaterOrbital& O4, const qmt_vector t1, const qmt_vector t2, const qmt_vector t3, const qmt_vector t4);
    static double get_value(const  SlaterOrbital& O,const qmt_vector& position,const qmt_vector& translation = qmt::QmtVector(0,0,0));

};

/*
 * 1s orbital
 */
class SltOrb_1s : public SlaterOrbital {
public:
    SltOrb_1s(qmt::QmtVector r, double alpha, unsigned int Num_Gaussians);
    SltOrb_1s(qmt::QmtVector r, double alpha, std::vector<double> coefs, std::vector<double> gammas, unsigned int Num_Gaussians);
    SltOrb_1s(const qmt::SltOrb_1s& orbital);
};


/*
 * 2s orbital
 */
class SltOrb_2s : public SlaterOrbital {
public:
    SltOrb_2s(qmt::QmtVector r, double alpha);
    SltOrb_2s(const qmt::SltOrb_2s& orbital);
};


/*
 * 2pX orbital
 */
class SltOrb_2pX : public SlaterOrbital {
public:
    SltOrb_2pX(qmt::QmtVector r, double alpha, int phase = 1);
    SltOrb_2pX(const qmt::SltOrb_2pX& orbital);
};


/*
 * 2pY orbital
 */
class SltOrb_2pY : public SlaterOrbital {
public:
    SltOrb_2pY(qmt::QmtVector r, double alpha, int phase = 1);
    SltOrb_2pY(const qmt::SltOrb_2pY& orbital);
};


/*
 * 2pZ orbital
 */
class SltOrb_2pZ : public SlaterOrbital {
public:
    SltOrb_2pZ(qmt::QmtVector r, double alpha, int phase = 1);
    SltOrb_2pZ(const qmt::SltOrb_2pZ& orbital);
};


/*
 * 3s orbital
 */
class SltOrb_3s : public SlaterOrbital {
public:
    SltOrb_3s(qmt::QmtVector r, double alpha);
    SltOrb_3s(const qmt::SltOrb_3s& orbital);
};


/*
 * 3pX orbital
 */
class SltOrb_3pX : public SlaterOrbital {
public:
    SltOrb_3pX(qmt::QmtVector r, double alpha, int phase = 1);
    SltOrb_3pX(const qmt::SltOrb_3pX& orbital);
};


/*
 * 3pY orbital
 */
class SltOrb_3pY : public SlaterOrbital {
public:
    SltOrb_3pY(qmt::QmtVector r, double alpha, int phase = 1);
    SltOrb_3pY(const qmt::SltOrb_3pY& orbital);
};


/*
 * 3pZ orbital
 */
class SltOrb_3pZ : public SlaterOrbital {
public:
    SltOrb_3pZ(qmt::QmtVector r, double alpha, int phase = 1);
    SltOrb_3pZ(const qmt::SltOrb_3pZ& orbital);
};




} //end of namespace qmt

#endif
