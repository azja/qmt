#ifndef QMVARS_H_INCLUDED
#define QMVARS_H_INCLUDED

namespace QmVariables {

enum QmParallel {
    no,
    omp,
    mpi,
    gpu_cuda
};

enum class QmtSign {
    plus,
    minus
};

}

#endif // QMVARS_H_INCLUDED
