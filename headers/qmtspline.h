#ifndef QMT_SPLINE_H_INCLUDED
#define QMT_SPLINE_H_INCLUDED

#include <vector>
#include <algorithm>


#ifdef GSL_SUPPORT
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#endif 

namespace qmt {
class QmtSpline1d {

protected:
      QmtSpline1d(){};

public:
      virtual double get_value(double x) = 0;
      virtual ~QmtSpline1d() {}
};

#ifdef GSL_SUPPORT
class QmtSpline1dGsl : public QmtSpline1d {

gsl_interp_accel *acc;
gsl_spline *spline; 


public:  
    QmtSpline1dGsl(double *x, double *y, size_t size);
    QmtSpline1dGsl(const QmtSpline1dGsl&) = delete;
    double get_value(double x);
    ~QmtSpline1dGsl();

};

#endif

class QmtTableFunction1d {

std::vector<double> values;
std::vector<double> args;

static bool comparer(const double t1, const double t2);

public:
    QmtTableFunction1d(double *x, double *y, size_t size);
    QmtTableFunction1d(const QmtSpline1dGsl&) = delete;
    double get_value(double x);
  

};


} //end namespace qmt

#endif //QMT_SPLINE_H_INCLUDED
