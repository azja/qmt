#include "mpimin.h"


template <typename Functor,int mpi_type>
typename Functor::returned_value  qmtMpiBruteOptimizer(typename Functor::returned_value begin, typename Functor::returned_value end,Functor& functor) {

    typedef typename Functor::returned_value internal_type;

    int taskid;
    int n_workers;

    int converged = -1;

    MPI_Comm_size(MPI_COMM_WORLD,&n_workers);

    if( n_workers < 2 ) {
        std::cerr<<"qmtMpiBruteOptimizer:: Number of process is: "<<n_workers<<" hence it is less than 2...exiting "<<std::endl;
        return typename Functor::returned_value();
    }

    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);

    internal_type start = begin;
    internal_type finish = end;


    internal_type range = finish - start;
    internal_type alfa = range / n_workers;
    internal_type alfa_old = 0;
    internal_type alfa_k;
    internal_type data;
    internal_type min_value;
    internal_type min_value_old = 0;  //Due to initialization warning


    unsigned int winner;
    unsigned int cntr = 0;

    do {
        MPI_Barrier(MPI_COMM_WORLD);
        alfa_k = alfa * taskid + alfa/2 + start;
        data = functor(alfa_k);

        qmt_MPI_min<internal_type, mpi_type>(&data, &min_value, 1, 0, &winner, MPI_COMM_WORLD);

        MPI_Barrier(MPI_COMM_WORLD);

        MPI_Bcast( &winner, 1, MPI_INT,0, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);

        if(winner > 0 && winner < n_workers - 1 ) {
            start =  start + alfa * winner - 1.1 * alfa;
            finish  =  start + 2.1 * alfa;
        }

        if(winner == 0) {
            finish = start + 1.5 * alfa;
        }

        if( winner == n_workers - 1) {
            start = finish - 1.5 * alfa;
        }

        alfa_old = alfa;
        alfa = (finish - start) / n_workers;

        if(taskid == 0) {
            if(/*cntr > 0 && functor.stop(min_value, min_value_old)*/ functor.stop(alfa_old,alfa) ) {
                converged = 1;
            }
            cntr++;
            min_value_old = min_value;
        }

        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast( &converged, 1, MPI_INT,0, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    while(converged < 0 );

    return alfa * winner + alfa/2 + start;
}
