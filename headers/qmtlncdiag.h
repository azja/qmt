#ifndef QMT_LNC_DIAG_H
#define QMT_LNC_DIAG_H

//#include <mpi.h>
#include <iostream>
#include "qmtdiagonalizer.h"
#include "qmthubbard.h"
#include "qmthbuilder.h"
#include "qmtbasisgenerator.h"
#include "MatrixAlgebras/SparseAlgebra.h"
#include "qmtmatrixformula.h"
#include "qmtreallanczos.h"
#include "qmtparsertools.h"



namespace qmt {

  enum QmtLanczosOperators{
    AUp,
    CUp,
    ADo,
    CDo
  };
  
 class QmtLanczosDiagonalizer : public QmtDiagonalizer {
    typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 
    typedef qmt::QmtNState<qmt::uint, int> NState;                                             //Output - eigen state

    typedef qmt::SqOperator<NState> sq_operator;

    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<NState> states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula<qmt_hamiltonian,LocalAlgebras::SparseAlgebra> formula; //Tranlate Hamiltonian to matrix 
                                                                              
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;                //Eigen problem solver
    
    int _lanczos_steps;
    int _lanczos_eps;
    
    int problem_size;
    int number_of_centers;
    int electron_number;
    int alphas_number;

    sq_operator Op(QmtLanczosOperators o, unsigned int i);
 
  public:
  QmtLanczosDiagonalizer(const char* file_name,int option = 1);
  double Diagonalize(const std::vector<double>& one_body_integrals, const std::vector<double>& two_body_integrals);
  void NWaveFunctionEssentials(std::vector<double>& averages);
  double DoubleOccupancy();
  double AvCUpCUp(unsigned int i, unsigned int j);
  double AvCUpCDo(unsigned int i, unsigned int j);
  double AvCDoCDo(unsigned int i, unsigned int j);
  double AvCUpAUp(unsigned int i, unsigned int j);
  double AvCUpADo(unsigned int i, unsigned int j);
  double AvCDoAUp(unsigned int i, unsigned int j);
  double AvCDoADo(unsigned int i, unsigned int j);

  double Average_2(QmtLanczosOperators Op1, unsigned int s1, 
                   QmtLanczosOperators Op2, unsigned int s2);
  double Average_4(QmtLanczosOperators Op1, unsigned int s1,
                   QmtLanczosOperators Op2, unsigned int s2,
		   QmtLanczosOperators Op3, unsigned int s3,
		   QmtLanczosOperators Op4, unsigned int s4);
  double Average_6(QmtLanczosOperators Op1, unsigned int s1,
                   QmtLanczosOperators Op2, unsigned int s2,
		   QmtLanczosOperators Op3, unsigned int s3,
		   QmtLanczosOperators Op4, unsigned int s4,
		   QmtLanczosOperators Op5, unsigned int s5,
		   QmtLanczosOperators Op6, unsigned int s6);
  double Average_8(QmtLanczosOperators Op1, unsigned int s1,
                   QmtLanczosOperators Op2, unsigned int s2,
		   QmtLanczosOperators Op3, unsigned int s3,
		   QmtLanczosOperators Op4, unsigned int s4,
		   QmtLanczosOperators Op5, unsigned int s5,
		   QmtLanczosOperators Op6, unsigned int s6,
		   QmtLanczosOperators Op7, unsigned int s7,
		   QmtLanczosOperators Op8, unsigned int s8);
  int size(){
  	return number_of_centers;
  }
  double DimerDimerAvs(size_t i, size_t j);
  virtual ~QmtLanczosDiagonalizer();
}; 
  
  
}

#endif
