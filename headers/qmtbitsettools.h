#ifndef QMTBITSETTOOLS_H
#define QMTBITSETTOOLS_H

#include <bitset>


namespace qmt{
namespace bitsettools{

template<std::size_t N>
bool gt(const std::bitset<N>& x, const std::bitset<N>& y)
	{
    	for (int i = N-1; i >= 0; i--) {
       		if (x[i] ^ y[i]) return x[i];
    		}
    	return false;
	}

template<std::size_t N>
bool lt(const std::bitset<N>& x, const std::bitset<N>& y)
	{
    	for (int i = N-1; i >= 0; i--) {
       		if (x[i] ^ y[i]) return y[i];
    		}
    	return false;
	}

template<std::size_t N>
bool ge(const std::bitset<N>& x, const std::bitset<N>& y)
	{
    	for (int i = N-1; i >= 0; i--) {
       		if (x[i] ^ y[i]) return x[i];
    		}
    	return true;
	}

template<std::size_t N>
bool le(const std::bitset<N>& x, const std::bitset<N>& y)
	{
    	for (int i = N-1; i >= 0; i--) {
       		if (x[i] ^ y[i]) return y[i];
    		}
    	return true;
	}

template<std::size_t N>
bool eq(const std::bitset<N>& x, const std::bitset<N>& y){
	return (x ^ y).none();
	}

template <size_t N>
void increment (std::bitset<N>& in ) { // from http://stackoverflow.com/questions/16761472/how-can-i-increment-stdbitset
	//  add 1 to each value, and if it was 1 already, carry the 1 to the next.
    	for ( size_t i = 0; i < N; ++i ) {
        	if ( in[i] == 0 ) {  // There will be no carry
        	    in[i] = 1;
        	    break;
        	    }
       	 in[i] = 0;  // This entry was 1; set to zero and carry the 1
       	 }
    	}



} // end of namespace bitsettools
} // end of namespace qmt
#endif
