#ifndef QMT_SYSTEM_STANDARD_H
#define QMT_SYSTEM_STANDARD_H



#include "qmtsystem.h"
#include "qmtwanniercreator.h"
#include "qmtslaterorbitals.h"
#include "qmtmicroscopic.h"
#include "qmtmicroscopicmove.h"
#include "qmtmicrotrans.h"
#include "qmthubbard.h"

namespace qmt {

  class QmtSystemStandard : public qmt::QmtSystem {
   
   typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 
   typedef  qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> qmt_standard_system;
   typedef  qmt::QmtMicroscopicMove<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> qmt_standard_system_move;
   typedef  qmt::QmtMicroscopicTransInvariant<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> qmt_invariant_system;
   qmt_standard_system *microscopic;
   
  public:
    QmtSystemStandard(const char* file);
    QmtSystemStandard(const qmt::QmtSystemStandard& system) = delete;
   
   /*
    * qmt::QmtSystem interface
    */
    
   int  get_one_body_number();
   int  get_two_body_number();
    
   double  get_one_body_integral(size_t index);
   double  get_two_body_integral(size_t index);
   
    
   void set_parameters(std::vector<double> params,const qmt::QmtVector& scale);
   double get_overlap(int i, int j,const qmt::QmtVector& first, const qmt::QmtVector& second);
   double get_wfs_products_sum(const qmt::QmtVector& r,const std::vector<double>& averages, const std::vector<int>& index_map) const;
   double get_wfs_value(size_t index,const qmt::QmtVector& r) const;  
   virtual ~QmtSystemStandard();
   
   qmt_standard_system* get_microscopic();
  };

  }
  
#endif //
