#ifndef QMT_WANNIER_BASIS_H_INCLUDED
#define QMT_WANNIER_BASIS_H_INCLUDED

#include "qmtwanniercreator.h"
#include "qmtvector.h"
#include "qmtbform.h"
#include "qmteqsolver.h"
#include "qmtlowdin.h"
#include <vector>
#include <algorithm>    // std::sort
#include <tuple>
#include <stdexcept> // std::logic_error()


namespace qmt {

template<typename WannierCreator>
/*
 WannierCreator must contain:
	(*) public typedef Wannier
	(*) public function Create(double)
	(*) public function Create(std::vector<double>)

 Wannier must contain:
	(*) public function get_id()
	(*) overlap(Wannier w1,Wannier w2,qmt::QmtVector t1=qmt::QmtVector(0,0,0),
					  qmt::QmtVector t2=qmt::QmtVector(0,0,0))
	(*) kinetic_integral(Wannier w1,Wannier w2,qmt::QmtVector t1=qmt::QmtVector(0,0,0),
						   qmt::QmtVector t2=qmt::QmtVector(0,0,0))
	(*) attractive_integral(Wannier w1,Wannier w2,
		qmt::QmtVector t1=qmt::QmtVector(0,0,0),qmt::QmtVector t2=qmt::QmtVector(0,0,0),
		double r_x, double r_y, double r_z)
	(*) v_integral(Wannier w1,Wannier w2, Wannier w3,Wannier w4,
			qmt::QmtVector t1=qmt::QmtVector(0,0,0),qmt::QmtVector t2=qmt::QmtVector(0,0,0),
			qmt::QmtVector t3=qmt::QmtVector(0,0,0),qmt::QmtVector t4=qmt::QmtVector(0,0,0))
        (*) get_value(Wannier w,qmt::QmtVector position)
*/
class QmtWannierBasis {

private:
    std::vector<typename WannierCreator::Wannier *> wanniers;
    unsigned int num_of_betas;
    std::vector<BilinearForm> forms;
    static bool compare_wanniers(const typename WannierCreator::Wannier* w1, const typename WannierCreator::Wannier* w2) {
        return ( w1->get_id() < w2->get_id() );
    }


public:
    QmtWannierBasis(const std::vector<WannierCreator*>& creators, std::vector< std::tuple<int,int,int> > equations, 
		    std::vector<double> starting_betas, double param, const qmt::QmtVector& scale=qmt::QmtVector(1,1,1)) {
	num_of_betas=0;

	// unpacking creators
        for(const auto& iter : creators) {
	    unsigned int tmp_num_of_betas=iter->get_max_beta_index();
	    if (tmp_num_of_betas > num_of_betas)
		num_of_betas=tmp_num_of_betas;

            typename WannierCreator::Wannier* new_wannier;
            new_wannier = iter->Create(param,scale);
            wanniers.push_back(new_wannier);
        }

	num_of_betas+=1; // because one indexes starting from 0

	if(num_of_betas!=starting_betas.size())
		throw std::logic_error("qmt::QmtWannierBasis: The number of starting values is different from the number of orthogonalization coefficents \'beta_i\'");

	if(num_of_betas!=equations.size())
		throw std::logic_error("qmt::QmtWannierBasis: The number of mixing coefficents \'beta_i\' is different from the number of equations");

	//one requires wanniers in vector "wanniers" to be sorted
        std::sort(wanniers.begin(), wanniers.end(), compare_wanniers);

	// orthogonalization
	double** matrix = new double*[num_of_betas];
	for(unsigned int i=0; i<num_of_betas; i++)
		matrix[i] = new double[num_of_betas];	
	double *betas=new double[num_of_betas];	
	double *input=new double[num_of_betas];
	for(unsigned int i=0; i<num_of_betas; i++){
		input[i]=starting_betas[i];
	}


	forms.clear();
	for(const auto& equation : equations){
	WannierCreator::Wannier::multiply(*(wanniers[std::get<0>(equation)]),*(wanniers[std::get<1>(equation)]),matrix,num_of_betas);

	if(std::get<2>(equation))
		forms.push_back(BilinearForm(matrix,num_of_betas,-std::get<2>(equation)));
	else
		forms.push_back(BilinearForm(matrix,num_of_betas));
	}


	NonLinearSystemSolve(forms, input, betas, 1.0e-12);


#ifdef _QMT_DEBUG
	for(int i=0; i<num_of_betas; i++)
		std::cout<<"b["<<i<<"]= "<<betas[i]<<" ";
   	std::cout<<std::endl;
#endif

	// setting betas
        for(auto& iter : wanniers) {
		for(unsigned int i=0; i<num_of_betas; i++){
            		iter-> set_c_by_id(i,betas[i]);
            	}
        }
	
	for (unsigned int i = 0; i < num_of_betas; i++)
    		delete[] matrix[i];
	delete[] matrix;

	delete[] betas;
	delete[] input;

    }

    QmtWannierBasis(const std::vector<WannierCreator*>& creators, std::vector< std::tuple<int,int,int> > equations, std::vector<double> starting_betas,const std::vector<double>& params, const qmt::QmtVector& scale=qmt::QmtVector(1,1,1)) {
	num_of_betas=0;

        for(const auto& iter : creators) {
	    int tmp_num_of_betas=iter->get_max_beta_index();
	    if (tmp_num_of_betas > num_of_betas)
		num_of_betas=tmp_num_of_betas;

            typename WannierCreator::Wannier* new_wannier;
            new_wannier = iter->Create(params,scale);
            wanniers.push_back(new_wannier);
        }

	num_of_betas+=1; // because one indexes starting from 0

	if(num_of_betas!=starting_betas.size())
		throw std::logic_error("qmt::QmtWannierBasis: The number of starting values is different from the number of orthogonalization coefficents \'beta_i\'");

	if(num_of_betas!=equations.size())
		throw std::logic_error("qmt::QmtWannierBasis: The number of mixing coefficents \'beta_i\' is different from the number of equations");

        std::sort(wanniers.begin(), wanniers.end(), compare_wanniers);

	// orthogonalization
	double** matrix = new double*[num_of_betas];
	for(int i=0; i<num_of_betas; i++)
		matrix[i] = new double[num_of_betas];	
	double *betas=new double[num_of_betas];
	double *input=new double[num_of_betas];
	for(int i=0; i<num_of_betas; i++){
		input[i]=starting_betas[i];
	}


	forms.clear();
#ifndef _LOWDIN_ORTHO //TEMPORARY!!!!!!!!!!!!
	for(const auto& equation : equations){ 
	WannierCreator::Wannier::multiply(*(wanniers[std::get<0>(equation)]),*(wanniers[std::get<1>(equation)]),matrix,num_of_betas);

	if(std::get<2>(equation))
		forms.push_back(BilinearForm(matrix,num_of_betas,-std::get<2>(equation)));
	else
		forms.push_back(BilinearForm(matrix,num_of_betas));
	}


	NonLinearSystemSolve(forms, input, betas, 1.0e-12);
#endif 
#ifdef _LOWDIN_ORTHO
	WannierCreator::Wannier::multiply(*(wanniers[std::get<0>(equations[0])]),*(wanniers[std::get<1>(equations[0])]),matrix,num_of_betas);
	qmt::qmtLowdinOrthogonalization(matrix,num_of_betas);
#ifdef _QMT_DEBUG
	std::cout<<"################################################################################################################################"<<std::endl;
//	for(int i=0; i<num_of_betas; i++){
//	    for(int j=0; j<num_of_betas; j++)
//		std::cout<<matrix[i][j]<<" ";
//	    std::cout<<std::endl;
//	}
//	std::cout<<"################################################################################################################################"<<std::endl;
#endif
#endif
//double s = matrix[0][0];
//	betas[0] = 1;//(sqrt(1-s)+sqrt(1+s))/(2*sqrt(1-s*s));
//	betas[1] = 0;//(sqrt(1-s)-sqrt(1+s))/(2*sqrt(1-s*s));
//std::cout<<"OVER = "<<s<<std::endl;

#ifdef _QMT_DEBUG
#ifndef _LOWDIN_ORTHO	
	for(int i=0; i<num_of_betas; i++)
		std::cout<<"b["<<i<<"]= "<<betas[i]<<" ";
	
   	std::cout<<std::endl;
#endif
#ifdef _LOWDIN_ORTHO	
    int lcntr = 0;
    for(auto& iter : wanniers) {
        std::cout<<"WANNIER#"<<lcntr;
	for(int i=0; i<num_of_betas; i++)
		std::cout<<" "<<matrix[lcntr][i];
	lcntr++;
   	std::cout<<std::endl;
   	}
#endif
#endif
	// setting betas
#ifdef _LOWDIN_ORTHO	
	int cntr = 0;
#endif
        for(auto& iter : wanniers) {
        
		for(int i=0; i<num_of_betas; i++){
#ifdef _LOWDIN_ORTHO
		  iter-> set_c_by_id(i,matrix[cntr][i]);
#else 
		   iter-> set_c_by_id(i,betas[i]);
#endif
            	}
#ifdef _LOWDIN_ORTHO
            	cntr++;
#endif
        }

#ifdef _QMT_DEBUG

        int cntr1 = 0;
	int cntr2 = 0;
	for(const auto& w1 : wanniers){
	   
	  for(const auto& w2 : wanniers){
	     std::cout<<"<w"<<cntr1<<"|w"<<cntr2<<"> = "<<WannierCreator::Wannier::overlap(*w1, *w2);
	     cntr2++;
	  }
	  cntr1++;
	  cntr2 = 0;
	}
	
   	std::cout<<std::endl;
#endif
        
	for (int i = 0; i < num_of_betas; i++)
    		delete[] matrix[i];
	delete[] matrix;

	delete[] betas;
	delete[] input;
    }

    ~QmtWannierBasis() {
        for(auto& iter : wanniers) {
            delete iter;
        }


    }


public:
    double overlap(const unsigned int id1, const unsigned int id2, const qmt::QmtVector t1=qmt::QmtVector(0,0,0), const qmt::QmtVector t2=qmt::QmtVector(0,0,0)) {
        return WannierCreator::Wannier::overlap(*(wanniers[id1]), *(wanniers[id2]), t1-wanniers[id1]->get_position(), t2-wanniers[id2]->get_position());
    }

    double kinetic_integral(const unsigned int id1, const unsigned int id2, const qmt::QmtVector t1=qmt::QmtVector(0,0,0), const qmt::QmtVector t2=qmt::QmtVector(0,0,0)) {
        return WannierCreator::Wannier::kinetic_integral(*wanniers[id1], *wanniers[id2], t1-wanniers[id1]->get_position(), t2-wanniers[id2]->get_position());
    }

    double attractive_integral(const unsigned int id1, const unsigned int id2, const qmt::QmtVector r, const qmt::QmtVector t1=qmt::QmtVector(0,0,0), const qmt::QmtVector t2=qmt::QmtVector(0,0,0)) {
        return WannierCreator::Wannier::attractive_integral(*wanniers[id1], *wanniers[id2], t1-wanniers[id1]->get_position(), t2-wanniers[id2]->get_position(), r.get_x(), r.get_y(), r.get_z());
    }

    double v_integral(const unsigned int id1, const unsigned int id2, const unsigned int id3, const unsigned int id4, qmt::QmtVector t1=qmt::QmtVector(0,0,0),qmt::QmtVector t2=qmt::QmtVector(0,0,0), qmt::QmtVector t3=qmt::QmtVector(0,0,0),qmt::QmtVector t4=qmt::QmtVector(0,0,0)) {
        return WannierCreator::Wannier::v_integral(*wanniers[id1], *wanniers[id2], *wanniers[id3], *wanniers[id4], t1-wanniers[id1]->get_position(), t2-wanniers[id2]->get_position(), t3-wanniers[id3]->get_position(), t4-wanniers[id4]->get_position());
    }
    
    double get_value(const unsigned int id,const qmt::QmtVector& v,const qmt::QmtVector& translation = qmt::QmtVector(0,0,0)) {
      return WannierCreator::Wannier::get_value(*wanniers[id],v,translation);
   }

   qmt::QmtVector get_position(int id) {
      return wanniers[id]->get_position();
   }

}; // end of class QmtWannierBasis

}// end of namespace qmt

#endif
