#ifndef QMT_MICRO_TRANS_H
#define QMT_MICRO_TRANS_H

#include "qmtmicroscopic.h"

namespace qmt {

template <class Creator, class Hamiltonian>
class QmtMicroscopicTransInvariant : public QmtMicroscopic<Creator,Hamiltonian> {
using QmtMicroscopic<Creator,Hamiltonian>::local_supercell;
using QmtMicroscopic<Creator,Hamiltonian>::local_one_body_intgs;
using QmtMicroscopic<Creator,Hamiltonian>::wannier_basis;
using QmtMicroscopic<Creator,Hamiltonian>::atomic_numbers;
  qmt::QmtVector cut_off;
  qmt::QmtVector periodic1; 
  qmt::QmtVector periodic2; 
  qmt::QmtVector periodic3; 
  
  qmt::QmtVector periodic1_orig; 
  qmt::QmtVector periodic2_orig; 
  qmt::QmtVector periodic3_orig; 
  
static double eps;
  struct vcr_cmpr;
  std::set<qmt::QmtVectorizedAtom,vcr_cmpr> sphere;
  
  

  struct vcr_cmpr{
    bool operator()(const qmt::QmtVectorizedAtom& c1,const qmt::QmtVectorizedAtom& c2){
      if(fabs(c1.position.get_x() - c2.position.get_x()) <= eps){
	if(fabs(c1.position.get_y() - c2.position.get_y())<= eps) {
	  if(fabs(c1.position.get_z() - c2.position.get_z())<= eps) {
	    return c1.id < c2.id; 
	  }
	  else return c1.position.get_z() < c2.position.get_z();
	}
	else return c1.position.get_y() < c2.position.get_y();
      }
      else return c1.position.get_x() < c2.position.get_x();
	
      return false;
      
    }
  };
  
  
void update_sphere() {
      
	sphere.clear();
	double a = qmt::QmtVector::norm(periodic1);
	double b = qmt::QmtVector::norm(periodic2);
	double c = qmt::QmtVector::norm(periodic3);
//			std::cout<<"a = "<<a<< " b = "<<b<<" c = "<<c<<std::endl;

	int dir1 = 0;
	int dir2 = 0; 
	int dir3 = 0; 

	if(a < eps) dir1 = 0;
	else
	  dir1 = 5*ceil(cut_off.get_x()/a); 
	
	if(b < eps) dir2 = 0;
	else
	  dir2 = 5*ceil(cut_off.get_x()/b); 
	
	if(c < eps) dir3 = 0;
	else
	  dir3 = 5*ceil(cut_off.get_x()/c); 


	
	for(int i = -dir1 ; i <=dir1; ++i) {
	  for(int j = -dir2 ; j <=dir2; ++j){
	      for(int k = -dir3 ; k <=dir3; ++k){
//		std::cout<<" i = "<<i<<" j = "<<j<<" k = "<< k<<std::endl;
//		if( 0 == i && 0 == j && 0 == k) continue;
	  std::vector<qmt::QmtVectorizedAtom> supercell_copy(local_supercell.size());
	  std::copy(local_supercell.begin(),local_supercell.end(),supercell_copy.begin());	    
	  std::transform(supercell_copy.begin(),supercell_copy.end(),supercell_copy.begin(),
			[&](qmt::QmtVectorizedAtom& qa)->
			qmt::QmtVectorizedAtom&{qa.position = qa.position +  i * periodic1 + j  * periodic2 + k  * periodic3; return qa;});
	
//	  for(const auto& original : local_supercell) {
	    for(const auto& cp : supercell_copy){
//	      std::cout<<"orig_p:"<<original.position<<" cp_p:"<<cp.position<<" distance"<<qmt::QmtVector::norm(original.position - cp.position)<<" cut_off = "<<cut_off.get_x()<<std::endl;
//	      if(qmt::QmtVector::norm(original.position - cp.position) < cut_off.get_x()){
		sphere.insert(cp);
 	  //    std::cout<<"He "<<cp.position<<" cut off = "<<cut_off.get_x()<<" orig position = "<<original.position<<std::endl;
//                }
              }
//	    }
	  }	  
	  }
	}
	
	//for(const auto &i : sphere)
	//  std::cout<<i<<std::endl;
//	exit(0);
	}
		      

  
  
public:
  QmtMicroscopicTransInvariant<Creator,Hamiltonian>(const std::string& hamiltonian_fn, const std::string& basis_fn, 
		      const std::string& supercell_fn,const std::string& geometry_fn,const std::vector<double> &params):
		      QmtMicroscopic<Creator,Hamiltonian>(hamiltonian_fn,basis_fn,supercell_fn,supercell_fn,params)
		      {
	
			std::vector<qmt::QmtVector> tmp = qmt::parser::get_QmtVector_from_file(geometry_fn); // vectors given as follows:
			 
			periodic1 = tmp[0];
			periodic2 = tmp[1];
			periodic3 = tmp[2];
			
			periodic1_orig = tmp[0];
			periodic2_orig = tmp[1];
			periodic3_orig = tmp[2];
			
			
			
			cut_off = tmp[3];
			
			  update_sphere();
			  
		      }
		      
		      
  void set_parameters(const std::vector<double>& params, const  qmt::QmtVector& v = qmt::QmtVector(1,1,1)) {
    QmtMicroscopic<Creator,Hamiltonian>::set_parameters(params,v);
    periodic1 = v.get_x() * periodic1_orig; 
    periodic2 = v.get_y() * periodic2_orig; 
    periodic3 = v.get_z() * periodic3_orig;   
    update_sphere();
    
  }
		      
  void set_parameters(double param, const  qmt::QmtVector& v = qmt::QmtVector(1,1,1)) {
    QmtMicroscopic<Creator,Hamiltonian>::set_parameters(param,v);
    periodic1 = v.get_x() * periodic1_orig; 
    periodic2 = v.get_y() * periodic2_orig; 
    periodic3 = v.get_z() * periodic3_orig; 
    update_sphere();
  }
  
  qmt::QmtVector apply_geometry(qmt::QmtVector trans) const{
  qmt::QmtVector result(0,0,0);

  double p1 = qmt::QmtVector::norm(periodic1_orig);
  qmt::QmtVector v1 = periodic1_orig;
  if (p1>1e-16)  v1/=p1;
  double p2 = qmt::QmtVector::norm(periodic2_orig);
  qmt::QmtVector v2 = periodic2_orig;
  if (p2>1e-16)  v2/=p2;
  double p3 = qmt::QmtVector::norm(periodic3_orig);
  qmt::QmtVector v3 = v1^v2;
  double p3p = qmt::QmtVector::norm(v3);
  if (p3p>1e-16) v3/=p3p;
  if (p2>1e-16)  v2 = v1^v3;
  
  
  
  if (p1>1e-16)
    result += (trans*v1)*v1;
  if (p2>1e-16)
    result += (trans*v2)*v2;
  if (p3>1e-16)
    result += (trans*v3)*v3;


    
//    result = v1+v2+v3;
//    std::cout<<trans<<" "<<result<<" "<<v1<<" "<<v2<<" "<<v3<<std::endl;
    
    return result;
  }

  double get_one_body(size_t index) const {
          
		if (index >= local_one_body_intgs.size()) {
		std::cerr
				<< "MicroscopicH2chain::get_one_body: index exceeding the number of one-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 2 > (local_one_body_intgs[index])) {
		std::cerr
				<< "MicroscopicH2chain::get_one_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

	qmt::QmtVector trans1 =
			(std::get < 0 > (local_one_body_intgs[index])).position;
					
	qmt::QmtVector trans2 =
			(std::get < 1 > (local_one_body_intgs[index])).position;
					


//	std::cout<<index<<" "<<trans1<<" "<<trans2<<std::endl;
	 double result = 0;

//	 result += wannier_basis->kinetic_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id ,trans1,trans2);
	 if(qmt::QmtVector::norm(trans1 - trans2) < eps){
	 	 result += wannier_basis->kinetic_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id);

	 for(auto iter=sphere.begin(); iter!=sphere.end(); iter++) {
	         auto neigbor_position = iter->position;
	         if(qmt::QmtVector::norm(trans1 - neigbor_position /*- qmt::QmtVector(0, trans1.get_y() - neigbor_position.get_y() ,trans1.get_z() - neigbor_position.get_z())*/ ) < cut_off.get_x()){
		 result +=  atomic_numbers[(iter->id)]*(wannier_basis->attractive_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id, iter->position - trans1));
// std::cerr<<"Ar "<<iter->position<<" "<<trans1<<" "<<trans2<<std::endl;
		}
	 }
//	std::cout<<"Eps = "<<result; exit(0);
	 }
	 else
	 {
	 	 result += wannier_basis->kinetic_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id ,trans1,trans2);
	   std::set<qmt::QmtVectorizedAtom,vcr_cmpr> twined_sphere;
	   
	    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	    
/*	    for(auto iter=sphere.begin(); iter!=sphere.end(); ++iter) {
	    auto neigbor_position = iter->position ;
	     if(qmt::QmtVector::norm(neigbor_position) < cut_off.get_x()){
	      
	      twined_sphere.insert(QmtVectorizedAtom(iter->position + apply_geometry(trans1), iter->id));
	      twined_sphere.insert(QmtVectorizedAtom(iter->position + apply_geometry(trans2), iter->id));
	      
	     }
	    } */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	    






////////////////////////NEW VERSION///////////////////////////////////////////////////////////////////////////////////////////
auto vectorized1 = std::get < 0 > (local_one_body_intgs[index]);
					
auto vectorized2 = std::get < 1 > (local_one_body_intgs[index]);

for(auto iter=sphere.begin(); iter!=sphere.end(); ++iter) {
	    auto neigbor_position = iter->position ;
	     if(qmt::QmtVector::norm(neigbor_position -vectorized1.position) < cut_off.get_x())
	      
	      twined_sphere.insert(QmtVectorizedAtom(iter->position, iter->id));
	     if(qmt::QmtVector::norm(neigbor_position -vectorized2.position) < cut_off.get_x())
	      twined_sphere.insert(QmtVectorizedAtom(iter->position, iter->id));
	      
//	     	        std::cerr<<"Ar "<<iter->position<<std::endl;
	    } 
//std::cout<<vectorized1<<" "<<vectorized2<<std::endl;
////////////////////////END OF NEW VERSION///////////////////////////////////////////////////////////////////////////////////////////
	    
	    for(auto iter=twined_sphere.begin();iter!=twined_sphere.end();++iter){
//	       auto neigbor_position = iter->position;
	        result +=  atomic_numbers[(iter->id)]*(wannier_basis->attractive_integral(std::get < 0 > (local_one_body_intgs[index]).id ,std::get < 1 > (local_one_body_intgs[index]).id, iter->position, trans1 ,trans2));
//		if(index==0)
//	        std::cerr<<"Ar "<<iter->position<<" "<<trans1<<" "<<trans2<<std::endl;
	    }
//	    	exit(0);
	}
//	   std::cout<<"id#"<<index<<" value = "<<result<<std::endl;
         return result;
        }

        double get_coulomb_repulsion() const {
        	double CoulombEnergy = 0.0;
		for (auto i = local_supercell.begin(); i != local_supercell.end(); i++) {
			for (auto j = sphere.begin(); j != sphere.end(); j++) {
				double distance = qmt::QmtVector::norm(i->position - j->position);
//				double dist_XY = qmt::QmtVector::norm(i->position - j->position  - qmt::QmtVector(0,0,i->position.get_z()) + qmt::QmtVector(0,0,j->position.get_z()));
				double dist_X = fabs(i->position.get_x() - j->position.get_x());
				#ifdef _QMT_DEBUG
				if ( i == local_supercell.begin() && j == sphere.begin() )
				std::cout<<"He	"<<i->position<<std::endl;
				#endif
				if (distance > 1e-5 && /*distance*/ dist_X < cut_off.get_x()) {
				#ifdef _QMT_DEBUG
					if( i == local_supercell.begin()){
					std::cout<<"H	"<<j->position<<" dis_X = "<<dist_X<<std::endl;
					}
				#endif
					CoulombEnergy += 2.0 / distance;
			}
		}
	}
	 return CoulombEnergy / 2;
	}
  
  
  
};

template <class Creator, class Hamiltonian>
  double QmtMicroscopicTransInvariant<Creator,Hamiltonian> :: eps = 1.0e-9;
}


#endif //end of qmtmicrotrans.h
