#ifndef QMT_ENGINE_DATA_H
#define QMT_ENGINE_DATA_H

#include "qmtsystem.h"
#include "qmtdiagonalizer.h"

namespace qmt {

struct QmtEngineData {
  qmt::QmtSystem* microscopic;
  qmt::QmtDiagonalizer* diagonalizer;
};

}
#endif