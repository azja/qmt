#include <iostream>
#include <ostream>
#include <vector>
#include <numeric>
#include "qmtstate.h"


class QmtStatesGenerator {


    int  safe_sum(std::vector<int>& nums) {

//0 - no electron
//1 - spin up
//2 - spin down
//3 double occupied


        int result = 0;
        int item = 0;
        for(unsigned int i = 0; i < nums.size(); ++i) {
            item = nums[i];
            if(item == 2) item = 1;
            if(item == 3) item = 2;
            result += item;
        }
        return result;
    }
    std::vector<qmt::QmtNState<qmt::QmtOrbitalState> > states;
    std::vector<int> num;

    void push_state( std::vector<int>& state) {
        qmt::QmtNState<qmt::QmtOrbitalState> n_state(state.size());

        for(unsigned int i = 0; i < state.size(); ++i) {

            if(state[i] == 0 )
                n_state[i] = qmt::QmtOrbitalState::zero_state();
            if(state[i] == 1 )
                n_state[i] = qmt::QmtOrbitalState::up_state();
            if(state[i] == 2 )
                n_state[i] = qmt::QmtOrbitalState::down_state();
            if(state[i] == 3 )
                n_state[i] = qmt::QmtOrbitalState::double_state();
        }

        states.push_back(n_state);

    }


    void generator(int k, int N,int cond, int index = 0) {

        if(index == N ) {
            if( safe_sum(num) == cond)
                push_state(num);
            return;
        }
        for(int i = 0; i < k; ++i) {

            num[index] = i;
            generator(k, N, cond, index + 1);
        }

    }

public:
    typedef std::vector<qmt::QmtNState<qmt::QmtOrbitalState> > qmtNState;

    void generate(int size,int num_el, qmtNState& output) {
        num.clear();
        num.resize(size);
        num.reserve(size);
        generator(4, size, num_el);
        output.clear();
        for(unsigned int i = 0; i < states.size(); ++i)
            output.push_back(states[i]);
    }



};
