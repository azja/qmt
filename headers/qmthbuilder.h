/*
 * Provides Fermionic Hamiltonian in a second quantization language building from the script file
 */

#ifndef QMTHBUILDER_H
#define QMTHBUILDER_H


#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <sstream>
#include <tuple>
#include <stdexcept>
#include <cctype>
#include <set>
#include <algorithm>
#include "qmtstate.h"
#include "qmvars.h"
#include "qmtvector.h"

namespace qmt {


struct QmtCoordinatedAtom {
    double i;
    double j;
    double k;

    int id;

    friend std::ostream& operator<<(std::ostream& stream,const qmt::QmtCoordinatedAtom& atom) {
        stream<<"["<<atom.i<<", "<<atom.j<<" ,"<<atom.k<<"]"<<" ("<<atom.id<<")";
        return stream;
    }

};

struct QmtVectorizedAtom {
    qmt::QmtVector position;
    int id;

    QmtVectorizedAtom(const qmt::QmtVector& v, int ids):position(v),id(ids) {}
    QmtVectorizedAtom():position(0,0,0),id(0){}

    static QmtVectorizedAtom converter(const QmtCoordinatedAtom& atom) {
        return  QmtVectorizedAtom(qmt::QmtVector(atom.i , atom.j , atom.k), atom.id);
    }

    friend std::ostream& operator<<(std::ostream& stream,const qmt::QmtVectorizedAtom& atom) {
        stream<<atom.position<<" "<<" ("<<atom.id<<")";
        return stream;
    }

    bool operator == (QmtVectorizedAtom const& atom) const{
      return atom.position == position &&
                   atom.id == id;
    }

};

template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct QmtVectorizedAtomHash{
  std::size_t operator()(const QmtVectorizedAtom& atom)const{
    std::size_t seed = 0;
    hash_combine(seed, atom.position.get_x());
    hash_combine(seed, atom.position.get_y());
    hash_combine(seed, atom.position.get_z());
    hash_combine(seed, atom.id);

    return seed;
  }
};


struct QmtInteractionDefinition {

/*
/Convention:
/ -term is: creator(i,spin_ik) creator(j,spin_jl) anihilator(l,spin_jl) anihilator(k,spin_ik)
/ -factor is multiplying this term
/ -term refers to  integral(microscopic parameter) id as it is numbered in a Hamiltonian definition
*/

size_t i;
size_t j;
size_t l;
size_t k;

qmt::QmtSpin spin_ik;
qmt::QmtSpin spin_jl;

double factor;

int term;

};


typedef std::tuple<qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom, unsigned int> one_body_integral_v;
typedef std::tuple<qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,unsigned int>
two_body_integral_v;


typedef std::tuple<qmt::QmtCoordinatedAtom,qmt::QmtCoordinatedAtom, unsigned int> one_body_integral;
typedef std::tuple<qmt::QmtCoordinatedAtom,qmt::QmtCoordinatedAtom,qmt::QmtCoordinatedAtom,qmt::QmtCoordinatedAtom,unsigned int>
two_body_integral;

template<typename Hamiltonian>
class QmtHamiltonianBuilder {

protected:
    enum class OpKind {
        creator,
        anihilator
    };

    typedef unsigned int band;
    typedef typename qmt::QmtSpin spin;
    typedef OpKind kind;
    typedef std::tuple<band, kind, spin> parsed_word;



    static parsed_word wordParser(const std::string& word) {
        int band;
        std::string spin_kind;
        std::string::size_type sz;
        parsed_word parsed;
        try {

            band = std::stoi(word,&sz);
            spin_kind = word.substr(sz);
            std::get<0>(parsed) = band;


            if(spin_kind == "cup" ) {
                std::get<1>(parsed) = OpKind::creator;
                std::get<2>(parsed) = qmt::QmtSpin::up;
                return parsed;
            }

            if(spin_kind == "aup" ) {
                std::get<1>(parsed) = OpKind::anihilator;
                std::get<2>(parsed) = qmt::QmtSpin::up;
                return parsed;
            }

            if(spin_kind == "adown" ) {
                std::get<1>(parsed) = OpKind::anihilator;
                std::get<2>(parsed) = qmt::QmtSpin::down;
                return parsed;
            }

            if(spin_kind == "cdown" ) {
                std::get<1>(parsed) = OpKind::creator;
                std::get<2>(parsed) = qmt::QmtSpin::down;
                return parsed;
            }


            else throw std::invalid_argument("QmtHamiltonianBuilder::word_parser::Cannot parse spin value");

        }
        catch(const std::invalid_argument& ia) {
            //Handle exception here
        }
        return parsed;
    }

    static std::vector<int> getIndicies(const std::string& line, char c ) {
        std::vector<int> indicies;
        unsigned int index = 0;
        for (std::string::const_iterator it=line.begin(); it!=line.end(); ++it) {
            if(*it == c) indicies.push_back(index);
            index++;
        }
        return indicies;
    }

    static void showOperator(const parsed_word& word) {
        std::string  op_kind = std::get<1>(word) == OpKind::creator ? "c" : "a";
        std::string  spin = std::get<2>(word) ==qmt::QmtSpin::down ? "down" : "up";
        std::cout<<std::get<0>(word)<<op_kind<<spin;
    }

    typedef typename Hamiltonian::HamiltonianOperator Operator;
    static auto getOperator(parsed_word& parsed)->Operator {
        if(std::get<1>(parsed) == OpKind::creator && std::get<2>(parsed) == qmt::QmtSpin::up)
            return Operator::get_up_creator(std::get<0>(parsed));
        else if(std::get<1>(parsed) == OpKind::creator && std::get<2>(parsed) == qmt::QmtSpin::down)
            return Operator::get_down_creator(std::get<0>(parsed));
        else if(std::get<1>(parsed) == OpKind::anihilator && std::get<2>(parsed) == qmt::QmtSpin::up)
            return Operator::get_up_anihilator(std::get<0>(parsed));
        else return Operator::get_down_anihilator(std::get<0>(parsed));
    };

    static double getPrefixFromString(const std::string& prefix_string) {
#ifdef QMT_HAMILTONIAN_BUILDER_DEBUG
        std::cout<<"QmtHamiltonianBuilder::getPrefixFromString given string prefix"<<prefix_string<<std::endl;
#endif
        if(prefix_string == "+") return 1.0;
        if(prefix_string == "-") return -1.0;
        else
            return std::stod(prefix_string);
    }

    static void oneBodyParser(std::string& line, Hamiltonian* h) {
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies = getIndicies(line,',');

        auto first = wordParser(line.substr(0,indicies[0]));
        auto second = wordParser(line.substr(indicies[0] + 1, indicies[1] - indicies[0] - 1));
        //auto sign = line.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1) == "-" ? -1 : 1;
        auto term = std::stoi(line.substr(indicies[2] + 1, indicies[3] - indicies[2]));
        double prefix = getPrefixFromString(line.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1));

        if(! (h -> termExistsOne(term)) )
            h->addOneBodyTerm(/*sign*/ prefix);
        h->addOneBody(term,getOperator(first), getOperator(second));
#ifdef QMT_HAMILTONIAN_BUILDER_DEBUG
        showOperator(first);
        std::cout<< " ";
        showOperator(second);
        std::cout<< " "<<prefix<<" "<<term<<std::endl;
#endif
    }



    static void twoBodyParser(std::string& line, Hamiltonian* h) {
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies = getIndicies(line,',');

        auto first = wordParser(line.substr(0,indicies[0]));
        auto second = wordParser(line.substr(indicies[0] + 1, indicies[1] - indicies[0] - 1));
        auto third = wordParser(line.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1));
        auto fourth = wordParser(line.substr(indicies[2] + 1, indicies[3] - indicies[2] - 1));
        //auto sign = line.substr(indicies[3] + 1, indicies[4] - indicies[3] - 1) == "-" ? -1 : 1;
        double prefix = getPrefixFromString(line.substr(indicies[3] + 1, indicies[4] - indicies[3] - 1));


        auto term = std::stoi(line.substr(indicies[4] + 1, indicies[5] - indicies[4]));

        if(! (h -> termExistsTwo(term)) )
            h->addTwoBodyTerm(/*sign*/prefix);
        h->addTwoBody(term,getOperator(first), getOperator(second),getOperator(third), getOperator(fourth));
#ifdef QMT_HAMILTONIAN_BUILDER_DEBUG
        showOperator(first);
        std::cout<< " ";
        showOperator(second);
        std::cout<< " ";
        showOperator(third);
        std::cout<< " ";
        showOperator(fourth);

        std::cout<< " "<<prefix<<" "<<term<<std::endl;
#endif
    }

    static void HoppingMap(std::string& line, std::set<std::tuple<size_t,size_t,size_t>>& resultant_set, std::vector<std::tuple<size_t,size_t,size_t>>& result_pbc) {
        
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies = getIndicies(line,',');

        auto first = wordParser(line.substr(0,indicies[0]));
        auto second = wordParser(line.substr(indicies[0] + 1, indicies[1] - indicies[0] - 1));
        auto term = std::stoi(line.substr(indicies[2] + 1, indicies[3] - indicies[2]));


        auto indicies_open = getIndicies(line,'(');
        auto indicies_close = getIndicies(line,')');
        auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
        auto second_string = line.substr(indicies_open[1] + 1,indicies_close[1] - indicies_open[1] - 1);
        

        size_t i1 = std::get<0>(first);
	size_t i2 = std::get<0>(second);

	if(first_string.compare(second_string) != 0 && i1 != i2)
		resultant_set.insert(std::make_tuple(i1,i2,term));    
        
      //  if(first_string.compare(second_string) != 0 && i1 == i2)
        //        result_pbc.push_back(std::make_tuple(i1,i2,term));	
        
    }

static  std::vector<std::tuple<size_t,size_t,size_t>> HoppingMapParser(const std::ifstream& input) {
	std::set<std::tuple<size_t,size_t,size_t>> result;        
	std::vector<std::tuple<size_t,size_t,size_t>> result_pbc;      
	std::stringstream buffer;
        std::string   line;

        bool in_block_one_body = false;

        buffer << input.rdbuf();

        while(std::getline(buffer,line)) {
            if(line.substr(0,1) == "#") continue;

            if (line == "one_body" ) {
                in_block_one_body = true;
                //  std::cout<<"Enter block two_body"<<std::endl;
                continue;
            }

            if (line == "end_one_body") {
                in_block_one_body = false;

                // std::cout<<"Exit block two body"<<std::endl;
                continue;
            }

            if(in_block_one_body) {
                HoppingMap(line,result,result_pbc);
            }


        }

	std::vector<std::tuple<size_t,size_t,size_t>> result_vector(result.size());

	std::copy(result.begin(),result.end(),result_vector.begin());	
        for( const  auto &item : result_pbc)
         result_vector.push_back(item);
         
	return result_vector;

    }   

    static void SPEnergyMap(std::string& line, std::set<std::tuple<size_t,int,int>>& result) {
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies = getIndicies(line,',');

        auto first = wordParser(line.substr(0,indicies[0]));
        auto term = std::stoi(line.substr(indicies[2] + 1, indicies[3] - indicies[2]));


        auto indicies_open = getIndicies(line,'(');
        auto indicies_close = getIndicies(line,')');
        auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
        auto second_string = line.substr(indicies_open[1] + 1,indicies_close[1] - indicies_open[1] - 1);
        
        
        size_t i1 = std::get<0>(first);

	if(first_string.compare(second_string) == 0){
		if( std::get<2>(first) == qmt::QmtSpin::up);
			result.insert(std::make_tuple(i1,1,term));
		if( std::get<2>(first) == qmt::QmtSpin::down);
			result.insert(std::make_tuple(i1,-1,term));
	}
    }

static  std::vector<unsigned int> SPEnergyMapParser(const std::ifstream& input) {
	std::set<std::tuple<size_t,int,int>> result;        

	std::stringstream buffer;
        std::string   line;

        bool in_block_one_body = false;

        buffer << input.rdbuf();

        while(std::getline(buffer,line)) {
            if(line.substr(0,1) == "#") continue;

            if (line == "one_body" ) {
                in_block_one_body = true;
                //  std::cout<<"Enter block two_body"<<std::endl;
                continue;
            }

            if (line == "end_one_body") {
                in_block_one_body = false;

                // std::cout<<"Exit block two body"<<std::endl;
                continue;
            }

            if(in_block_one_body) {
                SPEnergyMap(line,result);
            }


        }

	std::vector<unsigned int> result_vector(result.size());

	for(const auto& item : result){
	    if(std::get<1>(item)==-1)
		    result_vector[std::get<0>(item)+result.size()/2]=std::get<2>(item);
	    if(std::get<1>(item)==1)
		    result_vector[std::get<0>(item)]=std::get<2>(item);
	}
	return result_vector;

    }   

    static void InteractionMap(std::string& line, std::vector<qmt::QmtInteractionDefinition>& definitions) {
         auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies = getIndicies(line,',');

        auto first = wordParser(line.substr(0,indicies[0]));
        auto second = wordParser(line.substr(indicies[0] + 1, indicies[1] - indicies[0] - 1));
        auto third = wordParser(line.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1));
        auto fourth = wordParser(line.substr(indicies[2] + 1, indicies[3] - indicies[2] - 1));
        double factor = std::stod(line.substr(indicies[3] + 1, indicies[4] - indicies[3]));
        auto term = std::stoi(line.substr(indicies[4] + 1, indicies[5] - indicies[4]));
        
        qmt::QmtInteractionDefinition def;
        def.i = std::get<0>(first);
        def.k = std::get<0>(second);
        def.l = std::get<0>(third);
        def.k = std::get<0>(fourth);

        def.spin_ik = std::get<1>(first);
        def.spin_jl = std::get<1>(second);
 
        def.term = term;
        def.factor = factor;

	definitions.push_back(def);

    }

    static void InteractionMap(std::string& line, std::vector<std::tuple<size_t,size_t,size_t,size_t>>& resultant_set) {
	
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies = getIndicies(line,',');

        auto first = wordParser(line.substr(0,indicies[0]));
        auto second = wordParser(line.substr(indicies[0] + 1, indicies[1] - indicies[0] - 1));
        auto third = wordParser(line.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1));
        auto fourth = wordParser(line.substr(indicies[2] + 1, indicies[3] - indicies[2] - 1));
        auto term = std::stoi(line.substr(indicies[4] + 1, indicies[5] - indicies[4]));
        
        auto indicies_open = getIndicies(line,'(');
        auto indicies_close = getIndicies(line,')');
        auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
        auto second_string = line.substr(indicies_open[1] + 1,indicies_close[1] - indicies_open[1] - 1);
        auto third_string = line.substr(indicies_open[2] +1, indicies_close[2] - indicies_open[2] -1);
        auto fourth_string = line.substr(indicies_open[3] + 1,indicies_close[3] - indicies_open[3] - 1);

	size_t i1 = std::get<0>(first);
	size_t i2 = std::get<0>(second);
	size_t i3 = std::get<0>(third);
	size_t i4 = std::get<0>(fourth);

/*	

	if (first_string.compare(second_string) == 0 && first_string.compare(third_string) == 0 && first_string.compare(fourth_string) == 0)
		resultant_set.insert(std::make_tuple(i1,i1,term,0)); // U
	
	else if (first_string.compare(third_string)==0 && second_string.compare(fourth_string)==0 ){
		if (i1 == i2 && i3 == i4)
			resultant_set.insert(std::make_tuple(i1,i3,term,1)); // K
	
		else if (i1 == i3 && i2 == i4)
			resultant_set.insert(std::make_tuple(i1,i2,term,1)); // K
	
		else if (i1 == i4 && i3 == i2)
			resultant_set.insert(std::make_tuple(i1,i3,term,1)); // K
	}*/


    if (first_string.compare(second_string) == 0 && first_string.compare(third_string) == 0 && first_string.compare(fourth_string) == 0)
		resultant_set.push_back(std::make_tuple(i1,i1,term,0)); // U
	
	else if (first_string.compare(third_string)==0 && second_string.compare(fourth_string)==0 ){
		if (i1 == i2 && i3 == i4){
			resultant_set.push_back(std::make_tuple(i1,i3,term,1)); // K

	        }
		else if (i1 == i3 && i2 == i4)
			resultant_set.push_back(std::make_tuple(i1,i2,term,1)); // K
	
		else if (i1 == i4 && i3 == i2)
			resultant_set.push_back(std::make_tuple(i1,i3,term,1)); // K

     }
    }

    static  std::vector<std::tuple<size_t,size_t,size_t,size_t>> InteractionMapParser(const std::ifstream& input) {
	std::vector<std::tuple<size_t,size_t,size_t,size_t>> result;        

	std::stringstream buffer;
        std::string   line;

        bool in_block_two_body = false;

        buffer << input.rdbuf();

        while(std::getline(buffer,line)) {
            if(line.substr(0,1) == "#") continue;

            if (line == "two_body" ) {
                in_block_two_body = true;
                //  std::cout<<"Enter block two_body"<<std::endl;
                continue;
            }

            if (line == "end_two_body") {
                in_block_two_body = false;

                // std::cout<<"Exit block two body"<<std::endl;
                continue;
            }

            if(in_block_two_body) {
                InteractionMap(line,result);
            }


        }

	std::vector<std::tuple<size_t,size_t,size_t,size_t>> result_vector(result.size());

	std::copy(result.begin(),result.end(),result_vector.begin());	

	return result_vector;

    }   



   static void parseCAtom(const std::string& catom, QmtCoordinatedAtom& atom ) {
        auto indicies = getIndicies(catom,',');
        atom.i = std::stod(catom.substr(0,indicies[0]));
        atom.j = std::stod(catom.substr(indicies[0] + 1,indicies[1] - indicies[0] - 1 ));
        atom.k = std::stod(catom.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1));
        atom.id = std::stoi(catom.substr(indicies[2] + 1, catom.length() - indicies[2] - 1));
    } 

   static void parseCAtom(const std::string& catom, QmtVectorizedAtom& atom ) {
        auto indicies = getIndicies(catom,',');
        auto x = std::stod(catom.substr(0,indicies[0]));
        auto y = std::stod(catom.substr(indicies[0] + 1,indicies[1] - indicies[0] - 1 ));
        auto z = std::stod(catom.substr(indicies[1] + 1, indicies[2] - indicies[1] - 1));
        
	atom.position = qmt::QmtVector(x,y,z);
        atom.id = std::stoi(catom.substr(indicies[2] + 1, catom.length() - indicies[2] - 1));
    } 


    static void oneBodyAtom(std::string& line, std::vector<one_body_integral>& h) {
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies_open = getIndicies(line,'(');
        auto indicies_close = getIndicies(line,')');
        auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
        auto second_string = line.substr(indicies_open[1] + 1,indicies_close[1] - indicies_open[1] - 1);
        QmtCoordinatedAtom catom1;
        QmtCoordinatedAtom catom2;
        parseCAtom(first_string,catom1);
        parseCAtom(second_string,catom2);
        auto indicies_term = getIndicies(line,',');
        auto term = std::stoi(line.substr(indicies_term[2] + 1, indicies_term[3] - indicies_term[2]));
        h.push_back(std::make_tuple(catom1,catom2,term));
    }


    static void twoBodyAtom(std::string& line, std::vector<two_body_integral>& h) {
        auto isp =  [](const char c)->bool { return c == ';';};
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
        auto indicies_open = getIndicies(line,'(');
        auto indicies_close = getIndicies(line,')');
        auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
        auto second_string = line.substr(indicies_open[1] + 1,indicies_close[1] - indicies_open[1] - 1);
        auto third_string = line.substr(indicies_open[2] +1, indicies_close[2] - indicies_open[2] -1);
        auto fourth_string = line.substr(indicies_open[3] + 1,indicies_close[3] - indicies_open[3] - 1);
        QmtCoordinatedAtom catom1;
        QmtCoordinatedAtom catom2;
        QmtCoordinatedAtom catom3;
        QmtCoordinatedAtom catom4;
        parseCAtom(first_string,catom1);
        parseCAtom(second_string,catom2);
        parseCAtom(third_string,catom3);
        parseCAtom(fourth_string,catom4);

        auto indicies_term = getIndicies(line,',');
        auto term = std::stoi(line.substr(indicies_term[4] + 1, line.length() - indicies_term[3]));
        h.push_back(std::make_tuple(catom1,catom2,catom3,catom4, term));
    }

    template< typename OneBodyFunction, typename TwoBodyFunction >
    static  void initialParser(const std::ifstream& input, OneBodyFunction oneBody, TwoBodyFunction twoBody) {
        std::stringstream buffer;
        std::string   line;

        bool in_block_one_body = false;
        bool in_block_two_body = false;

        buffer << input.rdbuf();

        while(std::getline(buffer,line)) {
            if(line.substr(0,1) == "#") continue;

            if (line == "one_body" ) {
                in_block_one_body = true;
                //  std::cout<<"Enter block one_body"<<std::endl;
                continue;
            }

            if (line == "end_one_body") {
                in_block_one_body = false;

                // std::cout<<"Exit block one body"<<std::endl;
                continue;
            }


            if (line == "two_body" ) {
                in_block_two_body = true;
                //  std::cout<<"Enter block two_body"<<std::endl;
                continue;
            }

            if (line == "end_two_body") {
                in_block_two_body = false;

                // std::cout<<"resultExit block two body"<<std::endl;
                continue;
            }

            if(in_block_one_body) {
                oneBody(line);
            }
            if(in_block_two_body) {
                twoBody(line);
            }


        }

    }

    static std::vector<QmtCoordinatedAtom> getAll(const std::ifstream& input) {
        std::vector<QmtCoordinatedAtom> output;
        std::stringstream buffer;
        std::string   line;
        buffer << input.rdbuf();
        while(std::getline(buffer,line)) {
            auto isp =  [](const char c)->bool { return c == ';';};
            line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
            line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
            auto indicies_open = getIndicies(line,'(');
            auto indicies_close = getIndicies(line,')');
            auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
            QmtCoordinatedAtom catom;
            parseCAtom(first_string,catom);
            output.push_back(catom);
        }
        return output;
    }

    static std::vector<QmtVectorizedAtom> getAllVectorized(const std::ifstream& input) {
        std::vector<QmtVectorizedAtom> output;
        std::stringstream buffer;
        std::string   line;
        buffer << input.rdbuf();
        while(std::getline(buffer,line)) {
            auto isp =  [](const char c)->bool { return c == ';';};
            line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
            line.erase(std::remove_if(line.begin(), line.end(), isp), line.end());
            auto indicies_open = getIndicies(line,'(');
            auto indicies_close = getIndicies(line,')');
            auto first_string = line.substr(indicies_open[0] +1, indicies_close[0] - indicies_open[0] -1);
            QmtVectorizedAtom catom;
            parseCAtom(first_string,catom);
            output.push_back(catom);
        }
        return output;
    }




    /*
      QmtHamiltonianBuilder(const std::string& input_file_name) {
    	 std::ofstream input;
    	 input.open(input_file_name);

      }
    */
public:
    static  Hamiltonian* getHamiltonian(const std::string& input_file_name, int option=0) { // 0- both terms; 1- kinetic term; 2- interaction term
        std::ifstream input;
        input.open(input_file_name);
        Hamiltonian* h = new Hamiltonian;

	if (option<0 || option>2)
		throw std::logic_error("Hamiltonian Builder: wrong getHamiltonian option");


	if(option == 0){
        auto oneBodyF = [&]( std::string& s) {
            oneBodyParser(s,h);
        };
        auto twoBodyF = [&]( std::string& s) {
            twoBodyParser(s,h);
        };
        initialParser(input, oneBodyF, twoBodyF);
	}
	if(option == 1){
        auto oneBodyF = [&]( std::string& s) {
            oneBodyParser(s,h);
        };
        auto twoBodyF = [&]( std::string& s) { };
        initialParser(input, oneBodyF, twoBodyF);
	}
	if(option == 2){
        auto oneBodyF = [&]( std::string& s) { };
        auto twoBodyF = [&]( std::string& s) {
            twoBodyParser(s,h);
        };
        initialParser(input, oneBodyF, twoBodyF);
	}
	
        return h;
    }


    static  std::vector<std::tuple<size_t,size_t,size_t,size_t>> getInteractionMap(const std::string& input_file_name){
        std::ifstream input;
        input.open(input_file_name);
	return InteractionMapParser(input);
     }

    static  std::vector<std::tuple<size_t,size_t,size_t>> getHoppingMap(const std::string& input_file_name){
        std::ifstream input;
        input.open(input_file_name);
	return HoppingMapParser(input);
     }

    static  std::vector<unsigned int> getSPEnergyMap(const std::string& input_file_name){
        std::ifstream input;
        input.open(input_file_name);
	return SPEnergyMapParser(input);
     }

    static std::vector<one_body_integral> getOneBodyIntegrals(const std::string& input_file_name) {
        std::ifstream input;
        input.open(input_file_name);
        std::vector<one_body_integral> atoms;

        auto oneBodyF = [&]( std::string& s) {
            oneBodyAtom(s,atoms);
        };
        auto twoBodyF = [&]( std::string& s) {  };

        initialParser(input, oneBodyF, twoBodyF);
        return atoms;
    }

    static std::vector<one_body_integral_v> getOneBodyIntegralsV(const std::string& input_file_name) {
        std::ifstream input;
        input.open(input_file_name);
        std::vector<one_body_integral_v> integrals_definitions;

        auto fresh = getOneBodyIntegrals(input_file_name);
        auto comp1 = [](const one_body_integral& first, const one_body_integral& second)->bool {return  std::get<2>(first) == std::get<2>(second);};
        auto comp2 = [](const one_body_integral& first, const one_body_integral& second)->bool {return  std::get<2>(first) < std::get<2>(second);};
        std::stable_sort(fresh.begin(),fresh.end(), comp2);
        auto end = std::unique(fresh.begin(),fresh.end(), comp1);

        for(auto i = fresh.begin(); i != end; ++i)
            integrals_definitions.push_back(std::make_tuple(QmtVectorizedAtom::converter(std::get<0>(*i)),QmtVectorizedAtom::converter(std::get<1>(*i)),std::get<2>(*i)));
        return integrals_definitions;
    }


    static std::vector<two_body_integral_v> getTwoBodyIntegralsV(const std::string& input_file_name) {
        std::ifstream input;
        input.open(input_file_name);
        std::vector<two_body_integral_v> integrals_definitions;

        auto fresh = getTwoBodyIntegrals(input_file_name);
        auto comp = [](const two_body_integral& first, const two_body_integral& second)->bool {return std::get<4>(first) == std::get<4>(second);};
        auto comp2 = [](const two_body_integral& first, const two_body_integral& second)->bool {return  std::get<4>(first) < std::get<4>(second);};
        std::stable_sort(fresh.begin(),fresh.end(), comp2);
        auto end = std::unique(fresh.begin(),fresh.end(), comp);

        for(auto i = fresh.begin(); i != end; ++i)
            integrals_definitions.push_back(std::make_tuple(QmtVectorizedAtom::converter(std::get<0>(*i)),QmtVectorizedAtom::converter(std::get<1>(*i)),
                                            QmtVectorizedAtom::converter(std::get<2>(*i)),QmtVectorizedAtom::converter(std::get<3>(*i)), std::get<4>(*i)));
        return integrals_definitions;
    }

    static std::vector<two_body_integral> getTwoBodyIntegrals(const std::string& input_file_name) {
        std::ifstream input;
        input.open(input_file_name);
        std::vector<two_body_integral> atoms;

        auto oneBodyF = [&]( std::string& s) {  };
        auto twoBodyF = [&]( std::string& s) {
            twoBodyAtom(s,atoms);
        };

        initialParser(input, oneBodyF, twoBodyF);
        return atoms;
    }

    static std::vector<QmtVectorizedAtom> getAllAtoms(const std::string& input_file_name) {
        std::ifstream input;
        input.open(input_file_name);
        auto megacell = getAllVectorized(input);
        std::vector<QmtVectorizedAtom> vcell;
        for(auto i = megacell.begin(); i != megacell.end(); ++i)
            vcell.push_back(*i);
        return vcell;
    }

};

}// end of namespace qmt

#endif
