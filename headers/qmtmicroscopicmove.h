#ifndef QMTMICROSCOPICMOVE_H
#define QMTMICROSCOPICMOVE_H

#include <iostream>

#include "qmtmicroscopic.h"

namespace qmt{
template <class Creator, class Hamiltonian>
class QmtMicroscopicMove : public QmtMicroscopic<Creator,Hamiltonian>{

public:
  QmtMicroscopicMove(const std::string& hamiltonian_fn, const std::string& basis_fn, 
                     const std::string& supercell_fn, const std::string& background_fn,
                     double param) :
		     QmtMicroscopic<Creator,Hamiltonian>(hamiltonian_fn, basis_fn, 
                                                         supercell_fn, background_fn, param){}
  QmtMicroscopicMove(const std::string& hamiltonian_fn, const std::string& basis_fn, 
                     const std::string& supercell_fn, const std::string& background_fn,
                     const std::vector<double> &params) :
		     QmtMicroscopic<Creator,Hamiltonian>(hamiltonian_fn, basis_fn, 
		                                         supercell_fn,background_fn, params){}

  void Print(char C){
    switch(C){
      case 's':
        std::cout<<std::endl;
	std::cout<<"***********************************************************************"<<std::endl;
        std::cout<<"SuperCell: "<<std::endl;
        for (const auto& atom : this->supercell)
	  std::cout<<atom<<std::endl;
        break;
      case 'S':
        std::cout<<std::endl;
	std::cout<<"***********************************************************************"<<std::endl;
        std::cout<<"Local SuperCell: "<<std::endl;
        for (const auto& atom : this->local_supercell)
	  std::cout<<atom<<std::endl;
        break;
      case 'm':
        std::cout<<std::endl;
	std::cout<<"***********************************************************************"<<std::endl;
        std::cout<<"MegaCell: "<<std::endl;
        for (const auto& atom : this->megacell)
	  std::cout<<atom<<std::endl;
        break;
      case 'M':
        std::cout<<std::endl;
	std::cout<<"***********************************************************************"<<std::endl;
        std::cout<<"Local MegaCell: "<<std::endl;
        for (const auto& atom : this->local_megacell)
	  std::cout<<atom<<std::endl;
        break;
    }
  }


};//end of class QmtMicroscopicMove
} //end of namespace qmt
#endif
