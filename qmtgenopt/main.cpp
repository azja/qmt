#include <iostream>
#include "headers/qmtgenopt.h"
#include <math.h>


double F(std::vector<double> args) {
 
  double x = args[0];
  double y = args[1];
  double z = args[2];
  double xx = args[3];

  return (fabs(x*x + y*y -1) + fabs(y-x*x))/2;
  
}


int main(int argc, char **argv) {
    std::cout<<"ULONG_MAX:"<<ULONG_MAX<<std::endl;
    auto f = std::function<double(std::vector<double>)>(F);
    qmt::QmtGenOpt<64> opt(1000U,2U, std::function<double(std::vector<double>)>(F));
    auto result = opt.Run();
    for(const auto &x : result)
      std::cout<<x<<" ";
    std::cout<< std::endl;
  
    return 0;
}
