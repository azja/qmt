/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  Andrzej Biborski <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QMTGENOPT_H
#define QMTGENOPT_H

#include<functional>
#include<vector>
#include<bitset>
#include<numeric>
#include<tuple>
#include<algorithm>
#include<random>
#include <climits>

namespace qmt {

template<int Nb>
class Chromosome {
  
  std::vector<std::bitset<Nb>> genes;
  std::vector<std::tuple<double,double>> weights;
 
  
public:
virtual void Cross(Chromosome& cr1, size_t cross_point) {

     auto &cr2 = *this;
     size_t broken_gene_index = cross_point/Nb;
     size_t offset = cross_point%Nb;
     for(size_t k = offset; k < Nb ; ++k) {
  	bool temp = (cr2.genes[broken_gene_index])[k];
	(cr2.genes[broken_gene_index])[k] = (cr1.genes[broken_gene_index])[k];
	(cr1.genes[broken_gene_index])[k] = temp;
     }
     
     for(size_t i = broken_gene_index + 1; i < genes.size();++i){
       for(size_t j = 0; j < Nb; ++j) {
	 bool temp = (cr2.genes[i])[j];
	(cr2.genes[i])[j] = (cr1.genes[i])[j];
	(cr1.genes[i])[j] = temp;
       }
     }

}


virtual void Mutate( size_t p){
   for(auto &item : genes) {
      item.flip(p);
   }
}

Chromosome(const std::vector<size_t> &args, const std::vector<std::tuple<double,double>> &w) {
  
  for(const auto &item : args)
     genes.push_back(std::bitset<Nb>(item));

   for(const auto &item : w)
     weights.push_back(std::make_tuple(std::get<0>(item),std::get<1>(item)));
}

double geneToDouble(size_t index) const  {
   auto a = std::get<0>(weights[index]);
   auto b = std::get<1>(weights[index]);

   return a + genes[index].to_ulong() * (b - a)/ULONG_MAX ;
}

virtual std::vector<double> getDoubleRepresentation() const {
  std::vector<double> vars;
   for(size_t i = 0U; i < genes.size(); ++i){
     vars.push_back(geneToDouble(i));
   }
   return vars;
}

virtual void showDoubleRepresentation() const {
   std::cout<<"("; 
   for(size_t i = 0U; i < genes.size(); ++i){
     std::cout<<geneToDouble(i);
     if(i != genes.size() - 1)
       std::cout<<", ";
   }
   std::cout<<")"; 
}

};
  

  
  
template<int Nb>
class QmtGenOpt
{

 

size_t _N;                                       //population size
size_t _n_var;                                   //number of chromosomes/variables
std::function<double(std::vector<double>)> _F;   //fitness function
std::function<size_t(size_t)> _randomize_int;    //Integer random number generator  [0,arg)
std::vector<Chromosome<Nb>> generation;
double p_m;					 //muation probability [0,1)

static size_t random(size_t max) {
  static  std::random_device rd;
  static size_t lmax = rd.max();
  static size_t lmin = rd.min();
  return max * static_cast<double>(rd())/(lmax - lmin);
}
     
     
virtual int Initialize() { 

   generation.clear();
     for(int i = 0; i < _N;++i) {
        std::vector<size_t> genes(_n_var);
        std::generate_n(genes.begin(),_n_var,[&]()->size_t{return _randomize_int(ULONG_MAX);});  
	std::vector<std::tuple<double,double> > boundaries(_n_var);
	std::fill_n(boundaries.begin(),_n_var,std::make_tuple(-2.0,2.0));
        generation.push_back(Chromosome<Nb>(genes,boundaries));
     }
}


virtual void Cross(size_t i, size_t j) {
  size_t cross_point = _randomize_int(Nb * _n_var);
 // std::cout<<"Cross"<<cross_point<<std::endl;
  generation[i].Cross(generation[j],cross_point);
  
}

virtual void Mutate(size_t i) {
    size_t mutate_bit = _randomize_int(Nb);
    generation[i].Mutate(mutate_bit);
}




public:
    QmtGenOpt<Nb>(size_t N, size_t n_var,
	      std::function<double(std::vector<double>)> F,
	      std::function<size_t(size_t)> randomize_int = std::function<size_t(size_t)>(QmtGenOpt::random)):
	      _N(N),_n_var(n_var),_F(F),_randomize_int(randomize_int),p_m(0.01){ }
	    
    
	      
            
    virtual std::vector<double> Run() {
   
    Initialize();
      
     std::vector<double> chromosome_efs; 
     for(auto const &item : generation){
             chromosome_efs.push_back(1.0/(1.0+_F(item.getDoubleRepresentation())));
       
     }
    
     
     
     int parent_a = -1;
     int parent_b = -1;
     int mutated = -1;
     
     /*
      * Algorithm loop starts here:
      */
     

     /*
      * Roulette based random demands partial sum
      */
     
    
      size_t max_index;
      
     for(int ii = 0; ii < 1000000;++ii) {
     std::vector<double> partial_sum(chromosome_efs.size());
 
     std::partial_sum(chromosome_efs.begin(),chromosome_efs.end(),partial_sum.begin());
   
     auto rand =  [&]()->double{return static_cast<double>(_randomize_int(ULONG_MAX))/ULONG_MAX;};
     
      max_index = std::max_element(chromosome_efs.begin(),chromosome_efs.end()) - chromosome_efs.begin(); 
     
     /*
      * Elitism: we allow the best chromosome to be in next generation
      */
     
      
      do {
      double target_value = rand() * (partial_sum.back()); 
          auto lb = std::lower_bound(partial_sum.begin(),partial_sum.end(),target_value,std::less_equal<double>());
      parent_a = lb - partial_sum.begin() -1;
      if( parent_a < 0) parent_a = 0;
     
      } while(parent_a == max_index);
      
      do {
          double target_value = rand() * (partial_sum.back()); 
          auto lb = std::lower_bound(partial_sum.begin(),partial_sum.end(),target_value,std::less_equal<double>());
          parent_b = lb - partial_sum.begin() -1;
	  if( parent_b < 0) parent_b = 0;
      }
      while(parent_a == parent_b || parent_b == max_index);
   
      Cross(parent_a,parent_b);
      chromosome_efs[parent_a] = 1.0/(1.0 + _F(generation[parent_a].getDoubleRepresentation()));
      chromosome_efs[parent_b] = 1.0/(1.0 + _F(generation[parent_b].getDoubleRepresentation()));
       
      
      if(rand() < 0.05 ){
      do{
	mutated = _randomize_int(_n_var);      
      }while(mutated == max_index);
      Mutate(mutated);
            chromosome_efs[mutated] =1.0/(1.0 + _F(generation[mutated].getDoubleRepresentation()));
      }

     }
     
     return generation[max_index].getDoubleRepresentation();
     
     
     
   /*  
     for(size_t i = 0; i < generation.size();++i){
      if( i != a_individual)
	chromosome_efs[i]
     }*/
       
       
     
     
      
    }

    
    
    ~QmtGenOpt(){};
};

}
#endif // QMTGENOPT_H
