#include "../../qmtmpioptimizer.h"
#include <math.h>

struct Functor {
    typedef double returned_value;

    bool stop(double x, double y) const {
        return fabs(x-y) < 1.0e-5;

    }

    returned_value operator()(returned_value x) const {
        return (x-2)*(x-5);
    }

};



int main() {


    MPI_Init(NULL,NULL);

    int data;
    int taskid;
    int receive = -1;
    int winner;
    double result;
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);

    result = qmtMpiBruteOptimizer<Functor, MPI_DOUBLE>(-10,10.0, Functor());

    if(taskid == 0)
        std::cout<<"x0 = "<<result<<std::endl;

    MPI_Finalize();



    return 0;
}