/*
 * main.cpp
 *
 *  Created on: 06-02-2014
 *      Author: andrzej kadzielawa
 */


#include "main.h"


int main()
{
    FILE *ext,*ext2,*err;

    err = fopen("error.dat","w+"); // log - if you turn on the DEBUG in SolverSGA returns some information
    ext = stdout;
    ext2= stdout;


    s_SGA_out outcome0= {0.01, 0.0001, 0.001, 0., -0.}; //Starting values
    s_SGA_out outcome, outcomed0;

    fprintf(ext, "h (T)\t\td^2\t\td\t\tm\t\tlmb_m\t\tlmb_n\t\tmu\t\tEG\n");
    fprintf(ext2,"h (T)\t\td^2\t\td\t\tm\t\tlmb_m\t\tlmb_n\t\tmu\t\tEG\n");

    for(int i=0; i<101; i++)
    {
        double h=0.0000427663*i*0.01; // in atomic units 0.0000427663 Ry <=> 1 T

        // d != 0
        Solver_SGA* solver = new Solver_SGA(1000,-1, 1.5,4.1000,h,err,ext,false);
        outcome   = solver->solve(outcome0.d,outcome0.m,outcome0.lmb_m,outcome0.mu,outcome0.lmb_n);
        fprintf(ext,"%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\n",0.01*i,outcome.d*outcome.d,outcome.d,outcome.m,outcome.lmb_m,outcome.lmb_n,outcome.mu,outcome.EG);
        delete solver;

        // d == 0
        Solver_SGA* solver2 = new Solver_SGA(1000,-1, 1.5,4.1000,h,err,ext,true);
        outcomed0 = solver2->solve(outcome0.m,outcome0.lmb_m,outcome0.mu,outcome0.lmb_n);
        fprintf(ext2,"%e\t0.000000e+00\t0.000000e+00\t%e\t%e\t%e\t%e\t%e\n",0.01*i,outcomed0.m,outcomed0.lmb_m,outcomed0.lmb_n,outcomed0.mu,outcomed0.EG);
        delete solver2;

        if(ext==ext2) fprintf(ext,"_____________________________________________________________________________________________________________________________\n");
    }

    fclose(ext);
    fclose(ext2);
    fclose(err);

    return 0;
}

