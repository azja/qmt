#include "../../headers/qmtorbitals.h"
#include <iostream>

int main() {
    Slater1S slater2(0,0,0,1);
    Slater1S slater1(0,1.43,0,1);
    Wannier<Slater1S> wannier1;
    Wannier<Slater1S> wannier2;

    wannier1.addOrbital(slater1,0);
    wannier2.addOrbital(slater2,0);

    wannier1.addOrbital(slater2,1);
    wannier2.addOrbital(slater1,1);


    double **M = new double*[2];

    M[0] = new double;
    M[1] = new double;

    Wannier<Slater1S>::multiply(wannier1,wannier2,M);


    for(int i = 0; i < 2; ++i) {
        for(int j = 0; j < 2; ++j) {
            std::cout<<M[i][j]<<" ";
        }
        std::cout<<std::endl;
    }

    return 0;
}
