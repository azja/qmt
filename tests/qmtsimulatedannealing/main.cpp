#include <iostream>
#include <cmath>
#include "../../headers/qmtsimulatedannealing.h"


double F(std::vector<double> args) {
 
  double x = args[0];
  double y = args[1];
  double z = args[2];
  double w = args[3];
  double q = args[4];

  return -cos(20*(x+y+z+w+q)) + 10*(x*x+y*y+z*z+w*w+q*q);
  
}


int main(){

  unsigned int number_of_steps=100;

  std::vector<double> lower({-1,-1,-1,-1,-1});
  std::vector<double> upper({1,1,1,1,1});
  
  
  qmt::QmtSimulatedAnnealing solver(10,5,std::function<double(std::vector<double>)>(F),lower,upper,10.0,0.1);
  
  std::vector<double> values;
  std::vector<std::vector<double>> results;
  
  solver.run(number_of_steps,1,values,results);

  for(int i=0; i<16; i++){
     std::cout<<"F("<<results[i][0]<<","<<results[i][1]<<","<<results[i][2]<<","<<results[i][3]<<","<<results[i][4]<<")= "<<values[i]<<std::endl;
  }
  
  double min_val;
  std::vector<double> min_args;
  solver.run(number_of_steps,1,min_val,min_args);
  std::cout<<"+------------------------------------------------+"<<std::endl;
  std::cout<<"F("<<min_args[0]<<","<<min_args[1]<<","<<min_args[2]<<","<<min_args[3]<<","<<min_args[4]<<")= "<<min_val<<std::endl;
  
return 0;
}
