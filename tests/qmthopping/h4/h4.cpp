#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../../../headers/qmtorbitals.h"
#include "../../../headers/gaussexp.h"
#include <math.h>
#include "../../../headers/qmtbform.h"
#include "../../../headers/qmteqsolver.h"
#include <omp.h>
#include <stdlib.h>


/////////////////////////////////////////////////////////////////////

double slater1soverlap(double R, double alfa) {
    return exp(-alfa * R)*(1 + alfa * R + alfa *alfa * R *R/3);
}

double slater1skinetic(double R, double alfa) {
    return alfa * alfa * exp(-alfa * R) * (1.0 + alfa * R - alfa*alfa *R*R/3);
}
///////////////////////////////////////////////////////////////////////////


int H2_envelope(double a,double b, double alpha, std::vector<double>& betas, double** M) {



    Slater1S slater1(0,a,0, alpha);
    Slater1S slater2(0,0,0, alpha);
    Slater1S slater3(b,0,0, alpha);
    Slater1S slater4(b,a,0, alpha);

    Wannier<Slater1S> wannier1;
    Wannier<Slater1S> wannier2;
    Wannier<Slater1S> wannier3;
    Wannier<Slater1S> wannier4;



    wannier1.addOrbital(slater1,0);
    wannier1.addOrbital(slater2,1);
    wannier1.addOrbital(slater3,3);
    wannier1.addOrbital(slater4,2);
    
    

    wannier2.addOrbital(slater1,1);
    wannier2.addOrbital(slater2,0);
    wannier2.addOrbital(slater3,2);
    wannier2.addOrbital(slater4,3);
    

    wannier3.addOrbital(slater1,3);
    wannier3.addOrbital(slater2,2);
    wannier3.addOrbital(slater3,0);
    wannier3.addOrbital(slater4,1);
    

    wannier4.addOrbital(slater1,2);
    wannier4.addOrbital(slater2,3);
    wannier4.addOrbital(slater3,1);
    wannier4.addOrbital(slater4,0);
    

/*
    double **M = new double*[4];

    for(int i = 0; i < 4; ++i) {
        M[i] = new double[4];
    }

*/


    /*
        double **M = new double*[4];

        for(int i = 0; i < 4; ++i) {
            M[i] = new double[4];
        }

    */

    std::vector<BilinearForm> forms;

    Wannier<Slater1S>::multiply(wannier1,wannier2,M,4);
    forms.push_back(BilinearForm(M,4));

    Wannier<Slater1S>::multiply(wannier1,wannier3,M,4);
    forms.push_back(BilinearForm(M,4));


    Wannier<Slater1S>::multiply(wannier1,wannier4,M,4);
    forms.push_back(BilinearForm(M,4));


    Wannier<Slater1S>::multiply(wannier1,wannier1,M,4);
    forms.push_back(BilinearForm(M,4,-1));


    double in[4] = { 1.0, 0.0, 0.0, 0.0 };
    double out[4] = {0.0, 0.0, 0.0, 0.0 };

    NonLinearSystemSolve(forms,in,out,1.0e-8);

//    std::cout<<" "<<out[0]<<" "<<out[1]<<" "<<out[2]<<" "<<out[3]<<std::endl;
    betas.clear();
    betas.push_back(out[0]);
    betas.push_back(out[1]);
    betas.push_back(out[2]);
    betas.push_back(out[3]);

<<<<<<< HEAD
    /*
        for(int i = 0; i < 4; ++i)
            delete [] M[i];
        delete [] M;
    */
=======
/*
    for(int i = 0; i < 4; ++i)
        delete [] M[i];
    delete [] M;
*/
>>>>>>> 27cb4c601b7726e20a3c1b462f380a959f700c57

    return 0;
}


///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int main() {
<<<<<<< HEAD

=======
    
>>>>>>> 27cb4c601b7726e20a3c1b462f380a959f700c57
    int N = 19;
    double alfa = 1.19378;

    std::vector<double> coefs;
    std::vector<double> gammas;


    qmt::qmGtoNSlater1s(2.0,2.0 , 1.0e-5, 0.01, 0.01, coefs, gammas, N);
    

    
    std::vector<Gauss> atom1;
    std::vector<Gauss> atom2;
    std::vector<Gauss> atom3;
    std::vector<Gauss> atom4;

   double a = 1.0;
   double b = 10;

    

    for(int i=0; i < N; ++i) {
        atom1.push_back(Gauss(0,a,0,alfa*gammas[i]));
        atom2.push_back(Gauss(0,0,0,alfa*gammas[i]));
        atom3.push_back(Gauss(b,0,0,alfa*gammas[i]));
        atom4.push_back(Gauss(b,a,0,alfa*gammas[i]));

    }
    
    typedef QmExpansion<Gauss, QmVariables::QmParallel::no> g_omp;

    g_omp slater1(0,a,0,0);
    g_omp slater2(0,0,0,1);
    g_omp slater3(b,0,0,2);
    g_omp slater4(b,a,0,3);


    for(int i=0; i < N; ++i) {
        slater1.add_element(i, coefs[i], atom1[i]);
        slater2.add_element(i, coefs[i], atom2[i]);
        slater3.add_element(i, coefs[i], atom3[i]);
        slater4.add_element(i, coefs[i], atom4[i]);
        

    }

//////////////////////////////////////////////////////////////

    double** M = new double* [4];
<<<<<<< HEAD
    for(int i = 0; i < 4; ++i)
        M[i] = new double [4];


    double start_time = omp_get_wtime();

    std::cout<<"a [a0] b[a0] epsilon[Ry] t1[Ry] t2[Ry] t3[Ry] U[Ry] K_1122[Ry] K_1133[Ry] K_1144[Ry] J_1212[Ry] J_1313[Ry] J_1414[Ry] V_1112[Ry] V_1113[Ry] V_1114[Ry] "<<std::endl;

    for(int iterations = 0; iterations < 1000; ++iterations) {

=======
    for(int i = 0; i < 4;++i)
     M[i] = new double [4];
>>>>>>> 27cb4c601b7726e20a3c1b462f380a959f700c57


double start_time = omp_get_wtime();

std::cout<<"a [a0] b[a0] epsilon[Ry] t1[Ry] t2[Ry] t3[Ry] U[Ry] K_1122[Ry] K_1133[Ry] K_1144[Ry] J_1212[Ry] J_1313[Ry] J_1414[Ry] V_1112[Ry] V_1113[Ry] V_1114[Ry] "<<std::endl;

for(int iterations = 0; iterations < 1000; ++iterations) {



/*    for(int iterations1 = 0; iterations1 < 1000;++iterations1)*/ {
    
   
    a = 1.43042;//0.5 + iterations * 0.01;
    b = iterations * 0.01 +  0.1;
    for(int i=0; i < N; ++i) {
    
        atom1[i].set_gamma(alfa * gammas[i]);
        atom2[i].set_gamma(alfa * gammas[i]);
        atom3[i].set_gamma(alfa * gammas[i]);
        atom4[i].set_gamma(alfa * gammas[i]);

        atom1[i].set_r(0, a, 0);
        atom2[i].set_r(0, 0, 0);
        atom3[i].set_r(b, 0, 0);
        atom4[i].set_r(b, a, 0);

    }

    slater1.set_r(0,a,0);
    slater2.set_r(0,0,0);
    slater3.set_r(b,0,0);
    slater4.set_r(b,a,0);
    
    std::vector<double> betas;
    H2_envelope(a, b, alfa, betas, M); 

   
   typedef QmExpansion < g_omp, QmVariables::QmParallel::omp > w_omp;
    w_omp wannier1(0,a,0);
    wannier1.add_element(0, betas[0], slater1);
    wannier1.add_element(1, betas[1], slater2);
    wannier1.add_element(3, betas[3], slater3);
    wannier1.add_element(2, betas[2], slater4);

    w_omp  wannier2(0,0,0);
    wannier2.add_element(1, betas[1], slater1);
    wannier2.add_element(0, betas[0], slater2);
    wannier2.add_element(2, betas[2], slater3);
    wannier2.add_element(3, betas[3], slater4);


    w_omp wannier3(b,0,0);
    wannier3.add_element(3, betas[3], slater1);
    wannier3.add_element(2, betas[2], slater2);
    wannier3.add_element(0, betas[0], slater3);
    wannier3.add_element(1, betas[1], slater4);


    w_omp wannier4(b,a,0);
    wannier4.add_element(2, betas[2], slater1);
    wannier4.add_element(3, betas[3], slater2);
    wannier4.add_element(1, betas[1], slater3);
    wannier4.add_element(0, betas[0], slater4);

    typedef w_omp expansion;

    std::vector<w_omp*> system;

    system.push_back(&wannier1);
    system.push_back(&wannier2);
    system.push_back(&wannier3);    
    system.push_back(&wannier4);

    
//    typedef QmExpansion< /*QmExpansion<Gauss>*/ g_omp > expansion;
    
    double t0 = expansion::kinetic_integral(wannier1,wannier1);
    double t1 = expansion::kinetic_integral(wannier1,wannier2);
    double t2 = expansion::kinetic_integral(wannier1,wannier4);
    double t3 = expansion::kinetic_integral(wannier1,wannier3);

    int i;

 #pragma omp parallel for default(none) shared(system, wannier1, wannier2, wannier3, wannier4)  private(i)  reduction(+ : t0, t1, t2, t3)
    
    for( i = 0; i < 4;++i) {
     t0 = t0 + expansion::attractive_integral(wannier1, wannier1,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     t1 = t1 + expansion::attractive_integral(wannier1, wannier2,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     t2 = t2 + expansion::attractive_integral(wannier1, wannier4,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     t3 = t3 + expansion::attractive_integral(wannier1, wannier3,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     }

    double U = 0;
    double K12 = 0;
    double K13 = 0;
    double K14 = 0;
    double J12 = 0;
    double J13 = 0;
    double J14 = 0;
    double V12 = 0;
    double V13 = 0;
    double V14 = 0;
    
    U = expansion::v_integral(wannier1, wannier1, wannier1, wannier1); // U
    K12 = expansion::v_integral(wannier1, wannier2, wannier1, wannier2); // K12
    K13 = expansion::v_integral(wannier1, wannier3, wannier1, wannier3); // K13
    K14 = expansion::v_integral(wannier1, wannier4, wannier1, wannier4); // K14
    J12 = expansion::v_integral(wannier1, wannier1, wannier2, wannier2); // J
    J13 = expansion::v_integral(wannier1, wannier1, wannier3, wannier3); // J
    J14 = expansion::v_integral(wannier1, wannier1, wannier4, wannier4); // J
    V12 = expansion::v_integral(wannier1, wannier1, wannier1, wannier2); // V
    V13 = expansion::v_integral(wannier1, wannier1, wannier1, wannier3); // V
    V14 = expansion::v_integral(wannier1, wannier1, wannier1, wannier4); // V
  
    std::cout<<a<<" "<<b<<" "<<t0<<" "<<t1<<" "<<t2<<" "<<t3<<" "<<U<<" "<<K12<<" "<<K13<<" "<<K14<<" "<<J12<<" "<<J13<<" "<<J14<<" "<<V12<<" "<<V13<<" "<<V14<<std::endl;
  }
 }
 
 double time = omp_get_wtime() - start_time;
 std::cout<<" Total time = "<<time<<std::endl;
     for(int i = 0; i < 4; ++i)
        delete [] M[i];
    delete [] M;

    return 0;
}
