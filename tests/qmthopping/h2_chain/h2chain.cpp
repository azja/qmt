#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../../../headers/qmtorbitals.h"
#include "../../../headers/gaussexp.h"
#include <math.h>
#include "../../../headers/qmtbform.h"
#include "../../../headers/qmteqsolver.h"



/////////////////////////////////////////////////////////////////////

double slater1soverlap(double R, double alfa) {
    return exp(-alfa * R)*(1 + alfa * R + alfa *alfa * R *R/3);
}

double slater1skinetic(double R, double alfa) {
    return alfa * alfa * exp(-alfa * R) * (1.0 + alfa * R - alfa*alfa *R*R/3);
}
///////////////////////////////////////////////////////////////////////////



int H2_envelope(double a,double b, double alpha, std::vector<double>& betas, double** M) {



    Slater1S slater1(0,a,0, alpha);
    Slater1S slater2(0,0,0, alpha);
    Slater1S slater3(b,0,0, alpha);
    Slater1S slater4(b,a,0, alpha);


    Slater1S slater5(-b,a,0, alpha);
    Slater1S slater6(-b,0,0, alpha);
    Slater1S slater7(2*b,a,0, alpha);
    Slater1S slater8(2*b,0,0, alpha);

    Wannier<Slater1S> wannier1;
    Wannier<Slater1S> wannier2;
    Wannier<Slater1S> wannier3;
    Wannier<Slater1S> wannier4;



    wannier1.addOrbital(slater1,0);
    wannier1.addOrbital(slater2,1);
    wannier1.addOrbital(slater3,3);
    wannier1.addOrbital(slater4,2);
    wannier1.addOrbital(slater5,2);
    wannier1.addOrbital(slater6,4);

    wannier2.addOrbital(slater1,1);
    wannier2.addOrbital(slater2,0);
    wannier2.addOrbital(slater3,2);
    wannier2.addOrbital(slater4,3);
    

    wannier3.addOrbital(slater1,3);
    wannier3.addOrbital(slater2,2);
    wannier3.addOrbital(slater3,0);
    wannier3.addOrbital(slater4,1);
    

    wannier4.addOrbital(slater1,2);
    wannier4.addOrbital(slater2,3);
    wannier4.addOrbital(slater3,1);
    wannier4.addOrbital(slater4,0);
    

/*
    double **M = new double*[4];

    for(int i = 0; i < 4; ++i) {
        M[i] = new double[4];
    }

*/

    std::vector<BilinearForm> forms;

    Wannier<Slater1S>::multiply(wannier1,wannier2,M,4);
    forms.push_back(BilinearForm(M,4));

    Wannier<Slater1S>::multiply(wannier1,wannier3,M,4);
    forms.push_back(BilinearForm(M,4));


    Wannier<Slater1S>::multiply(wannier1,wannier4,M,4);
    forms.push_back(BilinearForm(M,4));


    Wannier<Slater1S>::multiply(wannier1,wannier1,M,4);
    forms.push_back(BilinearForm(M,4,-1));


    double in[4] = { 1.0, 0.0, 0.0, 0.0 };
    double out[4] = {0.0, 0.0, 0.0, 0.0 };

    NonLinearSystemSolve(forms,in,out,1.0e-8);

//    std::cout<<" "<<out[0]<<" "<<out[1]<<" "<<out[2]<<" "<<out[3]<<std::endl;
    betas.clear();
    betas.push_back(out[0]);
    betas.push_back(out[1]);
    betas.push_back(out[2]);
    betas.push_back(out[3]);

/*
    for(int i = 0; i < 4; ++i)
        delete [] M[i];
    delete [] M;
*/

    return 0;
}


///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int main() {
    
    int N = 19;
    double alfa = 1.19378;

    std::vector<double> coefs;
    std::vector<double> gammas;


    qmt::qmGtoNSlater1s(2.0,2.0 , 1.0e-5, 0.01, 0.01, coefs, gammas, N);
    

    
    std::vector<Gauss> atom1;
    std::vector<Gauss> atom2;
    std::vector<Gauss> atom3;
    std::vector<Gauss> atom4;

   double a = 1.0;
   double b = 10;

    

    for(int i=0; i < N; ++i) {
        atom1.push_back(Gauss(0,a,0,alfa*gammas[i]));
        atom2.push_back(Gauss(0,0,0,alfa*gammas[i]));
        atom3.push_back(Gauss(b,0,0,alfa*gammas[i]));
        atom4.push_back(Gauss(b,a,0,alfa*gammas[i]));

    }

    QmExpansion<Gauss> slater1(0,0,0,0);
    QmExpansion<Gauss> slater2(0,a,0,1);
    QmExpansion<Gauss> slater3(b,0,0,2);
    QmExpansion<Gauss> slater4(b,a,0,3);


    for(int i=0; i < N; ++i) {
        slater1.add_element(i, coefs[i], atom1[i]);
        slater2.add_element(i, coefs[i], atom2[i]);
        slater3.add_element(i, coefs[i], atom3[i]);
        slater4.add_element(i, coefs[i], atom4[i]);
        

    }

//////////////////////////////////////////////////////////////

    double** M = new double* [4];
    for(int i = 0; i < 4;++i)
     M[i] = new double [4];



for(int iterations = 0; iterations < 10000; ++iterations) {
 // std::cout<<std::endl;
/*    for(int iterations1 = 0; iterations1 < 1000;++iterations1) */{
    
   
    a = 1.43042;//0.5 + iterations * 0.01;
    b = iterations * 0.05 + 0.01;
    for(int i=0; i < N; ++i) {
    
        atom1[i].set_gamma(alfa * gammas[i]);
        atom2[i].set_gamma(alfa * gammas[i]);
        atom3[i].set_gamma(alfa * gammas[i]);
        atom4[i].set_gamma(alfa * gammas[i]);

        atom1[i].set_r(0, a, 0);
        atom2[i].set_r(0, 0, 0);
        atom3[i].set_r(b, 0, 0);
        atom4[i].set_r(b, a, 0);

    }

    slater1.set_r(0,a,0);
    slater2.set_r(0,0,0);
    slater3.set_r(b,0,0);
    slater4.set_r(b,a,0);
    
    std::vector<double> betas;
    H2_envelope(a, b, alfa, betas, M); 
//    for(int i = 0; i < 2; ++i)
//     std::cout<<"beta["<<i<<"] = "<<betas[i]<<std::endl; 

   
    QmExpansion< QmExpansion<Gauss> > wannier1(0,a,0);
    wannier1.add_element(0, betas[0], slater1);
    wannier1.add_element(1, betas[1], slater2);
    wannier1.add_element(3, betas[3], slater3);
    wannier1.add_element(2, betas[2], slater4);


    QmExpansion< QmExpansion<Gauss> > wannier2(0,0,0);
    wannier2.add_element(1, betas[1], slater1);
    wannier2.add_element(0, betas[0], slater2);
    wannier2.add_element(2, betas[2], slater3);
    wannier2.add_element(3, betas[3], slater4);


    QmExpansion< QmExpansion<Gauss> > wannier3(b,0,0);
    wannier3.add_element(3, betas[3], slater1);
    wannier3.add_element(2, betas[2], slater2);
    wannier3.add_element(0, betas[0], slater3);
    wannier3.add_element(1, betas[1], slater4);


    QmExpansion< QmExpansion<Gauss> > wannier4(b,a,0);
    wannier4.add_element(2, betas[2], slater1);
    wannier4.add_element(3, betas[3], slater2);
    wannier4.add_element(1, betas[1], slater3);
    wannier4.add_element(0, betas[0], slater4);

    typedef QmExpansion< QmExpansion<Gauss> > expansion;

    std::vector<expansion*> system;

    system.push_back(&wannier1);
    system.push_back(&wannier2);
    system.push_back(&wannier3);    
    system.push_back(&wannier4);

    
    typedef QmExpansion< QmExpansion<Gauss> > expansion;
    
    double t0 = expansion::kinetic_integral(wannier1,wannier1);
    double t1 = expansion::kinetic_integral(wannier1,wannier2);
    double t2 = expansion::kinetic_integral(wannier1,wannier4);
    double t3 = expansion::kinetic_integral(wannier1,wannier3);


    
    for(int i = 0; i < 4;++i) {
     t0 += expansion::attractive_integral(wannier1, wannier1,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     t1 += expansion::attractive_integral(wannier1, wannier2,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     t2 += expansion::attractive_integral(wannier1, wannier4,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     t3 += expansion::attractive_integral(wannier1, wannier3,system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
     }
    
     

    std::cout<<a<<" "<<b<<" "<<t0<<" "<<t1<<" "<<t2<<" "<<t3<<::std::endl;
  }
 }
     for(int i = 0; i < 4; ++i)
        delete [] M[i];
    delete [] M;

    return 0;
}
