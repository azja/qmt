#!/usr/bin/python

import math
import numpy
import sys

# lattice constant
R=1.43
verZ=numpy.array([0,0,1])
latticeZ=R*verZ

# super cell
SuperCell = [
[0,numpy.array([0,0,0]),0,0,0,0],
[1,numpy.array([0,0,0]),0,0,0,1],
[2,latticeZ,0,0,1,2],
[3,latticeZ,0,0,1,3]
]

#mega cell
MegaCell = [
[0,numpy.array([0,0,0]),0,0,0,1],
[5,latticeZ,0,0,1,6],
]


def length(vector):
	return math.sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2])
#length

def distance(atom1,atom2):
	return length(atom1[1]-atom2[1])
#	return math.sqrt(math.pow(atom1[1][0]-atom2[1][0],2)+math.pow(atom1[1][1]-atom2[1][1],2)+math.pow(atom1[1][2]-atom2[1][2],2))
#distance

SuperCellFile = open('supercell.dat', 'w+')
for atom in SuperCell:
	SuperCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
SuperCellFile.close()

MegaCellFile = open('megacell.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
MegaCellFile.close()

############################################################################################################################################################
# jmol Megacell
MegaCellFile = open('jmol_megacell.dat', 'w+')
MegaCellFile.write(str(len(MegaCell))+"\n\n") # for jmol
for atom in MegaCell:
 	if atom[0]==0:
 		MegaCellFile.write("Li ")
 	if atom[0]==1:
 		MegaCellFile.write("Be ")
 	if atom[0]==2:
 		MegaCellFile.write("B ")
 	if atom[0]==3:
 		MegaCellFile.write("C ")
 	if atom[0]==4:
 		MegaCellFile.write("N ")
 	if atom[0]==5:
 		MegaCellFile.write("O ")
 	if atom[0]==6:
 		MegaCellFile.write("F ")
 	if atom[0]==7:
 		MegaCellFile.write("Ne ")
 	MegaCellFile.write(str(atom[2])+" "+str(atom[3])+" "+str(atom[4])+"\n")
MegaCellFile.close()




############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################

# anihilator
def an(N,s):
	return str(N)+"a"+str(s)
# creator
def cr(N,s):
	return str(N)+"c"+str(s)
# number operator
def num(N,s):
	return cr(N,s)+",	"+an(N,s)

# spins
spin = ["up","down"]

def cc(s):
	if s=="up":
		return "down"
	if s=="down":
		return "up"
	else:
		return ""

# actual microscopic parameter flag

one_body_flag = 0
two_body_flag = 0

# dictionaries

one_body_dict = [ ]
two_body_dict = [ ]

# epsilon_a
print ""
print "one_body"
for itemS in SuperCell:
	for s in spin:
		print 	cr(itemS[0],s)+",	"+an(itemS[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"

	one_body_flag+=1
print "end_one_body"

one_body_dict.append([0, 0.0])

# t in SuperCell
print ""
print "one_body"
for i in range(0,len(SuperCell)):
	for j in range(i+1,len(SuperCell)):
		if (i==j):
			continue

	

		for s in spin:
			print cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		";"
			print cr(SuperCell[j][0],s)+",	"+an(SuperCell[i][0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		";"
		
		one_body_flag+=1
print "end_one_body"

# U
print ""
print "two_body"
for itemS in SuperCell:
	print num(itemS[0],spin[0])+",	"+num(itemS[0],spin[1])+",	"+" +,	"+str(two_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"

	two_body_flag+=1
print "end_two_body"

two_body_dict.append([0, 0.0])

# K
print ""
print "two_body"
for i in range(0,len(SuperCell)):
	for j in range(i+1,len(SuperCell)):
		if (i==j):
			continue

	

		for s1 in spin:
			for s2 in spin:
				print num(SuperCell[i][0],s1)+",	"+num(SuperCell[j][0],s2)+",	"+" +,	"+str(two_body_flag)+",	"\
			+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
			+		",	"\
			+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
			+		",	"\
			+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
			+		",	"\
			+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
			+		";"
				print num(SuperCell[j][0],s1)+",	"+num(SuperCell[i][0],s2)+",	"+" +,	"+str(two_body_flag)+",	"\
			+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
			+		",	"\
			+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
			+		",	"\
			+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
			+		",	"\
			+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
			+		";"
		
		two_body_flag+=1
print "end_two_body"

# J
print ""
print "two_body"
for i in range(0,len(SuperCell)):
	for j in range(i+1,len(SuperCell)):
		if (i==j):
			continue

		for s in spin:
			print cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+cr(SuperCell[i][0],cc(s))+",	"+an(SuperCell[j][0],cc(s))+",	"+" -,	"+str(two_body_flag)+",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		";"
			print cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+" -,	"+str(two_body_flag)+",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		";"
		
		print cr(SuperCell[i][0],"up")+",	"+an(SuperCell[j][0],"down")+",	"+cr(SuperCell[i][0],"up")+",	"+an(SuperCell[j][0],"down")+",	"+" -,	"+str(two_body_flag)+",	"\
	+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
	+		",	"\
	+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
	+		",	"\
	+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
	+		",	"\
	+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
	+		";"
		print cr(SuperCell[j][0],"up")+",	"+an(SuperCell[i][0],"down")+",	"+cr(SuperCell[j][0],"up")+",	"+an(SuperCell[i][0],"down")+",	"+" -,	"+str(two_body_flag)+",	"\
	+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
	+		",	"\
	+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
	+		",	"\
	+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
	+		",	"\
	+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
	+		";"
		two_body_flag+=1
print "end_two_body"

# V
print ""
print "two_body"
for itemN in SuperCell:
	for itemV in SuperCell:
		if itemN[5]==itemV[5]:
			continue
		for s in spin:
			print num(itemN[0],s)+",	"+cr(itemN[0],cc(s))+",	"+an(itemV[0],cc(s))+",	"+" +,	"+str(two_body_flag)+",	"\
				+		"("+str(itemN[2])+",  "+str(itemN[3])+",  "+str(itemN[4])+",  "+str(itemN[5])+")"\
				+		",	"\
				+		"("+str(itemN[2])+",  "+str(itemN[3])+",  "+str(itemN[4])+",  "+str(itemN[5])+")"\
				+		",	"\
				+		"("+str(itemN[2])+",  "+str(itemN[3])+",  "+str(itemN[4])+",  "+str(itemN[5])+")"\
				+		",	"\
				+		"("+str(itemV[2])+",  "+str(itemV[3])+",  "+str(itemV[4])+",  "+str(itemV[5])+")"\
				+       ";"
			print num(itemN[0],s)+",	"+cr(itemV[0],cc(s))+",	"+an(itemN[0],cc(s))+",	"+" +,	"+str(two_body_flag)+",	"\
				+		"("+str(itemN[2])+",  "+str(itemN[3])+",  "+str(itemN[4])+",  "+str(itemN[5])+")"\
				+		",	"\
				+		"("+str(itemN[2])+",  "+str(itemN[3])+",  "+str(itemN[4])+",  "+str(itemN[5])+")"\
				+		",	"\
				+		"("+str(itemN[2])+",  "+str(itemN[3])+",  "+str(itemN[4])+",  "+str(itemN[5])+")"\
				+		",	"\
				+		"("+str(itemV[2])+",  "+str(itemV[3])+",  "+str(itemV[4])+",  "+str(itemV[5])+")"\
				+       ";"

	two_body_flag+=1
print "end_two_body"

print two_body_flag+one_body_flag

sys.exit()
