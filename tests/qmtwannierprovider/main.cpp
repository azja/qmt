#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtwannierbasis.h"
#include "../../headers/qmtvector.h"
#include "../../headers/qmtwannierprovider.h"
#include <vector>


int main() {

//H2 molecule 1s approach

qmt::QmtWannierBasisProvider<qmt::QmtWannierSlaterBasedCreator> provider("defs.dat");

std::vector< qmt::QmtVector > SuperCell;
SuperCell.push_back(qmt::QmtVector(0,0,0));
SuperCell.push_back(qmt::QmtVector(1.43,0,0));


qmt::QmtWannierBasis<qmt::QmtWannierSlaterBasedCreator> basis(provider.get_definitions(),provider.get_equations(),provider.get_betas(),1.19,qmt::QmtVector(1,1,1));
std::cout<<"------------------------OVERLAPS------------------------"<<std::endl<<std::flush;
std::cout<<"<W_1|W_2>= "<<basis.overlap(0,1,SuperCell[0],SuperCell[1])<<std::endl<<std::flush;
std::cout<<"<W_1|W_1>= "<<basis.overlap(0,0)<<std::endl<<std::flush;
std::cout<<"<W_2|W_2>= "<<basis.overlap(1,1)<<std::endl<<std::flush;
std::cout<<"<W_2|W_1>= "<<basis.overlap(1,0)<<std::endl<<std::flush;


double eps = 0.0;
double t =0.0;

eps = basis.kinetic_integral(0,0);
t = basis.kinetic_integral(0,1,SuperCell[0],SuperCell[1]);

for (const auto& vec : SuperCell){
	eps += basis.attractive_integral(0,0,vec);
	t += basis.attractive_integral(0,1,vec,SuperCell[0],SuperCell[1]);
}
std::cout<<"------------------------INTEGRALS------------------------"<<std::endl<<std::flush;
std::cout<<"<W_1|H1|W_1>= "<<eps<<std::endl;
std::cout<<"<W_1|H1|W_2>= "<<t<<std::endl;
std::cout<<"<W_1 W_1|I|W_1 W_1>= "<<basis.v_integral(0,0,0,0)<<std::endl;
std::cout<<"<W_1 W_2|I|W_1 W_2>= "<<basis.v_integral(0,1,0,1,SuperCell[0],SuperCell[1],SuperCell[0],SuperCell[1])<<std::endl;


std::cout<<"<W_1|W_2>= "<<basis.overlap(0,1)<<std::endl<<std::flush;
std::cout<<"<W_1|W_1>= "<<basis.overlap(0,0)<<std::endl<<std::flush;
std::cout<<"<W_2|W_2>= "<<basis.overlap(1,1)<<std::endl<<std::flush;
std::cout<<"<W_2|W_1>= "<<basis.overlap(1,0)<<std::endl<<std::flush;


return 0;
}
