#include <iostream>
#include <cmath>
#include "../../headers/qmtsimplex.h"


double F(std::vector<double> args) {
 
  double x = args[0];
  double y = args[1];
  double z = args[2];
  double w = args[3];
  double q = args[4];

  return -cos(20*(x+y+z+w+q)) + 10*(x*x+y*y+z*z+w*w+q*q);
  
}


int main(){

  unsigned int number_of_steps=10;

  std::vector<std::vector<std::vector<double>>> points;

  points.push_back(std::vector<std::vector<double>>());
  points[0].push_back(std::vector<double>({-0.01,-0.01,0,0,0}));
  points[0].push_back(std::vector<double>({1,0,0,0,0}));
  points[0].push_back(std::vector<double>({0,1,0,0,0}));
  points[0].push_back(std::vector<double>({0,0,1,0,0}));
  points[0].push_back(std::vector<double>({0,0,0,1,0}));
  points[0].push_back(std::vector<double>({0,0,0,0,1}));


  qmt::QmtSimplex solver(1,5,std::function<double(std::vector<double>)>(F),points);
  
  
  double min_val;
  std::vector<double> min_args;
  solver.run(number_of_steps,0.0001,min_val,min_args);
  std::cout<<"+------------------------------------------------+"<<std::endl;
  std::cout<<"F("<<min_args[0]<<","<<min_args[1]<<","<<min_args[2]<<","<<min_args[3]<<","<<min_args[4]<<")= "<<min_val<<std::endl;


  qmt::QmtSimplex solver_random(16,5,std::function<double(std::vector<double>)>(F),0.0001,0.001);
   
  solver_random.run(number_of_steps,0.0001,min_val,min_args);
  std::cout<<"+------------------------------------------------+"<<std::endl;
  std::cout<<"F("<<min_args[0]<<","<<min_args[1]<<","<<min_args[2]<<","<<min_args[3]<<","<<min_args[4]<<")= "<<min_val<<std::endl;
  
return 0;
}
