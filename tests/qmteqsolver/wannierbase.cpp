#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "../../headers/qmtorbitals.h"
#include <iostream>
#include "../../headers/qmtbform.h"
#include "../../headers/qmteqsolver.h"
#include <math.h>



#define PI 3.14159265


int H2_molecule(double d, double alpha) {

//H2 Molecule alfa = 1, R = 1.43


    Slater1S slater2(0,0,0, alpha);
    Slater1S slater1(0,d,0, alpha);
    Wannier<Slater1S> wannier1;
    Wannier<Slater1S> wannier2;

    wannier1.addOrbital(slater1,0);
    wannier2.addOrbital(slater2,0);

    wannier1.addOrbital(slater2,1);
    wannier2.addOrbital(slater1,1);


    double **M = new double*[2];

    for(int i = 0; i < 2; ++i) {
        M[i] = new double[2];
    }



    std::vector<BilinearForm> forms;

    Wannier<Slater1S>::multiply(wannier1,wannier2,M,2);
    forms.push_back(BilinearForm(M,2));

    Wannier<Slater1S>::multiply(wannier1,wannier1,M,2);
    forms.push_back(BilinearForm(M,2,-1));

    double in[2] = { 1.0, 0.0 };
    double out[2] = {0.0, 0.0 };

    NonLinearSystemSolve(forms,in,out,1.0e-8);

    std::cout<<" "<<out[0]<<" "<<out[1];

    for(int i = 0; i < 2; ++i)
        delete [] M[i];
    delete [] M;


    return 0;
}


int H2_chain(double a, double b, double theta, double alpha) {

    double dx = a * cos(theta);
    double dy = a * sin(theta);

    Slater1S slater1(dx, dy, 0, alpha);
    Slater1S slater2(0, 0, 0, alpha);
    Slater1S slater3(b, 0, 0, alpha);
    Slater1S slater4(b + dx, dy, 0, alpha);
    Slater1S slater5(2*b, 0, 0, alpha);
    Slater1S slater6(2*b + dx, dy, 0, alpha);
    Slater1S slater7(-b, 0, 0, alpha);
    Slater1S slater8(-b+dx, dy, 0, alpha);


    typedef Wannier<Slater1S> wannier_t;

    wannier_t w1;
    wannier_t w2;
    wannier_t w3;
    wannier_t w4;


    w1.addOrbital(slater1,0);
    w1.addOrbital(slater2,1);
    w1.addOrbital(slater3,3);
    w1.addOrbital(slater4,2);
    w1.addOrbital(slater8,2);
    w1.addOrbital(slater7,4);

    w2.addOrbital(slater2,0);
    w2.addOrbital(slater1,1);
    w2.addOrbital(slater8,3);
    w2.addOrbital(slater3,2);
    w2.addOrbital(slater7,2);
    w2.addOrbital(slater4,4);

    w3.addOrbital(slater3,0);
    w3.addOrbital(slater4,1);
    w3.addOrbital(slater1,3);
    w3.addOrbital(slater2,2);
    w3.addOrbital(slater5,2);
    w3.addOrbital(slater6,4);


    w4.addOrbital(slater4,0);
    w4.addOrbital(slater3,1);
    w4.addOrbital(slater5,3);
    w4.addOrbital(slater1,2);
    w4.addOrbital(slater6,2);
    w4.addOrbital(slater2,4);

    int N = 5;

    double **M = new double*[N];

    for(int i = 0; i < N; ++i) {
        M[i] = new double[N];
    }

    std::vector<BilinearForm> forms;

    Wannier<Slater1S>::multiply(w1,w4,M,N);
    forms.push_back(BilinearForm(M,N));

    Wannier<Slater1S>::multiply(w1,w2,M,N);
    forms.push_back(BilinearForm(M,N));

    Wannier<Slater1S>::multiply(w1,w3,M,N);
    forms.push_back(BilinearForm(M,N));

    Wannier<Slater1S>::multiply(w2,w4,M,N);
    forms.push_back(BilinearForm(M,N));

    Wannier<Slater1S>::multiply(w1,w1,M,N);
    forms.push_back(BilinearForm(M,N,-1));

    double in[5] = { 1.0, 0.0, 0.0, 0.0, 0.0 };
    double out[5] = {0.0, 0.0, 0.0, 0.0, 0.0 };

    NonLinearSystemSolve(forms,in,out,1.0e-12);

    std::cout<<" "<<out[0]<<" "<<out[1]<<" "<<out[2]<<" "<<out[3]<<" "<<out[4];

    for(int i = 0; i <N; ++i)
        delete [] M[i];
    delete [] M;

    return 0;
}



int
main (void)
{
    /*
    std::cout<<"--------------------------------------"<<std::endl;
    std::cout<<"                H2 Molecule           "<<std::endl;
    std::cout<<"--------------------------------------"<<std::endl;


    for(int i = 0; i < 1000; ++i){
     double distance = i*0.1 + 0.1;
     std::cout<<distance<<"       ";
     H2_molecule(distance, 1);
     std::cout<<std::endl;
    }

    std::cout<<"--------------------------------------"<<std::endl;
    std::cout<<"                H2 Chain              "<<std::endl;
    std::cout<<"--------------------------------------"<<std::endl;
    */
    for(int i = 0; i < 10000; ++i) {
        double a = i*0.001 + 1;
        double b = 10 * a;
        double theta = 30.0 * PI / 180;

        std::cout<<a<<" "<<b<<" "<<theta<<"      ";
        H2_chain(a, b, theta, 1.5);
        std::cout<<std::endl;
    }

    return 0;
}
