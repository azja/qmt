#include <iostream>
#include "../../headers/qmtstate.h"
#include "../../headers/qmtbasisgenerator.h"
#include <bitset>
#include <vector>

int main(){
	qmt::QmtBasisGenerator generator;

	std::cout<<"###############################################"<<std::endl<<"unsigned long based generator"<<std::endl<<"###############################################"<<std::endl;
	
	typedef qmt::QmtNState<unsigned long,int> ul_state;
	std::vector<ul_state>  output;

	generator.generate(4,8,-2,output);
	
	for (const auto & item : output)
		std::cout<<item<<std::endl;

	std::cout<<"###############################################"<<std::endl<<"std::bitset<> based generator"<<std::endl<<"###############################################"<<std::endl;

	typedef qmt::QmtNState<std::bitset<8>,int> bs_state;
	std::vector<bs_state> output2;

	generator.generate<8>(1,8,output2);
	
	for (const auto & item : output2)
		std::cout<<item<<std::endl;

	std::cout<<"###############################################"<<std::endl<<"std::bitset<> based generator with mask"<<std::endl<<"###############################################"<<std::endl;

	std::vector<bs_state> output3;

	std::bitset<8> mask("00010001");

	generator.generate<8,6>(1,8,output3,mask);
	
	for (const auto & item : output3)
		std::cout<<item<<std::endl;

	return 0;
}
