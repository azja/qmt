#include <iostream>
#include "../../headers/qmtbitsettools.h"
#include <bitset>

int main(){

	std::bitset<10> one ("0000010101");
	std::bitset<10> two ("0000010100");

	std::cout << std::boolalpha; 

	std::cout<<one<<"  > "<<two<<": "<<qmt::bitsettools::gt<10>(one,two)<<std::endl;
	std::cout<<one<<"  < "<<two<<": "<<qmt::bitsettools::lt<10>(one,two)<<std::endl;
	std::cout<<one<<" >= "<<two<<": "<<qmt::bitsettools::ge<10>(one,two)<<std::endl;
	std::cout<<one<<" <= "<<two<<": "<<qmt::bitsettools::le<10>(one,two)<<std::endl;
	std::cout<<one<<" == "<<two<<": "<<qmt::bitsettools::eq<10>(one,two)<<std::endl;

	std::cout<<std::endl<<"+-----------------------------+"<<std::endl;

	std::cout<<two<<"++ = ";
	qmt::bitsettools::increment<10>(two);
	std::cout<<two<<std::endl;

	std::cout<<"+-----------------------------+"<<std::endl<<std::endl;

	std::cout<<one<<"  > "<<two<<": "<<qmt::bitsettools::gt<10>(one,two)<<std::endl;
	std::cout<<one<<"  < "<<two<<": "<<qmt::bitsettools::lt<10>(one,two)<<std::endl;
	std::cout<<one<<" >= "<<two<<": "<<qmt::bitsettools::ge<10>(one,two)<<std::endl;
	std::cout<<one<<" <= "<<two<<": "<<qmt::bitsettools::le<10>(one,two)<<std::endl;
	std::cout<<one<<" == "<<two<<": "<<qmt::bitsettools::eq<10>(one,two)<<std::endl;

	std::cout<<std::endl<<"+-----------------------------+"<<std::endl;

	std::cout<<two<<"++ = ";
	qmt::bitsettools::increment<10>(two);
	std::cout<<two<<std::endl;

	std::cout<<"+-----------------------------+"<<std::endl<<std::endl;

	std::cout<<one<<"  > "<<two<<": "<<qmt::bitsettools::gt<10>(one,two)<<std::endl;
	std::cout<<one<<"  < "<<two<<": "<<qmt::bitsettools::lt<10>(one,two)<<std::endl;
	std::cout<<one<<" >= "<<two<<": "<<qmt::bitsettools::ge<10>(one,two)<<std::endl;
	std::cout<<one<<" <= "<<two<<": "<<qmt::bitsettools::le<10>(one,two)<<std::endl;
	std::cout<<one<<" == "<<two<<": "<<qmt::bitsettools::eq<10>(one,two)<<std::endl;

return 0;
}
