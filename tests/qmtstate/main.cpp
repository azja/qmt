#include <iostream>
#include "../../headers/qmtstate.h"
#include <bitset>

int main(){

std::cout<<"############################################################################"<<std::endl;
std::cout<<"unsigned long based state:"<<std::endl;

	typedef qmt::QmtNState<ulong,int> ul_state;

	ul_state state1(16,20);
	ul_state state2(16,0);

	std::cout<<"state1: \t\t\t\t"<<state1<<std::endl;
	std::cout<<"state2: \t\t\t\t"<<state2<<std::endl;
	state2.ul_state::create_up(2);
	state2.ul_state::create_up(4);
	std::cout<<"cr(up,4)cr(up,2)state2: \t\t\b"<<state2<<std::endl;
	std::cout<<"<state1|cr(up,4)cr(up,2)|state2>: \t\b"<<ul_state::dot_product(state1,state2)<<std::endl;


std::cout<<"############################################################################"<<std::endl;
std::cout<<"std::bitset based state:"<<std::endl;

	typedef qmt::QmtNState<std::bitset<50>,int> bs_state;

	bs_state state3(50,20);
	bs_state state4(50,0);

	std::cout<<"state3: \t\t\t\t"<<state3<<std::endl;
	std::cout<<"state4: \t\t\t\t"<<state4<<std::endl;
	state4.bs_state::create_up(2);
	state4.bs_state::create_up(4);
	std::cout<<"cr(up,4)cr(up,2)state4: \t\t\b"<<state4<<std::endl;
	std::cout<<"compare state3 and cr(up,4)cr(up,2)state4: "<<bs_state::compare_configuration(state3,state4)<<std::endl;
	std::cout<<"<state3|cr(up,4)cr(up,2)|state4>: \t\b"<<bs_state::dot_product(state3,state4)<<std::endl;
	state4.bs_state::anihilate_up(4);
	std::cout<<"an(up,4)cr(up,4)cr(up,2)state4: \t"<<state4<<std::endl;
	state4.bs_state::anihilate_up(4);
	std::cout<<"an(up,4)an(up,4)cr(up,4)cr(up,2)state4: "<<state4<<std::endl;
	std::cout<<"compare state3 and an(up,4)cr(up,4)cr(up,4)cr(up,2)state4: "<<bs_state::compare_configuration(state3,state4)<<std::endl;

std::cout<<"############################################################################"<<std::endl;
std::cout<<"std::bitset based state dot_product:"<<std::endl;

	bs_state state5(50,10);
	bs_state state6(50,20);
	bs_state state7(50,30);

	std::vector<bs_state> one;
	std::vector<bs_state> two;

	one.push_back(state5);
	one.push_back(state6);
	one.push_back(state7);
	two.push_back(state5);
	two.push_back(state7);

	std::cout<<"state5: \t\t\t\t"<<state5<<std::endl;
	std::cout<<"state6: \t\t\t\t"<<state6<<std::endl;
	std::cout<<"state7: \t\t\t\t"<<state7<<std::endl;
	std::cout<<"(state5 state6 state7) * (state5 state7) = "<<bs_state::dot_product(one,two)<<std::endl;
	return 0;
}
