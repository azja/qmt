/*Andrzej Biborski 11.02.2014*/

#include "../../headers/mpireduce.h"
#include "../../headers/SolverSGA.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

class TestFunctor {

    double _x;

public:

    typedef  double returned_value;
    explicit TestFunctor(double x) {
        _x = x;
    }

    double operator()() const {
        return _x;
    }

};

/*****************************************************************************/

class TestFunctorParameter {

    double _x;

public:

    typedef  double returned_value;
    explicit TestFunctorParameter(double x) {
        _x = x;
    }

    returned_value operator()(returned_value arg) const {
        return _x*arg;
    }

};


/*****************************************************************************/

class SolverSgaFunctor {

    Solver_SGA* _solver;
    FILE *ext,*err;

public:
    typedef  double returned_value;

    explicit SolverSgaFunctor(double h, double id) {

        std::stringstream id_text;
        id_text<<id;

        std::string ferror_name = "error" + id_text.str() + ".dat";
        std::string fext_name = "ext" + id_text.str() + ".dat";

        err = fopen(ferror_name.c_str(),"w+");
        ext = fopen(fext_name.c_str(),"w+");

        _solver =  new Solver_SGA(1000,-1, 1.5,4.1000,h,err,ext,false);
    }

    double operator()() const {
        s_SGA_out outcome0= {0.01, 0.0001, 0.001, 0., -0.};
        double result = _solver->solve(outcome0.d,outcome0.m,outcome0.lmb_m,outcome0.mu,outcome0.lmb_n).EG;
        return result;
    }


    ~SolverSgaFunctor() {

        fclose(err);
        fclose(ext);
        delete _solver;

    }
};

/*****************************************************************************/

int main() {


    int taskid;

    MPI_Init(NULL,NULL);
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);


    SolverSgaFunctor functor(0.0000427663*taskid*0.01, taskid);
    QmtMPIReduce<SolverSgaFunctor,MPI_MAX> reductor_1(functor);
    reductor_1.reduce();


    if(taskid == 0) {
        std::cout<<"Result is:"<<reductor_1.get_result()<<std::endl;
    }

    MPI_Barrier(MPI_COMM_WORLD);

    TestFunctorParameter functor_p(taskid);
    QmtMPIReduce<TestFunctorParameter,MPI_MAX> reductor_2(functor_p);
    reductor_2.reduce( static_cast<double>(taskid));


    if(taskid == 0) {
        std::cout<<"Result is:"<<reductor_2.get_result()<<std::endl;
    }


    MPI_Finalize();


    return 0;
}
