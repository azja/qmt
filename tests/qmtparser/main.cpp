#include <iostream>
#include <vector>
#include <unordered_map>
#include "../../headers/qmtparsertools.h"
#include "../../headers/qmthbuilder.h"
#include "../../headers/qmtvector.h"
#include <vector>

int main() {

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::get_delimited_words:"<<std::endl;

std::string delimiters(",.:/ }");
std::string phrase("//.,Ania:..Basia,Tomek,Ba rtek,:");

std::cout<<"INPUT:"<<std::endl<<phrase<<std::endl;

std::vector<std::string>  output = qmt::parser::get_delimited_words(delimiters,phrase);

std::cout<<"OUTPUT:"<<std::endl;
for(const auto &out : output)
  std::cout<<out<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::get_delimited_words (on strings):"<<std::endl;

std::vector<std::string> delimiters2({"Ania","Tomek","Basia"});

std::cout<<"INPUT:"<<std::endl<<phrase<<std::endl;

output = qmt::parser::get_delimited_words(delimiters2,phrase);

std::cout<<"OUTPUT:"<<std::endl;
for(const auto &out : output)
  std::cout<<out<<std::endl; 

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::stringToOrbitalType:"<<std::endl;

std::string orbital1("3s");
std::string orbital2("2p_zp");

std::cout<<"INPUT:"<<std::endl<<orbital1<<std::endl<<orbital2<<std::endl;

auto orb1 = qmt::parser::stringToOrbitalType(orbital1);
auto orb2 = qmt::parser::stringToOrbitalType(orbital2);

std::cout<<"OUTPUT:"<<std::endl<<orb1<<std::endl<<orb2<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::get_QmtVector_from_line:"<<std::endl;

std::string vector1("(0,0,0)");
std::string vector2("(0.123,0.12419201,0.1213412412312341243123)");

std::cout<<"INPUT:"<<std::endl<<vector1<<std::endl<<vector2<<std::endl;

auto vec1 = qmt::parser::get_QmtVector_from_line(vector1);
auto vec2 = qmt::parser::get_QmtVector_from_line(vector2);

std::cout<<"OUTPUT:"<<std::endl<<vec1<<std::endl<<vec2<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::get_QmtVector_from_file:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"file: vectors.dat"<<std::endl<<"OUTPUT: "<<std::endl;
std::vector<qmt::QmtVector> myvectors = qmt::parser::get_QmtVector_from_file("vectors.dat");

for (const auto& vec : myvectors)
   std::cout<<vec<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::remove_whitespace:"<<std::endl;

std::string with_whites("One \t Two\n\nThree\n");

std::cout<<"INPUT:"<<std::endl<<with_whites<<std::endl;

qmt::parser::remove_whitespace(with_whites);

std::cout<<"OUTPUT:"<<std::endl<<with_whites<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::remove_comments:"<<std::endl;

std::string with_comments("Lorem ipsum dolor sit amet, consectetur adipiscing elit, \nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \n#Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \nDuis aute irure dolor #in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n#Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

std::cout<<"INPUT:"<<std::endl<<with_comments<<std::endl;

qmt::parser::remove_comments(with_comments);

std::cout<<"OUTPUT:"<<std::endl<<with_comments<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::get_num_states:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"file: ham.dat"<<std::endl<<"OUTPUT: "<<std::endl;

std::cout<<qmt::parser::hamiltonian::get_num_states("ham.dat")<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::get_num_one_body:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"file: ham.dat"<<std::endl<<"OUTPUT: "<<std::endl;

std::cout<<qmt::parser::hamiltonian::get_num_one_body("ham.dat")<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::get_num_two_body:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"file: ham.dat"<<std::endl<<"OUTPUT: "<<std::endl;

std::cout<<qmt::parser::hamiltonian::get_num_two_body("ham.dat")<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::compare_supercell:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"files: ham.dat, supercell.dat"<<std::endl<<"OUTPUT: "<<std::endl;

qmt::parser::hamiltonian::QmtAtomMap map_supercell;
qmt::parser::hamiltonian::compare_supercell("ham.dat","supercell.dat",map_supercell);

for(const auto& atom : map_supercell)
  std::cout<<atom.first<<" "<<atom.second<<std::endl;

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::compare_megacell:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"files: ham.dat, megacell.dat"<<std::endl<<"OUTPUT (first 13): "<<std::endl;

qmt::parser::hamiltonian::QmtAtomMap map_megacell;
qmt::parser::hamiltonian::compare_megacell("ham.dat","megacell.dat",map_megacell);

unsigned int cntr=0;
for(const auto& atom : map_megacell){
  if(atom.second == 0)
    std::cout<<atom.first<<" "<<atom.second<<std::endl;
//  if(++cntr>=13)
//    break;
}

std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::compare_one_body:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"files: ham.dat"<<std::endl<<"OUTPUT (only index 0): "<<std::endl;

qmt::parser::hamiltonian::QmtOneBodyMap map_one_body;
qmt::parser::hamiltonian::compare_one_body("ham.dat",map_one_body);

for(const auto& igrl : map_one_body)
  if(!igrl.second)
    std::cout<<std::get<0>(igrl.first)<<" "<<std::get<1>(igrl.first)<<" "<<igrl.second<<std::endl;


std::cout<<"+-------------------------------------------------+"<<std::endl;
std::cout<<"test qmt::parser::hamiltonian::compare_two_body:"<<std::endl;

std::cout<<"INPUT:"<<std::endl<<"files: ham.dat"<<std::endl<<"OUTPUT (only index 0): "<<std::endl;

qmt::parser::hamiltonian::QmtTwoBodyMap map_two_body;
qmt::parser::hamiltonian::compare_two_body("ham.dat",map_two_body);

for(const auto& igrl : map_two_body)
  if(!igrl.second)
    std::cout<<std::get<0>(igrl.first)<<" "<<std::get<1>(igrl.first)<<" "<<std::get<2>(igrl.first)<<" "<<std::get<3>(igrl.first)<<" "<<igrl.second<<std::endl;

return 0;
}
