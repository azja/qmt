#include <iostream>
#include <tuple>
#include <unordered_set>
#include "../../headers/qmtvector.h"
#include "../../headers/qmthbuilder.h"
#include "../../headers/qmtintegrals.h"

int main(){

  qmt::integrals::unordered_set_two_body myset;
  qmt::integrals::unordered_set_one_body myset2;

  for(unsigned int i=0; i<4; ++i){
    std::vector<qmt::QmtVectorizedAtom> vec;
    for(unsigned int j=0; j<4; ++j){
      qmt::QmtVector v((double)i,0.,(double)j);

      vec.push_back(qmt::QmtVectorizedAtom(v,j));
    }

     myset.insert(qmt::integrals::two_body(vec[0],vec[1],vec[2],vec[3]));
    myset2.insert(qmt::integrals::one_body(vec[0],vec[1]));
  }


  for(unsigned int i=0; i<4; ++i){
    std::vector<qmt::QmtVectorizedAtom> vec;
    for(unsigned int j=0; j<4; ++j){
      qmt::QmtVector v((double)i,0.,(double)j);

      vec.push_back(qmt::QmtVectorizedAtom(v,j));
    }

    myset.insert(std::tuple<qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom,qmt::QmtVectorizedAtom>(vec[0],vec[1],vec[2],vec[3]));
    myset2.insert(qmt::integrals::one_body(vec[0],vec[1]));
    myset2.insert(qmt::integrals::one_body(vec[1],vec[0]));
  }

  for(const auto& atom : myset)
    std::cout<<std::get<0>(atom)<<" "<<std::get<1>(atom)<<" "<<std::get<2>(atom)<<" "<<std::get<3>(atom)<<std::endl;

  for(const auto& atom : myset2)
    std::cout<<std::get<0>(atom)<<" "<<std::get<1>(atom)<<std::endl;

  return 0;
}
