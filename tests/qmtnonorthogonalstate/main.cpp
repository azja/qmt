#include <iostream>
#include "../../headers/qmtnonorthogonalstate.h"
#include <bitset>

int main(){


std::cout<<"############################################################################"<<std::endl;
std::cout<<"std::bitset based state:"<<std::endl;

	typedef qmt::QmtNonOrthogonalNState<50> bs_state;

	bs_state state3(50,std::bitset<50>(20));
	bs_state state4(50,std::bitset<50>(0));

	std::cout<<"state3: \t\t\t\t"<<state3<<std::endl;
	std::cout<<"state4: \t\t\t\t"<<state4<<std::endl;
	state4.bs_state::create_up(2);
	state4.bs_state::create_up(4);
	std::cout<<"cr(up,4)cr(up,2)state4: \t\t\b"<<state4<<std::endl;
	std::cout<<"compare state3 and cr(up,4)cr(up,2)state4: "<<bs_state::compare_configuration(state3,state4)<<std::endl;
	std::cout<<"<state3|cr(up,4)cr(up,2)|state4>: \t\b"<<bs_state::dot_product(state3,state4)<<std::endl;
	state4.bs_state::anihilate_up(4);
	std::cout<<"an(up,4)cr(up,4)cr(up,2)state4: \t"<<state4<<std::endl;
	state4.bs_state::anihilate_up(4);
	std::cout<<"an(up,4)an(up,4)cr(up,4)cr(up,2)state4: "<<state4<<std::endl;
	std::cout<<"compare state3 and an(up,4)cr(up,4)cr(up,4)cr(up,2)state4: "<<bs_state::compare_configuration(state3,state4)<<std::endl;

std::cout<<"############################################################################"<<std::endl;
std::cout<<"std::bitset based state dot_product:"<<std::endl;

	bs_state state5(50,std::bitset<50>(10));
	bs_state state6(50,std::bitset<50>(20));
	bs_state state7(50,std::bitset<50>(30));

	std::vector<bs_state> one;
	std::vector<bs_state> two;

	one.push_back(state5);
	one.push_back(state6);
	one.push_back(state7);
	two.push_back(state5);
	two.push_back(state7);

	std::cout<<"state5: \t\t\t\t"<<state5<<std::endl;
	std::cout<<"state6: \t\t\t\t"<<state6<<std::endl;
	std::cout<<"state7: \t\t\t\t"<<state7<<std::endl;
//	std::cout<<"(state5 state6 state7) * (state5 state7) = "<<bs_state::dot_product(one,two)<<std::endl;
	return 0;
}
