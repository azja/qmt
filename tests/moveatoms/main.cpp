#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include "../../headers/qmtparsertools.h"
#include "../../headers/qmthbuilder.h"
#include "../../headers/qmtvector.h"
#include <vector>

struct TolerandDoubleEq{
  double eps=1000;
  bool operator()(const double& l, const double& r)const{
    return fabs(r-l)<eps;
  }
};

struct TolerandDoubleLess{
  double eps=1e-1;

 bool operator()(const double& l, const double& r)const{
   return l<r;
 }
};

void move(unsigned int op, unsigned int n_one, unsigned int n_two,
          qmt::QmtVector dr,
          qmt::parser::hamiltonian::QmtAtomMap& map_supercell,
          qmt::parser::hamiltonian::QmtAtomMap& map_megacell,
          qmt::parser::hamiltonian::QmtOneBodyMap& map_one_body,
          qmt::parser::hamiltonian::QmtTwoBodyMap& map_two_body){

  std::vector<std::shared_ptr<std::map<double,unsigned int, TolerandDoubleLess>>> dict_one_body(n_one);
  std::vector<std::shared_ptr<std::map<double,unsigned int>>> dict_two_body(n_two);

  for(auto& ptr : dict_one_body)
    ptr = std::shared_ptr<std::map<double,unsigned int, TolerandDoubleLess>>(new std::map<double,unsigned int, TolerandDoubleLess>);

  std::vector<unsigned int> idx_one(n_one,0U);
  std::vector<unsigned int> idx_two(n_two,0U);

  for(auto& one : map_one_body){
  
    int condition = static_cast<int>(map_megacell.at(std::get<0>(one.first)) == op) |
                   -static_cast<int>(map_megacell.at(std::get<1>(one.first)) == op);

    if(condition){
      double distance = qmt::QmtVector::norm(std::get<0>(one.first).position - std::get<1>(one.first).position + condition*dr);
      std::cout<<distance<<" "<<std::get<0>(one.first)<<" "<<std::get<1>(one.first)<<" "<<one.second<<std::endl;

      auto map_ptr = dict_one_body[one.second];

//      (*map_ptr)[distance] = idx_one[one.second]++;

      auto found = map_ptr->find(distance);
      if(found == map_ptr->end())
	map_ptr->insert(std::pair<double,unsigned int>(distance,idx_one[one.second]++));
      else
        std::cout<<distance<<" "<<found->first<<found->second<<std::endl;
    }
  }

  for (auto i=0U; i<dict_one_body.size(); ++i)
    for (const auto& j : *(dict_one_body[i]))
      std::cout<<i<<" "<<j.first<<" "<<j.second<<std::endl;

//  for(auto& two : map_two_body){
//    if(map_megacell.at(std::get<0>(two.first)) == op ||
//       map_megacell.at(std::get<1>(two.first)) == op ||
//       map_megacell.at(std::get<2>(two.first)) == op ||
//       map_megacell.at(std::get<3>(two.first)) == op ){
//      std::cout<<std::get<0>(two.first)<<" "<<std::get<1>(two.first)<<" "<<std::get<2>(two.first)<<" "<<std::get<3>(two.first)<<" "<<two.second<<std::endl;
//    }
//  }
}


int main() {

std::string hamiltonian("ham.dat");
std::string   supercell("supercell.dat");
std::string    megacell("megacell.dat");

qmt::parser::hamiltonian::QmtAtomMap map_supercell;
qmt::parser::hamiltonian::QmtAtomMap map_megacell;
qmt::parser::hamiltonian::QmtOneBodyMap map_one_body;
qmt::parser::hamiltonian::QmtTwoBodyMap map_two_body;
qmt::parser::hamiltonian::compare_supercell(hamiltonian,supercell,map_supercell);
qmt::parser::hamiltonian::compare_megacell(hamiltonian,megacell,map_megacell);
qmt::parser::hamiltonian::compare_one_body(hamiltonian,map_one_body);
qmt::parser::hamiltonian::compare_two_body(hamiltonian,map_two_body);

unsigned int n_one = qmt::parser::hamiltonian::get_num_one_body(hamiltonian);
unsigned int n_two = qmt::parser::hamiltonian::get_num_two_body(hamiltonian);

move(0,n_one,n_two,qmt::QmtVector(0,0,0),map_supercell,map_megacell,map_one_body,map_two_body);

return 0;
}
