#ifndef QMT_VMC_SLATER_STATE
#define QMT_VMC_SLATER_STATE


#include <vector>
#include<tuple>
#include "qmtconfiguration.h"
#include <gsl/gsl_linalg.h>
#include <omp.h>
namespace qmt {
namespace vmc {
/*
struct QmtConfigurationWMatrix {
std::vector<double> W;
std::vector<int> enumerator;
size_t from_index;
size_t to_index
size_t beta;
size_t m;
size_t n;

void jump(size_t from,size_t to) {
int temp = enumerator[from];
enumerator[from] = -1;
enumerator[to] = temp;
}

};
*/
class QmtParticleStateExpansion {

    double energy ; //single particle energy
    std::vector<double> up_c;
    std::vector<double> down_c;
    gsl_matrix *W;
   
public:

    virtual double get_up_c(size_t index) const;
    virtual double get_down_c(size_t index) const;
    virtual double get_energy(void) const;

    virtual size_t get_size_up(void)   const;
    virtual size_t get_size_down(void) const;


    virtual bool operator <(const QmtParticleStateExpansion& right) const;

    QmtParticleStateExpansion(const std::vector<double>& ups,
                              const std::vector<double>& downs);
    
    virtual ~QmtParticleStateExpansion() {}

};


///////////////////////////////////////////////////////////////////////////////


class QmtSlaterState {
    std::vector<QmtParticleStateExpansion*> states;

protected:

    size_t get_beta_index(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to) const;
    int get_to_index(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to) const;
    size_t get_from_index(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to) const;
    
    bool W_generated;
    gsl_matrix* W;
    gsl_matrix* W_copy;
    std::vector<int> electron_map;
    std::vector<int> electron_ids; //vector index is electron number, entry is the the number of band it occupies
public:

    QmtSlaterState():W_generated(false),W(NULL),W_copy(NULL){}

    void add(QmtParticleStateExpansion* state);
    void reset(); //clear all states
    
    std::vector<double> get_D_matrix_column(size_t col_index,
                                            const QmtConfigurationState& configuration) const;

     double get_W_ratio(const QmtConfigurationState& conf_from,
					    const QmtConfigurationState& conf_to) const;
 void recalculate_W_matrix(const QmtConfigurationState& conf_from);
 double update_W_matrix(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to);
 double simple_take(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to);
 bool check_configuration(const QmtConfigurationState& configuration);
 void reset_probability_matrix();
 double get_two_node_ratio(size_t i, size_t j, size_t k, size_t l);
 
    ~QmtSlaterState() {
     /* if(W_generated) {
         gsl_matrix_free(W);
         gsl_matrix_free(W_copy);
      }*/
     }
};






}


}
#endif// qmtslater.h


