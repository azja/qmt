#include "qmtvmcstatgenerator.h"

qmt::vmc:: QmtVmcStatGenerator::
	 QmtVmcStatGenerator(const char* filename) {
  std::string line;
  std::ifstream file(filename);
  if (file.is_open())
  {
    while ( getline (file,line) )
    {
      definitions.push_back(line);
    }
    file.close();
  }

}


///////////////////////////////////////////////////////////////////////////////

std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_n(const std::string& definition) {
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;

for(const auto& item : defs){
result.push_back(new qmt::vmc::QmtVmcLocalN(std::stoul(item)));
}
return result;
}

///////////////////////////////////////////////////////////////////////////////

std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_nn(const std::string& definition) {
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;

for(const auto& item : defs) {
 auto idx = qmt::parser::get_delimited_words(",",item);
 if(idx.size() < 2U) continue;
result.push_back(new qmt::vmc::QmtVmcLocalNN(std::stoul(idx[0]),std::stoul(idx[1])));
}

return result;
}

///////////////////////////////////////////////////////////////////////////////

std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_hop(const std::string& definition,qmt::vmc::QmtVmcData& vmc_data){
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;
auto spinstr = qmt::parser::get_bracketed_words(definition,'[',']',true);
int spin;
if(spinstr[0] == "up") spin = 1;
else spin = -1;

for(const auto& item : defs) {
 auto idx = qmt::parser::get_delimited_words(",",item);
 if(idx.size() < 2U) continue;
result.push_back(new QmtVmcLocalHopping(std::stoul(idx[0]),std::stoul(idx[1]),spin,vmc_data.map,vmc_data.prob_calc));
}

return result;
}


///////////////////////////////////////////////////////////////////////////////


std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_sdimdim(const std::string& definition) {
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;

for(const auto& item : defs) {
 auto idx = qmt::parser::get_delimited_words(",",item);
 if(idx.size() < 2U) continue;
result.push_back(new qmt::vmc::QmtVmcLocalSDimDim(std::stoul(idx[0]),std::stoul(idx[1])));
}

return result;
}

///////////////////////////////////////////////////////////////////////////////



std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_m(const std::string& definition) {
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;

for(const auto& item : defs){
result.push_back(new qmt::vmc::QmtVmcLocalM(std::stoul(item)));
}
return result;
}

///////////////////////////////////////////////////////////////////////////////

std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_ss(const std::string& definition) {
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;

for(const auto& item : defs) {
 auto idx = qmt::parser::get_delimited_words(",",item);
 if(idx.size() < 2U) continue;
result.push_back(new qmt::vmc::QmtVmcLocalSS(std::stoul(idx[0]),std::stoul(idx[1])));
}

return result;
}
/////////////////////////////////////////////////////////////////////////////

std::vector<qmt::vmc::QmtVmcLocalObservable*>  qmt::vmc::QmtVmcStatGenerator::get_d(const std::string& definition) {
auto defs = qmt::parser::get_bracketed_words(definition,'(',')',true);
std::vector<qmt::vmc::QmtVmcLocalObservable*> result;

for(const auto& item : defs){
result.push_back(new qmt::vmc::QmtVmcLocalD(std::stoul(item)));
}
return result;
}

///////////////////////////////////////////////////////////////////////////////



void qmt::vmc::QmtVmcStatGenerator::generate(qmt::vmc::QmtVmcData& vmc_data) {

std::vector<qmt::vmc::QmtVmcLocalObservable*> results;

for(const auto& definition : definitions) {
auto def = qmt::parser::get_bracketed_words(definition,'<','>',true);


if(def.size() < 1U) continue;

auto add = [&](std::vector<qmt::vmc::QmtVmcLocalObservable*> r)->void{
  for(auto item : r)
  results.push_back(item);
};

if(def[0] == "n") add(get_n(definition));
if(def[0] == "nn")add(get_nn(definition));
if(def[0] == "hopping")add(get_hop(definition,vmc_data));
if(def[0] == "sdimdim")add(get_sdimdim(definition));
if(def[0] == "m")add(get_m(definition));
if(def[0] == "ss")add(get_ss(definition));
if(def[0] == "d")add(get_d(definition));
}

//if(vmc_data.stat != NULL)
//delete(vmc_data.stat);
vmc_data.stat = new qmt::vmc::QmtVmcStat(results);
}
