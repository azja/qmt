#ifndef QMTVMCPROBABILITYGSL_H
#define QMTVMCPROBABILITYGSL_H

#include "qmtvmcprob.h"

#include "qmtconfiguration.h"
#include "qmtvmcjastrow.h"
#include "qmtconfbitset.h"
#include "qmtslaterstate.h"
#include <vector>
#include <omp.h>

#define _QMTVMC_GSL

#ifdef _QMTVMC_GSL
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_permutation.h>
#endif

#ifdef _QMTVMC_GSL
namespace qmt {
namespace vmc {
template<size_t NUM_SITES>
class QmtVmcProbabilityMultiJstr : public QmtVmcProbability {
private:
    std::vector<double> parameters;
    QmtVmcTwoBodyJastrow jastrow_correlator;
    QmtSlaterState* uncorrelated_state;
    bool PARAMETERS_SET;

  

public:
    QmtVmcProbabilityMultiJstr(size_t num_of_electrons, QmtVmcTwoBodyJastrow _jastrow_correlator, QmtSlaterState* _uncorrelated_state):jastrow_correlator(_jastrow_correlator) {
        uncorrelated_state=_uncorrelated_state;
        PARAMETERS_SET=false;

       
    }

    virtual ~QmtVmcProbabilityGSL() {
     
    
    }

    
    virtual double product(const QmtConfigurationState* x) const {
      
        return 0;
    }
    

virtual void reset() {
 uncorrelated_state->reset_probability_matrix();
}



virtual double probability(const QmtConfigurationState* x, const QmtConfigurationState* xprim) {
        
std::cout<<"WARNING:  QmtVmcProbabilityMultiJstr::probability(const QmtConfigurationState* x, const QmtConfigurationState* xprim) has nothing to do within this instance - returning zero";

return  0;
}



virtual double probability((const QmtConfigurationState* from, const QmtConfigurationState* to, size_t i, size_t j){

   double jastrow_coeff = jastrow_correlator(parameters,*x,i,j);
   double ratio = uncorrelated_state->update_W_matrix(*x,*xprim) * (xprim->get_phase() / x->get_phase());
   double prob = pow( jastrow_coeff * (ratio),2);
   return  prob>1.0 ? 1.0 : prob;
}



virtual double density_ratio(const QmtConfigurationState* from,const QmtConfigurationState* to, size_t i, size_t j) const {
        if(!PARAMETERS_SET) {
            throw std::invalid_argument ("QmtVmcProbablityGSL::density_ratio: parameters not set!");
        }



        double jastrow_coeff = jastrow_correlator(parameters,*from,i,j);

	double ratio = uncorrelated_state->update_W_matrix(*from,*to)* (to->get_phase() / from->get_phase());

        double prob =  jastrow_coeff * (ratio);



        return prob;
    }

virtual void set_parameters(const std::vector<double>& _parameters) {
        PARAMETERS_SET=true;
        parameters=_parameters;
}


double jastrow(const QmtConfigurationState* x) const {
std::cout<<"WARNING:  QmtVmcProbabilityMultiJstr::jastrow(const QmtConfigurationState* x) has nothing to do within this instance - returning zero";
        return 0;
}


double jastrow(const QmtConfigurationState* x,const QmtConfigurationState* y) const {
std::cout<<"WARNING:  QmtVmcProbabilityMultiJstr::jastrow(const QmtConfigurationState* x,const QmtConfigurationState* y) has nothing to do within this instance - returning zero";
        return jastrow_correlator(parameters,*x,*y);
}

double two_nodes(size_t i,size_t j, size_t k, size_t l) const {
return uncorrelated_state->get_two_node_ratio(i,j,k,l)*jastrow_correlator(parameters,i,j,k,l);
}


}; // end of class QmtVmcProbablityGSL

} // end of namespace vmc
} //end of namespace qmt
#endif //_QMTVMC_GSL

#endif // QMTVMCPROBABILITYGSL_H
