#include "qmtvmcgsloptimizer.h"

double qmt::vmc::QmtVmcGslOptimizer::get_value(const gsl_vector* x, void* params)
{
  Params *p = static_cast<Params*>(params);
  
  
  
  
  std::vector<double> v_p;
//  v_p.push_back(0);
  for(size_t i = 0U; i < x->size; ++i){
   v_p.push_back(gsl_vector_get(x,i));
  // std::cout<<gsl_vector_get(x,i)<<" ";
//   if(i > 0)
//    if(v_p[i] > v_p[i-1]) return 5;
  }
  std::cout<<std::endl;
  //v_p.push_back(0);

//  v_p[v_p.size() - 1] = 0.0;
  double tot = 0;
  
  for(auto item : v_p){
   tot += item;
  //if( item <0 || item > 1.0  ) return 5.0; 
  }
   
//  if( fabs(tot) >1.0) return 5.0;
  
  p->d->prob_calc->set_parameters(v_p);
  p->alg->run(p->steps);
  return    p->alg->get_variance_estimator(p->probing_steps);
}



double qmt::vmc::QmtVmcGslOptimizer::optimize( qmt::vmc::QmtVmcData& d, 
					       qmt::vmc::QmtVmcAlgorithm& alg,
					      const std::vector< std::tuple< double, double, double > >& in_par, 
					      std::vector< double >& out_par,
					      size_t steps, 
					      size_t probing_steps) {
  out_par.clear();
  Params p;
  p.d = &d;
  p.alg = &alg;
  p.probing_steps=probing_steps;
  p.steps = steps;
  
  int dim = 5;//in_par.size();// - 1 ;
  
  const gsl_multimin_fminimizer_type *T = 
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;
  
  

  /* Starting point */
  x = gsl_vector_alloc (dim);

 // for(int i = 0; i < dim; ++i)
//    gsl_vector_set (x, i, 0);
 // get_value(x,(void*)(&p));


for(int i = 0; i < dim; ++i)
    gsl_vector_set (x, i, std::get<1>(in_par[i]));
  
  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc (dim);
  gsl_vector_set_all (ss, 0.1);

  /* Initialize method and iterate */
  minex_func.n = dim;
  minex_func.f = get_value;
  minex_func.params = &p;

  s = gsl_multimin_fminimizer_alloc (T, dim);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      
//      if (status) 
//        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-5);
//      std::cout<<"ITERATION #"<<iter<<std::endl;
    }

  while (status == GSL_CONTINUE && iter < 100);
    
  double result = 0.0;
  
  out_par.clear();
//  out_par.push_back(0);
  for(int i = 0; i < dim;++i)
   out_par.push_back(gsl_vector_get(s->x,i));
   
  //out_par.push_back(0.0);
  
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);
  p.d->prob_calc->set_parameters(out_par);
  std::cout<<"ITEMS"<<std::endl;
  for(auto item : out_par)
  std::cout<<item<<" ";
  
  std::cout<<std::endl;
  std::vector<std::tuple<double,double,double>> input_cycle;

  
  static size_t cycle = 0U;
  std::cout<<"---------------------------------------------------------------------------"<<std::endl;
  std::cout<<"Cycle #"<<cycle<<std::endl;
  std::cout<<"---------------------------------------------------------------------------"<<std::endl;
  for(auto item : out_par)
  std::cout<<item<<" ";
  
  //if( cycle < 1) {
  //p.alg->run(p.steps - p.probing_steps);
  result =   p.alg->get_local_energy(p.probing_steps);
  std::cout<<"Result ="<<result<<std::endl;
//  input_cycle.clear();
 // for(auto item : out_par)
  // input_cycle.push_back(std::make_tuple(0,item,0));
  //cycle++;
  //optimize(d,alg, input_cycle,out_par,steps,probing_steps);
  
  //}
  
  
return result;
}
