#ifndef _QMTVMCPROB_H
#define _QMTVMCPROB_H

#include "qmtconfiguration.h"
#include <vector>

namespace qmt {
namespace vmc {

class QmtVmcProbability {
public:
    virtual double probability(const QmtConfigurationState* x, const QmtConfigurationState* xprim) = 0;
    virtual double density_ratio(const QmtConfigurationState* x,const  QmtConfigurationState* xprim) const = 0;
    virtual double product(const QmtConfigurationState* x) const = 0;
    virtual double two_nodes(size_t i,size_t j, size_t k, size_t l) const {return 0;}
/*
    virtual double probability(const QmtConfigurationState* from, const QmtConfigurationState* to, size_t i, size_t j){return 0;} ;
    virtual double density_ratio(const QmtConfigurationState* from, const QmtConfigurationState* to, size_t i, size_t j){return 0;};
    virtual void update_jastrow_after_jump(size_t i, size_t j,std::vector<double> parameters){};*/

    virtual double jastrow(const QmtConfigurationState* x) const = 0;
    virtual double jastrow(const QmtConfigurationState* x,const QmtConfigurationState* y) const = 0;
    virtual void set_parameters(const std::vector<double>& parameters) = 0;
    virtual void reset() {}
    virtual void update(const QmtConfigurationState* x) {} //Must update both Jastrow and density
    virtual ~QmtVmcProbability() {};
}; // end of class QmtVmcProbablity





} // end of namespace vmc
} //end of namespace qmt
#endif //_QMTVMCPROB_H
