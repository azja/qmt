#ifndef QMTVMCLOCALOBSERVABLE_H
#define QMTVMCLOCALOBSERVABLE_H

#include <iostream>
#include "qmtconfiguration.h"

namespace qmt {
 namespace vmc {

class QmtVmcLocalObservable {

virtual std::ostream& print(std::ostream&) const = 0;

public:

virtual double get_value(const QmtConfigurationState& x) = 0;

friend std::ostream& operator << (std::ostream& os, const QmtVmcLocalObservable& b) {
      return b.print(os);
 }

virtual ~QmtVmcLocalObservable(){}

};



 }
}
#endif
