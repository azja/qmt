#ifndef QMTVMCLOCALM_H
#define QMTVMCLOCALM_H
#include "qmtvmclocalobs.h"

namespace qmt {
namespace vmc {

class QmtVmcLocalM : public QmtVmcLocalObservable {

size_t _id;
std::ostream& print(std::ostream& s) const;
public:

QmtVmcLocalM(size_t id);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);

};

}
}
#endif
