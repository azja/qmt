#ifndef QMT_VMC_ALGORITHM_H
#define QMT_VMC_ALGORITHM_H

#include<vector>
#include "qmtvmcdata.h"
#include "qmtvmcstat.h"

namespace qmt {
namespace vmc {

class QmtVmcAlgorithm {
public:

    /*
     * Return energy after each n_steps of simulation
     */

    virtual double get_local_energy(size_t n_steps) = 0; //returns energy probed on MC steps = n_steps
    virtual double get_observables(size_t n_steps,qmt::vmc::QmtVmcStat& stat){ return 0;}
    virtual double get_variance_estimator(size_t n_steps) = 0; //returns variance probed on MC steps = n_steps
    virtual void run(size_t n_steps) = 0;		 //just probe configuration space without the observable estimiation
    virtual size_t get_number_of_steps() const = 0;
    virtual void block() {}
    virtual void unblock(){};



    virtual ~QmtVmcAlgorithm() { }
};



}
}




#endif //end of qmtvmcalgorithm.h.