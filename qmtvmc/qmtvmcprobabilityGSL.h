#ifndef QMTVMCPROBABILITYGSL_H
#define QMTVMCPROBABILITYGSL_H

#include "qmtvmcprob.h"

#include "qmtconfiguration.h"
#include "qmtvmcjastrow.h"
#include "qmtconfbitset.h"
#include "qmtslaterstate.h"
#include <vector>
#include <omp.h>

#define _QMTVMC_GSL

#ifdef _QMTVMC_GSL
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_permutation.h>
#endif

#ifdef _QMTVMC_GSL
namespace qmt {
namespace vmc {

#ifndef QMTVMC_MULTICENTER_ENERGY
   typedef QmtVmcJastrow  VMCJastrow;
#else
   typedef QmtVmcTwoBodyJastrow VMCJastrow;
#endif

template<size_t NUM_SITES>
class QmtVmcProbabilityGSL : public QmtVmcProbability {
private:
    std::vector<double> parameters;
    VMCJastrow* jastrow_correlator;
    QmtSlaterState* uncorrelated_state;
    bool PARAMETERS_SET;

    gsl_matrix * D;
    gsl_matrix * Dprim;

    size_t D_matrix_size;

    gsl_permutation * permutation;


public:


    QmtVmcProbabilityGSL(size_t num_of_electrons, VMCJastrow* _jastrow_correlator, QmtSlaterState* _uncorrelated_state):jastrow_correlator(_jastrow_correlator) {
        uncorrelated_state=_uncorrelated_state;
        PARAMETERS_SET=false;

        D_matrix_size=num_of_electrons;

        D	=gsl_matrix_alloc(D_matrix_size,D_matrix_size);
        Dprim	=gsl_matrix_alloc(D_matrix_size,D_matrix_size);
        permutation = gsl_permutation_alloc (D_matrix_size);
    }

    virtual ~QmtVmcProbabilityGSL() {
     
        gsl_matrix_free (D);
        gsl_matrix_free (Dprim);
        gsl_permutation_free (permutation);
    }

    
    virtual double product(const QmtConfigurationState* x) const {
         int sgn;
        

        for(unsigned int i=0; i<D_matrix_size; i++) {
            std::vector<double> D_column = uncorrelated_state->get_D_matrix_column(i,*x);


            for(unsigned int j=0; j<D_column.size(); j++)
                gsl_matrix_set(D,j,i,D_column[j]);

            
        }
//         std::cout<<"D-matrix"<<std::endl;
//         	for (int i = 0; i < 4; i++){
//  		  
//  		    for (int j = 0; j < 4; j++)
//                       std::cout<<gsl_matrix_get (D, i, j)<<"         ,";
//  		    std::cout<<std::endl;
//  		}
 		gsl_linalg_LU_decomp (D, permutation, &sgn);
		auto det =   gsl_linalg_LU_det (D, sgn);
      //  std::cout<<" det = "<<det<<std::endl;
        return det;// gsl_linalg_LU_decomp (D, permutation, &sgn);
    }
    

virtual void reset() {
 uncorrelated_state->reset_probability_matrix();
 jastrow_correlator->reset(parameters);
}



virtual double probability(const QmtConfigurationState* x, const QmtConfigurationState* xprim) {
        if(!PARAMETERS_SET) {
            throw std::invalid_argument ("QmtVmcProbablityGSL::probability: parameters not set!");
        }



        double jastrow_coeff = exp((*jastrow_correlator)(parameters,*x,*xprim));

//double t0 = omp_get_wtime();

double ratio = uncorrelated_state->update_W_matrix(*x,*xprim) * (xprim->get_phase() / x->get_phase());
double prob = pow( jastrow_coeff * (ratio),2);

return  prob>1.0 ? 1.0 : prob;
}
/*
#ifdef QMTVMC_MULTICENTER_ENERGY

virtual double probability(const QmtConfigurationState* x, const QmtConfigurationState* xprim){

   double jastrow_coeff = exp(jastrow_correlator(parameters,*x,i,j);
   double ratio = uncorrelated_state->update_W_matrix(*x,*xprim) * (xprim->get_phase() / x->get_phase());
   double prob = pow( jastrow_coeff * (ratio),2);
   return  prob>1.0 ? 1.0 : prob;
}

#endif
*/
virtual double density_ratio(const QmtConfigurationState* x, const QmtConfigurationState* xprim) const {
        if(!PARAMETERS_SET) {
            throw std::invalid_argument ("QmtVmcProbablityGSL::density_ratio: parameters not set!");
        }

//std::cerr<<"States:"<<std::endl;
//std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>*>(x))<<std::endl;
//std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>*>(xprim))<<std::endl;

        double jastrow_coeff = exp((*jastrow_correlator)(parameters,*x,*xprim));//exp(exp1 - exp2);
//        std::cerr<<"Jastro ok..."<<std::endl;
	double ratio = uncorrelated_state->update_W_matrix(*x,*xprim)* (xprim->get_phase() / x->get_phase());
//std::cerr<<"Prob ok"<<std::endl;
//        std::cout<<"From:"<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<16>*>(x))<<" To:"<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<16>*>(xprim))<<" ratio:"<<ratio<<std::endl;


//double ratio = uncorrelated_state->simple_take(*x,*xprim)* (xprim->get_phase() / x->get_phase());
//double ratio = uncorrelated_state->get_W_ratio(*x,*xprim)* (xprim->get_phase() / x->get_phase());

        double prob =  jastrow_coeff * (ratio);

/*
double nominator = product(xprim);
double denominator = product(x);


std::cout<<"nominator = "<<nominator<<" denominator = "<< denominator<< " ratio = "<<   nominator/denominator *  (xprim->get_phase() / x->get_phase()) << std::endl;

std::cout<<"prob = "<<prob * (xprim->get_phase() / x->get_phase())<<std::endl;

//        std::cout<<"prob = "<<prob <<"ratio = "<<ratio<<std::endl;


if(fabs(ratio- nominator/denominator *  (xprim->get_phase() / x->get_phase())) > 1.0e-4){
std::cout<<"Ratio error:"<< nominator/denominator *  (xprim->get_phase() / x->get_phase())<<" != "<<ratio<<std::endl;

exit(0);
}
*/





        return prob;
    }

virtual void set_parameters(const std::vector<double>& _parameters) {
        PARAMETERS_SET=true;
        parameters=_parameters;
}


double jastrow(const QmtConfigurationState* x) const {
        return 0;//jastrow_correlator(parameters,*x);
}


double jastrow(const QmtConfigurationState* x,const QmtConfigurationState* y) const {
        return (*jastrow_correlator)(parameters,*x,*y);
}

double two_nodes(size_t i,size_t j, size_t k, size_t l) const {

#ifdef QMTVMC_MULTICENTER_ENERGY
//std::cout<<"Jastrow = "<<(*jastrow_correlator)(parameters,i,j,k,l)<<std::endl;
return uncorrelated_state->get_two_node_ratio(i,j,k,l)*exp((*jastrow_correlator)(parameters,i,j,k,l));
#else
return 0;
#endif
}

void update(const QmtConfigurationState* x){
//for(const auto item : parameters)
  // std::cout<<"P = "<<item<<std::endl;

jastrow_correlator->update(parameters,*x);

}


}; // end of class QmtVmcProbablityGSL

} // end of namespace vmc
} //end of namespace qmt
#endif //_QMTVMC_GSL

#endif // QMTVMCPROBABILITYGSL_H
