#ifndef QMTVMCLOCALHOPPING_H
#define QMTVMCLOCALHOPPING_H

#include "qmtvmclocalobs.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmcprob.h"
#include <memory>

namespace qmt {
namespace vmc {

class QmtVmcLocalHopping : public QmtVmcLocalObservable {

size_t _id1;
size_t _id2;

int _spin;  //up: spin >= 0,down spin  < 0 

const QmtVmcHoppingsMap* _map;
const QmtVmcProbability* _probability;

std::ostream& print(std::ostream& ) const;

public:

QmtVmcLocalHopping(size_t id1, size_t id2, int spin, 
		   const QmtVmcHoppingsMap* map,
                   const QmtVmcProbability* probability);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);



};

}
}
#endif
