#include "qmtvmcmetropolis.h"
#include <omp.h>

void qmt::vmc::QmtVmcRta::prepare_seed()
{
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        seed = d.count();
	generator.seed(seed);
}

double qmt::vmc::QmtVmcRta::rnd()
{
  std::uniform_real_distribution<double> distribution(0.0,1.0);
  return distribution(generator);
}

int qmt::vmc::QmtVmcRta::rnd(int max)
{
  std::uniform_int_distribution<int> distribution(0,max);
  return distribution(generator);
}

qmt::vmc::QmtVmcRta::QmtVmcRta(qmt::vmc::QmtVmcData& d,std::shared_ptr<qmt::vmc::QmtConfigurationState> st):
    data(d),steps(0U),starting_state(st),seed(0)
{
  prepare_seed();
}


double qmt::vmc::QmtVmcRta::engine(size_t n_steps,size_t option){

  double energy = 0.0;
 std::vector<double> vals;
#ifdef _QMT_VMC_DEBUG
 std::cout<<"qmt::vmc::QmtVmcMetropolis::get_local_energy(n_steps) started"<<std::endl;
#endif


  
//  auto det0 = data.prob_calc->product(starting_state.get());
/*  while(fabs(det0) < 1.0e-9) {
    starting_state = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
    det0 = data.prob_calc->product(starting_state.get());
   }*/
   
    for(size_t i = 0U; i < n_steps; i++) {
//double time = omp_get_wtime();
//std::cout<<"Eneter energy"<<std::endl;        	
      auto hoppings_tr = starting_state->get_hoppings(*data.map);


      auto r1 = rnd(hoppings_tr.size() - 1);
      auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(hoppings_tr[r1].get_to_ptr());
        /* auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());//hopps_tr[r].get_to_ptr();
        
        
       det0 = data.prob_calc->product(to.get());
	while(fabs(det0) < 1.0e-9) {
	  to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
	  det0 = data.prob_calc->product(to.get());
	}*/
	  
        
        double r2 = rnd();

	double prob = data.prob_calc->probability(starting_state.get(),to.get());
//std::cout<<"Probaility calculated"<<std::endl;
	if(r2 < prob ) {
	     starting_state = to;	 
        }
        double val = data.energy_calc->getEnergy(*starting_state);

        vals.push_back(val);
        energy += val;
        //std::cout<<"Energy calculated step#"<<i<<std::endl;
        steps++;
//time = omp_get_wtime() - time;        
//std::cout<<"Local energy time:"<<time<<std::endl;
    }
    double std = 0.0;
if(option > 0){
   double std = 0.0;
    for(auto v : vals)
      std += (energy/n_steps - v)*(energy/n_steps -v);
      
      std = std/(n_steps - 1);
     if(std < 1.0e-4) std = 100;
     std::cout<<"Local_Energy/step = "<<energy/n_steps<< " variancy = "<<std<<std::endl;
//   

      return std;
    }
//    #ifdef _QMT_VMC_DEBUG
     std::cout<<"Local_Energy/step = "<<energy/n_steps<< " variancy = "<<std<<std::endl;
//    #endif

return energy/n_steps;
}


double qmt::vmc::QmtVmcRta::get_local_energy(size_t n_steps){
return engine(n_steps,0);
}


double qmt::vmc::QmtVmcRta::get_variance_estimator(size_t n_steps){
return engine(n_steps,1);
}



void qmt::vmc::QmtVmcRta::run(size_t n_steps)
{
    /*auto det0 = data.prob_calc->product(starting_state.get());
    while(fabs(det0) < 1.0e-9) {
    starting_state = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
    det0 = data.prob_calc->product(starting_state.get());
    
//        std::cout<<"det0 calculated: det0 = "<<det0<<std::endl;

  }*/


#ifdef _QMT_VMC_DEBUG
 std::cout<<"qmt::vmc::QmtVmcMetropolis::run(n_steps) started"<<std::endl;
#endif
 
for(size_t i = 0U; i < n_steps; i++) {
//double time = omp_get_wtime();       
    auto hoppings_tr = starting_state->get_hoppings(*data.map);
      auto r1 = rnd(hoppings_tr.size() - 1);
      auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(hoppings_tr[r1].get_to_ptr());
  
        /*auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());//hopps_tr[r].get_to_ptr();
         det0 = data.prob_calc->product(to.get());

	 while(fabs(det0) < 1.0e-9) {
	  to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
	  det0 = data.prob_calc->product(to.get());
	 }*/
        
        double r2 = rnd();
        #ifdef _QMT_VMC_DEBUG
        std::cout<<"Step#"<<i<<" probability = ";
        #endif

        double prob = data.prob_calc->probability(starting_state.get(),to.get());

        #ifdef _QMT_VMC_DEBUG
        std::cout<<prob<<std::endl;
        #endif


	if(r2 < prob ) {
            starting_state = to;
        }
         steps++;
// time = omp_get_wtime() - time;        
//std::cout<<"Run energy time:"<<time<<std::endl;


    }
}

size_t qmt::vmc::QmtVmcRta::get_number_of_steps() const
{
  return steps;
}

