#ifndef QMT_VMC_METROPOLIS_H
#define QMT_VMC_METROPOLIS_H

#include "qmtvmcalgorithm.h"

#include "qmtslaterstate.h"
#include "qmtvmcjastrow.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmcprobabilityGSL.h"
#include "qmtvmchamiltonianenergy.h"


namespace qmt {
namespace vmc {
class QmtVmcRta : public QmtVmcAlgorithm {
    qmt::vmc::QmtVmcData& data;
    size_t steps;
    std::shared_ptr<qmt::vmc::QmtConfigurationState> starting_state;
    
    unsigned seed;
    std::default_random_engine generator;
    typedef std::chrono::high_resolution_clock myclock;

    void prepare_seed();
    double rnd();
    int rnd(int max);
    
    double engine(size_t n_steps,size_t option); //option: 0 - energy, else variance
    
public:

    QmtVmcRta(QmtVmcData& d, std::shared_ptr<qmt::vmc::QmtConfigurationState> st);


    double get_local_energy(size_t n_steps);
    double get_variance_estimator(size_t n_steps);
    void run(size_t n_steps);
    size_t get_number_of_steps() const;

};

}
}

#endif // qmtvmcmetropolis.h

