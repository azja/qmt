#ifndef QMTVMCLOCALN_H
#define QMTVMCLOCALN_H
#include "qmtvmclocalobs.h"

namespace qmt {
namespace vmc {

class QmtVmcLocalN : public QmtVmcLocalObservable {

size_t _id;
std::ostream& print(std::ostream& s) const;
public:

QmtVmcLocalN(size_t id);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);

};

}
}
#endif
