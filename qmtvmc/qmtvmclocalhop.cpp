#include "qmtvmclocalhop.h"


qmt::vmc::QmtVmcLocalHopping::QmtVmcLocalHopping(size_t id1, size_t id2, int spin, 
		   const QmtVmcHoppingsMap* map,
                   const QmtVmcProbability* probability):_id1(id1),_id2(id2),_spin(spin),
		   _map(map), _probability(probability)  {

	}



/////////////////////////////////////////////////////////////////////////////////////////////////////////


double qmt::vmc::QmtVmcLocalHopping::get_value(const QmtConfigurationState& x){
    auto hopping = x.get_single_hopping(_id1,_id2,*_map,_spin);
//    std::cerr<<"Entered, hopping.size() = "<<hopping.size()<<std::endl;
    
    if(hopping == NULL) return 0;
//std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>*>(&hopping[0].get_to()))<<std::endl;
//std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>*>(&hopping[0].get_from()))<<std::endl;

// auto a = hopping->get_to().get_similar();
// auto b =hopping->get_from().get_similar();

//std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>*>(a))<<std::endl;
//std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>*>(b))<<std::endl;

auto a = std::shared_ptr<const qmt::vmc::QmtConfigurationState>(&hopping->get_to());
auto b = std::shared_ptr<const qmt::vmc::QmtConfigurationState>(&hopping->get_from());

    //std::cout<< _probability->density_ratio(&hopping->get_to(),&hopping->get_from())  * (hopping->get_to().get_phase() / hopping->get_from().get_phase());
//  std::cout<<"PROB"<<_probability<<std::endl;
//    std::cout<< _probability->density_ratio(a.get(),b.get());//hopping->get_to().get_phase() / hopping->get_from().get_phase());
    return  _probability->density_ratio(b.get(),a.get()) * hopping->get_to().get_phase() / hopping->get_from().get_phase();
  /* return 0;*/ // _probability->density_ratio(&hopping->get_to(),&hopping->get_from());
}




/*
/print
*/

std::ostream& qmt::vmc::QmtVmcLocalHopping::print(std::ostream& s) const {
if(_spin > 0)
 s<<"<cup"<<_id1<<"_aup"<<_id2<<">";
else
 s<<"<cdown"<<_id1<<"_adown"<<_id2<<">";
return s;
}
