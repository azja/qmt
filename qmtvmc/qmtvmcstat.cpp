#include "qmtvmcstat.h"
#include "qmtvmcstatgenerator.h"

qmt::vmc::QmtVmcStat::QmtVmcStat(std::vector<qmt::vmc::QmtVmcLocalObservable*> obs) {
	observables = obs;
	values.resize(observables.size());
	errors.resize(observables.size());
}

////////////////////////////////////////////////////////////////////////////////

void qmt::vmc::QmtVmcStat::collect(const QmtConfigurationState& x,double multiplier) {
for(size_t i = 0U; i < observables.size(); ++i) {
  values[i] += ((observables[i])->get_value(x))*multiplier;
 }
}

////////////////////////////////////////////////////////////////////////////////

void qmt::vmc::QmtVmcStat::normalize(double norm) {
for(size_t i = 0U; i < observables.size(); ++i) {
  values[i] /= norm;
 }
}

////////////////////////////////////////////////////////////////////////////////

void qmt::vmc::QmtVmcStat::reset() {

values.clear();
errors.clear();

values.resize(observables.size());
errors.resize(observables.size());

}

////////////////////////////////////////////////////////////////////////////////


const std::vector<double> qmt::vmc::QmtVmcStat::get_values() const {
 return values;
}

const std::vector<double> qmt::vmc::QmtVmcStat::get_errors() const {
 return errors;
}

const std::string qmt::vmc::QmtVmcStat::get_names() const {
 std::ostringstream stream;
 for(const auto obs : observables)
  stream<<(*obs)<<" ";
 
 return stream.str();
}


////////////////////////////////////////////////////////////////////////////////

qmt::vmc::QmtVmcStat::~QmtVmcStat(){
for(size_t i = 0U; i < observables.size(); ++i)
    delete  observables[i];
}
