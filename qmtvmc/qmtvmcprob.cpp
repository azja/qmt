#include "qmtvmcprob.h"

#ifdef _QMTVMC_GSL

qmt::vmc::QmtVmcProbabilityGSL::QmtVmcProbabilityGSL(size_t num_of_electrons, qmt::vmc::QmtVmcJastrow _jastrow_correlator, const qmt::vmc::QmtSlaterState* _uncorrelated_state):jastrow_correlator(_jastrow_correlator) {
    uncorrelated_state=_uncorrelated_state;
    PARAMETERS_SET=false;

    D_matrix_size=num_of_electrons;

    D	=gsl_matrix_alloc(D_matrix_size,D_matrix_size);
    Dprim	=gsl_matrix_alloc(D_matrix_size,D_matrix_size);
    permutation = gsl_permutation_alloc (D_matrix_size);
}


qmt::vmc::QmtVmcProbabilityGSL::~QmtVmcProbabilityGSL() {
    gsl_matrix_free (D);
    gsl_matrix_free (Dprim);
    gsl_permutation_free (permutation);
}


void qmt::vmc::QmtVmcProbabilityGSL::set_parameters(const std::vector<double>& _parameters) {
    PARAMETERS_SET=true;
    parameters=_parameters;
}

double qmt::vmc::QmtVmcProbabilityGSL::probability(const qmt::vmc::QmtConfigurationStateBitset<NUM_SITES>& x, const qmt::vmc::QmtConfigurationStateBitset<NUM_SITES>& xprim) {
    if(!PARAMETERS_SET) {
        throw std::invalid_argument ("QmtVmcProbablityGSL::probability: parameters not set!");
    }

    double jastrow_coeff = jastrow_correlator(parameters,x)/jastrow_correlator(parameters,xprim);

    for(unsigned int i=0; i<D_matrix_size; i++) {
        std::vector<double> D_column = uncorrelated_state->get_D_matrix_column(i,x);

        for(unsigned int j=0; j<D_column.size(); j++)
            gsl_matrix_set(D,j,i,D_column[j]);

        D_column = uncorrelated_state->get_D_matrix_column(i,xprim);

        for(unsigned int j=0; j<D_column.size(); j++)
            gsl_matrix_set(Dprim,j,i,D_column[j]);
    }

    gsl_linalg_LU_decomp (D, permutation, &sgn);
    gsl_linalg_LU_decomp (Dprim, permutation, &sgnprim);

    double prob = jastrow_coeff* gsl_linalg_LU_det (D, sgn)/ gsl_linalg_LU_det (Dprim, sgnprim);

    return prob;
}

#endif //_QMTVMC_GSL