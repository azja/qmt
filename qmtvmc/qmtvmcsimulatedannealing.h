#ifndef QMT_VMC_SIMULATED_ANNEALING_H
#define QMT_VMC_SIMULATED_ANNEALING_H

#include "qmtvmcoptimizer.h"
#include "../headers/qmtsimulatedannealing.h"
#include <algorithm>

namespace qmt {
 namespace vmc {

   class QmtVmcSimulatedAnnealing : public QmtVmcOptimizer {
   
   static double get_value(const std::vector<double>& x, void * params);
     
   struct Params {
      QmtVmcData* d;
      QmtVmcAlgorithm* alg; 
      size_t steps;
      size_t probing_steps;
   };
   
   public:
     
     double optimize( QmtVmcData& d, 
		      QmtVmcAlgorithm& alg, 
                     const std::vector<std::tuple<double,double,double>>& in_par,
                     std::vector<double> out_par,
		     size_t steps,
		     size_t probing_steps);
     
   };
   

} //end of namespace vmc
} // end of namespace qmt

#endif //QMT_VMC_SIMULATED_ANNEALING_H
