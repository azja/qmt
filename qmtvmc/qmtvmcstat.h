#ifndef QMTVMCSTAT_H
#define QMTVMCSTAT_H

#include<vector>
#include<string>
#include<sstream>
#include "qmtvmclocalobs.h"

namespace qmt {
 namespace vmc {

class QmtVmcStat {
std::vector<qmt::vmc::QmtVmcLocalObservable*> observables;
std::vector<double> values;
std::vector<double> errors;

public:

QmtVmcStat(std::vector<qmt::vmc::QmtVmcLocalObservable*> obs);

void collect(const QmtConfigurationState& x,double multiplier = 1.0);
void normalize(double norm);
void reset();

const std::vector<double> get_values() const;
const std::vector<double> get_errors() const;
const std::string get_names() const;
~QmtVmcStat();

};


 }
}


#endif
