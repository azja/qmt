#ifndef QMT_VMC_CONFIGURATION_STATE
#define QMT_VMC_CONFIGURATION_STATE


#include <vector>
#include <set>

#ifndef _QMT_VMC_HOPPING_MAP_MAP
#include <unordered_map>
#endif

#ifdef _QMT_VMC_HOPPING_MAP_MAP
#include <map>
#endif

#include <memory>
#include <tuple>

namespace qmt {
namespace vmc {


/*
 * Implements configuration of N electrons and generates all possible transitions
 * attainable via qmt::vmc::QmtVmcHoppingsMap object
 */

class QmtVmcHopping;
class QmtVmcHoppingsMap;

class QmtConfigurationState {

public:

    virtual bool   is_occupied_up(size_t index) const = 0;
    virtual bool   is_occupied_down(size_t index) const = 0;
    virtual size_t get_size(void) const = 0;	//returns number of bands
    virtual double get_phase(void) const = 0;	//returns the phase of the occupied state
    virtual std::vector<QmtVmcHopping> get_hoppings(const QmtVmcHoppingsMap& map) = 0;
  virtual QmtVmcHopping* get_single_hopping(size_t i, size_t j,const QmtVmcHoppingsMap& map, int spin) const  = 0;
    virtual QmtConfigurationState* get_similar() const = 0;
    virtual QmtConfigurationState* get_copy() const = 0;
    virtual void set_phase(double phase) = 0;
    virtual void set_bit(int i, bool v) = 0;
    virtual bool compare_configuration(const QmtConfigurationState& s) const = 0;
    virtual ~QmtConfigurationState() {}
    
    

};


class QmtVmcHopping {
    std::shared_ptr<QmtConfigurationState> from_state;
//    QmtConfigurationState* from_state;
//    QmtConfigurationState* to_state;
    std::shared_ptr<QmtConfigurationState> to_state;
    size_t _id;
     size_t from_id;
    size_t to_id;
public:
    //QmtVmcHopping(QmtConfigurationState* from,QmtConfigurationState* to,const size_t id);
   QmtVmcHopping(std::shared_ptr<QmtConfigurationState>from,     std::shared_ptr<QmtConfigurationState> to,const size_t id);
    const QmtConfigurationState& get_from() const;
    const QmtConfigurationState& get_to() const ;

    size_t get_from_id() const { return from_id;}
    size_t get_to_id() const {return to_id;}

    void set_from_id(size_t i)  { from_id = i;}
    void set_to_id(size_t i)  { to_id = i;}


    std::shared_ptr<QmtConfigurationState> get_from_ptr();
    std::shared_ptr<QmtConfigurationState> get_to_ptr();
    size_t get_id() const;


};




class QmtVmcHoppingsMap {
    typedef std::tuple<size_t,size_t> pair;
     std::vector<size_t> empty_v;
    /*
     * for the map base implementation
     */

#ifdef _QMT_VMC_HOPPING_MAP_MAP
    struct Cmpr {

        bool operator()(const pair& l, const pair& r) const {

            if(std::get<0>(l) < std::get<0>(r)) return true;
            else if(std::get<0>(l)== std::get<0>(r)) {
                if(std::get<1>(l) < std::get<1>(r)) return true;
                else return false;
            }
            else return false;
        }
    };
#endif

#ifndef _QMT_VMC_HOPPING_MAP_MAP
    struct Hash {

        size_t operator()(const pair &p) const {
            size_t k0 = std::get<0>(p);
            size_t k1 = std::get<1>(p);
            return static_cast<size_t>((k0 + k1)*(k0+k1+1)*0.5 + k1);  //Cantor pairing function
        }
    };

#endif

#ifdef _QMT_VMC_HOPPING_MAP_MAP
    std::map<pair, std::vector<size_t>, Cmpr> hoppings_map;
#endif

#ifndef _QMT_VMC_HOPPING_MAP_MAP
    std::unordered_map<pair, std::vector<size_t>, Hash> hoppings_map;
#endif
    size_t n;
    
    std::set<size_t> hopping_ids;

public:

    QmtVmcHoppingsMap() = default;
    QmtVmcHoppingsMap(const QmtVmcHoppingsMap&) = delete;

    QmtVmcHoppingsMap(std::vector<std::tuple<size_t,size_t,size_t>>& map);


    virtual size_t get_size() const;
    virtual void set_jump(size_t i, size_t j,size_t id);
     virtual const std::vector<size_t>& is_allowed(size_t i, size_t j) const; //jump from i-th to the j-th site where i,j number sites/bands,
    //returns possible hoppings id if empty vector no possible jumps
    virtual size_t get_number_of_hoppings() const;
    virtual ~QmtVmcHoppingsMap() {};

};

/*
Needed  for computation of energy coming from the multi-nodes interaction matrix elements 
*/
class QmtVmcTwoCentresMap {
 typedef std::tuple<size_t,size_t,size_t,size_t,size_t,double> configuration;
 std::unordered_map<size_t,configuration> conf_map;

 size_t generate_key(size_t i ,size_t j,size_t k,size_t l) const;
 size_t generate_key(const configuration& conf) const;
 
public:

 QmtVmcTwoCentresMap(std::vector<configuration>& map);
};

}
}
#endif // qmtconfiguation.h
