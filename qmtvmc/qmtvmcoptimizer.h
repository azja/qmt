#ifndef QMT_VMC_OPTIMZER_H
#define QMT_VMC_OPTIMZER_H

#include <tuple>
#include <vector>
#include "qmtvmcdata.h"
#include "qmtvmcalgorithm.h"

namespace qmt {
 namespace vmc {
   
   class QmtVmcOptimizer {
     
   public:
     virtual double optimize(QmtVmcData& d, 
		     QmtVmcAlgorithm& alg, 
                     const std::vector<std::tuple<double,double,double>>& in_par,
                     std::vector<double>& out_par,
		     size_t steps,
		     size_t probing_steps) = 0;
     
      virtual ~QmtVmcOptimizer() {}
   };
   
 }  
}


#endif //end of qmtvmcoptimizer.h
