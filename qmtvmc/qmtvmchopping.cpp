#include "qmtconfiguration.h"



/*
qmt::vmc::QmtVmcHopping::QmtVmcHopping(qmt::vmc::QmtConfigurationState* from,
                                       qmt::vmc::QmtConfigurationState* to,
                                       const size_t id):from_state(from),to_state(to), _id(id)
{ }
*/


qmt::vmc::QmtVmcHopping::QmtVmcHopping(std::shared_ptr<qmt::vmc::QmtConfigurationState> from,
                                       std::shared_ptr<qmt::vmc::QmtConfigurationState> to,
                                       const size_t id):from_state(from),to_state(to), _id(id),from_id(0U),to_id(0U)
{ }



const qmt::vmc::QmtConfigurationState&  qmt::vmc::QmtVmcHopping::get_from() const {
    return *from_state;
}

const qmt::vmc::QmtConfigurationState&  qmt::vmc::QmtVmcHopping::get_to() const {
    return *to_state;
}

std::shared_ptr<qmt::vmc::QmtConfigurationState>  qmt::vmc::QmtVmcHopping::get_from_ptr()  {
return from_state;}

std::shared_ptr<qmt::vmc::QmtConfigurationState> qmt::vmc::QmtVmcHopping::get_to_ptr()  {
    return to_state;
}



size_t qmt::vmc::QmtVmcHopping::get_id() const {
    return _id;
}

