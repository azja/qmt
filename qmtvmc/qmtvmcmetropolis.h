#ifndef QMT_VMC_METROPOLIS_H
#define QMT_VMC_METROPOLIS_H

#include "qmtvmcalgorithm.h"

#include "qmtslaterstate.h"
#include "qmtvmcjastrow.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmcprobabilityGSL.h"
#include "qmtvmchamiltonianenergy.h"


namespace qmt {
namespace vmc {
class QmtVmcMetropolis : public QmtVmcAlgorithm {

    unsigned seed;
    qmt::vmc::QmtVmcData& data;
    size_t steps;
    std::shared_ptr<qmt::vmc::QmtConfigurationState> starting_state;
    
    std::default_random_engine generator;
    typedef std::chrono::high_resolution_clock myclock;

    void prepare_seed();
    double rnd();
    int rnd(int max);
    
    double engine(size_t n_steps,size_t option); //option: 0 - energy, else variance
    
public:

    QmtVmcMetropolis(QmtVmcData& d, std::shared_ptr<qmt::vmc::QmtConfigurationState> st);


    double get_local_energy(size_t n_steps);
    double get_variance_estimator(size_t n_steps);
    void run(size_t n_steps);
    size_t get_number_of_steps() const;

};

/////////////////////////////////////////////////////////////////////////////////////////////////////

class QmtVmcMetropolisCorrelatedSampling : public QmtVmcAlgorithm {
    


    unsigned seed;
    qmt::vmc::QmtVmcData& data;
    size_t steps;
    std::shared_ptr<qmt::vmc::QmtConfigurationState> starting_state;
    bool blocked;
    bool PREPARED;    

    std::default_random_engine generator;
    typedef std::chrono::high_resolution_clock myclock;

    void prepare_seed();
    double rnd();
    int rnd(int max);
    
    std::vector<double> engine(size_t n_steps,size_t option, qmt::vmc::QmtVmcStat *stats = NULL); //option: 0 - energy, 1 - variance, 2 - return energy and collect stats
    
    std::vector<std::shared_ptr<qmt::vmc::QmtConfigurationState>> states;
    std::vector<size_t> repetitions;
    std::vector<double> jastrows;

    std::vector<std::vector<double>> hopping_energies;
    std::vector<double> on_site_energies;
    std::vector<double> interaction_energies;
    
    void update(std::shared_ptr<qmt::vmc::QmtConfigurationState> ptr,bool changed);

public:

     QmtVmcMetropolisCorrelatedSampling(QmtVmcData& d, std::shared_ptr<qmt::vmc::QmtConfigurationState> st);

    void block();
    void unblock();

    double get_local_energy(size_t n_steps);
    double get_variance_estimator(size_t n_steps);
    double get_observables(size_t n_steps,qmt::vmc::QmtVmcStat& stats);
    void run(size_t n_steps);
    size_t get_number_of_steps() const;

};




}
}

#endif // qmtvmcmetropolis.h

