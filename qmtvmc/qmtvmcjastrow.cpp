#include "qmtvmcjastrow.h"
#include <iostream>
#include <thread>

qmt::vmc::QmtVmcJastrow::QmtVmcJastrow(const std::vector<std::tuple<size_t,size_t,size_t,size_t>>& defs) {
    definitions = defs;
    std::set<size_t> to_cnt;
    for(const auto& item : definitions)
       to_cnt.insert(std::get<0>(item));
    
    size_t size = to_cnt.size();
    
    conections.resize(size);
    std::fill(conections.begin(),conections.end(),SiteConections());
    for(const auto& item : definitions) {
     size_t id = std::get<0>(item);
     size_t connection = std::get<1>(item);
     size_t param_id = std::get<2>(item);
     
     conections[id].sp_ids.push_back(connection);
     conections[id].param_ids.push_back(param_id);
     
    }   
       
}

double qmt::vmc::QmtVmcJastrow::operator()(const std::vector< double >& parameters, const qmt::vmc::QmtConfigurationState& configuration) const
{
    double exponent = 0.0;
    for(const auto &def : definitions) {
        auto id1 = std::get<0>(def);
        auto id2 = std::get<1>(def);
        auto param = std::get<2>(def);
        auto u_flag = std::get<3>(def);
        auto converter = [&](bool p)->size_t {return p ? 1 : 0;};
 {
        if(u_flag){
            exponent += 0.5* (converter(configuration.is_occupied_up(id1)) + converter(configuration.is_occupied_down(id1)))*
                        (converter(configuration.is_occupied_up(id2)) + converter(configuration.is_occupied_down(id2))) *  parameters[param]/4;
                }        
        else {
            exponent +=  (converter(configuration.is_occupied_up(id1))* converter(configuration.is_occupied_down(id2))) * parameters[param]/2;
            }
        }

    }
    return  -exponent;

}


double qmt::vmc::QmtVmcJastrow::operator()(const std::vector< double >& parameters, const qmt::vmc::QmtConfigurationState& from,
										    const qmt::vmc::QmtConfigurationState& to) const
{

size_t from_id = 0;
size_t to_id = 0;

for(auto i=0U; i < from.get_size(); ++i) {
   bool from_occ = from.is_occupied_up(i);
   bool to_occ = to.is_occupied_up(i);
   
   if(from_occ != to_occ) {
     if(from_occ) from_id = i;
     else to_id = i;
   }
   
}

for(auto i=0U; i < from.get_size(); ++i) {
   bool from_occ = from.is_occupied_down(i);
   bool to_occ = to.is_occupied_down(i);
   
   if(from_occ != to_occ) {
     if(from_occ) from_id = i;
     else to_id = i;
   }
   
}

auto to_int_converter = [](bool v)->size_t { return v == true ? 1 : 0;};

auto get_w_from = [&](size_t i,size_t j)->size_t {
 
return  (to_int_converter(from.is_occupied_up(i)) + to_int_converter(from.is_occupied_down(i)))*
 (to_int_converter(from.is_occupied_up(j)) + to_int_converter(from.is_occupied_down(j)));
};

auto get_w_to = [&](size_t i,size_t j)->size_t {
 
return  (to_int_converter(to.is_occupied_up(i)) + to_int_converter(to.is_occupied_down(i)))*
 (to_int_converter(to.is_occupied_up(j)) + to_int_converter(to.is_occupied_down(j)));
};

double from_part = 0.0;
double to_part = 0.0;

for(size_t i = 0; i < conections[from_id].sp_ids.size(); ++i){

from_part -= get_w_from(from_id,conections[from_id].sp_ids[i]) * parameters[conections[from_id].param_ids[i]];
from_part += get_w_to(from_id,conections[from_id].sp_ids[i]) * parameters[conections[from_id].param_ids[i]];
}

for(size_t i = 0; i < conections[to_id].sp_ids.size(); ++i){
to_part -= get_w_from(to_id,conections[to_id].sp_ids[i]) * parameters[conections[to_id].param_ids[i]];
to_part += get_w_to(to_id,conections[to_id].sp_ids[i]) * parameters[conections[to_id].param_ids[i]];
}

return -0.25*(from_part + to_part);// - exponent; //0.25 from Hamiltonian construction
}

size_t qmt::vmc::QmtVmcJastrow::get_number_of_params() const
{
  std::set<unsigned int> indicies;
  for(const auto &def : definitions){
    indicies.insert(std::get<2>(def));
  }
    return indicies.size();
}


//////////////////////////////QmtVmcTwoBodyJastrow implementation////////////////////////////////////

qmt::vmc::QmtVmcTwoBodyJastrow::QmtVmcTwoBodyJastrow(const std::vector<qmt::QmtInteractionDefinition>& defs,size_t sector_size):N(sector_size){

    configuration = NULL;
    std::vector<size_t> to_cnt;
    std::vector<size_t> terms;
    std::vector<size_t> ind1;
    std::vector<size_t> ind2;
    size_t cntr = 0U;

    lambda_ids = new int* [sector_size];
    for(size_t i=0U; i < sector_size; i++)
       lambda_ids[i] = new int [sector_size];

    for(size_t i=0U; i < sector_size; i++){
        for(size_t j=0U; j < sector_size; j++)
         lambda_ids[i][j] = -1;
     }

    for(const auto& item : defs){
      if(item.i == item.j && item.l == item.k){
          if(terms.size() > 0 && std::find(terms.begin(),terms.end(),item.term)==terms.end()  && ((std::find(ind1.begin(),ind1.end(),item.i)!=ind1.end() && std::find(ind2.begin(),ind2.end(),item.l)!=ind2.end()) || (std::find(ind2.begin(),ind2.end(),item.i)!=ind2.end() && std::find(ind1.begin(),ind1.end(),item.l)!=ind1.end())))
           continue;
          else{
            terms.push_back(item.term);
            ind1.push_back(item.i);
            ind2.push_back(item.l);
          }

       to_cnt.push_back(cntr);       
      }
       cntr++;
    }

    size_t size = to_cnt.size();
    
    conections.resize(sector_size);
    std::fill(conections.begin(),conections.end(),SiteConections());
    
    cntr = 0;
    for(const auto& item : to_cnt){
         size_t id_i = defs[item].i;
         if(id_i >= sector_size)
                id_i = id_i - sector_size;

         size_t id_k = defs[item].k;
         if(id_k >= sector_size)
                id_k = id_k - sector_size;



       
         conections[id_i].sp_ids.push_back(id_k);
         conections[id_i].param_ids.push_back(defs[item].term);
         conections[id_i].factors.push_back(defs[item].factor);

         conections[id_i].size += 1U;
       
      if(lambda_map.find(defs[item].term) == lambda_map.end()){
            lambda_map[defs[item].term] = cntr;

            cntr++;
          
         }
    }

   
}
//////////////////////////////////////////////////////////////////////////////////////////////

qmt::vmc::QmtVmcTwoBodyJastrow::QmtVmcTwoBodyJastrow(std::ifstream &file,size_t sector_size):N(sector_size){

    configuration = NULL;
   
   lambda_ids = new int* [sector_size];
    for(size_t i=0U; i < sector_size; i++)
       lambda_ids[i] = new int [sector_size];

    for(size_t i=0U; i < sector_size; i++){
        for(size_t j=0U; j < sector_size; j++)
         lambda_ids[i][j] = -1;
     }
    size_t cntr = 0U;

    std::vector<std::string> definitions;
   
    std::string line;
   if (file.is_open())
    {
    while ( std::getline (file,line) )
    {
      definitions.push_back(line);
//      std::cout<<line<<std::endl;
    }
  }

    conections.resize(sector_size);
    std::fill(conections.begin(),conections.end(),SiteConections());

 
   for(const auto& definition : definitions) {
   
   
    auto def = qmt::parser::get_bracketed_words(definition,'<','>',true);
    size_t i = std::stoul(def[0]);
    auto items = qmt::parser::get_bracketed_words(definition,'(',')',true);
    for(const auto& item : items){    
      auto partner = qmt::parser::get_delimited_words(",",item);
         conections[i].sp_ids.push_back(std::stoul(partner[0]));
       //  std::cout<<"i = "<<i<<std::endl;
         conections[i].param_ids.push_back(std::stoul(partner[1]));
         if(i==std::stoul(partner[0]))
           conections[i].factors.push_back(1);
         else
           conections[i].factors.push_back(2);
          conections[i].size += 1U;
         if(lambda_map.find(std::stoul(partner[1])) == lambda_map.end()){
            lambda_map[std::stoul(partner[1])] = cntr;
//std::cout<<"i = "<<i<<std::endl;
            cntr++;
          
         }
    }
}


 //  exit(0);
   
}

//////////////////////////////////////////////////////////////////////////////////////////////
size_t qmt::vmc::QmtVmcTwoBodyJastrow::get_number_of_params() const {

 return lambda_map.size();

}

void qmt::vmc::QmtVmcTwoBodyJastrow::reset(const std::vector<double> &lambdas){

	#ifdef _QMT_VMC_DEBUG
	std::cerr<<" qmt::vmc::QmtVmcTwoBodyJastrow::reset(const std::vector<double> &lambdas) - reseting";
	#endif

  T.resize(N);
  std::fill(T.begin(),T.end(),0.0);

  for(size_t i = 0U; i < N;++i) {
   for(size_t j = 0U; j < conections[i].size; ++j) {

       size_t id = conections[i].sp_ids[j];

       size_t lambda_id = conections[i].param_ids[j];

       lambda_ids[i][id] = conections[i].param_ids[j];

       auto converter=[&](size_t p)->double {
         double result = 0;

         if(configuration->is_occupied_up(p))   result++;
         if(configuration->is_occupied_down(p)) result++;
      
         if(i==id)
         return result;
         else
         return result/2;
       }; 
      
      
       //std::cout<<conections[i].factors[j]<<std::endl;
      T[i] = T[i] + conections[i].factors[j]*lambdas[lambda_id]*(converter(id));
      
    }
  }

        #ifdef _QMT_VMC_DEBUG
	std::cerr<<"done"<<std::endl;
	#endif
//exit(0);
}



/*
/update state
*/

void qmt::vmc::QmtVmcTwoBodyJastrow::update(const std::vector<double>& parameters,const qmt::vmc::QmtConfigurationState& state) {




 if(configuration == NULL) {
        #ifdef _QMT_VMC_DEBUG
	std::cerr<<std::endl<<" qmt::vmc::QmtVmcTwoBodyJastrow::update - setting initial configuration...";
	#endif
        configuration = state.get_similar();
        reset(parameters);   
        #ifdef _QMT_VMC_DEBUG
	std::cerr<<"done"<<std::endl;
	#endif
        return;     
      }

if(!state.compare_configuration(*configuration)){
//std::cout<<"INPUT STATE:"<<(static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>&>(state))<<std::endl;
delete configuration;
configuration = state.get_copy();
reset(parameters);   
return;
}
configuration = state.get_similar();
     size_t from_id = 0;
     size_t to_id = 0;

   for(auto i=0U; i < configuration->get_size(); ++i) {
       bool from_occ = configuration->is_occupied_up(i);
       bool to_occ = state.is_occupied_up(i);
   
      if(from_occ != to_occ) {
         if(from_occ) from_id = i;
         else to_id = i;
     }
   
   }

   for(auto i=0U; i < configuration->get_size(); ++i) {
       bool from_occ = configuration->is_occupied_down(i);
       bool to_occ = state.is_occupied_down(i);
   
      if(from_occ != to_occ) {
         if(from_occ) from_id = i;
         else to_id = i;
      }
   
   }



  
   auto get_lambda =[&](size_t p,size_t q)->int {
        auto l_id = lambda_ids[p][q];     
        if(l_id >= 0){
        //   for(const auto item : parameters)
         //    std::cout<<"param = "<<item<<" ";
           // std::cout<<std::endl;

            return parameters[lambda_map[l_id]];
         }
        return 0;
   };
   
  for(size_t p = 0U; p < N;++p) {
    T[p] = T[p] + get_lambda(p,from_id) - get_lambda(p,to_id);
  }

}




double qmt::vmc::QmtVmcTwoBodyJastrow::operator()(const std::vector<double>& parameters,
                              const qmt::vmc::QmtConfigurationState& from,const qmt::vmc::QmtConfigurationState& to) const  {


   
     size_t from_id = 0;
     size_t to_id = 0;

for(auto i=0U; i < from.get_size(); ++i) {
   bool from_occ = from.is_occupied_up(i);
   bool to_occ = to.is_occupied_up(i);
   
   if(from_occ != to_occ) {
     if(from_occ) from_id = i;
     else to_id = i;
   }
   
}

for(auto i=0U; i < from.get_size(); ++i) {
   bool from_occ = from.is_occupied_down(i);
   bool to_occ = to.is_occupied_down(i);
   
   if(from_occ != to_occ) {
     if(from_occ) from_id = i;
     else to_id = i;
   }
   
}


    const auto get_lambda =[&](size_t p,size_t q)->double {
        int l_id = lambda_ids[p][q];  
     
        if(l_id >= 0){
 
   
          if(p!=q) 
         return parameters[lambda_map.at(l_id)];
          else
         return parameters[lambda_map.at(l_id)];
         }

        return 0;
     };
double res = -(T[to_id] - T[from_id] + 0.5*(-2*get_lambda(to_id,from_id) +  get_lambda(to_id,to_id) + get_lambda(from_id,from_id)));
/*std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>*>(configuration))<<std::endl;
std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>&>(from)<<std::endl;
std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>&>(to)<<std::endl;
std::cout<<"From_id = "<<from_id<<" To_id = "<<to_id<<std::endl;
std::cout<<"Jastrow = "<<res<<" "<<T[to_id]<<" "<<T[from_id]<<" "<< 0.5*(-2*get_lambda(to_id,from_id) +  get_lambda(to_id,to_id) + get_lambda(from_id,from_id))<<" "<<get_lambda(to_id,to_id)<<" "<<get_lambda(from_id,to_id)<<" "<<parameters[0]<<" "<<parameters[1]<<std::endl;;*/
//exit(0);
/*
if(from.compare_configuration(to)) return 0;
else {
if((from.is_occupied_up(0) && from.is_occupied_down(0)) || (from.is_occupied_up(1) && from.is_occupied_down(1)))
return parameters[1] - parameters[0];
else
return parameters[0] - parameters[1];

}

*/
 return res;//(T[to_id] - T[from_id] - 0.5*(-2*get_lambda(to_id,from_id) +  get_lambda(to_id,to_id) + get_lambda(from_id,from_id)));

}


double qmt::vmc::QmtVmcTwoBodyJastrow::operator()(const std::vector<double>& parameters,size_t l,size_t k, size_t m, size_t n) const {
   

     auto get_lambda =[&](size_t p,size_t q)->int {
        auto l_id = lambda_ids[p][q];     
        if(l_id >= 0)
          return parameters[lambda_map.at(l_id)];
        return 0;
     };

if(l>=N) l = l - N;
if(k>=N) k = k - N;
if(m>=N) m = m - N;
if(n>=N) n = n - N;


double res = (T[l] - T[k] + T[m] - T[n]) - 0.5*(-2*get_lambda(l,k) + 2*get_lambda(m,l) - 2*get_lambda(l,n) - 2*get_lambda(k,m)+ 2*get_lambda(k,n) - 2*get_lambda(m,n)+ get_lambda(l,l) + get_lambda(k,k) + get_lambda(m,m) + get_lambda(n,n));
//if(l!=k && m!=n)
//std::cout<<"Jastrow = "<<res<<" l = "<<l<<" k = "<<k<<" m = "<<m<<" n = "<<n<<std::endl;
//exit(0);
 return res;

}


qmt::vmc::QmtVmcTwoBodyJastrow::~QmtVmcTwoBodyJastrow() {
  


 if(lambda_ids!=NULL){
  for(size_t i=0U; i < N;i++)
   delete [] lambda_ids[i];
  delete [] lambda_ids;
 }
  
 if(configuration != NULL)
   delete configuration;
  
}
///////////////////////////////////////////////////////////////////////////////////


double qmt::vmc::QmtVmcPairOperatorCorrelator::operator()(const std::vector<double>& parameters,const qmt::vmc::QmtConfigurationState& state) const{
return 1;
   size_t act_down = 0U;

   double factor = 1.0;
   size_t size = state.get_size();
   size_t offset = 4;
   for(size_t i = 0U; i < state.get_size();++i) {
        if(state.is_occupied_up(i)) {
             for(size_t j = act_down; j < state.get_size();++j) {
              if(state.is_occupied_down(j)){
                factor *= parameters[i*(size - i) + j + offset];             
                act_down = j + 1U;                 
                 continue;
               }   
             }
          
        }
   }

 return factor;
}



/////////////////////////////////////////////////
/////////////////////////////////////////////////
#ifdef SPIN_DEPENDENT_JASTROW

qmt::vmc::QmtVmcJastrow::QmtVmcJastrow(const std::vector<std::tuple<size_t,size_t,size_t,size_t>>& defs) {
    definitions = defs;
    std::set<size_t> to_cnt;
    std::set<size_t> u_cntr;
    for(const auto& item : definitions){
       to_cnt.insert(std::get<0>(item));
      if(!std::get<3>(item))
       u_cntr.insert(std::get<2>(item));
     }
    
    u_params_size = u_cntr.size();
    size_t size = to_cnt.size();
    params_size = size;
    
    std::set<std::tuple<size_t,size_t>> helper_spin;

    conections.resize(size);
    size_t cntr = 0;;
    std::fill(conections.begin(),conections.end(),SiteConections());
    for(const auto& item : definitions) {
     size_t id = std::get<0>(item);
     size_t connection = std::get<1>(item);
     size_t param_id = std::get<2>(item);
     
     conections[id].sp_ids.push_back(connection);
     conections[id].param_ids.push_back(param_id);
     if(std::get<3>(item)){
        helper_spin.insert(std::make_tuple(std::get<2>(item),cntr));
      cntr++;
     }
    }   
 
    for(const auto item : helper_spin){
    //std::cout<<"Adding:"<<std::get<0>(item)<<std::endl;
  spin_map[std::get<0>(item)] = std::get<1>(item);
   }
      
}




double qmt::vmc::QmtVmcJastrow::operator()(const std::vector< double >& parameters, const qmt::vmc::QmtConfigurationState& configuration) const
{
    double exponent = 0.0;
    for(const auto &def : definitions) {
        auto id1 = std::get<0>(def);
        auto id2 = std::get<1>(def);
        auto param = std::get<2>(def);
        auto u_flag = std::get<3>(def);
        auto converter = [&](bool p)->size_t {return p ? 1 : 0;};
 {
        if(u_flag){
            exponent += 0.5 * (converter(configuration.is_occupied_up(id1)))*(converter(configuration.is_occupied_up(id2))) *  				parameters[param]/4;
	   exponent += 0.5 * (converter(configuration.is_occupied_down(id1)))*(converter(configuration.is_occupied_down(id2)))* 			parameters[param]/4;

           exponent += 0.5 * (converter(configuration.is_occupied_up(id1)))*
                         (converter(configuration.is_occupied_down(id2))) *  parameters[spin_map.at(param) + params_size]/4;

	   exponent += 0.5 * (converter(configuration.is_occupied_down(id1)))*
                         (converter(configuration.is_occupied_up(id2))) *  parameters[spin_map.at(param) + params_size]/4;

                }        
        else {
            exponent +=  (converter(configuration.is_occupied_up(id1))* converter(configuration.is_occupied_down(id2))) * parameters[param]/2;
            }
        }

    }
    return  -exponent;

}




double qmt::vmc::QmtVmcJastrow::operator()(const std::vector< double >& parameters, const qmt::vmc::QmtConfigurationState& from,
										    const qmt::vmc::QmtConfigurationState& to) const
{
return (*this)(parameters,to) - (*this)(parameters,from);
}
 

size_t qmt::vmc::QmtVmcJastrow::get_number_of_params() const
{
   return 2*params_size - u_params_size;
}




#endif
