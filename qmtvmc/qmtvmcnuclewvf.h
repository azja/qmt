#ifndef QMTVMCNUCLEWVF_INCLUDED_H
#define #ifndef QMTVMCNUCLEWVF_INCLUDED_H

#include <vector>
#include <tuple>
#include <math>
#include "../headers/qmtvector.h"
#include <set>
#include <algorithm>
#include <string>
#include <stdexcept>

namespace qmt {
 namespace vmc {


struct QmtVmcIon {

size_t id;
qmt::QmtVector position; //a.u
double mass;  //a.u.
double charge; //a.u.
std::vector<QmtVmcIon> neigbours;

};





class QmtVmcNuclearWaveFunction {

size_t map_size;
double *map_matrix;

std::vector<std::tuple<size_t,size_t>> pairs;
std::vector<size_t> ids;
std::vector<qmt::QmtVector> lpositions;      //lattice/site positions
std::vector<qmt::QmtVector> dpositions;      //"dynamic" positions
std::vector<double> masses;		     //a.u. m_e=1/2
std::vector<double> charges;		     //a.u.e=1
std::vector<double> u_pp;
std::vector<double> c;
std::vector<double> pair_distances;
std::vector<double> site_relative_positions;

double *map_matrix;

std::vector<std::vector<size_t>> associations;


public:
  QmtVmcNuclearWaveFunction(const std::vector<QmtVmcIon>& ions, std::vector<std::tuple<size_t,size_t,int>> upp_params, std::vector<std::tuple<size_t,int>> c_params);


  /*
  double get_energy(const std::vector<double>& u, const std::vector<double>& c);
  double get_local_energy(const std::vector<double>& u, const std::vector<double>& c);
  double get_norm(const std::vector<double>& u, const std::vector<double>& c);
  double get_value(const std::vector<double>& u, const std::vector<double>& c);*/

  double get_value() const;
  void displace(size_t,const qmt:QmtVector&);
  void set_parameters(const std::vector<double>& u, const std::vector<double>& c);
      

  size_t get_number_of_ions() const;
  size_t get_number_of_pairs() const;
  
  ~QmtVmcNuclearWaveFunction() {
   }

};


  }
}
#endif 
