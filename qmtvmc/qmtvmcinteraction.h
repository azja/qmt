#ifndef QMT_VMC_INTERACTION_INCLUDED
#define QMT_VMC_INTERACTION_INCLUDED
#include "qmtconfbitset.h"
#include "qmtconfiguration.h"
#include "../headers/qmthbuilder.h"

namespace qmt {
 namespace vmc {


class QmtVmcInteraction{

public:
 virtual double get_energy(QmtConfigurationState&) = 0;

};



/************** U ****************/

class QmtVmcInteractionU : public QmtVmcInteraction {

size_t _band;
double _U;
double _factor;

public:

 QmtVmcInteractionU(size_t band,double U, double factor):_band(band),_U(U),_factor(factor) {
 
 }

  double get_energy(QmtConfigurationState&);
};


/************** K ****************/

class QmtVmcInteractionK : public QmtVmcInteraction {

size_t id_i;
size_t id_j;

double val_K;
qmt::QmtSpin _s_i;
qmt::QmtSpin _s_j;

double _factor;

public:

 QmtVmcInteractionU(size_t i, size_t j, double K, qmt::QmtSpin s_i,qmt::QmtSpin s_j,double factor):
					id_i(i),id_j(j),val_K(K), _s_i(s_i),_s_j(s_j),_factor(factor) {
 
 }

  double get_energy(QmtConfigurationState&);
};

//Correlated hopping
/**************** P ***************/

class QmtVmcInteractionP : public QmtVmcInteraction {

size_t id_i;
size_t id_j;

double val_K;
qmt::QmtSpin _s_i;
qmt::QmtSpin _s_j;

double _factor;

public:

 QmtVmcInteractionP(size_t i, size_t j, double P, qmt::QmtSpin s_i,qmt::QmtSpin s_j,double factor):
					id_i(i),id_j(j),val_K(K), _s_i(s_i),_s_j(s_j),_factor(factor) {
 
 }

  double get_energy(QmtConfigurationState&);
};




 }
}
#endif
