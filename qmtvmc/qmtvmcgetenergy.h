#ifndef QMTVMCGETENERGY_H
#define QMTVMCGETENERGY_H

#include "qmtconfiguration.h"

namespace qmt {
namespace vmc {

class QmtVmcGetEnergy
{
public:
    virtual ~QmtVmcGetEnergy() {};
    virtual double getEnergy(QmtConfigurationState& x) = 0;
    virtual double getOneBodyEnergy(QmtConfigurationState& x) = 0;
    virtual double getTwoBodyEnergy(QmtConfigurationState& x) = 0;
   
     virtual double getOnSiteEnergy(QmtConfigurationState& x) = 0;
//     virtual double getTwoCenterEnergy(QmtConfigurationState& x) = 0;
/*    virtual std::vector<double> getHoppingEnergies(qmt::vmc::QmtConfigurationState& x) = 0;
    virtual double getHoppingEnergyByList(qmt::vmc::QmtConfigurationState& x,const std::vector<double>& defs) = 0;
   */
};


} // end of namespace vmc
} //end of namespace qmt
#endif // QMTVMCGETENERGY_H
