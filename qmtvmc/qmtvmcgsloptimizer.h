#ifndef QMT_VMC_GSL_OPTIMIZER_H
#define QMT_VMC_GSL_OPTIMIZER_H

#include "qmtvmcoptimizer.h"
#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

namespace qmt {
 namespace vmc {
   
   class QmtVmcGslOptimizer : public QmtVmcOptimizer {
   
   static double get_value(const gsl_vector * x, void * params);
     
   struct Params {
      QmtVmcData* d;
      QmtVmcAlgorithm* alg; 
      size_t steps;
      size_t probing_steps;
   };
   
   public:
     
     double optimize( QmtVmcData& d, 
		      QmtVmcAlgorithm& alg, 
                     const std::vector<std::tuple<double,double,double>>& in_par,
                     std::vector<double>& out_par,
		     size_t steps,
		     size_t probing_steps);
     
   };
   
 }    
}



#endif 
