#ifndef _QMT_VMC_CONFIGURATION_BITSET
#define _QMT_VMC_CONFIGURATION_BITSET
#include "qmtconfiguration.h"
#include "../headers/qmtstate.h"
#include <random>
#include <chrono>
#include <memory>



namespace qmt {
namespace vmc {

template <size_t N>
class QmtConfigurationStateBitset:public QmtConfigurationState {
    typedef qmt::QmtNState<std::bitset<N>,int> n_state;
    n_state state;
    unsigned seed;

    size_t ups;
    size_t downs;
    
    
    typedef std::chrono::high_resolution_clock myclock;

    void prepare_seed() {
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        seed = d.count();
    }

    int rnd(int min, int max) {
        static std::default_random_engine generator(seed);
        std::uniform_int_distribution<int> distribution(min,max);
        return distribution(generator);
    }

    

public:

int phase_change(size_t i, size_t j) const {
        // for clarity ii<jj
        size_t ii = i<j ? i : j;
        size_t jj = i<j ? j : i;

        size_t sum=0U;

        for(size_t k=ii+1; k<jj; k++) {
            sum+=state.get_bit(k);
        }

        return sum%2 ? -1 : 1;

    }

    QmtConfigurationStateBitset(const n_state& s) {
        state = s;
        seed = 0;
        prepare_seed();
	
	ups = 0U;
        downs = 0U;
      for(auto i = 0U; i < N/2; ++i){
	if(state.is_occupied(i)) ups++;
	if(state.is_occupied(N/2 + i)) downs++;
      }
    }

    QmtConfigurationStateBitset(size_t up, size_t down):ups(up),downs(down) {
        
        prepare_seed();
        state = n_state(N/2);
        size_t cntr = 0U;
        while(cntr < up) {
            int i = rnd(0,N/2 - 1);
            if(!state.is_occupied(i)) {
                state.set_bit(i,true);
                cntr++;
            }
        }

        cntr = 0;

        while(cntr < down) {
            int i = rnd(N/2,N-1);
            if(!state.is_occupied(i)) {
                state.set_bit(i,true);
                cntr++;
            }
        }
        state.setPhase(1);
    }

    bool   is_occupied_up(size_t index) const {
        return state.is_occupied(index);
    }

    bool   is_occupied_down(size_t index) const {
        return state.is_occupied(N/2+ index);
    }

    QmtConfigurationState* get_similar() const {
      return new QmtConfigurationStateBitset<N>(ups,downs); 
    }

    QmtConfigurationState* get_copy() const {
      return new QmtConfigurationStateBitset<N>(*this); 
    }
    
    size_t get_size(void) const {
        return N/2;
    }


    double get_phase(void) const {
        return state.getPhase();
    }

    void set_phase(double phase) {
	state.setPhase(phase);
    }
    
    void set_bit(int i,bool v) {
     state.set_bit(i,v);
    }

    bool compare_configuration(const QmtConfigurationState& s) const{
     return n_state::compare_configuration(state,static_cast<const QmtConfigurationStateBitset<N>&>(s).state);
    }
    
    std::vector<QmtVmcHopping> get_hoppings(const QmtVmcHoppingsMap& map) {
        size_t spin_sector_size = N/2;
        std::vector<QmtVmcHopping> hoppings;

        /*
         * up
         */

        std::vector<size_t> occupied_idx;
        std::vector<size_t> empty_idx;
        for(size_t i = 0; i < spin_sector_size; ++i)
            if(is_occupied_up(i)) occupied_idx.push_back(i);
            else empty_idx.push_back(i);
        for(auto i : occupied_idx) {
            for(auto j : empty_idx) {
                const std::vector<size_t> &is_allowed_h_id = map.is_allowed(i ,j);
                if(is_allowed_h_id.size() == 0) continue;
//                QmtConfigurationStateBitset* fs = new  QmtConfigurationStateBitset(*this);
//                QmtConfigurationStateBitset* ts = new  QmtConfigurationStateBitset(*this);

//                QmtConfigurationState* from_state = fs;
//                QmtConfigurationState* to_state = ts;

		std::shared_ptr<QmtConfigurationState> from_state(new  QmtConfigurationStateBitset(*this));
		std::shared_ptr<QmtConfigurationState> to_state(new  QmtConfigurationStateBitset(*this));

                to_state->set_bit(i, false);
                to_state->set_bit(j, true);

                int sgn = phase_change(i,j);
                to_state->set_phase(to_state->get_phase()*sgn);
//                std::cout<<"("<<i<<","<<j<<") allowes #"<<is_allowed_h_id.size()<<std::endl;
                for(const auto item : is_allowed_h_id){
//                std::cout<<"("<<i<<","<<j<<") allowes #"<<is_allowed_h_id.size()<<" item = "<<item<<std::endl;
                    hoppings.push_back(QmtVmcHopping(from_state,to_state,item));
                }
                
            }
        }

	for(auto i : occupied_idx){
	        const std::vector<size_t> &is_allowed_h_id = map.is_allowed(i ,i);
                if(is_allowed_h_id.size() == 0) continue;

		std::shared_ptr<QmtConfigurationState> from_state(new  QmtConfigurationStateBitset(*this));
		std::shared_ptr<QmtConfigurationState> to_state(new  QmtConfigurationStateBitset(*this));


                for(const auto item : is_allowed_h_id){
//                        std::cout<<"("<<i<<","<<i<<") allowes #"<<is_allowed_h_id.size()<<" item = "<<item<<std::endl;
                    hoppings.push_back(QmtVmcHopping(from_state,to_state,item));
                }
        
	}
        occupied_idx.clear();
        empty_idx.clear();

        /*
         * down
         */

        for(size_t i = 0; i < spin_sector_size; ++i)
            if(is_occupied_down(i)) occupied_idx.push_back(i);
            else empty_idx.push_back(i);

        for(auto i : occupied_idx) {
            for(auto j : empty_idx) {
                const std::vector<size_t>& is_allowed_h_id = map.is_allowed(i,j);
                if(is_allowed_h_id.size() == 0) continue;
            //    QmtConfigurationStateBitset* fs = new  QmtConfigurationStateBitset(*this);
            //    QmtConfigurationStateBitset* ts = new  QmtConfigurationStateBitset(*this);

            //    QmtConfigurationState* from_state = fs;
            //    QmtConfigurationState* to_state = ts;
		std::shared_ptr<QmtConfigurationState> from_state(new  QmtConfigurationStateBitset(*this));
		std::shared_ptr<QmtConfigurationState> to_state(new  QmtConfigurationStateBitset(*this));

                to_state->set_bit(i+spin_sector_size, false);
                to_state->set_bit(j+spin_sector_size, true);

                int sgn = phase_change(i + spin_sector_size,j + spin_sector_size);
                //to_state->state.setPhase(state.getPhase()*sgn);
                to_state->set_phase(to_state->get_phase()*sgn);
                for(const auto item : is_allowed_h_id)
                    hoppings.push_back(QmtVmcHopping(from_state,to_state,item));
            }
        }
        
        for(auto i : occupied_idx){
	        const std::vector<size_t> &is_allowed_h_id = map.is_allowed(i ,i);
                if(is_allowed_h_id.size() == 0) continue;

		std::shared_ptr<QmtConfigurationState> from_state(new  QmtConfigurationStateBitset(*this));
		std::shared_ptr<QmtConfigurationState> to_state(new  QmtConfigurationStateBitset(*this));


                for(const auto item : is_allowed_h_id){
                
                    hoppings.push_back(QmtVmcHopping(from_state,to_state,item));
                }
        }
	
        
        
        return hoppings;
    }

     
QmtVmcHopping* get_single_hopping(size_t i, size_t j,const QmtVmcHoppingsMap& map, int spin) const {
   // if( 0U == map.is_allowed(i,j).size() ) return 0.0;

    size_t offset = 0U;
    if(spin < 0)
    offset += get_size();
    
    if(spin > 0)
    if( !is_occupied_up(i) || is_occupied_up(j)) return NULL;

    if(spin < 0)
    if( !is_occupied_down(i) || is_occupied_down(j)) return NULL;


    auto to_state = new QmtConfigurationStateBitset(*this);
    auto from_state = new QmtConfigurationStateBitset(*this);

    to_state->set_bit(i + offset,false);
    to_state->set_bit(j + offset,true);
    int sgn = phase_change(i + offset,j + offset);
    to_state->set_phase(to_state->get_phase()*sgn);
    return new QmtVmcHopping(std::shared_ptr<QmtConfigurationState>(from_state),std::shared_ptr<QmtConfigurationState>(to_state),0);
   }


    friend std::ostream& operator<<(std::ostream& stream,const QmtConfigurationStateBitset<N>& state) {
        stream<<state.state;
        return stream;
    }

};


}
}

#endif // qmtconfbitset.h
