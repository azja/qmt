#include "qmtvmcgslderoptimizer.h"

double qmt::vmc::QmtVmcGslDerOptimizer::get_value(const gsl_vector* x, void* params)
{
  Params *p = static_cast<Params*>(params);
  std::vector<double> v_p;
  //v_p.push_back(0);
  for(size_t i = 0U; i < x->size; ++i){
   v_p.push_back(gsl_vector_get(x,i));
  }

  p->d->prob_calc->set_parameters(v_p);
  p->alg->run(p->steps);

  double estimator = p->alg->get_variance_estimator(p->probing_steps);

/*  for(size_t i = 0U; i < x->size; ++i){
   std::cout<<gsl_vector_get(x,i)<<" ";
  }
  std::cout<<" var = "<<estimator<<std::endl;  */

  return  estimator; 
}


void qmt::vmc::QmtVmcGslDerOptimizer::get_gradient(const gsl_vector* x, void* params,gsl_vector* df)
{

  
  Params *p = static_cast<Params*>(params);
  auto get_v = [&](double h,size_t id)->double{
   std::vector<double> v_p;
  //v_p.push_back(0);
   for(size_t i = 0U; i < x->size; ++i){
     double add = 0.0;
     if(i == id) add = h;
     v_p.push_back(gsl_vector_get(x,i) + add);
   }
   
  p->d->prob_calc->set_parameters(v_p);
  p->alg->run(p->steps - p->probing_steps);
  return p->alg->get_variance_estimator(p->probing_steps);};
  
  double dh = 0.01;
  for(size_t i = 0U; i < x->size;++i){
    double der = 0.5*(get_v(dh,i)- get_v(-dh,i))/dh;
  gsl_vector_set(df,i,der);
  }
/*
std::cout<<"Gradient:";
for(size_t i = 0U; i < x->size;++i){
 std::cerr<<" "<<gsl_vector_get(df,i);
}
std::cout<<std::endl;*/
}

void qmt::vmc::QmtVmcGslDerOptimizer::get_value_and_gradient(const gsl_vector *x, void *params, double *out, gsl_vector *df){
*out = qmt::vmc::QmtVmcGslDerOptimizer::get_value(x,params);
qmt::vmc::QmtVmcGslDerOptimizer::get_gradient(x,params,df);
}


double qmt::vmc::QmtVmcGslDerOptimizer::optimize( qmt::vmc::QmtVmcData& d, 
					       qmt::vmc::QmtVmcAlgorithm& alg,
					      const std::vector< std::tuple< double, double, double > >& in_par, 
					      std::vector< double >& out_par,
					      size_t steps, 
					      size_t probing_steps) {
  out_par.clear();
  Params p;
  p.d = &d;
  p.alg = &alg;
  p.probing_steps=probing_steps;
  p.steps = steps;
  
  int dim = in_par.size();// - 1 ;
  

    double result = 0.0;

std::vector<std::tuple<double,double,double>> input;
int cycle = 0;

for(auto item : in_par)
input.push_back(item);


while(cycle<5) { //Było 5


  size_t iter = 0;
  int status;
  const gsl_multimin_fdfminimizer_type *T = 
    gsl_multimin_fdfminimizer_vector_bfgs2;
  gsl_multimin_fdfminimizer *s = gsl_multimin_fdfminimizer_alloc(T,dim);
  gsl_vector *ss, *x;
  gsl_multimin_function_fdf minex_func;


  x = gsl_vector_alloc (dim);
for(int i = 0; i < dim; ++i)
    gsl_vector_set (x, i, std::get<1>(input[i]));
  
  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc (dim);
  gsl_vector_set_all (ss, 0.01);

  /* Initialize method and iterate */
  minex_func.n = dim;
  minex_func.f = get_value;
  minex_func.df = get_gradient;
  minex_func.fdf = get_value_and_gradient;
  minex_func.params = &p;

  gsl_multimin_fdfminimizer_set (s, &minex_func, x, 0.1, 0.01);

  do
    {
      iter++;
      status = gsl_multimin_fdfminimizer_iterate(s);
      
      if (status) 
        break;

      status = gsl_multimin_test_gradient (s->gradient, 1e-4); // było 1e-4
//        std::cout<<"ITERATION #"<<iter<<std::endl;
    }

  while (status == GSL_CONTINUE && iter < 500); //Było 100
  cycle++;
  

  input.clear();
  
  out_par.clear();



  for(int i = 0; i < dim;++i)
   out_par.push_back(gsl_vector_get(s->x,i));

 for(int i = 0; i < dim; ++i)
    input.push_back(std::make_tuple(0,out_par[i],0));  

  gsl_vector_free(x);
  gsl_vector_free(ss);

  p.d->prob_calc->set_parameters(out_par);

 // result =   p.alg->get_local_energy(p.probing_steps);
 p.alg->unblock();
/*
std::cout<<"----------------------------------------------------------------"<<std::endl;
for(auto item : out_par) {
 std::cout<<item<<" ";
}
std::cout<<std::endl;
std::cout<<"<< ENERGY = "<<result<<" >>"<<std::endl;
std::cout<<"----------------------------------------------------------------"<<std::endl;*/
  gsl_multimin_fdfminimizer_free (s);

 
 }
  
  
  p.alg->run(p.steps);
 // result =   p.alg->get_local_energy(p.steps);
  result = p.alg->get_observables(p.steps,*(p.d->stat));

//PRINT JASTROWS

//for(int i = 0; i < dim;++i)
//   std::cout<<" "<<out_par[i]<<" ";

  
return result;
}
