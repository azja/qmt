#include "qmtvmclocald.h"


qmt::vmc::QmtVmcLocalD::QmtVmcLocalD(size_t id):_id(id){

}

/*
/QmtVmcLocalObservable implementation
*/

double qmt::vmc::QmtVmcLocalD::get_value(const QmtConfigurationState& x){
double v1 = 0;
double v2 = 0;
	if(x.is_occupied_up(_id)) v1 += 1.0;
	if(x.is_occupied_down(_id)) v2 += 1.0;
 return v1*v2;
}

/*
/print
*/


std::ostream& qmt::vmc::QmtVmcLocalD::print(std::ostream& s) const {
 s<<"<D"<<_id<<">";
return s;
}

