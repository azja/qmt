#include "qmtvmclocalm.h"


qmt::vmc::QmtVmcLocalM::QmtVmcLocalM(size_t id):_id(id){

}

/*
/QmtVmcLocalObservable implementation
*/

double qmt::vmc::QmtVmcLocalM::get_value(const QmtConfigurationState& x){
double v = 0;
	if(x.is_occupied_up(_id)) v += 1.0;
	if(x.is_occupied_down(_id)) v -= 1.0;
 return v;
}

/*
/print
*/


std::ostream& qmt::vmc::QmtVmcLocalM::print(std::ostream& s) const {
 s<<"<M"<<_id<<">";
return s;
}

