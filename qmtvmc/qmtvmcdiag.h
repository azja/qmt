#ifndef QMT_VMCDIAGONALIZER_H
#define QMT_VMCDIAGONALIZER_H

#include "../headers/qmtdiagonalizer.h"
#include "../headers/qmtstate.h"
#include "../headers/qmthubbard.h"
#include "../headers/qmthbuilder.h"
#include "../headers/qmtbasisgenerator.h"
#include "qmtvmcprobabilityGSL.h"
#include "qmtslaterstate.h"
#include "qmtvmcsingleparticleproblemsolver.h"
#include "qmtvmcgsloptimizer.h"
#include "qmtvmcgslderoptimizer.h"
#include "qmtvmcsimulatedannealing.h"
#include "qmtvmcmetropolis.h"
#include "qmtvmcstat.h"
#include "qmtvmcstatgenerator.h"
#include <fstream>

namespace qmt {
  namespace vmc {
  template <size_t N>
    class QmtVmcGslMetropolisDiagonalizer:public qmt::QmtDiagonalizer {
        typedef qmt::QmtNState<std::bitset<N>, int> qmt_nstate;
	typedef qmt::QmtHubbard<qmt::SqOperator<qmt_nstate >> qmt_hamiltonian;
	typedef qmt::vmc::QmtVmcProbabilityGSL<N> qmt_vmc_probability;
	typedef qmt::vmc::QmtConfigurationStateBitset<N> qmt_vmc_bitset_conf;
	 
	size_t nup;
	size_t ndown;
	size_t n;
	std::vector<std::tuple<size_t,size_t,size_t>> hop_map_vec;
#ifndef QMTVMC_MULTICENTER_ENERGY
	qmt::vmc::QmtVmcJastrow* jastrow;
#else
	qmt::vmc::QmtVmcTwoBodyJastrow* jastrow;
#endif
	qmt::vmc::QmtSlaterState* uncorrelated_state;
	qmt_hamiltonian* hamiltonian;
	qmt::QmtBasisGenerator generator;
	std::vector<qmt_nstate> basis;
	qmt::vmc::QmtVmcSingleParticleProblemSolver<qmt_hamiltonian> * problem_solver;
	qmt::vmc::QmtVmcProbability *probability;
	qmt::vmc::QmtVmcHoppingsMap *map;
	std::shared_ptr<qmt::vmc::QmtConfigurationState> x;
#ifndef QMTVMC_MULTICENTER_ENERGY
	std::vector<std::tuple<size_t, size_t, size_t,size_t>> interaction_definition;
#else
        std::vector<qmt::QmtInteractionDefinition> interaction_definition;
#endif
	std::vector<unsigned int> sp_energy;
	std::vector<std::tuple<double,double,double>> old_input;
	size_t n_steps;
	
	qmt::vmc::QmtVmcStat *stat;
	
    public:
      
      QmtVmcGslMetropolisDiagonalizer(const std::string ham_file,
				      size_t n_up, 
				      size_t n_down,
				      size_t steps)
				      :nup(n_up), ndown(n_down),n(n_up + n_down),n_steps(steps) {
					
					
	sp_energy = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getSPEnergyMap(ham_file);

#ifndef QMTVMC_MULTICENTER_ENERGY	
        auto js = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getInteractionMap(ham_file);
	
	
	jastrow = new qmt::vmc::QmtVmcJastrow(js);
#endif

	uncorrelated_state = new qmt::vmc::QmtSlaterState();


#ifdef QMTVMC_MULTICENTER_ENERGY	
             
             
	    auto js = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getInteractionMapVMC(ham_file,N/2);
       	    std::cout<<"Size of interactions:"<<js.size()<<std::endl;
        /*    
           std::vector<std::tuple<size_t,size_t,size_t,size_t>> resultant_set;
            qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::GetJastrows(js,resultant_set,N/2);
	
	    jastrow = new qmt::vmc::QmtVmcJastrow(resultant_set);
            std::cout<<"Jastrow number of params:"<<jastrow->get_number_of_params()<<" size of resultant set"<<resultant_set.size()<<std::endl;*/

           #ifdef _QMT_VMC_DEBUG
	     std::cerr<<"QmtVmcGslMetropolisDiagonalizer::Diagonalize - creating QmtVmCTwoBodyJastrow...";
	   #endif

           std::ifstream JastrowFile("jastrows.dat");
           if(JastrowFile.fail()){   
                         std::cout<<"Jastrow not found"<<std::endl;
                         jastrow = new qmt::vmc::QmtVmcTwoBodyJastrow(js,N/2);
           }  
           else{
           jastrow = new qmt::vmc::QmtVmcTwoBodyJastrow(JastrowFile,N/2);
           JastrowFile.close();
           }

           
           #ifdef _QMT_VMC_DEBUG
	     std::cerr<<"done"<<std::endl;
	   #endif
      
       
#endif


	// get hamiltonian
	hamiltonian =  qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(ham_file,1);
	
	// create single-particle basis
	

	generator.generate_single_particle<N>(basis);
//generator.generate<N>(1,N/2,basis);
	// solve single-particle problem

	problem_solver = new qmt::vmc::QmtVmcSingleParticleProblemSolver<qmt_hamiltonian> (hamiltonian,basis);
		
	/*
	 * probability
	 */
	
        #ifdef _QMT_VMC_DEBUG
	std::cerr<<"QmtVmcGslMetropolisDiagonalizer::Diagonalize - creating probability...";
	#endif


	probability = new qmt::vmc::QmtVmcProbabilityGSL<N>(n,jastrow,uncorrelated_state);
	
        #ifdef _QMT_VMC_DEBUG
	std::cerr<<"done"<<std::endl;;
	#endif
	


	 x = std::shared_ptr<qmt::vmc::QmtConfigurationState>(new qmt::vmc::QmtConfigurationStateBitset<N>(n_up,n_down));
	
	
	/*
	 * Hoppings map
	 */
	
	hop_map_vec = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHoppingMap(ham_file);
		

	map = new qmt::vmc::QmtVmcHoppingsMap(hop_map_vec);
	
	/*
	 * Interaction definitions
	 */
	
#ifndef QMTVMC_MULTICENTER_ENERGY
	
	interaction_definition  = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getInteractionMap(ham_file);
#else
        interaction_definition  = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getInteractionMapVMC(ham_file,N/2);
#endif


	auto generator = qmt::vmc::QmtVmcStatGenerator("avgdefs.dat");
	qmt::vmc::QmtVmcData temp_data; 
	temp_data.prob_calc = probability;
	generator.generate(temp_data);
        stat = temp_data.stat;



      }
      
      double Diagonalize(const std::vector<double>& one_body_integrals, const std::vector<double>& two_body_integrals){

	std::vector<double>  one_body_integrals_bis(one_body_integrals.size() - 1);
	std::copy(one_body_integrals.begin() + 1,one_body_integrals.end(),one_body_integrals_bis.begin());

	problem_solver->get_single_particle_eigenstates(n,nup,ndown,*uncorrelated_state,one_body_integrals_bis);
	#ifdef _QMT_VMC_DEBUG
	std::cout<<"QmtVmcGslMetropolisDiagonalizer::Diagonalize  - Single particle eigenstates computed"<<std::endl;
	#endif

	qmt::vmc::QmtVmcGetEnergy  *energy_calculator = new qmt::vmc::QmtVmcHamiltonianEnergy(
	  *uncorrelated_state,interaction_definition,two_body_integrals,sp_energy,one_body_integrals_bis,map,probability);
        
	qmt::vmc::QmtVmcData data; 
	double product = 0.0;
	double threshold = 1.0e-8;
	int cntr = 0U;	
        
        do {
        cntr++;
        x = std::shared_ptr<qmt::vmc::QmtConfigurationState>(new qmt::vmc::QmtConfigurationStateBitset<N>(nup,ndown));
        product= probability->product(x.get());
        if(cntr%50000 == 0) threshold /= 100; 
//	std::cout<<"product = "<<product<<" threshold:"<<threshold<<std::endl;
        }
        while(fabs(product) < threshold);

	data.prob_calc = probability;
	data.energy_calc = energy_calculator;
	data.map = map;
	data.slater = uncorrelated_state;
        data.stat = stat;
	



//	auto generator = qmt::vmc::QmtVmcStatGenerator("avgdefs.dat");
//	generator.generate(data);
//        stat = data.stat;

	stat->reset();

	#ifdef _QMT_VMC_DEBUG
	std::cerr<<"QmtVmcGslMetropolisDiagonalizer::Diagonalize  - QmtVmcMetropolisCorrelatedSampling creating...";
	#endif

	qmt::vmc::QmtVmcAlgorithm* metropolis = new qmt::vmc::QmtVmcMetropolisCorrelatedSampling(data,x);//new qmt::vmc::QmtVmcMetropolis(data,x);

	#ifdef _QMT_VMC_DEBUG
	std::cerr<<"done"<<std::endl;
	#endif


	qmt::vmc::QmtVmcOptimizer *optimizer = /*new qmt::vmc::QmtVmcSimulatedAnnealing();/*/ new qmt::vmc::QmtVmcGslDerOptimizer();
	
	std::vector<std::tuple<double,double,double>> input;
	if(old_input.size() < 1U) {
	for(size_t i=0; i < jastrow->get_number_of_params();++i)
	  input.push_back(std::make_tuple(0.0,0.0,0.0)); //here only second is important cause qmt::vmc::QmtVmcGslOptimizer() uses simplex // not in simulated annelaing, here we need first and third ones
        }
        else{
        for(size_t i=0U; i < jastrow->get_number_of_params();++i)
	  input.push_back(old_input[i]); //here only second is important cause qmt::vmc::QmtVmcGslOptimizer() uses simplex // not in simulated annelaing, here we need first and third ones
        }
        
/*input.push_back(std::make_tuple(0.2,0.225,0.25));
input.push_back(std::make_tuple(0.29,0.312,0.35));
input.push_back(std::make_tuple(0.13,0.143,0.16));
input.push_back(std::make_tuple(0.78,0.809,0.90));
input.push_back(std::make_tuple(0.17,0.187,0.2));*/

//	for(size_t i=0; i < 10;++i)
//	  input.push_back(std::make_tuple(-0.01,-0.3,0.1)); //here only second is important cause qmt::vmc::QmtVmcGslOptimizer() uses simplex // not in simulated annelaing, here we need first and third ones

	
	 std::vector<double> output; 
	
	stat->reset();
	double result = optimizer->optimize(data,*metropolis,input,output,n_steps,0.8*n_steps) + one_body_integrals[0] ;
	
	old_input.clear();
	for(size_t i = 0U; i < output.size();++i)
	old_input.push_back(std::make_tuple(0.0,output[i],0.0));
	
	delete energy_calculator;
	delete metropolis;
	delete optimizer;
/*

        std::cout<<stat->get_names()<<std::endl;
	
	for(auto item : stat->get_values())
        std::cout<<item<<" ";
        std::cout<<std::endl;  */



	return result;
      }
  
  virtual ~QmtVmcGslMetropolisDiagonalizer(){
	delete jastrow;
	delete uncorrelated_state;
	delete hamiltonian;
	delete problem_solver;
	delete probability;
	delete map;
        delete stat;
  }
  
const qmt::vmc::QmtVmcStat& get_stat() const {
 return *stat;
}
  
  };
  
  
  
  }
}




#endif //end of qmtvmcdiag.h

