#ifndef QMTVMCLOCALSDIMDIM_H
#define QMTVMCLOCALSDIMDIM_H
#include "qmtvmclocalobs.h"

namespace qmt {
namespace vmc {

class QmtVmcLocalSDimDim : public QmtVmcLocalObservable {

size_t _id1;
size_t _id2;

std::ostream& print(std::ostream& s) const;

public:

QmtVmcLocalSDimDim(size_t id1, size_t id2);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);

};

}
}
#endif
