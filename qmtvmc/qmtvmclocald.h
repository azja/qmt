#ifndef QMTVMCLOCALD_H
#define QMTVMCLOCALD_H
#include "qmtvmclocalobs.h"

namespace qmt {
namespace vmc {

class QmtVmcLocalD : public QmtVmcLocalObservable {

size_t _id;
std::ostream& print(std::ostream& s) const;
public:

QmtVmcLocalD(size_t id);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);

};

}
}
#endif
