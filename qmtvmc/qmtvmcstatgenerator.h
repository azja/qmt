#ifndef QMTVMCSTATGENERATOR_H
#define QMTVMCSTATGENERATOR_H

#include "../headers/qmtparsertools.h"
#include <string>
#include "qmtvmclocalobs.h"
#include "qmtvmclocaln.h"
#include "qmtvmclocalnn.h"
#include "qmtvmclocalhop.h"
#include "qmtvmclocalm.h"
#include "qmtvmclocalsdimdim.h"
#include "qmtvmclocalss.h"
#include "qmtvmclocald.h"
#include "qmtvmcdata.h"


namespace qmt {
  namespace vmc {

class QmtVmcStatGenerator {

std::vector<std::string> definitions;

std::vector<qmt::vmc::QmtVmcLocalObservable*> get_n(const std::string& definition);
std::vector<qmt::vmc::QmtVmcLocalObservable*> get_nn(const std::string& definition);
std::vector<qmt::vmc::QmtVmcLocalObservable*> get_hop(const std::string& definition, qmt::vmc::QmtVmcData& vmc_data);
std::vector<qmt::vmc::QmtVmcLocalObservable*> get_sdimdim(const std::string& definition);
std::vector<qmt::vmc::QmtVmcLocalObservable*> get_m(const std::string& definition);
std::vector<qmt::vmc::QmtVmcLocalObservable*> get_ss(const std::string& definition);
std::vector<qmt::vmc::QmtVmcLocalObservable*> get_d(const std::string& definition);
public:

  QmtVmcStatGenerator(const char* filename);
  
  void generate(qmt::vmc::QmtVmcData &vmc_data);
 

  };
 }
}
#endif
