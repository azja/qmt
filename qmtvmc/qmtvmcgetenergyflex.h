#ifndef QMTVMCGETENERGYFLEXIBLE_H
#ifndef QMTVMCGETENERGYFLEXIBLE_H
#include "qmtvmcgetenergy.h"
#include "../headers/qmthbuilder.h"


namespace qmt {
namespace vmc {

/*
The computation of ALL interaction terms (in a given range) is from the practical point of view nearly impossible in a second quantization.
However, let's try to include at least all (in a given range) two-centre Hamiltonian terms
It for what this class is.
Therefeore we divide interaction for the following classes,assuming that single-paritcle basis is real:

-U terms (Hubbard repulsion)
-K terms (density-density terms)
-J terms (exchange terms)
-P terms (jump of pair)
-V terms (correlated hopping terms)
*/


class QmtVmcGetEnergyFlexible : public QmtVmcGetEnergy {

    QmtSlaterState uncorrelated_state;
    std::vector<qmt::QmtInteractionDefinition> interaction_defs;
    std::vector<double> interaction_parameters;
    std::vector<unsigned int> single_particle_energy;
    const QmtVmcHoppingsMap* hopping_map;
    const QmtVmcProbability* probability;
    std::vector<double> hopping_amplitudes;

protected:

struct QmtVmcInteraction {
double interaction;
};

struct QmtVmc_U : public QmtVmcInteraction {
size_t i;
};

struct QmtVmc_K : public QmtVmcInteraction {
size_t i,j;
};

struct QmtVmc_J : public QmtVmcInteraction {
size_t i,j;
int spin_i,spin_j;
};

struct QmtVmc_P : public : public QmtVmcInteraction {
size_t i,j;
int spin_i,spin_j;
};


public:
    QmtVmcHamiltonianEnergy(const QmtSlaterState _uncorrelated_state,
                            const std::vector<qmt::QmtInteractionDefinition>& _interaction_defs,
                            const std::vector<double>& _interaction_parameters,
                            const std::vector<unsigned int>& _single_particle_energy,
                            const std::vector<double>& _hopping_amplitudes,
                            const QmtVmcHoppingsMap* _hopping_map,
                            const QmtVmcProbability* _probability);

/*
/Implementation of qmt::vmc::QmtVmcGetEnergy
*/

    double getEnergy(QmtConfigurationState& x);
    double getOneBodyEnergy(QmtConfigurationState& x);
    double getTwoBodyEnergy(QmtConfigurationState& x);


};


}
}

#endif //QMTVMCGETENERGYFLEXIBLE_H
