#include "../../headers/qmtslaterorbitals.h"
#include "../../gorb/gaussexp.h"
#include "../../headers/qmtbform.h"
#include "../../headers/qmteqsolver.h"

int main(){
	qmt::SltOrb_1s jeden(qmt::QmtVector(0,0,0),1.0,19);
	qmt::SltOrb_2pX dwa(qmt::QmtVector(1,0,0),1);
	qmt::SltOrb_2pY trzy(qmt::QmtVector(1,0,0),1);
	qmt::SltOrb_2pZ cztery(qmt::QmtVector(0,0,1),1);

	std::cout<<qmt::SlaterOrbital::overlap(jeden,dwa)<<std::endl;
	std::cout<<qmt::SlaterOrbital::overlap(dwa,dwa)<<std::endl;
	std::cout<<qmt::SlaterOrbital::overlap(dwa,trzy)<<std::endl;
	std::cout<<qmt::SlaterOrbital::overlap(dwa,cztery)<<std::endl;

/*
	Wannier<qmt::SlaterOrbital> wannier_1s1;
	Wannier<qmt::SlaterOrbital> wannier_1s2;
	Wannier<qmt::SlaterOrbital> wannier_2s1;
	Wannier<qmt::SlaterOrbital> wannier_2s2;
*/

	QmExpansion<qmt::SlaterOrbital> w1s1;
	QmExpansion<qmt::SlaterOrbital> w2s1;
	QmExpansion<qmt::SlaterOrbital> w1s2;
	QmExpansion<qmt::SlaterOrbital> w2s2;
	QmExpansion<qmt::SlaterOrbital> w2p1;
	QmExpansion<qmt::SlaterOrbital> w2p2;

	double alpha1s=1.0;
	double alpha2s=0.5;
	double alpha2p=0.5;
	double R=1.43;

	qmt::SltOrb_1s s1(qmt::QmtVector(0,0,0),alpha1s,5);
	qmt::SltOrb_1s s2(qmt::QmtVector(0,0,R),alpha1s,5);

	qmt::SltOrb_1s s3(qmt::QmtVector(0,0,0),alpha2s,5);
	qmt::SltOrb_1s s4(qmt::QmtVector(0,0,R),alpha2s,5);
	qmt::SltOrb_2s s5(qmt::QmtVector(0,0,0),alpha2s);
	qmt::SltOrb_2s s6(qmt::QmtVector(0,0,R),alpha2s);

	qmt::SltOrb_1s s7(qmt::QmtVector(0,0,0),alpha2s,5);
	qmt::SltOrb_1s s8(qmt::QmtVector(0,0,R),alpha2s,5);
	qmt::SltOrb_2s s9(qmt::QmtVector(0,0,0),alpha2s);
	qmt::SltOrb_2s s10(qmt::QmtVector(0,0,R),alpha2s);
	qmt::SltOrb_2pZ s11(qmt::QmtVector(0,0,0),alpha2p);
	qmt::SltOrb_2pZ s12(qmt::QmtVector(0,0,R),alpha2p);

	w1s1.add_element_with_cindex(0,1.0,s1);
	w1s1.add_element_with_cindex(1,1.0,s2);

	w1s2.add_element_with_cindex(0,1.0,s2);
	w1s2.add_element_with_cindex(1,1.0,s1);

	w2s1.add_element_with_cindex(2,1.0,s3);
	w2s1.add_element_with_cindex(3,1.0,s4);
	w2s1.add_element_with_cindex(4,1.0,s5);
	w2s1.add_element_with_cindex(5,1.0,s6);

	w2s2.add_element_with_cindex(2,1.0,s4);
	w2s2.add_element_with_cindex(3,1.0,s3);
	w2s2.add_element_with_cindex(4,1.0,s6);
	w2s2.add_element_with_cindex(5,1.0,s5);

	w2p1.add_element_with_cindex(6,1.0,s7);
	w2p1.add_element_with_cindex(7,1.0,s8);
	w2p1.add_element_with_cindex(8,1.0,s9);
	w2p1.add_element_with_cindex(9,1.0,s10);
	w2p1.add_element_with_cindex(10,1.0,s11);
	w2p1.add_element_with_cindex(11,1.0,s12);

	w2p2.add_element_with_cindex(6,1.0,s8);
	w2p2.add_element_with_cindex(7,1.0,s7);
	w2p2.add_element_with_cindex(8,1.0,s10);
	w2p2.add_element_with_cindex(9,1.0,s9);
	w2p2.add_element_with_cindex(10,1.0,s12);
	w2p2.add_element_with_cindex(11,1.0,s11);

	double **M = new double*[12];
	for (int i = 0; i < 12; ++i)
	    M[i] = new double[12];

	std::vector<BilinearForm> forms;

	QmExpansion<qmt::SlaterOrbital>::multiply(w1s1,w1s1,M,12);
	forms.push_back(BilinearForm(M,12,-1));
	QmExpansion<qmt::SlaterOrbital>::multiply(w2s1,w2s1,M,12);
	forms.push_back(BilinearForm(M,12,-1));
	QmExpansion<qmt::SlaterOrbital>::multiply(w2p1,w2p1,M,12);
	forms.push_back(BilinearForm(M,12,-1));
	QmExpansion<qmt::SlaterOrbital>::multiply(w1s1,w1s2,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w2s1,w2s2,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w2p1,w2p2,M,12);
	forms.push_back(BilinearForm(M,12));

/*
	for(int i=0; i<12; ++i){
		for(int j=0; j<12; ++j)
			std::cout<<M[i][j]<<" ";
		std::cout<<std::endl;
	}
*/

	QmExpansion<qmt::SlaterOrbital>::multiply(w1s1,w2s1,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w1s1,w2p1,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w2s1,w2p1,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w1s1,w2s2,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w1s1,w2p2,M,12);
	forms.push_back(BilinearForm(M,12));
	QmExpansion<qmt::SlaterOrbital>::multiply(w2s1,w2p2,M,12);
	forms.push_back(BilinearForm(M,12));

	double in[]={1.0,-0.1,-0.1,0.1,1.0,0.0,0.0,-0.1,-0.1,0.1,1.0,0.0};
	double out[]={1.0,-0.1,-0.1,0.1,1.0,0.0,0.0,-0.1,-0.1,0.1,1.0,0.0};
	NonLinearSystemSolve(forms, in, out, 1.0e-12);

	std::vector<double> betas;

	for (int i = 0; i < 12; i++) {
		betas.push_back(out[i]);
		std::cout<<" b["<<i<<"] = "<<out[i];
	}
		std::cout<<std::endl;

	for(auto i=0; i<2; i++){
		w1s1.set_c_by_id(i,betas[i]);
		w1s2.set_c_by_id(i,betas[i]);
	}
	for(auto i=2; i<6; i++){
		w2s1.set_c_by_id(i,betas[i]);
		w2s2.set_c_by_id(i,betas[i]);
	}
	for(auto i=6; i<12; i++){
		w2p1.set_c_by_id(i,betas[i]);
		w2p2.set_c_by_id(i,betas[i]);
	}
	std::cout<<"<w1s1|w1s1>= "<<QmExpansion<qmt::SlaterOrbital>::overlap(w1s1, w1s1)<<std::endl;
	std::cout<<"<w1s1|w1s2>= "<<QmExpansion<qmt::SlaterOrbital>::overlap(w1s1, w1s2)<<std::endl;
	std::cout<<"<w1s1|w2s1>= "<<QmExpansion<qmt::SlaterOrbital>::overlap(w1s1, w2s1)<<std::endl;
	std::cout<<"<w2s1|w2s1>= "<<QmExpansion<qmt::SlaterOrbital>::overlap(w2s1, w2s1)<<std::endl;
	std::cout<<"<w2p1|w1s1>= "<<QmExpansion<qmt::SlaterOrbital>::overlap(w2p1, w1s1)<<std::endl;
	std::cout<<"<w2p1|w2p1>= "<<QmExpansion<qmt::SlaterOrbital>::overlap(w2p1, w2p1)<<std::endl;
	return 0;
}

