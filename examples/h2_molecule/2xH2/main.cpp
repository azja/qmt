#include "../../headers/qmtmpioptimizer.h"
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <algorithm>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

//class

struct Problem2xH2 {
    typedef double returned_value;

    qmt::examples::Microscopic2xH2 model;
    qmt::examples::Hamiltonian2xH2 hamiltonian;
    qmt::QmtLanczosSolver<gsl_matrix, gsl_vector> solver;

    double _a;
    double _b;

    Problem2xH2(double a, double b): _a(a), _b(b) {
        model.set(_a, _b, 1);
    }

    bool stop(double x, double y) const {
        return fabs(x-y) < 1.0e-7;

    }

    void set_ab(double a, double b) {
        _a = a;
        _b = b;

        model.set(a,b,1);
    }

    returned_value operator()(returned_value alpha)  {
        model.set_alpha(alpha);
        std::vector<double> integrals;
        model.get_integrals(integrals);

        hamiltonian.set_integrals(integrals);

        gsl_matrix * novae = hamiltonian.get_matrix();
        solver.setProblemMatrix(novae);
        solver.run(1.0e-4);
        return  solver.get_minimal_eigenvalue() + 4/(_a) + 4/(_b) + 4/(sqrt(_a*_a + _b*_b));


    }

};



#include <vector>

int main() {


    MPI_Init(NULL,NULL);


    int taskid;
    int nproc;
    double t_start = 0;
    double t_end = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);

    if(taskid == 0)
        t_start = MPI_Wtime();


    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    Problem2xH2 problem(1.4, 1.5);

    double result = qmtMpiBruteOptimizer<Problem2xH2, MPI_DOUBLE>( 0.5,2.0, problem);

    if(taskid == 0) {
        std::cout<<"alfa [1/a_0] E_G [eV] nproc time[s]"<<std::endl;
        t_end = MPI_Wtime();
        std::cout<<result<<" "<<problem(result)<<" "<<nproc<<" "<<t_end - t_start<<std::endl;
    }

    MPI_Finalize();

    return 0;
}


