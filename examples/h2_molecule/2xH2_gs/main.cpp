#include "../../headers/qmtmpioptimizer.h"
#include "../../headers/qmtgsltools.h"
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <algorithm>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

//class

struct Problem2xH2 {
    typedef double returned_value;

    qmt::examples::Microscopic2xH2 model;
    qmt::examples::Hamiltonian2xH2 hamiltonian;
    qmt::QmtLanczosSolver<gsl_matrix, gsl_vector> solver;

    double _a;
    double _b;

    Problem2xH2(double a, double b): _a(a), _b(b) {
        model.set(_a, _b, 1);
    }

    bool stop(double x, double y) const {
        return fabs(x-y) < 1.0e-7;

    }

    void set_ab(double a, double b) {
        _a = a;
        _b = b;

        model.set(a,b,1);
    }

    returned_value operator()(returned_value alpha)  {
        model.set_alpha(alpha);
        std::vector<double> integrals;
        model.get_integrals(integrals);

        hamiltonian.set_integrals(integrals);

        gsl_matrix * novae = hamiltonian.get_matrix();
        solver.setProblemMatrix(novae);
        solver.run(1.0e-5);

        return solver.get_minimal_eigenvalue() + 4/(_a) + 4/(_b) + 4/(sqrt(_a*_a + _b*_b));


    }

};



#include <vector>

double fn1 (double x, void * params)
{
return  static_cast<Problem2xH2*>(params)->operator()(x);
}

int main() {

MPI_Init(NULL,NULL);


    

    double t_start = MPI_Wtime();

    int status;
    int iter = 0, max_iter = 100;
    const gsl_min_fminimizer_type *T;
    gsl_min_fminimizer *s;
    gsl_function F;

    F.function = &fn1;
    
    Problem2xH2 problem(1.43, 4.0);

/*for(int i = 0; i < 100; ++i)*/ {

    double m = 2.0, m_expected = M_PI;
    double a = 1.1733, b = 10.0;

    
//    problem.set_ab(1.43, 1.5 + i * 0.1);

    F.params = static_cast<void*>(&problem);

    T = gsl_min_fminimizer_brent;
//      T = gsl_min_fminimizer_goldensection;
    s = gsl_min_fminimizer_alloc (T);
    gsl_min_fminimizer_set (s, &F, m, a, b);


    do
    {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        m = gsl_min_fminimizer_x_minimum (s);
        a = gsl_min_fminimizer_x_lower (s);
        b = gsl_min_fminimizer_x_upper (s);

        status
            = gsl_min_test_interval (a, b, 0.00001, 0.0);

    }
    while (status == GSL_CONTINUE && iter < max_iter);

    gsl_min_fminimizer_free (s);
    iter = 0;
    double t_end = MPI_Wtime();
        std::cout<<1.43<<" "<<m<<" "<<problem(m)<<" "<<t_end - t_start<<std::endl;
        }
    MPI_Finalize();

    return 0;
}


