#include "../../headers/qmtmpipool.h"
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {

    virtual double integral(qmt::examples::Microscopic2xH2 &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

    uint i,j,k,l;

    TwoBody(uint i1,uint i2, uint i3,uint i4):
        i(i1),j(i2),k(i3),l(i4) {}

    double integral(qmt::examples::Microscopic2xH2 &model) {
        return model.get_two_body_integral(i,j,k,l);
    }

};

struct OneBody:public Body {

    uint i,j;

    OneBody(uint i1, uint i2):i(i1), j(i2) {}

    double integral(qmt::examples::Microscopic2xH2 &model) {
        return model.get_one_body_integral(i,j);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    double _a;
    double _b;

    std::vector< Body*> bodies;
    double *integrals;
    
    qmt::examples::Microscopic2xH2 model;

   public:
    SimplePoolJob(double a, double b, unsigned int size): _a(a), _b(b) {
        model.set(_a, _b, 1);

        

        bodies.push_back(new OneBody(0,0));
        bodies.push_back(new OneBody(0,1));
        bodies.push_back(new OneBody(0,2));
        bodies.push_back(new OneBody(0,3));
        bodies.push_back(new TwoBody(0,0,0,0)); //U
        bodies.push_back(new TwoBody(0,1,0,1)); //K12
        bodies.push_back(new TwoBody(0,2,0,2)); //K13
        bodies.push_back(new TwoBody(0,3,0,3)); //K14
        bodies.push_back(new TwoBody(0,0,0,1)); //V12
        bodies.push_back(new TwoBody(0,0,0,2)); //V13
        bodies.push_back(new TwoBody(0,0,0,3)); //V14
        bodies.push_back(new TwoBody(0,0,1,1)); //J12
        bodies.push_back(new TwoBody(0,0,2,2)); //J13
        bodies.push_back(new TwoBody(0,0,3,3)); //J14
    
        integrals = new double[size];    
        

    }
    
    ~SimplePoolJob() {
     for(int i = 0; i < bodies.size(); ++i)
	delete bodies[i];	
    delete [] integrals;
    }


    void set_ab(double a, double b) {
        _a = a;
        _b = b;

        model.set(a,b,1);
    }

private:
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_alpha(arg);
        result = bodies [id]->integral(model);
        return result;
   }
};


/*
 * Diagonalization stage - now performed on the single core
 */

class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {
 qmt::examples::Hamiltonian2xH2 hamiltonian;
 qmt::QmtLanczosSolver<gsl_matrix, gsl_vector> solver;

 double Calculate(double* arg,int size,void * params) {
        std::vector<double> intgs(&arg[0], &arg[0] + size);
            hamiltonian.set_integrals(intgs);
            gsl_matrix * novae = hamiltonian.get_matrix();
            solver.setProblemMatrix(novae);
            solver.run(1.0e-5);
           double energy = solver.get_minimal_eigenvalue() ;
   return energy;
 }
};

/*
 * Minimization - final result
 */

class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
 double alfa;
 double minimal_alfa;
 double minimal_f;
 double Calculate(void * params) {

  // return FinalProcessJob<double>::OutFunc(alfa, params);
   int status;
    int iter = 0, max_iter = 100;
    const gsl_min_fminimizer_type *T;
    gsl_min_fminimizer *s;
    gsl_function F;

    F.function = FinalProcessJob<double>::OutFunc;
    F.params = params;
   
 //   std::cout<<FinalProcessJob<double>::OutFunc(0.9, params)<<std::endl;
 //    std::cout<<FinalProcessJob<double>::OutFunc(10.0, params)<<std::endl;
    double m = 1.0, m_expected = M_PI;
    double a = 0.9, b = 10.0;

  
     T = gsl_min_fminimizer_brent;
 //     T = gsl_min_fminimizer_goldensection;
    s = gsl_min_fminimizer_alloc (T);
    gsl_min_fminimizer_set (s, &F, m, a, b);


    do
    {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        m = gsl_min_fminimizer_x_minimum (s);
        a = gsl_min_fminimizer_x_lower (s);
        b = gsl_min_fminimizer_x_upper (s);
     //   std::cout<<m<<" "<<a<<" "<<b<<std::endl;
        status
            = gsl_min_test_interval (a, b, 0.0000001, 0.0);
 
    minimal_f = gsl_min_fminimizer_f_minimum(s);
    minimal_alfa = gsl_min_fminimizer_x_minimum(s);
    }
    while (status == GSL_CONTINUE && iter < max_iter);
   
    
    gsl_min_fminimizer_free (s);
 
  return minimal_alfa;
 }
 
 public:
  void set_alfa(double a) {
   alfa = a;
  }
  
  double get_min_alfa() const { return minimal_alfa;}
  double get_min_f() const {return minimal_f;}
};





int main() {


    MPI_Init(NULL, NULL);

    int taskid;
    int nproc;
    MPI_Comm comm;
    MPI_Comm_dup( MPI_COMM_WORLD, &comm);
    double t_start = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    double b = 1.43;
    double a = 1.0e+6;
	qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;
	qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
	qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a,b,nproc - 1);
        
      //for(int i = 0; i < 100; ++i) {
        static_cast<SimpleFinalJob*>(finalJob)->set_alfa(1.0);
	qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer(comm, finalJob, internalJob, processJob);

	double result;

	result = gatherer.run();

	if(taskid == nproc-1) {
	 std::cout<<  static_cast<SimpleFinalJob*>(finalJob)->get_min_f() +  4/(a) + 4/(b) + 4/(sqrt(a*a + b*b));
         std::cout<<" "<<  static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa()<<" "<<MPI_Wtime() - t_start<<std::endl;
        }
       
    MPI_Finalize();

    return 0;
}


