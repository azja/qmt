/*
 * hamiltonian.cpp
 *
 *  Created on: 1 lip 2014
 *      Author: abiborski
 */

#include "hamiltonian.h"

qmt::examples::Hamiltonian2xH2::Hamiltonian2xH2() {

    h4_hamiltonian.addOneBodyTerm(); //0 - epsilon // 00, 11, 22, 33

    h4_hamiltonian.setMicroscopicParameterOne(OneBodyHubbard::eps,-1.75079);
    for(int i=0; i<4; i++) {
        h4_hamiltonian.addOneBody(OneBodyHubbard::eps,
                                  sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i));
        h4_hamiltonian.addOneBody(OneBodyHubbard::eps,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i));
    }

    h4_hamiltonian.addOneBodyTerm(); //1 - hopping t_12 // 01, 23

    h4_hamiltonian.setMicroscopicParameterOne(OneBodyHubbard::t12,-0.727647);
    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_up_creator(1),
                              sq_operator::get_up_anihilator(0));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_down_creator(1),
                              sq_operator::get_down_anihilator(0));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_up_creator(0),
                              sq_operator::get_up_anihilator(1));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_down_creator(0),
                              sq_operator::get_down_anihilator(1));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_up_creator(2),
                              sq_operator::get_up_anihilator(3));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_down_creator(2),
                              sq_operator::get_down_anihilator(3));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_up_creator(3),
                              sq_operator::get_up_anihilator(2));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t12,
                              sq_operator::get_down_creator(3),
                              sq_operator::get_down_anihilator(2));

    h4_hamiltonian.addOneBodyTerm(); //2 - hopping t_13 // 02, 13

    h4_hamiltonian.setMicroscopicParameterOne(OneBodyHubbard::t13,-0.0);
    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_up_creator(2),
                              sq_operator::get_up_anihilator(0));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_down_creator(2),
                              sq_operator::get_down_anihilator(0));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_up_creator(0),
                              sq_operator::get_up_anihilator(2));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_down_creator(0),
                              sq_operator::get_down_anihilator(2));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_up_creator(1),
                              sq_operator::get_up_anihilator(3));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_down_creator(1),
                              sq_operator::get_down_anihilator(3));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_up_creator(3),
                              sq_operator::get_up_anihilator(1));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t13,
                              sq_operator::get_down_creator(3),
                              sq_operator::get_down_anihilator(1));


    h4_hamiltonian.addOneBodyTerm(); //3 - hopping t_14  // 03, 12

    h4_hamiltonian.setMicroscopicParameterOne(OneBodyHubbard::t14,0.00);
    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_up_creator(3),
                              sq_operator::get_up_anihilator(0));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_down_creator(3),
                              sq_operator::get_down_anihilator(0));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_up_creator(0),
                              sq_operator::get_up_anihilator(3));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_down_creator(0),
                              sq_operator::get_down_anihilator(3));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_up_creator(1),
                              sq_operator::get_up_anihilator(2));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_down_creator(1),
                              sq_operator::get_down_anihilator(2));

    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_up_creator(2),
                              sq_operator::get_up_anihilator(1));
    h4_hamiltonian.addOneBody(OneBodyHubbard::t14,
                              sq_operator::get_down_creator(2),
                              sq_operator::get_down_anihilator(1));



    h4_hamiltonian.addTwoBodyTerm(); //0 - U   // 0000, 1111, 2222, 3333

    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::U,1.65321);
    for(int i=0; i<4; i++) {
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::U, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i));
    }

    h4_hamiltonian.addTwoBodyTerm(); //1 - K_12, // 0011, 2233

    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::K12,0.956691);
    for(int i=0; i<4; i=i+2) {
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1));
    }

    h4_hamiltonian.addTwoBodyTerm(); //2 - K_13, // 0022, 1133

    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::K13,0.0);
    for(int i=0; i<2; i++) {
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i+2),
                                  sq_operator::get_down_anihilator(i+2));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(i+2),
                                  sq_operator::get_up_anihilator(i+2));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(i+2),
                                  sq_operator::get_down_anihilator(i+2));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i+2),
                                  sq_operator::get_up_anihilator(i+2));
    }

    h4_hamiltonian.addTwoBodyTerm(); //3 - K_14  // 0033, 1122

    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::K14,0.0);
    for(int i=0; i<2; i++) {
        int j = i == 0 ? 3 : 2;

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::K14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));
    }

    h4_hamiltonian.addTwoBodyTerm(); //4 - V12 // 0011, 2233
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::V12, -0.0117991);

    for(int i=0; i<4; i=i+2) {
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12, sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12, sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1), sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12,
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1), sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V12,
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i));

    }

    h4_hamiltonian.addTwoBodyTerm(); //5 - V13 // 0022, 1133
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::V13, 0.0);

    for(int i=0; i<2; i++) {
        int j = i == 0 ? 2 : 3;

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13, sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13, sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13,
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j), sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V13,
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(i));
    }

    h4_hamiltonian.addTwoBodyTerm(); //6 - V14 // 0033, 1122
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::V14, 0.0);

    for(int i=0; i<2; i++) {
        int j = i == 0 ? 3 : 2;

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14, sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j), sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14, sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14,
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j), sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::V14,
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(i));
    }


    h4_hamiltonian.addTwoBodyTerm(); //7 - J12, 0011, 2233
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::J12,0.0219085);


    for(int i=0; i<4; i=i+2) {
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J12, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i+1),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J12, sq_operator::get_up_creator(i+1),
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(i));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J12, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J12,
                                  sq_operator::get_down_creator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i),
                                  sq_operator::get_down_anihilator(i+1));
    }


    h4_hamiltonian.addTwoBodyTerm(); //8 - J13, 0022, 1133
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::J13,0.0);


    for(int i=0; i<2; i++) {
        int j = i == 0 ? 2 : 3;
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J13, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J13, sq_operator::get_up_creator(j),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(i));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J13, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J13,
                                  sq_operator::get_down_creator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(i),
                                  sq_operator::get_down_anihilator(j));
    }

    h4_hamiltonian.addTwoBodyTerm(); //9 - J14, 0033, 1122
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::J14,0.0);


    for(int i=0; i<2; i++) {
        int j = i == 0 ? 3 : 2;
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J14, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J14, sq_operator::get_up_creator(j),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(i));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J14, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::J14,
                                  sq_operator::get_down_creator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(i),
                                  sq_operator::get_down_anihilator(j));
    }


    h4_hamiltonian.addTwoBodyTerm(); //10 - P12 = -J12/2 // 0011, 2233
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::P12,-0.0219085/2.0);

    for(int i=0; i<4; i=i+2) {

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_up_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(i+1),
                                  sq_operator::get_down_anihilator(i+1));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(i+1), sq_operator::get_up_anihilator(i),
                                  sq_operator::get_down_anihilator(i+1));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P12,
                                  sq_operator::get_down_creator(i), sq_operator::get_up_creator(i+1),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(i+1));
    }

    h4_hamiltonian.addTwoBodyTerm(); //11 - P13 = -J13/2 // 0022, 1133
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::P13,0.0);


    for(int i=0; i<2; i++) {
        int j = i == 0 ? 2 : 3;
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(j), sq_operator::get_up_anihilator(i),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P13,
                                  sq_operator::get_down_creator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(j));
    }


    h4_hamiltonian.addTwoBodyTerm(); //12 - P14 = -J14/2 // 0033, 1122
    h4_hamiltonian.setMicroscopicParameterTwo(TwoBodyHubbard::P14,0.0);


    for(int i=0; i<2; i++) {
        int j = i == 0 ? 3 : 2;
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14, sq_operator::get_up_creator(i),
                                  sq_operator::get_up_anihilator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_up_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14,
                                  sq_operator::get_down_creator(i),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_down_creator(j),
                                  sq_operator::get_down_anihilator(j));

        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14, sq_operator::get_up_creator(i),
                                  sq_operator::get_down_creator(j), sq_operator::get_up_anihilator(i),
                                  sq_operator::get_down_anihilator(j));
        h4_hamiltonian.addTwoBody(TwoBodyHubbard::P14,
                                  sq_operator::get_down_creator(i), sq_operator::get_up_creator(j),
                                  sq_operator::get_down_anihilator(i),
                                  sq_operator::get_up_anihilator(j));
    }



    h4_formula = new qmt::QmtMatrixFormula<qmt::QmtHubbard<sq_operator>>(h4_hamiltonian);



    generator.generate(4,states);

    h4_formula->set_States(states); // setting the hamiltonian

    problem_size=states.size();

    hamiltonian_M = gsl_matrix_alloc(problem_size,problem_size);

}

qmt::examples::Hamiltonian2xH2::~Hamiltonian2xH2() {
    delete h4_formula;
    gsl_matrix_free(hamiltonian_M);
}

void qmt::examples::Hamiltonian2xH2::set_integrals(const std::vector<double> integrals) {
    unsigned int N_one_body = h4_hamiltonian.number_of_one_body();
    unsigned int N_two_body = h4_hamiltonian.number_of_two_body();

    for(uint i=0; i< N_one_body; i++) {
        h4_formula->set_microscopic_parameter(1, i, integrals[i]);
    }
    for(uint i=0; i<N_two_body-3; i++) { // last 3 params depend on second last 3 params
        h4_formula->set_microscopic_parameter(2, i, integrals[N_one_body+i]);
    }

    for(uint i=N_two_body-3; i<N_two_body; i++) {
        h4_formula->set_microscopic_parameter(2, i, -0.5*integrals[N_one_body+i-3]);
    }
}

gsl_matrix* qmt::examples::Hamiltonian2xH2::get_matrix() {
    gsl_matrix* matrix = gsl_matrix_alloc (h4_formula->getSize(),h4_formula->getSize());

    h4_formula->getMatrix(matrix);

    return matrix;
}





