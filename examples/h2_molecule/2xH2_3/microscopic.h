/*
 * microscopic.h
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

#ifndef MICROSCOPIC_H_
#define MICROSCOPIC_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../../headers/qmtorbitals.h"
#include "../../headers/gaussexp.h"
#include <math.h>
#include "../../headers/qmtbform.h"
#include "../../headers/qmteqsolver.h"
//#include "../../headers/qmtvars.h"
#include <omp.h>
#include <stdlib.h>

namespace qmt {
namespace examples {

class Microscopic2xH2 {

    int N;// = 19;
    double alfa;// = 1.19378;
    std::vector<double> coefs;
    std::vector<double> gammas;


    std::vector<Gauss> atom1;
    std::vector<Gauss> atom2;
    std::vector<Gauss> atom3;
    std::vector<Gauss> atom4;

    double a;// = 1.0;
    double b;// = 10;

    typedef QmExpansion<Gauss, QmVariables::QmParallel::no> g_omp;
    typedef unsigned int uint;
    std::vector<g_omp> slaters;


    double** M;


    typedef QmExpansion < g_omp, QmVariables::QmParallel::omp > w_omp;
    std::vector<w_omp> wanniers;

    std::vector<w_omp*> system;

    static void  H2x2(double a,double b, double alpha, std::vector<double>& betas, double** M);

    Microscopic2xH2(const Microscopic2xH2&) = delete;
    Microscopic2xH2 operator=(const Microscopic2xH2&) = delete;

public:
    Microscopic2xH2();
    ~Microscopic2xH2();

    void set(double a, double b, double alpha);
    void set_a(double a);
    void set_b(double b);
    void set_alpha(double alpha);
    void get_integrals(std::vector<double>& output);
    
    double get_one_body_integral(uint i,uint j);
    double get_two_body_integral(uint i,uint j, uint k, uint l);



};


}
}



#endif /* MICROSCOPIC_H_ */
