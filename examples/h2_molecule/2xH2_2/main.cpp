#include "../../headers/qmtmpipool.h"
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {

    virtual double integral(qmt::examples::Microscopic2xH2 &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

    uint i,j,k,l;

    TwoBody(uint i1,uint i2, uint i3,uint i4):
        i(i1),j(i2),k(i3),l(i4) {}

    double integral(qmt::examples::Microscopic2xH2 &model) {
        return model.get_two_body_integral(i,j,k,l);
    }

};

struct OneBody:public Body {

    uint i,j;

    OneBody(uint i1, uint i2):i(i1), j(i2) {}

    double integral(qmt::examples::Microscopic2xH2 &model) {
        return model.get_one_body_integral(i,j);
    }

};

////////////////////////////////////////////////////////////////////////////
class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    double _a;
    double _b;

    std::vector< Body*> bodies;
    double *integrals;
    
    qmt::examples::Microscopic2xH2 model;

   public:
    SimplePoolJob(double a, double b, unsigned int size): _a(a), _b(b) {
        model.set(_a, _b, 1);

        

        bodies.push_back(new OneBody(0,0));
        bodies.push_back(new OneBody(0,1));
        bodies.push_back(new OneBody(0,2));
        bodies.push_back(new OneBody(0,3));
        bodies.push_back(new TwoBody(0,0,0,0)); //U
        bodies.push_back(new TwoBody(0,1,0,1)); //K12
        bodies.push_back(new TwoBody(0,2,0,2)); //K13
        bodies.push_back(new TwoBody(0,3,0,3)); //K14
        bodies.push_back(new TwoBody(0,0,0,1)); //V12
        bodies.push_back(new TwoBody(0,0,0,2)); //V13
        bodies.push_back(new TwoBody(0,0,0,3)); //V14
        bodies.push_back(new TwoBody(0,0,1,1)); //J12
        bodies.push_back(new TwoBody(0,0,2,2)); //J13
        bodies.push_back(new TwoBody(0,0,3,3)); //J14
    
        integrals = new double[size];    
        

    }
    
    ~SimplePoolJob() {
     for(int i = 0; i < bodies.size(); ++i)
	delete bodies[i];	
    delete [] integrals;
    }


    void set_ab(double a, double b) {
        _a = a;
        _b = b;

        model.set(a,b,1);
    }

private:
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_alpha(arg);
        result = bodies [id]->integral(model);
        return result;
   }
};


//Diagonalization
class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {
 qmt::examples::Hamiltonian2xH2 hamiltonian;
 qmt::QmtLanczosSolver<gsl_matrix, gsl_vector> solver;

 double Calculate(double* arg,int size,void * params) {
        std::vector<double> intgs(&arg[0], &arg[0] + size);
            hamiltonian.set_integrals(intgs);
            gsl_matrix * novae = hamiltonian.get_matrix();
            solver.setProblemMatrix(novae);
            solver.run(1.0e-5);
           double energy = solver.get_minimal_eigenvalue() ;
   return energy;
 }
};



class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
 double alfa;
 double Calculate(void * params) {
   return FinalProcessJob<double>::OutFunc(alfa, params);
 }
 
 public:
  void set_alfa(double a) {
   alfa = a;
  }

};





int main() {


    MPI_Init(NULL, NULL);

    int taskid;
    int nproc;
    MPI_Comm comm;
    MPI_Comm_dup( MPI_COMM_WORLD, &comm);
    double t_start = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    double a = 1.43;
    double b = 1.0e+6;
	qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;
	qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
	qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a,b,nproc - 1);
        
      for(int i = 0; i < 100; ++i) {
        static_cast<SimpleFinalJob*>(finalJob)->set_alfa(1.0 + 0.01 * i);
	qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer(comm, finalJob, internalJob, processJob);

	double result;

	result = gatherer.run();

	if(taskid == 0)
	 std::cout<<result +  4/(a) + 4/(b) + 4/(sqrt(a*a + b*b))<<" "<<MPI_Wtime() - t_start<<std::endl;
       }
    MPI_Finalize();

    return 0;
}


