#include "../../headers/qmtmpioptimizer.h"
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <algorithm>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

//class

struct Problem2xH2 {
    typedef double returned_value;

    qmt::examples::Microscopic2xH2 model;
    qmt::examples::Hamiltonian2xH2 hamiltonian;
    qmt::QmtLanczosSolver<gsl_matrix, gsl_vector> solver;

    double _a;
    double _b;

    Problem2xH2(double a, double b): _a(a), _b(b) {
        model.set(_a, _b, 1);
    }

    bool stop(double x, double y) const {
        return fabs(x-y) < 1.0e-7;

    }

    void set_ab(double a, double b) {
        _a = a;
        _b = b;

        model.set(a,b,1);
    }

    returned_value operator()(returned_value alpha)  {
        model.set_alpha(alpha);
        std::vector<double> integrals;
        model.get_integrals(integrals);

        hamiltonian.set_integrals(integrals);

        gsl_matrix * novae = hamiltonian.get_matrix();
        solver.setProblemMatrix(novae);
        solver.run(1.0e-4);
        return  solver.get_minimal_eigenvalue() + 4/(_a) + 4/(_b) + 4/(sqrt(_a*_a + _b*_b));


    }

};



#include <vector>


//////////////////////////////////////////////////////////////////////////
struct Body {

virtual double Integral9qmt::example::Microscopic2xH2 &model) = 0;

};

struct TwoBody {

qmt::uint i,j,k,l;

TwoBody(qmt::uint i1, qmt::uint i2, qmt::uint i3,qmt::uint i4):
      i(i1),j(i2),k(i3),l(i4){}

double integral(qmt::example::Microscopic2xH2 &model) {
 return model.get_two_body_integral(i,j,k,l);
}

};

struct OneBody {

qmt::uimt i,j;

OneBody(qmt::uint i1, qmt::uint i2):i(i1), j(i2) {}

double integral(qmt::example::Microscopic2xH2 &model) {
 return model.get_one_body_integral(i,j);
}

};
////////////////////////////////////////////////////////////////////////////


int main() {


    MPI_Init(NULL,NULL);


    int taskid;
    int nproc;
    double integrals[13];
    double t_start = 0;
    double t_end = 0;
    Body* bodies[13];
    
    body[0] = new OneBody(0,0);
    body[1] = new OneBody(0,1);
    body[2] = new OneBody(0,2);
    body[3] = new OneBody(0,3);
    body[4] = new TwoBody(0,0,0,0); //U
    body[5] = new TwoBody(0,1,0,1); //K12
    body[6] = new TwoBody(0,2,0,2); //K13
    body[7] = new TwoBody(0,3,0,3); //K14
    body[8] = new TwoBody(0,0,1,1); //J12
    body[9] = new TwoBody(0,0,2,2); //J13
    body[10] = new TwoBody(0,0,3,3); //J14
    body[11] = new TwoBody(0,0,0,1); //V12
    body[12] = new TwoBody(0,0,0,2); //V13
    body[13] = new TwoBody(0,0,0,3); //V14


    qmt::examples::Microscopic2xH2 model;

    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);


    if(taskid == 0) {
        t_start = MPI_Wtime();
    }
    

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    double result = body[taskid]->integral(model);
    MPI_Gather(&result, 1, MPI_DOUBLE, integrals, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if(taskid == 0) {
      for(int i = 0; i < 13;++i) {
       std::cout<<integrals[i]<<" ";
      delete bodies[i];
      }
    }


    MPI_Finalize();

    return 0;
}


