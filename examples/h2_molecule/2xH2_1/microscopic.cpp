/*
microoscopic.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

#include "microscopic.h"

void qmt::examples::Microscopic2xH2::H2x2(double a,double b, double alpha,
        std::vector<double>& betas, double** M) {

    Slater1S slater1(0,a,0, alpha);
    Slater1S slater2(0,0,0, alpha);
    Slater1S slater3(b,0,0, alpha);
    Slater1S slater4(b,a,0, alpha);

    Wannier<Slater1S> wannier1;
    Wannier<Slater1S> wannier2;
    Wannier<Slater1S> wannier3;
    Wannier<Slater1S> wannier4;



    wannier1.addOrbital(slater1,0);
    wannier1.addOrbital(slater2,1);
    wannier1.addOrbital(slater3,3);
    wannier1.addOrbital(slater4,2);



    wannier2.addOrbital(slater1,1);
    wannier2.addOrbital(slater2,0);
    wannier2.addOrbital(slater3,2);
    wannier2.addOrbital(slater4,3);


    wannier3.addOrbital(slater1,3);
    wannier3.addOrbital(slater2,2);
    wannier3.addOrbital(slater3,0);
    wannier3.addOrbital(slater4,1);


    wannier4.addOrbital(slater1,2);
    wannier4.addOrbital(slater2,3);
    wannier4.addOrbital(slater3,1);
    wannier4.addOrbital(slater4,0);

    std::vector<BilinearForm> forms;

    Wannier<Slater1S>::multiply(wannier1,wannier2,M,4);
    forms.push_back(BilinearForm(M,4));

    Wannier<Slater1S>::multiply(wannier1,wannier3,M,4);
    forms.push_back(BilinearForm(M,4));


    Wannier<Slater1S>::multiply(wannier1,wannier4,M,4);
    forms.push_back(BilinearForm(M,4));


    Wannier<Slater1S>::multiply(wannier1,wannier1,M,4);
    forms.push_back(BilinearForm(M,4,-1));


    double in[4] = { 1.0, 0.0, 0.0, 0.0 };
    double out[4] = {0.0, 0.0, 0.0, 0.0 };

    NonLinearSystemSolve(forms,in,out,1.0e-8);

    betas.clear();
    betas.push_back(out[0]);
    betas.push_back(out[1]);
    betas.push_back(out[2]);
    betas.push_back(out[3]);

}

qmt::examples::Microscopic2xH2::Microscopic2xH2():N(23),alfa(1.19378),a(1.0), b(10) {
    M = new double* [4];
    for(int i = 0; i < 4; ++i)
        M[i] = new double [4];

    qmt::qmGtoNSlater1s(2.0,2.0 , 1.0e-7, 0.01, 0.01, coefs, gammas, N);

    for(int i=0; i < N; ++i) {
        atom1.push_back(Gauss(0,a,0,alfa*gammas[i]));
        atom2.push_back(Gauss(0,0,0,alfa*gammas[i]));
        atom3.push_back(Gauss(b,0,0,alfa*gammas[i]));
        atom4.push_back(Gauss(b,a,0,alfa*gammas[i]));

    }


    slaters.push_back(g_omp(0,a,0,0));
    slaters.push_back(g_omp(0,0,0,1));
    slaters.push_back(g_omp(b,0,0,2));
    slaters.push_back(g_omp(b,a,0,3));


    for(int i=0; i < N; ++i) {
        slaters[0].add_element(i, coefs[i], atom1[i]);
        slaters[1].add_element(i, coefs[i], atom2[i]);
        slaters[2].add_element(i, coefs[i], atom3[i]);
        slaters[3].add_element(i, coefs[i], atom4[i]);
    }

    std::vector<double> betas;

    H2x2(a, b, alfa, betas, M);

    wanniers.push_back(w_omp(0,a,0,0));
    wanniers.push_back(w_omp(0,0,0,1));
    wanniers.push_back(w_omp(b,0,0,2));
    wanniers.push_back(w_omp(b,a,0,3));

    wanniers[0].add_element(0, betas[0], slaters[0]);
    wanniers[0].add_element(1, betas[1], slaters[1]);
    wanniers[0].add_element(3, betas[3], slaters[2]);
    wanniers[0].add_element(2, betas[2], slaters[3]);

    wanniers[1].add_element(1, betas[1], slaters[0]);
    wanniers[1].add_element(0, betas[0], slaters[1]);
    wanniers[1].add_element(2, betas[2], slaters[2]);
    wanniers[1].add_element(3, betas[3], slaters[3]);


    wanniers[2].add_element(3, betas[3], slaters[0]);
    wanniers[2].add_element(2, betas[2], slaters[1]);
    wanniers[2].add_element(0, betas[0], slaters[2]);
    wanniers[2].add_element(1, betas[1], slaters[3]);


    wanniers[3].add_element(2, betas[2], slaters[0]);
    wanniers[3].add_element(3, betas[3], slaters[1]);
    wanniers[3].add_element(1, betas[1], slaters[2]);
    wanniers[3].add_element(0, betas[0], slaters[3]);


    system.push_back(&wanniers[0]);
    system.push_back(&wanniers[1]);
    system.push_back(&wanniers[2]);
    system.push_back(&wanniers[3]);


}

qmt::examples::Microscopic2xH2::~Microscopic2xH2() {
    for(int i = 0; i < 4; ++i)
        delete [] M[i];
    delete [] M;
}

void qmt::examples::Microscopic2xH2::set(double new_a, double new_b, double new_alfa) {
    a = new_a;
    b = new_b;
    alfa = new_alfa;

    atom1.clear();
    atom2.clear();
    atom3.clear();
    atom4.clear();

    for(int i=0; i < N; ++i) {
        atom1.push_back(Gauss(0,a,0,alfa*gammas[i]));
        atom2.push_back(Gauss(0,0,0,alfa*gammas[i]));
        atom3.push_back(Gauss(b,0,0,alfa*gammas[i]));
        atom4.push_back(Gauss(b,a,0,alfa*gammas[i]));

    }

    slaters.clear();

    slaters.push_back(g_omp(0,a,0,0));
    slaters.push_back(g_omp(0,0,0,1));
    slaters.push_back(g_omp(b,0,0,2));
    slaters.push_back(g_omp(b,a,0,3));


    for(int i=0; i < N; ++i) {
        slaters[0].add_element(i, coefs[i], atom1[i]);
        slaters[1].add_element(i, coefs[i], atom2[i]);
        slaters[2].add_element(i, coefs[i], atom3[i]);
        slaters[3].add_element(i, coefs[i], atom4[i]);
    }

    std::vector<double> betas;

    H2x2(a, b, alfa, betas, M);

    wanniers.clear();

    wanniers.push_back(w_omp(0,a,0,0));
    wanniers.push_back(w_omp(0,0,0,1));
    wanniers.push_back(w_omp(b,0,0,2));
    wanniers.push_back(w_omp(b,a,0,3));

    wanniers[0].add_element(0, betas[0], slaters[0]);
    wanniers[0].add_element(1, betas[1], slaters[1]);
    wanniers[0].add_element(3, betas[3], slaters[2]);
    wanniers[0].add_element(2, betas[2], slaters[3]);

    wanniers[1].add_element(1, betas[1], slaters[0]);
    wanniers[1].add_element(0, betas[0], slaters[1]);
    wanniers[1].add_element(2, betas[2], slaters[2]);
    wanniers[1].add_element(3, betas[3], slaters[3]);


    wanniers[2].add_element(3, betas[3], slaters[0]);
    wanniers[2].add_element(2, betas[2], slaters[1]);
    wanniers[2].add_element(0, betas[0], slaters[2]);
    wanniers[2].add_element(1, betas[1], slaters[3]);


    wanniers[3].add_element(2, betas[2], slaters[0]);
    wanniers[3].add_element(3, betas[3], slaters[1]);
    wanniers[3].add_element(1, betas[1], slaters[2]);
    wanniers[3].add_element(0, betas[0], slaters[3]);


    system.clear();

    system.push_back(&wanniers[0]);
    system.push_back(&wanniers[1]);
    system.push_back(&wanniers[2]);
    system.push_back(&wanniers[3]);

}


void  qmt::examples::Microscopic2xH2::set_a(double new_a) {
    set(new_a, b, alfa);
}

void  qmt::examples::Microscopic2xH2::set_b(double new_b) {
    set(a, new_b, alfa);
}

void  qmt::examples::Microscopic2xH2::set_alpha(double new_alfa) {
    set(a, b, new_alfa);
}


void qmt::examples::Microscopic2xH2::get_integrals(std::vector<double>& output) {

    typedef w_omp expansion;

    double t0 = expansion::kinetic_integral(wanniers[0],wanniers[0]);
    double t1 = expansion::kinetic_integral(wanniers[0],wanniers[1]);
    double t2 = expansion::kinetic_integral(wanniers[0],wanniers[3]);
    double t3 = expansion::kinetic_integral(wanniers[0],wanniers[2]);

    int i;



    // #pragma omp parallel for default(none) shared(qmt::examples::Microscopic2xH2::system, wanniers)  private(i)  reduction(+ : t0, t1, t2, t3)

    for( i = 0; i < 4; ++i) {
        t0 = t0 + expansion::attractive_integral(wanniers[0], wanniers[0],system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
        t1 = t1 + expansion::attractive_integral(wanniers[0], wanniers[1],system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
        t2 = t2 + expansion::attractive_integral(wanniers[0], wanniers[3],system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
        t3 = t3 + expansion::attractive_integral(wanniers[0], wanniers[2],system[i]->get_x(), system[i]->get_y(), system[i]->get_z());
    }

    double U = 0;
    double K12 = 0;
    double K13 = 0;
    double K14 = 0;
    double J12 = 0;
    double J13 = 0;
    double J14 = 0;
    double V12 = 0;
    double V13 = 0;
    double V14 = 0;


    U =   expansion::v_integral(wanniers[0], wanniers[0], wanniers[0], wanniers[0]); // U
    K12 = expansion::v_integral(wanniers[0], wanniers[1], wanniers[0], wanniers[1]); // K12
    K13 = expansion::v_integral(wanniers[0], wanniers[2], wanniers[0], wanniers[2]); // K13
    K14 = expansion::v_integral(wanniers[0], wanniers[3], wanniers[0], wanniers[3]); // K14
    J12 = expansion::v_integral(wanniers[0], wanniers[0], wanniers[1], wanniers[1]); // J
    J13 = expansion::v_integral(wanniers[0], wanniers[0], wanniers[2], wanniers[2]); // J
    J14 = expansion::v_integral(wanniers[0], wanniers[0], wanniers[3], wanniers[3]); // J
    V12 = expansion::v_integral(wanniers[0], wanniers[0], wanniers[0], wanniers[1]); // V
    V13 = expansion::v_integral(wanniers[0], wanniers[0], wanniers[0], wanniers[2]); // V
    V14 = expansion::v_integral(wanniers[0], wanniers[0], wanniers[0], wanniers[3]); // V

    if(output.size()>0) output.clear();

    output.push_back(t0);
    output.push_back(t1);
    output.push_back(t2);
    output.push_back(t3);
    output.push_back(U);
    output.push_back(K12);
    output.push_back(K14);
    output.push_back(K13);

    output.push_back(V12);
    output.push_back(V14);
    output.push_back(V13);

    output.push_back(J12);
    output.push_back(J14);
    output.push_back(J13);
}




double qmt::examples::get_one_body_integral(qmt::uint i, qmt::uint j) {
    typedef w_omp expansion;
    double result = 0;
    result += expansion::kinetic_integral(wanniers[j],wanniers[j]);
    for( k = 0; k < 4; ++k) {
        result +=  expansion::attractive_integral(wanniers[i], wanniers[j],system[k]->get_x(), system[k]->get_y(), system[k]->get_z());
    return result;
}



double qmt::examples::get_two_body_integral(qmt::uint i, qmt::uint j, qmt::uint k, qmt::uint k) {
    typedef w_omp expansion;
    return    expansion::v_integral(wanniers[i], wanniers[j], wanniers[k], wanniers[l]); 
}