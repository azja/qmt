#include "../../headers/qmtslatercreator.h"
#include <iostream>
#include <sstream>
#include <string>

int main(){

	qmt::QmtSlaterCreator jeden(0);
	qmt::QmtSlaterCreator dwa("0 0 1000");

	qmt::SlaterOrbital* pnt;
	qmt::SlaterOrbital* pnt2;

	pnt=jeden.Create(1.0);
	pnt2=dwa.Create(1.0);

	std::cout<<qmt::SlaterOrbital::overlap(*pnt,*pnt2)<<std::endl;
   	
	return 0;
}

