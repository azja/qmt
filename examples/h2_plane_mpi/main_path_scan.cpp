#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include <fstream>


struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(4.1,4.1,1.4);

double alpha_global;
double energy_global;



double func(double x, void *p) {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg({x});
 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
}



void F(void* params) {

  energy_global = func(alpha_global,params);
  
}



int main(int argc,const char* argv[]) {

 MPI_Init(nullptr,nullptr);
 int rank;
 

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat");
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;

 MPI_Comm_rank (mpi_settings.communicator, &rank); 




  std::ifstream inFilePath("path.dat"); 
  size_t n_lines = std::count(std::istreambuf_iterator<char>(inFilePath), 
             std::istreambuf_iterator<char>(), '\n');
  inFilePath.close();
  inFilePath.open("path.dat",std::ios::in);


for(size_t i = 0U; i < n_lines;++i) {
 double p_i;
 double a_i;
 double R_i;
 inFilePath>>p_i>>a_i>>R_i>>alpha_global;
//std::cout<<a_i<<" "<<R_i<<" "<<n_lines<<std::endl;
    scaler = qmt::QmtVector(a_i, a_i,R_i);
// std::cout<<scaler<<std::endl;
  double alpha0 = alpha_global;
    for(size_t j = 0U; j < 100;j++)  {
    alpha_global = alpha0 + j * 0.01;
    
    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
    if(rank == mpi_settings.root){
     std::cout<<scaler<<" "<<p_i<<" "<<energy_global<<" "<<alpha_global<<std::endl;

     }
    
    }
  }
 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
