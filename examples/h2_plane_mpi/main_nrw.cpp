#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include <fstream>


qmt::QmtVector scaler(4.1,4.1,1.4);



int main(int argc,const char* argv[]) {

 int rank;

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

  std::ifstream inFilePath("path.dat"); 
  size_t n_lines = std::count(std::istreambuf_iterator<char>(inFilePath), 
             std::istreambuf_iterator<char>(), '\n');
  inFilePath.close();
  inFilePath.open("path.dat",std::ios::in);

for(size_t i = 0U; i < n_lines;++i) {
 double a_i;
 double R_i; 
 inFilePath>>a_i>>R_i;
    scaler = qmt::QmtVector(a_i, a_i,R_i);
   system->set_parameters(std::vector<double>({1.0}),scaler);
   std::string wannier_fn = "wannier" + std::to_string(i) + ".dat";
   std::ofstream wannier_f(wannier_fn.c_str());
        
    for(int m=-60; m<=60; m++)
     for(int n=-60; n<=60; n++){
       for(int r= 0; r<=50; r++){
        wannier_f<<qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r)<<" "<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_value(0,qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r))<<std::endl;
        }
        }
    wannier_f.close();
  
  }
 
}
 delete system;

 return 0; 
}
