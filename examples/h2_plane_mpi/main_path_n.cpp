#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include <fstream>


struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(4.1,4.1,1.4);

double alpha_global;
double energy_global;



double func(double x, void *p) {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg({x});
 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
}



void F(void* params) {

  energy_global = func(alpha_global,params);
  
}



int main(int argc,const char* argv[]) {

 MPI_Init(nullptr,nullptr);
 int rank;
 

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat");
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 
   std::vector<double> avs;
#ifdef DENSITY
  std::vector<int> map;
   
  std::ifstream map_file("megacell_orbital.dat");
  std::string map_line;
  std::getline(map_file,map_line);
  map_file.close();
  std::vector<std::string> map_string = qmt::parser::get_delimited_words(", ",map_line);
  for(const auto& index : map_string)
     map.push_back(std::stoi(index));
#endif           
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;

 MPI_Comm_rank (mpi_settings.communicator, &rank); 

std::ofstream hopping_avrgs_up;
std::ofstream hopping_avrgs_down;
std::ofstream integrals;

if(rank == mpi_settings.root){
hopping_avrgs_up.open("hopping_avrgs_up.dat", std::ios::out);
hopping_avrgs_down.open("hopping_avrgs_down.dat", std::ios::out);
integrals.open("integrals.dat", std::ios::out);
}



  std::ifstream inFilePath("path.dat"); 
  size_t n_lines = std::count(std::istreambuf_iterator<char>(inFilePath), 
             std::istreambuf_iterator<char>(), '\n');
  inFilePath.close();
  inFilePath.open("path.dat",std::ios::in);

for(size_t i = 0U; i < n_lines;++i) {
 double p_i;
 double a_i;
 double R_i;
 inFilePath>>p_i>>a_i>>R_i>>alpha_global;
//std::cout<<a_i<<" "<<R_i<<" "<<n_lines<<std::endl;
    scaler = qmt::QmtVector(a_i, a_i,R_i);
// std::cout<<scaler<<std::endl;
    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
    if(rank == mpi_settings.root){
     std::cout<<scaler<<" "<<p_i<<" "<<energy_global<<" "<<alpha_global<<std::endl;

        avs.clear();
        static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer)->NWaveFunctionEssentials(avs);

       
/*
 *  Hopping averages
 */        
                for(auto mm = 0; mm < 8;++mm){
                hopping_avrgs_up<<avs[mm]<<" ";
                hopping_avrgs_down<<avs[64 + mm]<<" ";
                }
                hopping_avrgs_up<<std::endl;
                hopping_avrgs_down<<std::endl;
       
/*
 *  Integrals
 */

for(int i = 0; i < static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_number(); i++)
   integrals<<static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_integral(i)<<" "; //U & K

   for(int i = 1; i < 14;++i)
   integrals<<static_cast<qmt::QmtSystemStandard*>(system)->get_one_body_integral(i)<<" "; //t

   integrals<<std::endl;
#ifdef DENSITY
   std::string density_fn = "density" + std::to_string(i) + ".dat";
   std::string wannier_fn = "wannier" + std::to_string(i) + ".dat";
   std::ofstream density_f(density_fn.c_str());
   std::ofstream wannier_f(wannier_fn.c_str());
        
    for(int m=-60; m<=60; m++)
     for(int n=-60; n<=60; n++){
       for(int r= 0; r<=50; r++){
        density_f<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r),avs,map)<<std::endl;
        wannier_f<<qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r)<<" "<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_value(0,qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r))<<std::endl;
        }
        }
    density_f.close();
    wannier_f.close();
#endif
//        std::cout<<qmt::QmtVector(0.0,0.16*n,-1.0 + 0.16*r) <<" "<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(qmt::QmtVector(0.0,0.16*n,-1.0 + 0.16*r),avs,map)<<std::endl;}
//        std::cout<<std::endl;
        
   
//    std::cout<<std::endl;
  
  }
 
}
if(rank == mpi_settings.root){
hopping_avrgs_up.close();
hopping_avrgs_down.close();
integrals.close();
}
 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
