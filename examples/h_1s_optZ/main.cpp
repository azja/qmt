#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <algorithm>
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include "../../headers/qmtparsertools.h"

struct Data{
 qmt::QmtSystem *system;
 qmt::QmtDiagonalizer *diagonalizer;
 qmt::QmtVector *scale;
 double *Z0;
 double *Zm;
 double *Zp;
};

double TZ0,TZm,TZp;
double RZ0;

double energy(double x, void* p){
 std::vector<double> one_body;
 std::vector<double> two_body;

 Data *sol = (Data*) p;

 sol->system->set_parameters(std::vector<double>({x}),*(sol->scale));

 for(auto i = 0U; i < sol->system->get_one_body_number();++i)
   one_body.push_back(sol->system->get_one_body_integral(i));

 for(auto i = 0U; i < sol->system->get_two_body_number();++i)
   two_body.push_back(sol->system->get_two_body_integral(i));

return sol->diagonalizer->Diagonalize(one_body, two_body);
}

double minimize(void* solver, double& ZetaMin, double& Zeta0, double& ZetaMax, bool ALPHA_SCAN=false){

  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;

  gsl_function F;
  F.function = &energy;
  F.params = solver;

  double E;

  Zeta0=TZ0;
  ZetaMin=TZm;
  ZetaMax=TZp;

  if(!ALPHA_SCAN){
    T = gsl_min_fminimizer_brent;
    s = gsl_min_fminimizer_alloc (T);
//    std::cout<<Zeta0<<",\t"<<ZetaMin<<",\t"<<ZetaMax<<std::endl;
//    std::cout<<energy(Zeta0,solver)<<",\t"<<energy(ZetaMin,solver)<<",\t"<<energy(ZetaMax,solver)<<std::endl;
//    std::cout<<"##################################################################"<<std::endl;
    gsl_min_fminimizer_set (s, &F, Zeta0, ZetaMin, ZetaMax);

    do
      {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        Zeta0   = gsl_min_fminimizer_x_minimum (s);
        ZetaMin = gsl_min_fminimizer_x_lower (s);
        ZetaMax = gsl_min_fminimizer_x_upper (s);

        status = gsl_min_test_interval (ZetaMin, ZetaMax, 0.00001, 0.0);
      }
    while (status == GSL_CONTINUE && iter < max_iter);

    E = gsl_min_fminimizer_f_minimum(s);
    RZ0 = Zeta0;
  }
  else{
    double a=ZetaMin;
    E=1000000.0;
    while(a<ZetaMax){
	double tE = energy(a,solver);
	if(tE<E) E=tE;
  	std::cout<<a<<" "<<tE<<std::endl;
  	a+=0.02;
    }
    gsl_min_fminimizer_free (s);
  }

  return E;
}

double tominimizeR(double x, void* p){
  Data *sol = (Data*) p;

  sol->scale->set_z(x);

//  std::cout<<*(sol->scale)<<std::endl;

  return minimize((void*)sol, *(sol->Zm), *(sol->Z0), *(sol->Zp));
} 

double minimizeR(void* solver, double& R0, double& Rm, double& Rp, bool R_SCAN=false){
  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;

  gsl_function F;
  F.function = &tominimizeR;
  F.params = solver;

  Data* sol = (Data*) solver;

  double Z0 = *(sol->Z0);
  double Zm = *(sol->Zm);
  double Zp = *(sol->Zp);

  double E;

  if(!R_SCAN){
    T = gsl_min_fminimizer_brent;
    s = gsl_min_fminimizer_alloc (T);
    gsl_min_fminimizer_set (s, &F, R0, Rm, Rp);

    do
      {
	*(sol->Z0) = Z0;
	*(sol->Zm) = Zm;
	*(sol->Zp) = Zp;

        iter++;
        status = gsl_min_fminimizer_iterate (s);

        R0   = gsl_min_fminimizer_x_minimum (s);
        Rm = gsl_min_fminimizer_x_lower (s);
        Rp = gsl_min_fminimizer_x_upper (s);

        status = gsl_min_test_interval (Rm, Rp, 0.001, 0.0);
      }
    while (status == GSL_CONTINUE && iter < max_iter);

    E = gsl_min_fminimizer_f_minimum(s);
  }
  else{
    double R=Rm;
    E=1000000.0;
    while(R<=Rp){
        double tE = energy(R,solver);
        if(tE<E) E=tE;
        std::cout<<R<<" "<<tE<<std::endl;
        R+=0.02;
    }
    gsl_min_fminimizer_free (s);
  }

  return E;
}

void setme(const std::string& input,  bool& y_x,  bool& z_x,
                           bool& Ex,   bool& Ey,   bool& Ez,
                         double& x0, double& y0, double& z0,
                         double& dx, double& dy, double& dz,
                          int& resx,  int& resy,  int& resz,
                         double& Z0, double& Zm, double& Zp,
                         double& R0, double& Rm, double& Rp,
                   bool& ALPHA_SCAN);


int main(int argc, const char* argv[]) {

 bool ENABLE_x = false;
 bool ENABLE_y = false;
 bool ENABLE_z = false;
 bool  y_is_x = false;
 bool  z_is_x = false;
 bool  R_SCAN = false;

 std::string config("conf.dat");
 double x0 = 2.0;
 double y0 = 2.0;
 double z0 = 2.0;
 double dx = 0.5;
 double dy = 0.5;
 double dz = 0.5;
 int resx = 2;
 int resy = 2;
 int resz = 2;

 double Z0 = 1.0;
 double Zp = 2.0;
 double Zm = 0.6;

 double R0 = 1.4;
 double Rp = 1.5;
 double Rm = 0.9;

 if(argc>1){
   std::ifstream file(argv[1]);
   std::string input((std::istreambuf_iterator<char>(file)),
                      std::istreambuf_iterator<char>());
	
   //std::cerr<<input;
   setme(input,      y_is_x,   z_is_x,
         ENABLE_x, ENABLE_y, ENABLE_x,
               x0,       y0,       z0,
               dx,       dy,       dz,
             resx,     resy,     resz,
              TZ0,      TZm,      TZp,
               R0,       Rm,       Rp,
       R_SCAN);                     
 }

 if(argc>2)
  config=std::string(argv[2]);


// std::cout<<minimize((void*) &solver,Zm,Z0,Zp)<<std::endl;
// std::cout<<Z0<<" in ["<<Zm<<","<<Zp<<"]"<<std::endl;

// std::cout<<-resx/2<<" "<<resx/2<<std::endl;
// std::cout<<-resy/2<<" "<<resy/2<<std::endl;
// std::cout<<-resz/2<<" "<<resz/2<<std::endl;

// std::cout<<ALPHA_SCAN<<std::endl;

 double Z0_mem=Z0;
 double Zm_mem=Zm;
 double Zp_mem=Zp;

 double R0_mem=R0;
 double Rm_mem=Rm;
 double Rp_mem=Rp;

//std::cout<<R0<<", "<<Rm<<", "<<Rp<<", "<<std::endl;
 
 for(int i = -resx/2; i <= resx/2; i++){
   for(int j = -resy/2; j <= resy/2; j++){
	Data solver; 
	solver.system = new qmt::QmtSystemStandard(config.c_str()); 
	solver.diagonalizer = new qmt::QmtLanczosDiagonalizer(config.c_str(),1);
	solver.Z0 = &Z0;
	solver.Zm = &Zm;
	solver.Zp = &Zp;

	Z0=Z0_mem;
	Zm=Zm_mem;
	Zp=Zp_mem;

	Rm= Rm_mem;
	Rp= Rp_mem;

	double x,y,z=0;
	x = x0+i*dx;
	if(y_is_x)
	  y = x;
	else
	  y = y0+j*dy;
	z = R0;

	solver.scale = new qmt::QmtVector(x,y,z);
//	std::cout<<i<<" "<<j<<" "<<k<<" ";
//	std::cout<<*(solver.scale)<<std::endl;
//
//	std::cout<<*(solver.scale);
        std::cout<<x<<" "<<minimizeR((void*) &solver,R0,Rm,Rp,R_SCAN);
        std::cout<<" at ("<<R0<<","<<RZ0<<")"<<std::endl;
   }
 }

 return 0; 
}

void setme(const std::string& input,  bool& y_x,  bool& z_x,
                           bool& Ex,   bool& Ey,   bool& Ez,
                         double& x0, double& y0, double& z0,
                         double& dx, double& dy, double& dz,
                          int& resx,  int& resy,  int& resz,
                         double& Z0, double& Zm, double& Zp,
                         double& R0, double& Rm, double& Rp,
		   bool& ALPHA_SCAN){

  char x='x';
  char y='y';
  char z='z';
  char at='@';
  char R='r';
  char semicolon=';';
  std::string colon(",");
  std::string eqx("=x");
  std::string alpha("scan r");

  std::string lc_input = input; // lower case input
  std::transform(lc_input.begin(), lc_input.end(), lc_input.begin(), ::tolower);
  std::size_t found = 0;
  while((found = lc_input.find(std::string("#"),found))!=std::string::npos){
        std::size_t f2 = lc_input.find(std::string("\n"),found);
	lc_input.erase(found,f2-found+1); // +1 to erase "\n" as well!
  }

  //std::cout<<input<<lc_input<<std::endl;

  found = lc_input.find(alpha);
  if (found!=std::string::npos)
    ALPHA_SCAN=true;

  std::string stringx = qmt::parser::get_bracketed_words(lc_input,x,semicolon,true)[0];
  std::vector<std::string> xs = qmt::parser::get_delimited_words(colon,stringx);
  std::string stringy = qmt::parser::get_bracketed_words(lc_input,y,semicolon,true)[0];
  std::vector<std::string> ys = qmt::parser::get_delimited_words(colon,stringy);
  std::string stringz = qmt::parser::get_bracketed_words(lc_input,z,semicolon,true)[0];
  std::vector<std::string> zs = qmt::parser::get_delimited_words(colon,stringz);
  std::string stringR = qmt::parser::get_bracketed_words(lc_input,R,semicolon,true)[0];
  std::vector<std::string> Rs = qmt::parser::get_delimited_words(colon,stringR);
  std::string stringzeta = qmt::parser::get_bracketed_words(lc_input,at,semicolon,true)[0];
  std::vector<std::string> zetas = qmt::parser::get_delimited_words(colon,stringzeta);

  std::string::size_type ptr;
  if(!xs.empty())	  x0 = std::stod(xs[0],&ptr);
  if(xs.size()>1)	  Ex = std::stoi(xs[1],&ptr);
    else		  Ex = false;
  if(Ex && xs.size()>2)   dx = std::stod(xs[2],&ptr);
  if(Ex && xs.size()>3) resx = std::stoi(xs[3],&ptr);
    else		resx = 0;

  if(eqx.compare(ys[0])==0){
    Ey=false;
    resy=0;
    y_x=true;
  }
  else{
    if(!ys.empty())	  y0 = std::stod(ys[0],&ptr);
    if(ys.size()>1)	  Ey = std::stoi(ys[1],&ptr);
      else		  Ey = false;
    if(Ey && ys.size()>2) dy = std::stod(ys[2],&ptr);
    if(Ey && ys.size()>3) resy = std::stoi(ys[3],&ptr);
      else		  resy = 0;
  }

  if(eqx.compare(zs[0])==0){
    Ez=false;
    resz=0;
    z_x=true;
  }
  else{
    if(!zs.empty())	  z0 = std::stod(zs[0],&ptr);
    if(zs.size()>1)	  Ez = std::stoi(zs[1],&ptr);
      else		  Ez = false;
    if(Ez && zs.size()>2) dz = std::stod(zs[2],&ptr);
    if(Ez && zs.size()>3) resz = std::stoi(zs[3],&ptr);
      else		  resz = 0;
  }

    if(!zetas.empty())	  Z0 = std::stod(zetas[0],&ptr);
    if(zetas.size()>1)	  Zm = std::stod(zetas[1],&ptr);
    if(zetas.size()>2)    Zp = std::stod(zetas[2],&ptr);

//    for (auto i : Rs) std::cout<<i<<std::endl;

    if(!Rs.empty())	  R0 = std::stod(Rs[0],&ptr);
    if(Rs.size()>1)	  Rm = std::stod(Rs[1],&ptr);
    if(Rs.size()>2)	  Rp = std::stod(Rs[2],&ptr);

//  std::cout<<x0<<" "<<Ex<<" "<<dx<<" "<<resx<<std::endl;
//  std::cout<<y0<<" "<<Ey<<" "<<dy<<" "<<resy<<std::endl;
//  std::cout<<z0<<" "<<Ez<<" "<<dz<<" "<<resz<<std::endl;
//  std::cout<<R0<<" "<<Rm<<" "<<Rp<<" "<<std::endl;
}
