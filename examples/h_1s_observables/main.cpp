#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <sstream>
#include <algorithm>
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include "../../headers/qmtparsertools.h"

struct Data{
 qmt::QmtSystem *system;
 qmt::QmtDiagonalizer *diagonalizer;
 qmt::QmtVector *scale;
};

double energy(double x, void* p){

 std::vector<double> one_body;
 std::vector<double> two_body;

 Data *sol = (Data*) p;

 sol->system->set_parameters(std::vector<double>({x}),*(sol->scale));

 for(auto i = 0; i < sol->system->get_one_body_number();++i)
   one_body.push_back(sol->system->get_one_body_integral(i));

 for(auto i = 0; i < sol->system->get_two_body_number();++i)
   two_body.push_back(sol->system->get_two_body_integral(i));

return sol->diagonalizer->Diagonalize(one_body, two_body);
}

void setme (std::string& input, std::vector<double>& XY,
                                std::vector<double>& Z,
                                std::vector<double>& zeta);

int main(int argc, const char* argv[]) {
 gsl_set_error_handler_off();

 std::string config("conf.dat");

 std::vector<double> XY({4.0});
 std::vector<double> Z({1.4});
 std::vector<double> zeta({1.16});

 std::vector<double> avs;
 std::vector<int> map;

 std::ofstream integrals("integrals.dat", std::ios::out);
 std::ofstream navs("Navs.dat", std::ios::out);
 std::ofstream oavs;
 std::ofstream multiavs("multiavs.dat", std::ios::out);
 std::ofstream dens;


 if(argc>1){
   std::ifstream file(argv[1]);
   std::string input((std::istreambuf_iterator<char>(file)),
                      std::istreambuf_iterator<char>());
	
   setme (input, XY, Z, zeta);
 }

 if(argc>2)
  config=std::string(argv[2]);

 if(argc>3){
   integrals = std::ofstream("integrals_"+std::string(argv[3])+".dat", std::ios::out);
   navs      = std::ofstream("Navs_"+std::string(argv[3])+".dat", std::ios::out);
   multiavs  = std::ofstream("multi_avs_"+std::string(argv[3])+".dat", std::ios::out);
 }


 std::ifstream map_file("megacell_orbital.dat");
 std::string map_line;
 std::getline(map_file,map_line);
 map_file.close();
 std::vector<std::string> map_string = qmt::parser::get_delimited_words(", ",map_line);
 for(const auto& index : map_string)
	map.push_back(std::stoi(index));
		        
 std::cout.precision(17);


 for(unsigned int i=0; i<XY.size(); i++){
	Data solver; 
	solver.system = new qmt::QmtSystemStandard(config.c_str()); 
	solver.diagonalizer = new qmt::QmtLanczosDiagonalizer(config.c_str(),1);

	auto diag = static_cast<qmt::QmtLanczosDiagonalizer*>(solver.diagonalizer);
	auto syst = static_cast<qmt::QmtSystemStandard*>(solver.system);

	solver.scale = new qmt::QmtVector(XY[i],XY[i],Z[i]);

	auto E = energy(zeta[i],(void*) &solver);

	avs.clear();
	
	diag->NWaveFunctionEssentials(avs);
	
        std::cout<<XY[i]<<" "<<Z[i]<<" ";
	std::cout<<zeta[i]<<" ";
	std::cout<<E<<" ";
	std::cout<<diag->DoubleOccupancy()<<" ";
	std::cout<<diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0)<<" ";
	std::cout<<std::endl;

	unsigned int num_avs = avs.size()/2;

	navs<<i<<" ";
	for(unsigned int j=0; j<num_avs; ++j)
	  navs<<avs[j]<<" ";
	for(unsigned int j=0; j<num_avs; ++j)
	  navs<<avs[num_avs+j]<<" ";
	navs<<std::endl;

	integrals<<i<<" ";
	for(int j=0; j<syst->get_one_body_number(); ++j)
	  integrals<<syst->get_one_body_integral(j)<<" ";
	for(int j=0; j<syst->get_two_body_number(); ++j)
	  integrals<<syst->get_two_body_integral(j)<<" ";
	integrals<<std::endl;

//	if(argc>3)
//          oavs = std::ofstream(std::string("Oavs_")+std::string(argv[3])+std::string("_")+std::to_string(i)+std::string(".dat"), std::ios::out);
//	else
//          oavs = std::ofstream(std::string("Oavs")+std::string("_")+std::to_string(i)+std::string(".dat"), std::ios::out);

	auto size = diag->size();

//	for(int j=0; j<size; ++j){
//	  for(int k=0; k<size; ++k){
//	    oavs<<j<<" "<<k<<" ";
//	    oavs<<diag->AvCUpCUp(j,k)<<" ";
//	    oavs<<diag->AvCUpCDo(j,k)<<" ";
//	    oavs<<diag->AvCDoCDo(j,k)<<" ";
//	    oavs<<std::endl;
//	  }
//	  oavs<<std::endl;
//	}

	multiavs<<i<<" ";
        /* 1 */ multiavs<<diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CUp,1,qmt::AUp,1)<<" ";
        /* 2 */ multiavs<<diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CDo,1,qmt::ADo,1)<<" ";
        /* 3 */ multiavs<<diag->Average_4(qmt::CDo,0,qmt::ADo,0,qmt::CDo,1,qmt::ADo,1)<<" ";

        /* 4 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CUp,1,qmt::AUp,1)<<" ";
        /* 5 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CUp,1,qmt::AUp,1,qmt::CDo,1,qmt::ADo,1)<<" ";
        /* 6 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CDo,1,qmt::ADo,1)<<" ";
        /* 7 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CUp,1,qmt::AUp,1,qmt::CDo,1,qmt::ADo,1)<<" ";
        /* 8 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CDo,1,qmt::ADo,1)<<" ";
        /* 9 */ multiavs<<diag->Average_6(qmt::CDo,0,qmt::ADo,0,qmt::CUp,1,qmt::AUp,1,qmt::CDo,1,qmt::ADo,1)<<" ";

        /*10 */ multiavs<<diag->Average_8(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CUp,1,qmt::AUp,1,qmt::CDo,1,qmt::ADo,1)<<" ";

        /*11 */ multiavs<<diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CUp,2,qmt::AUp,2)<<" ";
        /*12 */ multiavs<<diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CDo,2,qmt::ADo,2)<<" ";
        /*13 */ multiavs<<diag->Average_4(qmt::CDo,0,qmt::ADo,0,qmt::CDo,2,qmt::ADo,2)<<" ";

        /*14 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CUp,2,qmt::AUp,2)<<" ";
        /*15 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CUp,2,qmt::AUp,2,qmt::CDo,2,qmt::ADo,2)<<" ";
        /*16 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CDo,2,qmt::ADo,2)<<" ";
        /*17 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CUp,2,qmt::AUp,2,qmt::CDo,2,qmt::ADo,2)<<" ";
        /*18 */ multiavs<<diag->Average_6(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CDo,2,qmt::ADo,2)<<" ";
        /*19 */ multiavs<<diag->Average_6(qmt::CDo,0,qmt::ADo,0,qmt::CUp,2,qmt::AUp,2,qmt::CDo,2,qmt::ADo,2)<<" ";

        /*20 */ multiavs<<diag->Average_8(qmt::CUp,0,qmt::AUp,0,qmt::CDo,0,qmt::ADo,0,qmt::CUp,2,qmt::AUp,2,qmt::CDo,2,qmt::ADo,2)<<" ";

	// Spin0 Spin1
        auto Sp01 = diag->Average_4(qmt::CUp,0,qmt::ADo,0,qmt::CDo,1,qmt::AUp,1);
        auto Sm01 = diag->Average_4(qmt::CDo,0,qmt::AUp,0,qmt::CUp,1,qmt::ADo,1);
        auto Sz01a= diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CUp,1,qmt::AUp,1);
        auto Sz01b= diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CDo,1,qmt::ADo,1);
        auto Sz01c= diag->Average_4(qmt::CDo,0,qmt::ADo,0,qmt::CUp,1,qmt::AUp,1);
        auto Sz01d= diag->Average_4(qmt::CDo,0,qmt::ADo,0,qmt::CDo,1,qmt::ADo,1);

	/*21 */ multiavs<<Sp01<<" ";
	/*22 */ multiavs<<Sm01<<" ";
	/*23 */ multiavs<<Sz01a<<" ";
	/*24 */ multiavs<<Sz01b<<" ";
	/*25 */ multiavs<<Sz01c<<" ";
	/*26 */ multiavs<<Sz01d<<" ";
	/*27 */ multiavs<<(Sp01+Sm01)+0.5*(Sz01a-Sz01b-Sz01c+Sz01d)<<" ";

	// Spin0 Spin2
        auto Sp02 = diag->Average_4(qmt::CUp,0,qmt::ADo,0,qmt::CDo,2,qmt::AUp,2);
        auto Sm02 = diag->Average_4(qmt::CDo,0,qmt::AUp,0,qmt::CUp,2,qmt::ADo,2);
        auto Sz02a= diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CUp,2,qmt::AUp,2);
        auto Sz02b= diag->Average_4(qmt::CUp,0,qmt::AUp,0,qmt::CDo,2,qmt::ADo,2);
        auto Sz02c= diag->Average_4(qmt::CDo,0,qmt::ADo,0,qmt::CUp,2,qmt::AUp,2);
        auto Sz02d= diag->Average_4(qmt::CDo,0,qmt::ADo,0,qmt::CDo,2,qmt::ADo,2);

	/*28 */ multiavs<<Sp02<<" ";
	/*29 */ multiavs<<Sm02<<" ";
	/*30 */ multiavs<<Sz02a<<" ";
	/*31 */ multiavs<<Sz02b<<" ";
	/*32 */ multiavs<<Sz02c<<" ";
	/*33 */ multiavs<<Sz02d<<" ";
	/*34 */ multiavs<<(Sp02+Sm02)+0.5*(Sz02a-Sz02b-Sz02c+Sz02d)<<" ";

        multiavs<<std::endl;

/*
	if(argc>3)
          dens = std::ofstream(std::string("density_")+std::string(argv[3])+std::string("_")+std::to_string(i)+std::string(".dat"), std::ios::out);
	else
          dens = std::ofstream(std::string("density")+std::string("_")+std::to_string(i)+std::string(".dat"), std::ios::out);

        for(int m=-60; m<=60; m++){
          for(int n=-60; n<=60; n++){
	    for(int r= 0; r<=50; r++){
	      dens<<syst->get_wfs_products_sum(qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r),avs,map)<<std::endl;
	    }
	  }
	}
*/
  }

 return 0; 
}

void setme (std::string& input, std::vector<double>& XY,
                                std::vector<double>& Z,
                                std::vector<double>& zeta){
  XY.clear();
  Z.clear();
  zeta.clear();

  //remove comments
  std::size_t found = 0;
  while((found = input.find("#",found))!=std::string::npos){
        std::size_t f2 = input.find("\n",found);
	input.erase(found,f2-found+1); // +1 to erase "\n" as well!
  }

  std::istringstream iss(input);
  std::string line;
  std::string::size_type ptr;
  while (std::getline(iss, line)){
    std::vector<std::string> nums = qmt::parser::get_delimited_words(" ",line);

    if(nums.size()>2){
      XY.push_back(std::stod(nums[0],&ptr));
      Z.push_back(std::stod(nums[1],&ptr));
      zeta.push_back(std::stod(nums[2],&ptr));
    }

  }
}

