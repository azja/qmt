#include <iostream>
#include <bitset>
class A {

virtual void private_run() {std::cout<<"A::private_run"<<std::endl;}

public:

 void run_me() {
  private_run();
 }

};



class B:public A {
 void private_run() {std::cout<<"B::private_run"<<std::endl;}
};


int main(){

A* a = new A;
A* b = new B;

a->run_me();
b->run_me();

std::bitset<64> b1(78);
std::cout<<b1<<std::endl;
std::bitset<64> b2(b1);
std::cout<<b2<<std::endl;
b2[4] = b1[5];

return 0;
}
