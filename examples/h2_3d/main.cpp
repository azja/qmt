#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtmicroscopic.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"
#include "../../headers/qmtparsertools.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>


#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include <stdio.h>





typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 

struct System {

qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> *microscopic;

    

    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<qmt::QmtNState <qmt::uint, int> > states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula
         <qmt_hamiltonian,LocalAlgebras::SparseAlgebra> 
                                                                              h2_formula; //Tranlate Hamiltonian to matrix
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    unsigned int number_of_centers;							       //Number of atomic centers - spin obitals						
    std::vector<double> intgs;                                                                 //Integrals - microscopic parameters
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;                //Eigen problem solver
    typedef qmt::QmtNState<qmt::uint, int> NState;                                             //Output - eigen state
    int _lanczos_steps;
    int _lanczos_eps;
    double w_vs_u;
    int problem_size;
    int electron_number;
    int alphas_number;


System(std::vector<double>& alphas, const char* file_name): _lanczos_steps(500), _lanczos_eps(1.0e-9)  {
  
   std::ifstream cfile;       
   cfile.open(file_name); 
  std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
  
   std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
   std::string definitions_fn = qmt::parser::get_bracketed_words(content,"wfs_definitions","wfs_definitions_end")[0];
   std::string megacell_fn = qmt::parser::get_bracketed_words(content,"megacell","megacell_end")[0];;
   std::string supercell_fn =qmt::parser::get_bracketed_words(content,"supercell","supercell_end")[0];
   problem_size =std::stoi(qmt::parser::get_bracketed_words(content,"problem_size","problem_size_end")[0]);
   electron_number =std::stoi(qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0]);
   alphas_number  =std::stoi(qmt::parser::get_bracketed_words(content,"alphas_number","alphas_number_end")[0]);
   microscopic = new  qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian>
		(hamiltonian_fn,definitions_fn,supercell_fn, megacell_fn,alphas);

    hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(hamiltonian_fn);

    microscopic->set_parameters(alphas,qmt::QmtVector(4.0,4.0,1.43));
    //generating all posible electronic states
    generator.generate(electron_number,problem_size*2,states);
   std::cout<<"States generated"<<std::flush<<std::endl; 
    // preparing hamiltonian formula engine
    h2_formula.set_hamiltonian(*hamiltonian);
     std::cout<<"Hamiltonian generated"<<std::flush<<std::endl; 
    h2_formula.set_StatesOptional(states,1);
/*    
     for (auto state : states){
      std::cout<<state<<std::endl; 
     }
*/
    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);

    LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size,_lanczos_steps, _lanczos_eps); 
 }

double get_energy() {
 std::vector<double> one_body;

 for(unsigned int i = 0; i < microscopic->number_of_one_body();++i) {
   
   auto integral = microscopic->get_one_body(i);
//std::cout<<"0I#"<<i<<" = "<<integral<<std::flush<<std::endl;
//if(i == 6) integral=0;
   h2_formula.set_microscopic_parameter(1,i,integral);
   one_body.push_back(integral);
   

 }
 
  std::vector<double> two_body;

 for(unsigned int i = 0; i < microscopic->number_of_two_body();++i) {

   auto integral = microscopic->get_two_body(i);

    h2_formula.set_microscopic_parameter(2,i,integral);
//std::cout<<"I#"<<i<<" = "<<integral<<std::flush<<std::endl;
   two_body.push_back(integral);

 }

//std::cout<<"Integrals computed"<<std::endl;
h2_formula.get_Hamiltonian_Matrix(hamiltonian_M);
//std::cout<<*hamiltonian_M;
LanczosSolver->solve(hamiltonian_M);
//std::cout<<"Coulomb energy = "<<microscopic->get_coulomb_repulsion()/8<<std::endl;
double energy = LanczosSolver->get_minimal_eigenvalue() + microscopic->get_coulomb_repulsion();

/*
int s = states.size();
                        gsl_vector* eigenvalues=gsl_vector_calloc(s);
			gsl_matrix* eigenvectors = gsl_matrix_calloc(s,s);

                        gsl_matrix* matrix = gsl_matrix_calloc(s,s);
			gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
					s);
for(int i = 0; i <s; ++i) {
  for(int j = 0; j <s; ++j)
    gsl_matrix_set(matrix,i,j,hamiltonian_M->get_value(i,j));
}
			
			gsl_eigen_symmv(matrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);
	
			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
double  energy = gsl_vector_get(eigenvalues,0);
	                gsl_matrix_free(matrix);
			gsl_matrix_free(eigenvectors);
			gsl_vector_free(eigenvalues);
			
  //std::cout<<"S size = "<<states.size()<<std::endl;
  */
return  energy/8;


  
}

double get_overlap(int i,int j,const qmt::QmtVector& R_i = qmt::QmtVector(0,0,0),const qmt::QmtVector& R_j  = qmt::QmtVector(0,0,0)) {
 return microscopic->get_overlap(i,j,R_i, R_j);
}


~System() {

    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	delete LanczosSolver;
	delete microscopic;
}

void set(const std::vector<double> params,const qmt::QmtVector& scale) {
      microscopic->set_parameters(params,scale/*qmt::QmtVector(1,1,1)*/);
}

};


qmt::QmtVector scaler(1,1,1);


double my_f (double x, void *params) {
 System *s = static_cast<System*>(params);

 
  std::vector<double> args;
  args.push_back(x); 
  s->set(args,scaler);
//    std::cout<<"Overlap <1|0> = "<<s->get_overlap(1,0,qmt::QmtVector(0,0,0),qmt::QmtVector(0, 0,scaler.get_z()))<<std::endl;
double energy =  s->get_energy();

//  std::cout<<"E = "<<energy<<" "<<x<<std::endl;
  return energy;
}



int main(int argc, const char* argv[]) {

std::vector<double> alphs;

//alphs.push_back(1.0);
alphs.push_back(1.0);
std::cout<<argc;
double xy0 = 2.001;
double z0 = 1.43;
if(argc >2){
xy0 = std::atof(argv[1]);
std::cout<<"xy = "<<xy0<<std::endl;
z0 = std::atof(argv[2]);
std::cout<<"z0 = "<<z0<<std::endl;
}

System h2_system(alphs, "config.cfg");
/*
std::cout<<"Overlap <0|0> = "<<h2_system.get_overlap(0,0)<<std::endl;
std::cout<<"Overlap <0|1> = "<<h2_system.get_overlap(0,1,qmt::QmtVector(0,0,1.43),qmt::QmtVector(0,0,0))<<std::endl;
std::cout<<"Overlap <0|-5> = "<<h2_system.get_overlap(0,5,qmt::QmtVector(0,0,1.43),qmt::QmtVector(-4,-4,0))<<std::endl;
std::cout<<"Overlap <0|-5> = "<<h2_system.get_overlap(0,1,qmt::QmtVector(0,0,1.43),qmt::QmtVector(-4,-4,0))<<std::endl;


alphs.clear();
alphs.push_back(0.8);
h2_system.set(alphs);
std::cout<<"And again..."<<std::endl;
std::cout<<"Overlap <0|-5> = "<<h2_system.get_overlap(0,5,qmt::QmtVector(0,0,1.43),qmt::QmtVector(-4,-4,0))<<std::endl;
std::cout<<"Overlap <0|-5> = "<<h2_system.get_overlap(0,1,qmt::QmtVector(0,0,1.43),qmt::QmtVector(-4,-4,0))<<std::endl;


for(int i = 0 ; i < 6;++i)
std::cout<<" <0|H1|"<<i<<"> = "<<h2_system.microscopic->get_one_body(i)<<std::endl;
//std::cout<<"Overlap <0|3> = "<<h2_system.get_overlap(0,3)<<std::endl;
int NVAR = h2_system.alphas_number;
std::cout<<" <00|I|11> = "<<h2_system.microscopic->get_two_body(37)<<std::endl;
std::cout<<" <00|I|00> = "<<h2_system.microscopic->get_two_body(0)<<std::endl;

for(int i = 0; i < 20;++i) {

  std::vector<double> args;
  double a = 0.8 + i* 0.05;
  args.push_back(a); 
  h2_system.set(args);
  std::cout<<"Overlap <0|-5> = "<<h2_system.get_overlap(0,5,qmt::QmtVector(0,0,1.43),qmt::QmtVector(-4,-4,0))<<std::endl;
  std::cout<<"Overlap <0|-5> = "<<h2_system.get_overlap(0,1,qmt::QmtVector(0,0,1.43),qmt::QmtVector(-4,-4,0))<<std::endl;

  std::cout<<a;
  double energy =  h2_system.get_energy();  
  std::cout<<" "<<energy<<std::endl;
  
} 
*/

for(int i = 0; i < 38;++i) {
 for(int j = 0; j < 4;j++) {
scaler = qmt::QmtVector(xy0 + i * 0.1, xy0 +  i* 0.1,z0 + 0.05 * j);
//alphs.clear();
//alphs.push_back(0.9 + i *0.02);
//h2_system.set(alphs,scaler);



void*  sys = static_cast<void*>(&h2_system);




  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = 1.35, m_expected = 1.35;
  double a = 1.3,b = 2.0;
  gsl_function F;

  F.function = &my_f;
  F.params = sys;


  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.001, 0.0);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
  
  std::vector<double> args;
  args.push_back(m); 

  h2_system.set(args,scaler);
  std::cout<<scaler<<" "<<h2_system.get_energy()<<" "<<m<<std::endl;
   }
 std::cout<<std::endl;
  }

return 0;

}
