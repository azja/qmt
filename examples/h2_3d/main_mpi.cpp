#include "../../../headers/qmtenginedata.h"
#include "../../../headers/qmtmpisolver.h"
#include "../../../headers/qmtlncdiag.h"
#include "../../../headers/qmtsystemstandard.h"



struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  
};

qmt::QmtVector scaler(4.1,4.1,1.4);

double alpha_global;
double energy_global;

void F(void* params) {
 DataForF *data = (DataForF*)(params);



 auto myf = (*data->f);
 double result = myf(arg,scaler,*(data->data),*(data->settings));
 std::cout<<"Result = "<<result<<std::endl;

auto func = [&](double x, void *params) {
 std::vector<double> arg({x});
 return myf(arg,scaler,*(data->data),*(data->settings));
};

int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = 1.35, m_expected = 1.35;
  double a = 1.3,b = 2.0;
  gsl_function F;

  F.function = &func;
  F.params = null_ptr;


  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.001, 0.0);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
  
  std::vector<double> args;
  args.push_back(m); 

  energy_global = func(m,null_ptr);
  alpha_global = m;
}



int main() {

 MPI_Init(nullptr,nullptr);
 

std::cout<<argc;
double xy0 = 2.001;
double z0 = 1.43;
if(argc >2){
 xy0 = std::atof(argv[1]);
 z0 = std::atof(argv[2]);
}
 
 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat");
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 
 for(int i = 0; i < 50;++i){
   for(int j = 0; j < 50;j++) {
    scaler = qmt::QmtVector(xy0 + i * 0.1, xy0 +  i* 0.1,z0 + 0.05 * j);
    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
     std::cout<<scaler<<" "<<global_energy<<" "<<global_alpha<<std::endl;
   }
    std::cout<<std::endl;
  }
 

 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
