#!/usr/bin/python

import math
import numpy as np
import sys

def length(vector):
	return math.sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2])
#length

def distance(atom1,atom2):
	return length(atom1[1]-atom2[1])
#distance

# lattice constant
a=5.0
b=5.0
R=1.43042
verX=np.array([1,0,0])
verY=np.array([0,1,0])
verZ=np.array([0,0,1])
vers=[verX,verY,verZ]
latticeX=a*verX
latticeY=b*verY
latticeZ=R*verZ

# super celllatticeX
SuperCell = [
[0,np.array([0,0,0]),np.array([0,0,0])],
[1,latticeZ,verZ]
]

# directions of PBC
PBCdirections=[
latticeX,
latticeY
#latticeY*math.sin(math.pi/3)+latticeX*math.cos(math.pi/3)
]

# vers of PBC
PBCvers=[
latticeX/length(latticeX),
latticeY/length(latticeY)
#(latticeY*math.sin(math.pi/3)+latticeX*math.cos(math.pi/3))/length(latticeY*math.sin(math.pi/3)+latticeX*math.cos(math.pi/3))
]

# cut-off
#CutOff=math.sqrt(a*a+b*b+R*R)
CutOff=math.sqrt(2*a*2*a+2*b*2*b+R*R)
#CutOff=math.sqrt(100*a*a+R*R)

# zero tolerance
zero=10**-6

PBCsizes=[]
for PBCd in PBCdirections:
	PBCsizes.append(int(CutOff/length(PBCd)))
CutOffUnits=max(PBCsizes)


def CellTrans(translation,normal_translation,Cell):
	newCell=[]
	for atom in Cell:
		newatom=[atom[0],atom[1]+translation,atom[2]+normal_translation]
		newCell.append(newatom)
	return newCell
#translation

def ClearCell(Cell,referenceCell,cutoff):
	for i in range(len(Cell)-1,0-1,-1):
		IF_REMOVE = True
		for atom in referenceCell:
			if(distance(atom,Cell[i]) <= cutoff):
				IF_REMOVE = False
		if IF_REMOVE:
			Cell.pop(i)

# mega cell
MegaCell=[] 
#MegaCell+= SuperCell


for i in range(-CutOffUnits-1, CutOffUnits+2):
	for j in range(-CutOffUnits-1, CutOffUnits+2):
		direction_bare=(i*PBCvers[0]+j*PBCvers[1])
		direction_dres=(i*PBCdirections[0]+j*PBCdirections[1])
		MegaCell+=CellTrans(direction_dres,direction_bare,SuperCell)

ClearCell(MegaCell,SuperCell,CutOff)

############################################################################################################################################################
# jmol Megacell
MegaCellFile = open('jmol_megacell.dat', 'w+')
MegaCellFile.write(str(len(MegaCell))+"\n\n") # for jmol
for atom in MegaCell:
 	if atom[0]==0:
 		MegaCellFile.write("Li ")
 	if atom[0]==1:
 		MegaCellFile.write("Be ")
 	if atom[0]==2:
 		MegaCellFile.write("B ")
 	if atom[0]==3:
 		MegaCellFile.write("C ")
 	if atom[0]==4:
 		MegaCellFile.write("N ")
 	if atom[0]==5:
 		MegaCellFile.write("O ")
 	if atom[0]==6:
 		MegaCellFile.write("F ")
 	if atom[0]==7:
 		MegaCellFile.write("Ne ")
 	MegaCellFile.write(str(atom[2][0])+" "+str(atom[2][1])+" "+str(atom[2][2])+"\n")
MegaCellFile.close()


############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################

# creating beta dictionary
beta_dict=[]
beta_indx=0
for atomS in SuperCell:
	for atomM in MegaCell:
		dist=distance(atomS,atomM)

		is_in_dict=False
		for item in beta_dict:
			if (abs(dist-item[1]) < zero):
				is_in_dict=True
		if not is_in_dict:
			beta_dict.append([beta_indx,dist])
			beta_indx+=1

# sorting beta dictionary
beta_dict=np.sort(beta_dict,axis=0)

# creating basis for supercell
SCbasis=[]
for atomS in SuperCell:	
	output=[]
	for atomM in MegaCell:
		dist=distance(atomS,atomM)
		if (dist>CutOff):
			continue
	
		for item in beta_dict:
			if (abs(dist-item[1]) < zero):
				index=int(item[0])
				break

		output.append([index,atomM[0],atomM[2]])
	output=sorted(output,key=lambda item: item[0]) # sorting basis with respect to beta_indx
	SCbasis.append(output)

#for basis in SCbasis:
#	for item in basis:
#		print item[0],item[1],item[2]
#	print ""

def unique(basis):
	output=[]
	for item in basis:
		if_take=True
		for taken in output:
			if item[0] == taken[0]:
				if_take=False
		if if_take:
			output.append(item)
	return output
# unique with respect to first parameter of the list (here beta index)

minOrhogonalizationBasis = unique(SCbasis[0])

def translate_basis(basis,translation):
	output=[]
	for item in basis:
		output.append([item[0],item[1],item[2]+translation])
	return output
# translation of basis coordinates

# create final basis for orthogonalization
finalBasis=[]
for center in minOrhogonalizationBasis:
	translation=center[2]-SuperCell[center[1]][2]
	finalBasis.append(translate_basis(SCbasis[center[1]],translation))

#for basis in finalBasis:
#	for item in basis:
#		print item[0],item[1],item[2]
#	print ""
	
indx=0
for basis in finalBasis:
	print '{[',indx,'] [',basis[0][2][0],',',basis[0][2][1],',',basis[0][2][2],']',
	for item in basis:
		print '(',item[0],') [1s,',item[2][0],',',item[2][1],',',item[2][2],',',0,', 3]',
	print '}\n'
	indx+=1

print "\n"

for i in range(indx):
	if i == 0:
		print "< 0 ,",i,", 1 >"
	else:
		print "< 0 ,",i,", 0 >"

print "\n\nopen_betas"
for item in minOrhogonalizationBasis:
	dist=length(SCbasis[0][0][2]-item[2])
	if dist<1.0:
		print 1.0,
	else:
		print ",",0.0,
print ""
print "close_betas"

print "\n\nopen_atomic_numbers"
for i in range(len(SuperCell)):
	if i == 0:
		print "1",
	else:
		print ',',1,
print ""
print "close_atomic_numbers"

#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
#########################################################################################################################

#CHECKER
# jmol obital indx=indx
indx=0
ObitalFile = open('jmol_orbital.dat', 'w+')
ObitalFile.write(str(len(finalBasis[indx]))+"\n\n") # for jmol
for atom in finalBasis[indx]:
 	if atom[0]==0:
 		ObitalFile.write("Ca ")
 	if atom[0]==1:
 		ObitalFile.write("K ")
 	if atom[0]==2:
 		ObitalFile.write("Ar ")
 	if atom[0]==3:
 		ObitalFile.write("Cl ")
 	if atom[0]==4:
 		ObitalFile.write("S ")
 	if atom[0]==5:
 		ObitalFile.write("P ")
 	if atom[0]==6:
 		ObitalFile.write("Si ")
 	if atom[0]==7:
 		ObitalFile.write("Al ")
 	if atom[0]==8:
 		ObitalFile.write("Mg ")
 	if atom[0]==9:
 		ObitalFile.write("Na ")
 	if atom[0]==10:
 		ObitalFile.write("Ne ")
 	if atom[0]==11:
 		ObitalFile.write("F ")
 	if atom[0]==12:
 		ObitalFile.write("O ")
 	if atom[0]==13:
 		ObitalFile.write("N ")
 	if atom[0]==14:
 		ObitalFile.write("C ")
 	if atom[0]==15:
 		ObitalFile.write("B ")
 	if atom[0]==16:
 		ObitalFile.write("Be ")
 	if atom[0]==17:
 		ObitalFile.write("Li ")
 	if atom[0]==18:
 		ObitalFile.write("He ")
 	if atom[0]>18:
 		ObitalFile.write("H ")
 	ObitalFile.write(str(atom[2][0])+" "+str(atom[2][1])+" "+str(atom[2][2])+"\n")
ObitalFile.close()

sys.exit()


