#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"



struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(4.1,4.1,1.4);

double alpha_global;
double energy_global;


double func(double x, void *p) {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg({x});
 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
}



void F(void* params) {

/*
std::function<double(double,void*)>  func = [](double x, void *p)->double {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg({x});
 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
};
*/

int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = 1.35;
  double a = 1.3,b = 2.0;
  gsl_function F;

  typedef double function_t(double,void* );

  F.function = func;//.target<function_t>();
  F.params = params;


  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.001, 0.0);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
  
  std::vector<double> args;
  args.push_back(m); 

  energy_global = func(m,params);
  alpha_global = m;
  
}



int main(int argc,const char* argv[]) {

 MPI_Init(nullptr,nullptr);
 int rank;
 
double xy0 = 2.001;
double z0 = 1.43;
if(argc >2){
 xy0 = std::atof(argv[1]);
 z0 = std::atof(argv[2]);
}

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
/* qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat");
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;*/
 
 

  std::vector<double> avs;
  std::vector<int> map;
   
  std::ifstream map_file("megacell_orbital.dat");
  std::string map_line;
  std::getline(map_file,map_line);
  map_file.close();
  std::vector<std::string> map_string = qmt::parser::get_delimited_words(", ",map_line);
  for(const auto& index : map_string)
     map.push_back(std::stoi(index));
                 
 
/*
 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;
*/

system->set_parameters(std::vector<double>({1.19}),qmt::QmtVector(2.20001,2.20001,2.001));


for(int i = 0; i < 100;++i) {
    for(int j = 0; j < 40;++j) {
         std::cout<<qmt::QmtVector(-5.0 + 0.1 * i,0.0,-1.0 + j * 0.1 )<<" "<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(qmt::QmtVector(-5.0 + 0.1 * i,0.0,-1.0 + j * 0.1 ),avs,map)<<std::endl;
        }
        std::cout<<std::endl;
  }
  
 

 delete system;
// delete diagonalizer;

 return 0; 
}
