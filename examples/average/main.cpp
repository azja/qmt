#include <iostream>
#include "../../headers/qmtstate.h"
#include "../../headers/qmtbasisgenerator.h"
#include <bitset>
#include <vector>
#include <unordered_set>	
#include <iterator>
#include <cmath>

#define S 4

// form Boost
template <class T>
inline void hash_combine(std::size_t& seed, const T& v){
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

struct QmtStateHasher{
template <class State>
std::size_t operator()(const State& state)const{
        std::size_t seed = 0;
        hash_combine(seed, state.getPhase());
        hash_combine(seed, state.integerRepresentation());

        return seed;
}};

struct QmtStateCompare{
template <class State>
std::size_t operator()(const State& left,const State& right)const{
	return ((left.getPhase() == right.getPhase())&&(left.integerRepresentation() == right.integerRepresentation()));
}};

template<class State, class Operator, size_t N>
class QmtStateProjectorElement{
protected:
//  auto cUP = Operator::get_up_creator;
//  auto cDO = Operator::get_down_creator;
//  auto aUP = Operator::get_up_anihilator;
//  auto aDO = Operator::get_down_anihilator;

double multiplier;	
  
public:
  State left,right;
  QmtStateProjectorElement(State _left, State _right, double _multiplier = 1.0):multiplier(_multiplier),left(_left),right(_right){}

  State operator()(State input) const{
    State output = left;
    output.setPhase(multiplier*State::dot_product(right,input));

    return output;
  }

  std::vector<State> operator()(const std::vector<State>& input) const{
	  std::vector<State> output;
	  for(const auto & i : input){
            State state = left;
            state.setPhase(multiplier*State::dot_product(right,i));
	    output.push_back(state);
	  }

    return output;
  }

  friend std::ostream& operator<<(std::ostream& stream,const QmtStateProjectorElement& stateP) {
    if(stateP.left.getPhase()>0) stream<<"+";
    stream<<stateP.left.getPhase();
    stream<<" |";
    for(unsigned i =stateP.left.get_size(); i < 2 * stateP.left.get_size(); ++i) {
        stream<<stateP.left.get_bit(i);
    }
    stream<<">x|";
    for(unsigned i = 0; i < stateP.left.get_size(); ++i) {
        stream<<stateP.left.get_bit(i);
    }
    stream<<"><";

    for(unsigned i = stateP.right.get_size(); i>0; --i) {
        stream<<stateP.right.get_bit(i-1);
    }
    stream<<"|x<";
    for(unsigned i = 2*stateP.right.get_size(); i > stateP.right.get_size(); --i) {
        stream<<stateP.right.get_bit(i-1);
    }
    stream<<"|";
    return stream;

  }
};

template<class QmtStateProjectorElement, class State>
class QmtStateProjector{
public:
	std::vector<QmtStateProjectorElement> projectorElements;
public:
  QmtStateProjector(){}
  void add_element(QmtStateProjectorElement element){
    projectorElements.push_back(element);
    projectorElements.back().left.setPhase(1.0);
    projectorElements.back().right.setPhase(1.0);
  }

  friend std::ostream& operator<<(std::ostream& stream,const QmtStateProjector& projector) {
    for(const QmtStateProjectorElement& el : projector.projectorElements)
	    stream<<el<<" ";
    return stream;
  }

  std::vector<State> operator()(const State& input)const{
    std::vector<State> output;
    for(const QmtStateProjectorElement& el : projectorElements){
      State state = el(input);
      output.push_back(state);
    }
    return output;
  }

  std::vector<State> operator()(const std::vector<State>& input) const{
    std::unordered_set<State,QmtStateHasher,QmtStateCompare> output;
    for(const QmtStateProjectorElement& el : projectorElements){
      auto states = el(input);
      for(auto& state : states){
	auto isthere = output.find(state); 
        if (isthere == output.end()) output.insert(state);
        else{
	       state.setPhase(isthere->getPhase()+state.getPhase());
	       output.erase(isthere);
	       output.insert(state);
	}
      }
    }
    auto vector = std::vector<State>(output.size());
    std::copy(output.begin(), output.end(), vector.begin());
    return vector;
  }


};

int main(){
  qmt::QmtBasisGenerator generator;

  typedef qmt::QmtNState<std::bitset<S>,int> State;
  typedef qmt::SqOperator<State> Operator;
  auto cUP = Operator::get_up_creator;
  auto cDO = Operator::get_down_creator;
  auto aUP = Operator::get_up_anihilator;
  auto aDO = Operator::get_down_anihilator;

  std::vector<State> output;
//  for(unsigned i=0U; i<=S; ++i)
//    generator.generate<S>(i,S,output,false,false);
  
  generator.generate<S>(S/2,S,output,false,false);

  QmtStateProjector<QmtStateProjectorElement<State,Operator,S>,State> proj;
  for(const auto & itemL : output) for(const auto & itemR : output)
	  proj.add_element(QmtStateProjectorElement<State,Operator,S>(itemL,itemR,2.0));

  output.erase(output.begin());
  output.erase(output.end()-1);
  auto sign = 1.0;
  std::cout<<"|Psi0>: "<<std::endl;
  auto norm2 = 0.0;
  for (auto & item : output){
	item.setPhase(sign);
	norm2 += sign*sign;
        sign *= -1.17;	
  }
  for (auto & item : output){
	item.setPhase(item.getPhase()/sqrt(norm2));
  	std::cout<<item<<" ";
  }
  std::cout<<std::endl;

  std::cout<<"PG:"<<std::endl;
  std::cout<<proj<<std::endl;

  for(auto i=0; i<S/2; ++i) for(auto j=0; j<S/2; ++j){
    double valueUP  = 0.0;
    double valueDO  = 0.0;
    double valueUPu = 0.0;
    double valueDOu = 0.0;
    for(const auto & item : output){
        auto localStatesUP = proj(item);	
  	for(auto& state : localStatesUP) state = cUP(i)(aUP(j)(state));
  	localStatesUP = proj(localStatesUP);
	auto uncorrelUP = cUP(i)(aUP(j)(item));

        auto localStatesDO = proj(item);	
  	for(auto& state : localStatesDO) state = cDO(i)(aDO(j)(state));
  	localStatesDO = proj(localStatesDO);
	auto uncorrelDO = cDO(i)(aDO(j)(item));

	for(const auto & item2 : output){
   	  for(auto& state : localStatesUP)  valueUP += State::dot_product(item2,state);
   	  for(auto& state : localStatesDO)  valueDO += State::dot_product(item2,state);
	  valueUPu += State::dot_product(item2,uncorrelUP);
	  valueDOu += State::dot_product(item2,uncorrelDO);
	}
    }
    std::cout<<"<Phi0|PG cUP("<<i<<")aUP("<<j<<") PG|Phi0> = "<<valueUP<<std::endl;
    std::cout<<"<Phi0|PG cDO("<<i<<")aDO("<<j<<") PG|Phi0> = "<<valueDO<<std::endl;
    std::cout<<"<Phi0|   cUP("<<i<<")aUP("<<j<<")   |Phi0> = "<<valueUPu<<std::endl;
    std::cout<<"<Phi0|   cDO("<<i<<")aDO("<<j<<")   |Phi0> = "<<valueDOu<<std::endl;
    std::cout<<"<PG cUP("<<i<<")aUP("<<j<<") PG>/<cUP("<<i<<")aUP("<<j<<")> = "<<valueUP/valueUPu<<std::endl;
    std::cout<<"<PG cDO("<<i<<")aDO("<<j<<") PG>/<cDO("<<i<<")aDO("<<j<<")> = "<<valueDO/valueDOu<<std::endl;
  }

  
  return 0;
}
