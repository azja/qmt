#include "../../../headers/qmtenginedata.h"
#include "../../../headers/qmtmpisolver.h"
#include "../../../headers/qmtlncdiag.h"
#include "../../../headers/qmtsystemstandard.h"



struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  
};

qmt::QmtVector scaler(4.1,4.1,1.4);

void F(void* params) {
 DataForF *data = (DataForF*)(params);
 std::vector<double> arg({1});


 auto myf = (*data->f);
 double result = myf(arg,scaler,*(data->data),*(data->settings));
 std::cout<<"Result = "<<result<<std::endl;
}



int main() {

 MPI_Init(nullptr,nullptr);
 
 
 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat");
 qmt::QmtEngineData data;
//  qmt::QmtVector scaler(4.1,4.1,1.4);
//  system->set_parameters(std::vector<double>({1.0}),scaler);
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 
 
 Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
 

 
 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}