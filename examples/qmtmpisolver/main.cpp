#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtmpisolver.h"
#include<numeric> //accumulate
#include<functional>

struct QmtFakeSystem : public qmt::QmtSystem {

  const int N1;
  const int N2;

  QmtFakeSystem():N1(10),N2(12) {}
  
  int get_one_body_number() {
       return N1;
  }
  
  int get_two_body_number() {
       return N2;
  }
  
  double  get_one_body_integral(size_t index) {
    return static_cast<double>(index);
  }
  double  get_two_body_integral(size_t index) {
    return static_cast<double>(index);
  }
  
  void set_parameters(std::vector<double> params,const qmt::QmtVector& scale) {
  
    
  }
  
};


struct QmtFakeDiagonalizer: public qmt::QmtDiagonalizer{
  double Diagonalize(const std::vector<double>& one_body_integrals, const std::vector<double>& two_body_integrals) {

    auto x = std::accumulate(one_body_integrals.begin(),one_body_integrals.end(),0.0f,std::plus<double>());
    auto y = std::accumulate(two_body_integrals.begin(),two_body_integrals.end(),0.0f,std::plus<double>());
    return x * y;
  }
};

struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  
};


void F(void* params) {
 DataForF *data = (DataForF*)(params);
 std::vector<double> arg({1});
 qmt::QmtVector vec;

 auto myf = (*data->f);
 double result = myf(arg,vec,*(data->data),*(data->settings));
 std::cout<<"Result = "<<result<<std::endl;
}



int main() {

 MPI_Init(nullptr,nullptr);
 
 
 qmt::QmtSystem *system = new QmtFakeSystem(); 
 qmt::QmtDiagonalizer *diagonalizer = new QmtFakeDiagonalizer();
 qmt::QmtEngineData data;
 
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 
 
 Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
 

 
 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}