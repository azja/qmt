#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"



struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(4.1,4.1,4.1);

double alpha_global;
double energy_global;

double my_func(double x, void *p) {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg;

 arg.push_back(x); 

 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
}

void F(void* params) {




int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = 1.0;
  double a = 0.9,b = 1.1;
  gsl_function F;

  F.function = &my_func;
  F.params = params;

  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.00001, 0.0);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
/*
  m=0.2;
  while(m<2.0){
    std::cout<<m<<" "<<my_func(m,params)<<std::endl;
    m+=0.05;
  }
*/
  
//  m=1.0;
  std::vector<double> args;
  args.push_back(m); 

  energy_global = my_func(m,params);
  alpha_global = m;
}



int main(int argc,const char* argv[]) {

 MPI_Init(nullptr,nullptr);
 int rank;
 

//std::cout<<argc;
double xyz0 = 2.001;
std::string configure("conf.dat");
unsigned int max_step = 10;
double step_size = 0.1;
if(argc == 2){
 xyz0 = std::atof(argv[1]);
}
if(argc == 3){
 configure=std::string(argv[1]);
 xyz0 = std::atof(argv[2]);
}
if(argc == 5){
 configure=std::string(argv[1]);
 xyz0 = std::atof(argv[2]);
 max_step = std::atoi(argv[3]);
 step_size = std::atof(argv[4]);
// std::cerr<<configure<<" "<<xyz0<<" "<<max_step<<" "<<step_size<<std::endl;
}

 qmt::QmtSystem *system = new qmt::QmtSystemStandard(configure.c_str()); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer(configure.c_str());
 
// std::cout<<"done"<<std::endl;
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;

 MPI_Comm_rank (mpi_settings.communicator, &rank); 

 std::vector<double> avs;
 std::vector<int> map;

 std::ifstream map_file("megacell_orbital.dat");
 std::string map_line;
 std::getline(map_file,map_line);
 map_file.close();
 std::vector<std::string> map_string = qmt::parser::get_delimited_words(", ",map_line);

 for(const auto& index : map_string)
	map.push_back(std::stoi(index));



    if(rank == mpi_settings.root){
    std::cout<<"scale\tenergy (Ry)\talpha\td^2\t";
     for(int i=0; i<static_cast<qmt::QmtSystemStandard*>(system)->get_one_body_number(); ++i)
	std::cout<<"I1["<<i<<"]\t";
     for(int i=0; i<static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_number(); ++i)
	std::cout<<"I2["<<i<<"]\t";

std::cout<<std::endl;
}

 for(unsigned int i = 0; i < max_step;++i){
    scaler = qmt::QmtVector(xyz0 - i * step_size, xyz0 -  i* step_size,xyz0 - i*step_size);
    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
    if(rank == mpi_settings.root){
     std::cout<<scaler<<" "<<energy_global<<" "<<alpha_global<<" "<<static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer)->DoubleOccupancy()<<" ";

     for(int i=0; i<static_cast<qmt::QmtSystemStandard*>(system)->get_one_body_number(); ++i)
	std::cout<<static_cast<qmt::QmtSystemStandard*>(system)->get_one_body_integral(i)<<" ";
     for(int i=0; i<static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_number(); ++i)
	std::cout<<static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_integral(i)<<" ";
	
	std::cerr<<static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_number()<<std::endl;

std::cout<<std::endl;

/*     avs.clear();
     static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer)->NWaveFunctionEssentials(avs);
	for(int j=-20; j<=20; j++)
		for(int k=-20; k<=20; k++)
			for(int l=-20; l<=20; l++)
	     			std::cout<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(qmt::QmtVector(0.38*j,0.38*k,0.38*l),avs,map)<<std::endl;*/

/*

   int  get_one_body_number();
   int  get_two_body_number();
    
   double  get_one_body_integral(size_t index);
   double  get_two_body_integral(size_t index);
*/


    }
  }
 



 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
