#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtsystemstandard.h"
#include "../../headers/qmtmclanczos.h"
#include "../../headers/qmtsimulatedannealing.h"
#include "../../headers/qmtsimplex.h"
#include <bitset>



struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(4.1,4.1,4.1);

std::vector<double> alpha_global;
double energy_global;

double my_func(const std::vector<double> x, void *p) {
  DataForF *d = (DataForF*)(p);

 auto myf = (*d->f);
 return myf(x,*(d->scale),*(d->data),*(d->settings));
}

void F(void* params) {


//std::vector<double> lower({1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0});
//std::vector<double> upper({1.5,3.0,3.0,3.0,10.0,10.0,10.0,10.0});

std::vector<double> lower({1.0,1.0,1.0,1.0,1.0,1.0}); // HLi LiH
std::vector<double> upper({1.5,3.0,3.0,5.0,5.0,5.0});


//std::vector<double> lower({1.13,1.27,2.21,1.81});
//std::vector<double> upper({1.15,1.29,2.23,1.85});



std::vector<double> alphs;

alphs.push_back(1.19);
alphs.push_back(1.3);
alphs.push_back(1.3);
alphs.push_back(2.0);
alphs.push_back(2.0);
alphs.push_back(2.0);

auto F = [&](std::vector<double> v)->double {
  return (my_func(v,params)); 
};

std::cout<<"Minimizer: constructing... ";
//qmt::QmtSimulatedAnnealing solver(1,6,std::function<double(std::vector<double>)>(F),lower,upper,2.0,0.2);
qmt::QmtSimplex solver(1,6,std::function<double(std::vector<double>)>(F),0.8,5.0);
std::cout<<"done."<<std::endl;

double min_val;
std::vector<double> min_args;
solver.run(10,0.01,min_val,min_args);


  energy_global = my_func(min_args,params);
  alpha_global = min_args;
}



int main(int argc,const char* argv[]) {

 MPI_Init(nullptr,nullptr);
 int rank;
 

//std::cout<<argc;
double xyz0 = 17.90;
if(argc >1){
 xyz0 = std::atof(argv[1]);
}

std::cout<<"System: constructing... ";

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("config.cfg"); 
 
 std::cout<<"done."<<std::endl;

typedef qmt::QmtNState<std::bitset<20>, int> qmt_nstate;
typedef qmt::SqOperator<qmt_nstate> qmt_operator;
typedef qmt::QmtHubbard<qmt_operator> qmt_hamiltonian; 

std::cout<<"Diagonalizer: constructing... ";

 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtMCLanczosDiagonalizer<qmt_hamiltonian,qmt_nstate,20>("config.cfg",100000,100);
 
 std::cout<<"done."<<std::endl;
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;

 MPI_Comm_rank (mpi_settings.communicator, &rank); 

/* std::vector<double> avs;
 std::vector<int> map;

 std::ifstream map_file("megacell_orbital.dat");
 std::string map_line;
 std::getline(map_file,map_line);
 map_file.close();
 std::vector<std::string> map_string = qmt::parser::get_delimited_words(", ",map_line);

 for(const auto& index : map_string)
	map.push_back(std::stoi(index));

std::cout<<"scale\tenergy (Ry)\talpha\t";

     for(int i=0; i<static_cast<qmt::QmtSystemStandard*>(system)->get_one_body_number(); ++i)
	std::cout<<"I1["<<i<<"]\t";
     for(int i=0; i<static_cast<qmt::QmtSystemStandard*>(system)->get_two_body_number(); ++i)
	std::cout<<"I2["<<i<<"]\t";

std::cout<<std::endl;
*/

 for(int i = 0; i < 100;++i){
    scaler = qmt::QmtVector(1, 1,xyz0 - 0.05 *i);
    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
    if(rank == mpi_settings.root){
     std::cout<<scaler<<" "<<energy_global<<" "<<alpha_global[0]<<" "<<alpha_global[1]<<" "<<alpha_global[2]<<" "<<alpha_global[3]<<std::endl;

/*     avs.clear();
     static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer)->NWaveFunctionEssentials(avs);
	for(int j=-20; j<=20; j++)
		for(int k=-20; k<=20; k++)
			for(int l=-20; l<=20; l++)
	     			std::cout<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(qmt::QmtVector(0.38*j,0.38*k,0.38*l),avs,map)<<std::endl;*/

    }
  }
 
 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
