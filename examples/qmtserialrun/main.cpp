#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"






int main() {

 
 
 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat",1);

for(int k = 0; k < 100; k++) {
 std::vector<double> one_body;
 std::vector<double> two_body;

 system->set_parameters(std::vector<double>({1.05 + k* 0.01}),qmt::QmtVector(4.1,4.1,1.43));

 std::vector<int> indc({1,0});
 for(auto i = 0U; i < system->get_one_body_number();++i)
   one_body.push_back(system->get_one_body_integral(i));

 for(auto i = 0U; i < system->get_two_body_number();++i)
   two_body.push_back(system->get_two_body_integral(i));

int cntr = 0;
std::cout<< 1.05 + k *0.01<<" "<< diagonalizer->Diagonalize(one_body, two_body)/8<<std::endl;
}
std::vector<double> avrg; 
/*
static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer)->NWaveFunctionEssentials(avrg);
for(int i = 0; i < 20; ++i) {
  for(int j = 0; j < 20; ++j) {
     for(int k = 0; k < 20; ++k) {
    double x = k* 0.175 -1.75;
    double y = j* 0.175 -1.75;
    double z = i* 0.175 - 1;
    qmt::QmtVector r(x,y,z);
    double res = static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(r,avrg,indc);
//     if(fabs(res - 0.1) < 0.01 && cntr < 2){
//     cntr++;
       std::cout<<x<<" "<<y<<" "<<z<<" "<<res<<std::endl;
//     }
//     if(cntr >=2) break;
  }
//   if(cntr >=2)
   std::cout<<std::endl;
   cntr = 0;
 }

}*/
 return 0; 
}