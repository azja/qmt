#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"






int main() {

 
 
 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat",1);

 std::ifstream params("parameters.dat"); 
 std::string line;
 int cntr = 0;
 
 
   std::vector<int> map;
   std::ifstream map_file("megacell_orbital.dat");
   std::string map_line;
   std::getline(map_file,map_line);
   map_file.close();
   std::vector<std::string> map_string = qmt::parser::get_delimited_words(", ",map_line);

   for(const auto& index : map_string)
     map.push_back(std::stoi(index));

 
 while ( getline (params,line) ) {
 std::vector<double> one_body;
 std::vector<double> two_body;

 std::vector<std::string> ps = qmt::parser::get_delimited_words(" ",line);
  std::vector<double> pv;
  
 for(const auto& item : ps)
 pv.push_back(std::stod(item));
 
 double a = pv[0];
 double R = pv[1];
 double alpha = pv[2];
 for(int i = 3; i < 27; i++)
 two_body.push_back(pv[i]);
 
 system->set_parameters(std::vector<double>({alpha}),qmt::QmtVector(a,a,R));


 one_body.push_back(system->get_one_body_integral(0));
 for(int i = 27; i < pv.size();i++)
 one_body.push_back(pv[i]);
 

 std::vector<int> indc({1,0});
// for(auto i = 0U; i < system->get_one_body_number();++i)
//   one_body.push_back(system->get_one_body_integral(i));

// for(auto i = 0U; i < system->get_two_body_number();++i)
 // two_body.push_back(system->get_two_body_integral(i));

int cntr = 0;
std::cout<<"Energy = "<<" "<< diagonalizer->Diagonalize(one_body, two_body)/8<<std::endl;

std::vector<double> avrg; 

static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer)->NWaveFunctionEssentials(avrg);

  std::string density_fn = "density" + std::to_string(cntr) + ".dat";
 std::ofstream density_f(density_fn.c_str());

        for(int m=-60; m<=60; m++)
         for(int n=-60; n<=60; n++){
        for(int r= 0; r<=50; r++){
        density_f<<static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(qmt::QmtVector(0.08*m,0.08*n,-1.0 + 0.08*r),avrg,map)<<std::endl;
            }
        }
        density_f.close();
cntr++;
}


/*
for(int i = 0; i < 20; ++i) {
  for(int j = 0; j < 20; ++j) {
     for(int k = 0; k < 20; ++k) {
    double x = k* 0.175 -1.75;
    double y = j* 0.175 -1.75;
    double z = i* 0.175 - 1;
    qmt::QmtVector r(x,y,z);
    double res = static_cast<qmt::QmtSystemStandard*>(system)->get_wfs_products_sum(r,avrg,indc);
//     if(fabs(res - 0.1) < 0.01 && cntr < 2){
//     cntr++;
       std::cout<<x<<" "<<y<<" "<<z<<" "<<res<<std::endl;
//     }
//     if(cntr >=2) break;
  }
//   if(cntr >=2)
   std::cout<<std::endl;
   cntr = 0;
 }

}*/
 return 0; 
}