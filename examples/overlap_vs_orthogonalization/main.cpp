#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtmicroscopic.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"
#include "../../headers/qmtparsertools.h"

int main(){


typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 
typedef qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> qmt_microscopic;

	std::vector<double> alphas;
alphas.push_back(1.19);

   std::ifstream cfile;       
   cfile.open("conf.dat"); 
  std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
   std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
   std::string definitions_fn = qmt::parser::get_bracketed_words(content,"wfs_definitions","wfs_definitions_end")[0];
   std::string megacell_fn = qmt::parser::get_bracketed_words(content,"megacell","megacell_end")[0];;
   std::string supercell_fn =qmt::parser::get_bracketed_words(content,"supercell","supercell_end")[0];
  // unsigned int problem_size =std::stoi(qmt::parser::get_bracketed_words(content,"problem_size","problem_size_end")[0]);
//  unsigned int electron_number =std::stoi(qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0]);
//   unsigned int alphas_number  =std::stoi(qmt::parser::get_bracketed_words(content,"alphas_number","alphas_number_end")[0]);

    qmt_microscopic *microscopic1 = new  qmt_microscopic(hamiltonian_fn,"defs1.dat",supercell_fn, megacell_fn,alphas);
    qmt_microscopic *microscopic2 = new  qmt_microscopic(hamiltonian_fn,"defs2.dat",supercell_fn, megacell_fn,alphas);
    qmt_microscopic *microscopic3 = new  qmt_microscopic(hamiltonian_fn,"defs3.dat",supercell_fn, megacell_fn,alphas);
    qmt_microscopic *microscopic4 = new  qmt_microscopic(hamiltonian_fn,"defs4.dat",supercell_fn, megacell_fn,alphas);
    qmt_microscopic *microscopic5 = new  qmt_microscopic(hamiltonian_fn,"defs5.dat",supercell_fn, megacell_fn,alphas);

 //   qmt_hamiltonian* hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(hamiltonian_fn);

	std::cout<<std::endl<<"###############################################################"<<std::endl<<std::endl;
	std::cout<<"Micro 1"<<std::endl<<std::endl;
    microscopic1->set_parameters(alphas,qmt::QmtVector(1,1,1));
	std::cout<<"<w0|w0>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(0,0,0))<<std::endl;
	std::cout<<"<w0|w1>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(1,0,0))<<std::endl;
	std::cout<<"<w0|w2>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(2,0,0))<<std::endl;
	std::cout<<"<w0|w3>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(3,0,0))<<std::endl;
	std::cout<<"<w0|w4>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(4,0,0))<<std::endl;
	std::cout<<"<w0|w5>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(5,0,0))<<std::endl;
	std::cout<<"<w0|w6>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(6,0,0))<<std::endl;
	std::cout<<"<w0|w7>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(7,0,0))<<std::endl;
	std::cout<<"<w0|w8>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(8,0,0))<<std::endl;
	std::cout<<"<w0|w9>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(9,0,0))<<std::endl;
	std::cout<<"<w0|w10>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(10,0,0))<<std::endl;
	std::cout<<"<w0|w11>= "<<microscopic1->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(11,0,0))<<std::endl;

	std::cout<<std::endl<<"###############################################################"<<std::endl<<std::endl;
	std::cout<<"Micro 2"<<std::endl<<std::endl;
    microscopic2->set_parameters(alphas,qmt::QmtVector(1,1,1));
	std::cout<<"<w0|w0>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(0,0,0))<<std::endl;
	std::cout<<"<w0|w1>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(1,0,0))<<std::endl;
	std::cout<<"<w0|w2>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(2,0,0))<<std::endl;
	std::cout<<"<w0|w3>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(3,0,0))<<std::endl;
	std::cout<<"<w0|w4>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(4,0,0))<<std::endl;
	std::cout<<"<w0|w5>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(5,0,0))<<std::endl;
	std::cout<<"<w0|w6>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(6,0,0))<<std::endl;
	std::cout<<"<w0|w7>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(7,0,0))<<std::endl;
	std::cout<<"<w0|w8>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(8,0,0))<<std::endl;
	std::cout<<"<w0|w9>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(9,0,0))<<std::endl;
	std::cout<<"<w0|w10>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(10,0,0))<<std::endl;
	std::cout<<"<w0|w11>= "<<microscopic2->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(11,0,0))<<std::endl;

	std::cout<<std::endl<<"###############################################################"<<std::endl<<std::endl;
	std::cout<<"Micro 3"<<std::endl<<std::endl;
    microscopic3->set_parameters(alphas,qmt::QmtVector(1,1,1));
	std::cout<<"<w0|w0>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(0,0,0))<<std::endl;
	std::cout<<"<w0|w1>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(1,0,0))<<std::endl;
	std::cout<<"<w0|w2>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(2,0,0))<<std::endl;
	std::cout<<"<w0|w3>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(3,0,0))<<std::endl;
	std::cout<<"<w0|w4>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(4,0,0))<<std::endl;
	std::cout<<"<w0|w5>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(5,0,0))<<std::endl;
	std::cout<<"<w0|w6>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(6,0,0))<<std::endl;
	std::cout<<"<w0|w7>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(7,0,0))<<std::endl;
	std::cout<<"<w0|w8>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(8,0,0))<<std::endl;
	std::cout<<"<w0|w9>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(9,0,0))<<std::endl;
	std::cout<<"<w0|w10>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(10,0,0))<<std::endl;
	std::cout<<"<w0|w11>= "<<microscopic3->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(11,0,0))<<std::endl;

	std::cout<<std::endl<<"###############################################################"<<std::endl<<std::endl;
	std::cout<<"Micro 4"<<std::endl<<std::endl;
    microscopic4->set_parameters(alphas,qmt::QmtVector(1,1,1));
	std::cout<<"<w0|w0>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(0,0,0))<<std::endl;
	std::cout<<"<w0|w1>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(1,0,0))<<std::endl;
	std::cout<<"<w0|w2>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(2,0,0))<<std::endl;
	std::cout<<"<w0|w3>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(3,0,0))<<std::endl;
	std::cout<<"<w0|w4>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(4,0,0))<<std::endl;
	std::cout<<"<w0|w5>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(5,0,0))<<std::endl;
	std::cout<<"<w0|w6>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(6,0,0))<<std::endl;
	std::cout<<"<w0|w7>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(7,0,0))<<std::endl;
	std::cout<<"<w0|w8>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(8,0,0))<<std::endl;
	std::cout<<"<w0|w9>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(9,0,0))<<std::endl;
	std::cout<<"<w0|w10>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(10,0,0))<<std::endl;
	std::cout<<"<w0|w11>= "<<microscopic4->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(11,0,0))<<std::endl;

	std::cout<<std::endl<<"###############################################################"<<std::endl<<std::endl;
	std::cout<<"Micro 5"<<std::endl<<std::endl;
    microscopic5->set_parameters(alphas,qmt::QmtVector(1,1,1));
	std::cout<<"<w0|w0>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(0,0,0))<<std::endl;
	std::cout<<"<w0|w1>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(1,0,0))<<std::endl;
	std::cout<<"<w0|w2>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(2,0,0))<<std::endl;
	std::cout<<"<w0|w3>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(3,0,0))<<std::endl;
	std::cout<<"<w0|w4>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(4,0,0))<<std::endl;
	std::cout<<"<w0|w5>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(5,0,0))<<std::endl;
	std::cout<<"<w0|w6>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(6,0,0))<<std::endl;
	std::cout<<"<w0|w7>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(7,0,0))<<std::endl;
	std::cout<<"<w0|w8>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(8,0,0))<<std::endl;
	std::cout<<"<w0|w9>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(9,0,0))<<std::endl;
	std::cout<<"<w0|w10>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(10,0,0))<<std::endl;
	std::cout<<"<w0|w11>= "<<microscopic5->get_overlap(0,0,qmt::QmtVector(0,0,0),qmt::QmtVector(11,0,0))<<std::endl;
	return 0;
}
