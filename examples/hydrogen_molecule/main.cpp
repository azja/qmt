#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtmicroscopic.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>


#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include <stdio.h>



typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 

struct System {

qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> microscopic;

    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<qmt::QmtNState <qmt::uint, int> > states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula
         <qmt_hamiltonian,LocalAlgebras::SparseAlgebra> 
                                                                              h2_formula; //Tranlate Hamiltonian to matrix
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    unsigned int problem_size;
    unsigned int number_of_centers;							       //Number of atomic centers - spin obitals						
    std::vector<double> intgs;                                                                 //Integrals - microscopic parameters
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;                //Eigen problem solver
    typedef qmt::QmtNState<qmt::uint, int> NState;                                             //Output - eigen state
    int _lanczos_steps;
    int _lanczos_eps;
    double w_vs_u;



System(std::vector<double>& alphas):microscopic("ham2s.dat","defs2s.dat","supercell.dat", "megacell.dat",alphas),number_of_centers(4),
                                                                                          _lanczos_steps(6)  ,
									                  _lanczos_eps(1.0e-9)  {
   //uploading hamiltonian from the file
//std::cout<<"Hamiltonian reading"<<std::endl;
    hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian("ham2s.dat");
//std::cout<<"Hamiltonian read"<<std::endl;
    microscopic.set_parameters(alphas,qmt::QmtVector(1,1,1.0));
    //generating all posible electronic states
    generator.generate(2,number_of_centers*2,states);
    
    // preparing hamiltonian formula engine
    h2_formula.set_hamiltonian(*hamiltonian);
    
    h2_formula.set_States(states);
    
     for (auto state : states){
      std::cout<<state<<std::endl; 
     }

    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);

    LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size,_lanczos_steps, _lanczos_eps); 
 }

double get_energy() {
 std::vector<double> one_body;
// std::cout<<"Num of one body:"<<microscopic.number_of_one_body()<<std::endl;
 for(unsigned int i = 0; i < microscopic.number_of_one_body();++i) {
   
   auto integral = microscopic.get_one_body(i);

   h2_formula.set_microscopic_parameter(1,i,integral);
   one_body.push_back(integral);
  std::cout<<"I1#"<<i<<" = "<<integral<<std::endl;
 }
 
  std::vector<double> two_body;
// std::cout<<"Num of one body:"<<microscopic.number_of_one_body()<<"  Num of two body:"<<microscopic.number_of_two_body()<<std::endl;
 for(unsigned int i = 0; i < microscopic.number_of_two_body();++i) {
   auto integral = microscopic.get_two_body(i);
    if(i < 4)
    h2_formula.set_microscopic_parameter(2,i,integral);
   else if(i < 10)     h2_formula.set_microscopic_parameter(2,i, 0.5* integral);
   else     h2_formula.set_microscopic_parameter(2,i,0);
   two_body.push_back(integral);
//   std::cout<<"I2#"<<i<<" = "<<integral<<std::endl;
 }

h2_formula.get_Hamiltonian_Matrix(hamiltonian_M);
//std::cout<<*hamiltonian_M;
//LanczosSolver->solve(hamiltonian_M);
//double energy = LanczosSolver->get_minimal_eigenvalue()/2;
//std::cout<<"E = "<<energy<<std::endl;

int s = states.size();
                        gsl_vector* eigenvalues=gsl_vector_calloc(s);
			gsl_matrix* eigenvectors = gsl_matrix_calloc(s,s);

                        gsl_matrix* matrix = gsl_matrix_calloc(s,s);
			gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
					s);
for(int i = 0; i <s; ++i) {
  for(int j = 0; j <s; ++j)
    gsl_matrix_set(matrix,i,j,hamiltonian_M->get_value(i,j));
}
			
			gsl_eigen_symmv(matrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);
	
			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
double  energy = gsl_vector_get(eigenvalues,0);
	                gsl_matrix_free(matrix);
			gsl_matrix_free(eigenvectors);
			gsl_vector_free(eigenvalues);
			
  //std::cout<<"S size = "<<states.size()<<std::endl;
  
return  energy/2;


  
}

~System() {

    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	delete LanczosSolver;
}

void set(const std::vector<double> params) {
      microscopic.set_parameters(params,qmt::QmtVector(1,1,100000));
}

};

double my_f (const gsl_vector *xvec_ptr, void *params) {
 System* s = static_cast<System*>(params);
 std::vector<double> alphas;
 alphas.push_back(gsl_vector_get(xvec_ptr,0));
 alphas.push_back(gsl_vector_get(xvec_ptr,0)/2);
// alphas.push_back(gsl_vector_get(xvec_ptr,2));
// alphas.push_back(0.4287);
// alphas.push_back(0.5788);

//  alphas.push_back(gsl_vector_get(xvec_ptr,2));
  s->set(alphas);
  double energy =  s->get_energy() + 1.0/100000;
  
// if(alphas[0] < 0.35 || alphas[1] < 0.35 /*|| alphas[2] < 0.35*/) return -0.5;
//  if(fabs(energy) > 1.2) return 0;
  std::cout<<"E = "<<energy<<" "<<alphas[0]<<" "<<alphas[1]<<std::endl;
  return energy;
}



int main() {

//qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> microscopic("ham_px.dat","defs.dat","supercell.dat", "megacell.dat",1.19);
std::vector<double> alphs;

alphs.push_back(0.4287);
alphs.push_back(0.5788);
//alphs.push_back(0.5788);
//alphs.push_back(1.19/2);
//System h2_system(alphs);

System h2_system(alphs);



void*  sys = static_cast<void*>(&h2_system);

  const gsl_multimin_fminimizer_type *T = 
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  x = gsl_vector_alloc(2);
  gsl_vector_set (x, 0, 1);
  gsl_vector_set (x, 1, 0.5);
//  gsl_vector_set (x, 2, 0.5);

  ss = gsl_vector_alloc(2);
  gsl_vector_set_all (ss, 0.05);

  minex_func.n = 2;
  minex_func.f = my_f;
  minex_func.params = sys;

  s = gsl_multimin_fminimizer_alloc (T, 2);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      
      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);

      if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }

      printf ("%5d %10.3e %10.3e f() = %7.3f size = %.3f\n", 
              iter,
              gsl_vector_get (s->x, 0), 
              gsl_vector_get (s->x, 1), 
              s->fval, size);
    }
  while (status == GSL_CONTINUE && iter < 100);
  
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return status;









return 0;
}
