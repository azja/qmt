#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <sstream>
#include <algorithm>
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include "../../headers/qmtparsertools.h"

struct Data{
 qmt::QmtSystem *system;
 qmt::QmtDiagonalizer *diagonalizer;
 qmt::QmtVector *scale;
};

double energy(double x, void* p){

//return (x-1)*(x-1);

 std::vector<double> one_body;
 std::vector<double> two_body;

 Data *sol = (Data*) p;

 sol->system->set_parameters(std::vector<double>({x}),*(sol->scale));

 for(auto i = 0U; i < sol->system->get_one_body_number();++i)
   one_body.push_back(sol->system->get_one_body_integral(i));

 for(auto i = 0U; i < sol->system->get_two_body_number();++i)
   two_body.push_back(sol->system->get_two_body_integral(i));

return sol->diagonalizer->Diagonalize(one_body, two_body);
}

double minimize(void* solver, double& Zm, double& Z0, double& Zp, bool ALPHA_SCAN=false){

  int status;
  int iter = 0, max_iter = 25;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;

  gsl_function F;
  F.function = &energy;
  F.params = solver;

  double E;

  if(!ALPHA_SCAN){
    T = gsl_min_fminimizer_brent;
    s = gsl_min_fminimizer_alloc (T);
//    std::cout<<Zeta0<<",\t"<<ZetaMin<<",\t"<<ZetaMax<<std::endl;
//    std::cout<<energy(Zeta0,solver)<<",\t"<<energy(ZetaMin,solver)<<",\t"<<energy(ZetaMax,solver)<<std::endl;
//    std::cout<<"##################################################################"<<std::endl;

    bool IS_KILLED=false;

    double F0=energy(Z0, solver);
    double Fm=energy(Zm, solver);
    double Fp=energy(Zp, solver);
    unsigned int fuse = 0;
    while(F0>Fm || F0>Fp){
      if(Zm<0.5){
        std::cerr<<"Can't find a minimum at "<<Z0<<std::endl;
        IS_KILLED=true;
        break;
      }
      if(Fm<Fp){
  //   	std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
        Zp=Z0;
        Fp=F0;
        Z0=Zm;
        F0=F0;
        Zm-=2*(Zp-Z0);
        Fm=energy(Zm, solver);
  //    	std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
      }
      else{
  //    	std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
        Zm=Z0;
        Fm=F0;
        Z0=Zp;
        F0=Fp;
        Zp+=2*(Z0-Zm);
        Fp=energy(Zp, solver);
  //     	std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
      }
      fuse++;
      if(fuse>5){
        std::cerr<<"Can't find a minimum at"<<Z0<<std::endl;
        IS_KILLED=true;
	break;
      }
    }
    
    if(IS_KILLED){
      return F0;
    }

    gsl_min_fminimizer_set_with_values(s, &F, Z0, F0, Zm, Fm, Zp, Fp); 

    do
      {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        Z0 = gsl_min_fminimizer_x_minimum (s);
        Zm = gsl_min_fminimizer_x_lower (s);
        Zp = gsl_min_fminimizer_x_upper (s);

//	std::cout<<Z0<<" "<<Zm<<" "<<Zp<<std::endl;

        status = gsl_min_test_interval (Zm, Zp, 0.00001, 0.0);
      }
    while (status == GSL_CONTINUE && iter < max_iter);

    Z0 = gsl_min_fminimizer_x_minimum (s);
    Zm = gsl_min_fminimizer_x_lower (s);
    Zp = gsl_min_fminimizer_x_upper (s);
    E = gsl_min_fminimizer_f_minimum(s);

 //   std::cout<<Z0<<std::endl;
  }
  else{
    double a=Zm;
    E=1000000.0;
    while(a<Zp){
	double tE = energy(a,solver);
	if(tE<E) E=tE;
  	std::cout<<a<<" "<<tE<<std::endl;
  	a+=0.02;
    }
    gsl_min_fminimizer_free (s);
  }

  return E;
}


void setme (std::string& input, std::vector<double>& XY,
                                       std::vector<double>& Z,
                                    std::vector<double>& zeta,
                                  std::vector<double>& dzeta);

int main(int argc, const char* argv[]) {

 gsl_set_error_handler_off();

 std::string config("conf.dat");

 double Z0 = 1.0;
 double Zp = 2.0;
 double Zm = 0.6;

 double R0 = 1.4;

 std::vector<double> XY({4.0});
 std::vector<double> Z({1.4});
 std::vector<double> zeta({1.16});
 std::vector<double> dzeta({0.1});

 if(argc>1){
   std::ifstream file(argv[1]);
   std::string input((std::istreambuf_iterator<char>(file)),
                      std::istreambuf_iterator<char>());
	
   setme (input, XY, Z, zeta, dzeta);
 }

 if(argc>2)
  config=std::string(argv[2]);

std::cout.precision(17);
 for(unsigned int i=0; i<XY.size(); i++){
	Z0=zeta[i];
	Zm=zeta[i]-dzeta[i];
	Zp=zeta[i]+dzeta[i];

//	std::cout<<Z0<<" "<<Zm<<" "<<Zp<<" ";
//	exit(0);

	R0=Z[i];

	Data solver; 
	solver.system = new qmt::QmtSystemStandard(config.c_str()); 
	solver.diagonalizer = new qmt::QmtLanczosDiagonalizer(config.c_str(),1);

	solver.scale = new qmt::QmtVector(XY[i],XY[i],R0);

	auto energy = minimize((void*) &solver,Zm,Z0,Zp);

        std::cout<<XY[i]<<" "<<Z[i]<<" ";
	std::cout<<energy;
	std::cout<<" "<<Z0<<std::endl;
//	exit(0);
  }

 return 0; 
}

void setme (std::string& input, std::vector<double>& XY,
                                 std::vector<double>& Z,
                              std::vector<double>& zeta,
                             std::vector<double>& dzeta){
  XY.clear();
  Z.clear();
  zeta.clear();
  dzeta.clear();

  //remove comments
  std::size_t found = 0;
  while((found = input.find("#",found))!=std::string::npos){
        std::size_t f2 = input.find("\n",found);
	input.erase(found,f2-found+1); // +1 to erase "\n" as well!
  }

  std::istringstream iss(input);
  std::string line;
  std::string::size_type ptr;
  while (std::getline(iss, line)){
    std::vector<std::string> nums = qmt::parser::get_delimited_words(" ",line);

//    std::cout<<"line: "<<line<<std::endl;
    if(nums.size()>3){
      XY.push_back(std::stod(nums[0],&ptr));
      Z.push_back(std::stod(nums[1],&ptr));
      zeta.push_back(std::stod(nums[2],&ptr));
      dzeta.push_back(std::stod(nums[3],&ptr));
    }

  }
}

