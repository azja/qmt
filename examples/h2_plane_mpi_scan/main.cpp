#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"



struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(4.1,4.1,1.4);

double alpha_global;
double energy_global;

double alpha_start = 1.0;
double alpha_end = 1.8;
double alpha_medium = 1.1;

double func(double x, void* p) {
 DataForF *d = (DataForF*)(p);
 std::vector<double> arg({x});
 auto myf = (*d->f);
 double result = myf(arg,*(d->scale),*(d->data),*(d->settings));
// std::cout<<"Result:"<<result<<" alpha = "<<x<<std::endl;
 return result;
}



void F(void* params) {

/*
std::function<double(double,void*)>  func = [](double x, void *p) {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg({x});
 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
};
*/


int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = alpha_medium;
  double a = alpha_start,b = alpha_end;
  gsl_function F;

  typedef double function_t(double,void* );

  F.function = func;//.target<function_t>();
  F.params = params;


  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.001, 0.0);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
  
  std::vector<double> args;
  args.push_back(m);

  energy_global = func(m,params);
  alpha_global = m;
  energy_global = func(alpha_global,params);
}



int main(int argc,const char* argv[]) {

 MPI_Init(nullptr,nullptr);
 int rank;
 

double xy0 = 2.001;
double z0 = 1.43;
if(argc >2){
 xy0 = std::atof(argv[1]);
 z0 = std::atof(argv[2]);
}
 
if(argc >5){
 alpha_start = std::atof(argv[3]);
 alpha_medium = std::atof(argv[4]);
 alpha_end = std::atof(argv[5]);
}

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("conf.dat");
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 

 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;

 MPI_Comm_rank (mpi_settings.communicator, &rank); 

if(rank == mpi_settings.root) {
std::cout<<"alpha_start = "<<alpha_start<<std::endl;
std::cout<<"alpha_medium = "<<alpha_medium<<std::endl;
std::cout<<"alpha_end = "<<alpha_end<<std::endl;
}


 double t = 0;
 for(int i = 0; i < 1;++i){
   for(int j = 0; j < 100;j++) {
    scaler = qmt::QmtVector(xy0 + i * 0.025, xy0 +  i* 0.025,z0 + 0.025 * j);
//    scaler = qmt::QmtVector(xy0, xy0 ,z0);
//    alpha_global = 1.0 + j * 0.01;
    t =  MPI_Wtime();
    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
    if(rank == mpi_settings.root){
     std::cout<<scaler<<" "<<energy_global<<" "<<alpha_global<<" "<<MPI_Wtime() - t<<std::endl;
#ifdef COMPUTE_DENSITY 
       auto desnity_essentials = static_cast<QmtLanczosDiagonalizer*>(diagonalizer)->NWaveFunctionEssentials();
#endif
    }
   }
   if(rank == mpi_settings.root)
    std::cout<<std::endl;
  }
 

 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
