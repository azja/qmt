#include <mpi.h>
#include "../../headers/qmtenginedata.h"
#include "../../headers/qmtmpisolver.h"
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include<gsl/gsl_multimin.h>
#include<iostream>
#include<iomanip>


struct DataForF {
  QmtMPIEnergyFunction* f;
  qmt::QmtEngineData* data;
  Qmt_MPI* settings;
  qmt::QmtVector *scale;
  
};

qmt::QmtVector scaler(1.0,1.0,1.4);

std::vector<double> alpha_global;
double energy_global;


double my_func(const gsl_vector* x, void *p) {
  DataForF *d = (DataForF*)(p);
 std::vector<double> arg;

for(int i=0; i<4; i++)
	  arg.push_back(gsl_vector_get (x, i)); 

 auto myf = (*d->f);
 return myf(arg,*(d->scale),*(d->data),*(d->settings));
}


void F(void* params) {

 const gsl_multimin_fminimizer_type *T = 
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  /* Starting point */
  x = gsl_vector_alloc (4);
  gsl_vector_set (x, 0, 1.14);
  gsl_vector_set (x, 1, 1.265);
  gsl_vector_set (x, 2, 2.1);
  gsl_vector_set (x, 3, 1.78);

  ss = gsl_vector_alloc (4);
  gsl_vector_set_all (ss, 0.02);

typedef double function_t(const gsl_vector*,void* );
  /* Initialize method and iterate */
  minex_func.n = 4;
  minex_func.f = &my_func;
  minex_func.params = params;

  s = gsl_multimin_fminimizer_alloc (T, 4);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);


  do
    {
      iter++;
//std::cout<<"Tu4"<<std::endl;
      status = gsl_multimin_fminimizer_iterate(s);
      
//std::cout<<"Tu5"<<std::endl;
      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-9);

      if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }

      printf ("%5d %10.9e %10.9e %10.9e %10.9e f() = %7.9f size = %.9f\n", 
              iter,
              gsl_vector_get (s->x, 0), 
              gsl_vector_get (s->x, 1), 
              gsl_vector_get (s->x, 2), 
              gsl_vector_get (s->x, 3), 
              s->fval, size);
    }
  while (status == GSL_CONTINUE && iter < 100);

  std::vector<double> args;
	for(int i=0; i<4; i++)
	  args.push_back(gsl_vector_get (s->x, i)); 

  energy_global = s->fval;
  alpha_global = args;

  
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);
}



int main(int argc,const char* argv[]) {


 MPI_Init(nullptr,nullptr);
 int rank;
 


//std::cout<<argc;
double xy0 = 2.001;
double z0 = 1.43;
if(argc >2){
 xy0 = std::atof(argv[1]);
 z0 = std::atof(argv[2]);
}

 qmt::QmtSystem *system = new qmt::QmtSystemStandard("config.cfg"); 
 qmt::QmtDiagonalizer *diagonalizer = new qmt::QmtLanczosDiagonalizer("config.cfg",0);
 qmt::QmtEngineData data;
 data.microscopic = system;
 data.diagonalizer = diagonalizer;
 
 DataForF meta_data;
 auto mpi_function =  Qmt_Get_Predefined_MPI_Energy_Function(0);
 Qmt_MPI mpi_settings;
 
 meta_data.f = &mpi_function;
 meta_data.data = &data;
 meta_data.settings = &mpi_settings;
 meta_data.scale = &scaler;

 MPI_Comm_rank (mpi_settings.communicator, &rank); 


 for(int i = 0; i < 10;++i){
    scaler = qmt::QmtVector(xy0, xy0,z0 + 0.002 * i);

    Qmt_MPI_Engine(&meta_data,F,mpi_function,mpi_settings,data);
    if(rank == mpi_settings.root)
     std::cout<<std::setprecision(9)<<scaler<<" "<<energy_global<<" ";
     
     for(const auto& alpha : alpha_global)
        std::cout<<alpha<<" ";
        
    std::cout<<std::endl;
  }
 

 delete system;
 delete diagonalizer;

  MPI_Finalize();
 return 0; 
}
