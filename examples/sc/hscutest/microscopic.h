/*
 * microscopic.h
 *
 *  Created on: 15 sep 2014
 *      Author: andrzej kadzielawa
 */

#ifndef MICROSCOPIC_H_
#define MICROSCOPIC_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../../headers/qmtorbitals.h"
#include "../../headers/gaussexp.h"
#include <math.h>
#include "../../headers/qmtbform.h"
#include "../../headers/qmteqsolver.h"
#include "../../headers/qmtvector.h"
#include "../../headers/qmthbuilder.h"
#include "../../headers/qmthubbard.h"
#include <omp.h>
#include <stdlib.h>

namespace qmt {
namespace examples {

class MicroscopicH2Sc {

    int Num_Gaussians;// = 19;
    const int num_of_betas = 2;
    double alpha;
    std::vector<double> coefs;
    std::vector<double> gammas;

    double a;// = 4.0;
    double R;// = 1.43;
    double theta;// = pi/3
    double dx;
    double dy;
    double dz;



    std::vector<Gauss> atom0;
    std::vector<Gauss> atom1;
    std::vector<Gauss> atom2;
    std::vector<Gauss> atom3;
    std::vector<Gauss> atom4;
    std::vector<Gauss> atom5;
    std::vector<Gauss> atom6;
    std::vector<Gauss> atom7;



    typedef QmExpansion<Gauss, QmVariables::QmParallel::no> g_omp;
    typedef unsigned int uint;
    std::vector<g_omp>  slaters;

    typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;
    std::vector<qmt::QmtVectorizedAtom> megacell;
    std::vector<qmt::QmtVectorizedAtom> local_megacell;
    std::vector<qmt::QmtVectorizedAtom> supercell;
    std::vector<qmt::QmtVectorizedAtom> local_supercell;
    std::vector<qmt::one_body_integral_v> one_body_intgs;
    std::vector<qmt::two_body_integral_v> two_body_intgs;
    std::vector<qmt::one_body_integral_v> local_one_body_intgs;
    std::vector<qmt::two_body_integral_v> local_two_body_intgs;

    qmt::QmtVector reference_point;

    double** M;


    typedef QmExpansion < g_omp, QmVariables::QmParallel::no > w_omp;
    w_omp wannier;


    void calculate_betas(double a, double R, double alpha, std::vector<double>& betas, double** M);
    void prepare_wannier();

    qmt::QmtVectorizedAtom apply_basis(qmt::QmtVectorizedAtom in);

    MicroscopicH2Sc(const MicroscopicH2Sc&) = delete;
    MicroscopicH2Sc operator=(const MicroscopicH2Sc&) = delete;



public:
    MicroscopicH2Sc();
    ~MicroscopicH2Sc();

    double get_one_body(unsigned int index);
    double get_two_body(unsigned int index);
    double get_ion_repulsion();
    double get_1st_ion_repulsion();
    unsigned int number_of_one_body() const;
    unsigned int number_of_two_body() const;

    void set(double _a, double _alpha);
    void set_all(double _a, double _R, double _alpha, double _theta);
    void set_all(double _a, double _R, double _alpha, double _theta, unsigned int ng);
    void set_a(double _a);
    void set_R(double _R);
    void set_alpha(double _alpha);
    void set_theta(double _theta);
    void set_ng(unsigned int n);
    double get_alpha() {
        return alpha;
    }
    void set_dx(double x);
    void set_dy(double y);
    void set_dz(double z);
    void set_dxdy(double x, double y);
    void set_dxdydz(double x, double y, double z);
<<<<<<< HEAD

    double get_overlap() {
        return w_omp::overlap(wannier, wannier, qmt::QmtVector(0,0,0),qmt::QmtVector(a,0,0));
    }

=======
    
     double get_overlap(){
             return w_omp::overlap(wannier, wannier, qmt::QmtVector(0,0,0),qmt::QmtVector(a,0,0));
    }
                 
>>>>>>> 27cb4c601b7726e20a3c1b462f380a959f700c57
};


}
}



#endif /* MICROSCOPIC_H_ */
