#include "../../headers/qmtmpipool.h"
#include <iostream>
#include <fstream> 
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "qmtscsgasolverstable.h"

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {
   qmt::QmtVector translation;
    virtual double integral(qmt::examples::MicroscopicSC &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

   
    TwoBody(const qmt::QmtVector t)
         { translation = t; }

    double integral(qmt::examples::MicroscopicSC &model) {
        return model.get_two_body_integral(translation);
    }

};

struct OneBody:public Body {

	
    OneBody(const qmt::QmtVector t) { translation = t;}

    double integral(qmt::examples::MicroscopicSC &model) {
        return model.get_one_body_integral(translation);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    double _a;


    std::vector< Body*> bodies;
    double *integrals;
    
    qmt::examples::MicroscopicSC model;

   public:
    SimplePoolJob(double a, unsigned int size): _a(a) {
        model.set(_a, 1);

        
        typedef qmt::QmtVector vec3d;

        bodies.push_back(new OneBody(vec3d(0,0,0)));
        bodies.push_back(new OneBody(vec3d(a,0,0)));
        bodies.push_back(new TwoBody(vec3d(0,0,0))); //U

       for(int i = 0; i < model.number_of_pairs_size(); ++i)
         bodies.push_back(new TwoBody(model.get_pair_vector(i)));

        integrals = new double[size];    
        

    }
    
    ~SimplePoolJob() {
     for(int i = 0; i < bodies.size(); ++i)
	delete bodies[i];	
    delete [] integrals;
    }


    void set_a(double a) {
        _a = a;


        model.set_a(a);
    }
qmt::examples::MicroscopicSC* get_model()  {return &model;}

   private:
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_alpha(arg);
    
        result = bodies [id]->integral(model);
        return result;
   }
};


/*
 * Diagonalization stage - now performed on the single core
 */

class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {
QmtSC_SGA_Solver_Stable *solverD;
QmtSC_SGA_Solver_Stable *solver0;
s_SGA_out solutionD;
s_SGA_out solution0;
qmt::examples::MicroscopicSC* model;

public:
SimpleInternalJob(){
	solver0 = new QmtSC_SGA_Solver_Stable(true);
	solverD = new QmtSC_SGA_Solver_Stable;

	solver0->set_beta(1e+7); //3 mK
	solverD->set_beta(1e+7);
	solutionD = {0,0,0,0,0,0};
	solution0 = {0,0,0,0,0,0};
	model = NULL;
}

~SimpleInternalJob(){
	delete solver0;
	delete solverD;
}

s_SGA_out get_mean_fields(double d){
	if(fabs(d) < 1e-15)	return solution0;
	else				return solutionD;
}

double get_double_occ(){
	return solution0.EG > solutionD.EG ? solutionD.d :solution0.d;
}

void set_model(qmt::examples::MicroscopicSC* m) { model = m;}
private:
 double Calculate(double* arg,int size,void * params) {
        std::vector<double> intgs(&arg[0], &arg[0] + size);

    	solver0->set_all(intgs[1],intgs[2],0.0);
    	solverD->set_all(intgs[1],intgs[2],0.0);

    	solution0 = solver0->solve();
    	solutionD = solverD->solve();

 	double energy = solution0.EG > solutionD.EG ? solutionD.EG :solution0.EG;

	for(int i =0; i<3; i++)
	std::cout<<"I"<<i<<"= "<<intgs[i]<<" ";

	std::cout<<"E0= "<<solution0.EG<<" Ed= "<<solutionD.EG<<std::endl;

    	//Kij
    	double K_sum = 0;
    	for(unsigned int i = 3; i < intgs.size(); ++i ){
  	 K_sum += 0.5 * (2.0/qmt::QmtVector::norm(model->get_pair_vector(i-3)) + intgs[i] )* model->number_of_pairs(i-3) ;
	}

	
    	energy += intgs[0] + K_sum;///model->get_system_size() ;

   return energy;
 }
};

/*
 * Minimization - final result
 */

class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
 double alfa;
 double minimal_alfa;
 double minimal_f;
 double Calculate(void * params) {


   int status;
    int iter = 0, max_iter = 100;
    const gsl_min_fminimizer_type *T;
    gsl_min_fminimizer *s;
    gsl_function F;

    F.function = FinalProcessJob<double>::OutFunc;
    F.params = params;

    double m = 1.0, m_expected = M_PI;
    double a = 0.5, b = 2.0;

  
     T = gsl_min_fminimizer_brent;
//      T = gsl_min_fminimizer_goldensection;
    s = gsl_min_fminimizer_alloc (T);
   int  status_p = gsl_min_fminimizer_set (s, &F, m, a, b);

   if( status_p != GSL_EINVAL) {
    do
    {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        m = gsl_min_fminimizer_x_minimum (s);
        a = gsl_min_fminimizer_x_lower (s);
        b = gsl_min_fminimizer_x_upper (s);
        status
            = gsl_min_test_interval (a, b, 0.0000001, 0.0);
 
    minimal_f = gsl_min_fminimizer_f_minimum(s);
    minimal_alfa = gsl_min_fminimizer_x_minimum(s);
    }
    while (status == GSL_CONTINUE && iter < max_iter);
   }
    
    gsl_min_fminimizer_free (s);
 
  return minimal_alfa;
 }
 
 public:
  void set_alfa(double a) {
   alfa = a;
  }
  
  double get_min_alfa() const { return minimal_alfa;}
  double get_min_f() const {return minimal_f;}
};





/////////////////////////////Diff////////////////////////////////////////////

class DiffPoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {
 
    typedef qmt::QmtVector vec3d;
    double _a;
    double _alfa;
    unsigned int _size;
    vec3d _displace;
    double _displace_norm;

    std::vector<std::pair<int,double>> x_integrals;
    std::vector<std::pair<int,double>> y_integrals;
    std::vector<std::pair<int,double>> z_integrals;

    std::vector< Body*> bodies;
 
    
    qmt::examples::MicroscopicSC model;

   public:
    DiffPoolJob(double a, double alfa, unsigned int size,double displace): _a(a), _alfa(alfa), _size(size),_displace(qmt::QmtVector(displace,displace, displace)),_displace_norm(fabs(displace))   { /*displace is positive*/
        model.set(_a, _alfa);

      
        bodies.push_back(new OneBody(vec3d(0,0,0)));
        bodies.push_back(new OneBody(vec3d(a,0,0)));
        bodies.push_back(new TwoBody(vec3d(0,0,0))); //U

       for(int i = 1; i < model.get_system_size(); ++i)
         bodies.push_back(new TwoBody(model.get_system_vector(i)));

       
        

    }
    
    ~DiffPoolJob() {
     for(int i = 0; i < bodies.size(); ++i)
	delete bodies[i];	
     }

  void show_integrals() const {
    for(int i = 0; i < x_integrals.size(); ++i) {
           std::cout<<_a<<" I#"<<std::get<0>(x_integrals[i])<< " "<<std::get<1>(x_integrals[i])<<" "<<std::get<1>(y_integrals[i])<<" ";
           std::cout<<std::get<1>(z_integrals[i])<<" "<<bodies[std::get<0>(x_integrals[i])]->translation<<std::endl;

    }
  }


  qmt::examples::MicroscopicSC* get_model()  {return &model;}

   private:

    double Calculate(double arg,int id,void * params)  {
      
      double result = 0.0;
      
      auto getElectro = [](const qmt::QmtVector& v, double t)->double {return 2 *t/pow(qmt::QmtVector::norm(v),3);};
      auto sgn = [](double x)->double{ return copysign(1,x); };
	int Num_of_Intgs = bodies.size();
     
    if( id > 2) 
       { 

       if(id < (_size + 5)/3 ) {
       double integral_x = 0;
	int id0 = 3;
       for(unsigned int i = id-id0+3; i < Num_of_Intgs ; i=i+(_size-4)/3) {
          model.displace_center(qmt::QmtVector(_displace.get_x(),0,0));
          double integral_x = bodies[i]->integral(model);
          model.displace_center(qmt::QmtVector( -_displace.get_x(),0,0));
          integral_x -= bodies[i]->integral(model);
          result  += (integral_x/(2 * _displace_norm) +getElectro(bodies[i]->translation,bodies[i]->translation.get_x())) * sgn(bodies[i]->translation.get_x());
         }
       return 0.5 * result;
       }

      
      if( id >= (_size + 5)/3 && id < (2*_size +1)/3 ) {
       double integral_y = 0;
	int id0 =  (_size + 5)/3;
       for(unsigned int i = id-id0+3; i < Num_of_Intgs ; i=i+(_size-4)/3) {
          model.displace_center(qmt::QmtVector(0,_displace.get_y(),0));
          double integral_y = bodies[i]->integral(model);
          model.displace_center(qmt::QmtVector(0,-_displace.get_y(),0));
          integral_y -= bodies[i]->integral(model);
          result  += (integral_y/(2 * _displace_norm) + getElectro(bodies[i]->translation,bodies[i]->translation.get_y())) * sgn(bodies[i]->translation.get_y());
        }
       return 0.5 * result;
      }

      if( id >= (2*_size +1)/3  && id < _size -1 ) {
      double integral_z = 0;
	int id0 = (2*_size +1)/3;
       for(unsigned int i = id-id0+3; i < Num_of_Intgs ; i=i+(_size-4)/3) {
          model.displace_center(qmt::QmtVector(0, 0,_displace.get_z()));
          double integral_z = bodies[i]->integral(model);
          model.displace_center(qmt::QmtVector(0, 0, -_displace.get_z()));
          integral_z -= bodies[i]->integral(model);
          z_integrals.push_back(std::make_pair(i,integral_z/(2 * _displace_norm)));
          result  += (integral_z/(2 * _displace_norm) + getElectro(bodies[i]->translation,bodies[i]->translation.get_z())) * sgn(bodies[i]->translation.get_z());
        }
       return 0.5 * result;
       }
      }

      else {
       return 0;
      }

        
   }
};


/*
 * Nothing to do here
 */

class EmptyInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {
double x;
double y;
double z;
public:
private:
 double Calculate(double* arg,int size,void * params) {
  double* array = static_cast<double*>(arg);

 int offset =  (size-3)/3;
   
  x = std::accumulate(array + 3, array + 3 + offset, 0.0);
  y = std::accumulate(array + 3 + offset, array + 3 + 2 * offset, 0.0);
  z = std::accumulate(array + 3 + 2 * offset,array + 3 + 3 * offset, 0.0);

 return *arg;
 }

public:
double get_x() const {return x;}
double get_y() const {return y;}
double get_z() const {return z;}


};


/*
 * Nothing to do here
 */

class EmptyFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
 double Calculate(void * params) {
    return FinalProcessJob<double>::OutFunc(1.0, params);
  }
};



/////////////////////////////////////////////////////////////////////////////




int main() {


MPI_Init(NULL, NULL);

   int taskid;
   int nproc;
   MPI_Comm comm;
   MPI_Comm_dup( MPI_COMM_WORLD, &comm);
   double t_start;// = MPI_Wtime();
   MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
   MPI_Comm_size(MPI_COMM_WORLD,&nproc);


   double a;

       if(taskid == nproc-1) {
          std::fstream fs;
          fs.open ("energy.dat", std::fstream::in | std::fstream::out | std::fstream::app);    
          fs<<"latt_const	E0	d^2	alfa	def_x   def_y   def_z   elapsed_time"<<std::endl;
          fs.close();
        }

 //   for(int i = 0; i < 50; ++i) {
 //      a = 4.0 + i * 0.01;
	for(int i=0; i<30; ++i){
	a = 3.6 + i * 0.01;
        t_start = MPI_Wtime();
        double result = 0;
        qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;
	qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
	qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a,nproc - 1);

	static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());


	qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer(comm, finalJob, internalJob, processJob);

	result = gatherer.run();
        double alfa = static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa();


        MPI_Bcast(&alfa, 1, MPI_DOUBLE, nproc - 1, 
               MPI_COMM_WORLD);

	if(taskid == nproc-1) {
          std::fstream fs;
          fs.open ("energy.dat", std::fstream::in | std::fstream::out | std::fstream::app);    
          double sd = static_cast<SimpleInternalJob*>(internalJob)->get_double_occ();
	  fs<<a<<" "<<static_cast<SimpleFinalJob*>(finalJob)->get_min_f()<<" "<<sd * sd;
          fs<<" "<<  static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa()<<" ";
          fs.close();
        }

        qmt::FinalProcessJob<double, MPI_DOUBLE>* diffFinalJob = new ::EmptyFinalJob;
	qmt::InternalProcessJob<double, MPI_DOUBLE>* diffInternalJob = new ::EmptyInternalJob;
	qmt::PoolProcessJob<double, MPI_DOUBLE>* diffProcessJob = new ::DiffPoolJob(a, alfa, nproc , 0.001);
 
       qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> differ(comm, diffFinalJob, diffInternalJob, diffProcessJob);
       result +=differ.run();
       EmptyInternalJob *show_p =static_cast< EmptyInternalJob* >(diffInternalJob);
             
       if(taskid == nproc - 1) {
         std::fstream fs;

         fs.open ("energy.dat", std::fstream::in | std::fstream::out | std::fstream::app);    
         fs<<" "<<show_p->get_x()<<" "<<show_p->get_y()<<" "<<show_p->get_z()<<" "<<MPI_Wtime() - t_start<<std::endl;
	 fs.close();
        }



      delete finalJob;
      delete internalJob;
      delete processJob;

      delete diffFinalJob;
      delete diffInternalJob;
      delete diffProcessJob;

 }
   MPI_Finalize();


return 0;
}
