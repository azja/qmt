#include "qmtscphononfrequency.h"

qmt::QmtPhononFreq::QmtPhononFreq() {
	num_of_dimentions = 3;
	DynamicalMatrix = gsl_matrix_alloc(num_of_dimentions, num_of_dimentions);
	IF_DynamicalMatrix = true;
	Frequencies = gsl_vector_complex_alloc(num_of_dimentions);
	IF_Frequencies = true;
	GslWorkspace = gsl_eigen_nonsymmv_alloc(num_of_dimentions);
	IF_GslWorkspace = true;
	PolarizationVectors = gsl_matrix_complex_alloc(num_of_dimentions,
			num_of_dimentions);
	IF_PolarizationVectors = true;
}

qmt::QmtPhononFreq::QmtPhononFreq(unsigned int D) {
	num_of_dimentions = D;
	DynamicalMatrix = gsl_matrix_alloc(num_of_dimentions, num_of_dimentions);
	IF_DynamicalMatrix = true;
	Frequencies = gsl_vector_complex_alloc(num_of_dimentions);
	IF_Frequencies = true;
	GslWorkspace = gsl_eigen_nonsymmv_alloc(num_of_dimentions);
	IF_GslWorkspace = true;
	PolarizationVectors = gsl_matrix_complex_alloc(num_of_dimentions,
			num_of_dimentions);
	IF_PolarizationVectors = true;
}

qmt::QmtPhononFreq::~QmtPhononFreq() {
	if (IF_DynamicalMatrix)
		gsl_matrix_free(DynamicalMatrix);
	if (IF_Frequencies)
		gsl_vector_complex_free(Frequencies);
	if (IF_GslWorkspace)
		gsl_eigen_nonsymmv_free(GslWorkspace);
	if (IF_PolarizationVectors)
		gsl_matrix_complex_free(PolarizationVectors);
}

void qmt::QmtPhononFreq::FindFrequencies(gsl_matrix* EigenMatrix,gsl_vector_complex* Frequencies, gsl_matrix_complex* Polarizations) {
	set_Matrix(EigenMatrix);
	solve();
//	printFreqs();
	gsl_vector_complex_memcpy(Frequencies,this->Frequencies);
	gsl_matrix_complex_memcpy(Polarizations,PolarizationVectors);
}

void qmt::QmtPhononFreq::FindFrequencies(gsl_vector_complex* Frequencies, gsl_matrix_complex* Polarizations) {
	solve();
//	printFreqs();
	gsl_vector_complex_memcpy(Frequencies,this->Frequencies);
	gsl_matrix_complex_memcpy(Polarizations,PolarizationVectors);
}

void qmt::QmtPhononFreq::solve(){
	gsl_eigen_nonsymmv(DynamicalMatrix, Frequencies, PolarizationVectors,
			GslWorkspace);
	for (int i = 0; i < num_of_dimentions; i++)
			gsl_vector_complex_set(Frequencies,i,gsl_complex_sqrt(gsl_vector_complex_get(Frequencies,i)));

}

void qmt::QmtPhononFreq::printFreqs(){
	for (int i = 0; i < num_of_dimentions; i++) {
		std::cout<< "omega_0 = "
				<<std::setw(9)<< GSL_REAL(gsl_vector_complex_get(Frequencies, i)) << " + "
				<<std::setw(9)<< GSL_IMAG(gsl_vector_complex_get(Frequencies, i)) <<"i, polarization: (";

		for(int j=0; j<num_of_dimentions; j++){
			std::cout
			<<std::setw(9)<< GSL_REAL(gsl_matrix_complex_get(PolarizationVectors, i,j)) << " + "
			<<std::setw(9)<< GSL_IMAG(gsl_matrix_complex_get(PolarizationVectors, i,j)) <<"i";
			if(j+1 != num_of_dimentions)
				std::cout<<", ";
		}
			std::cout<<")"<< std::endl;
	}

}

void qmt::QmtPhononFreq::set_Matrix(gsl_matrix* EigenMatrix) {
		if (EigenMatrix->size1 == num_of_dimentions
				&& EigenMatrix->size2 == num_of_dimentions) {
			DynamicalMatrix->data = EigenMatrix->data;
			DynamicalMatrix->block = EigenMatrix->block;
			EigenMatrix->owner=0;
//		print();
		} else {
				std::cerr << "QmtPhononFreq::set_pointer_Matrix: matrix given is not "
						<< num_of_dimentions << "x" << num_of_dimentions
						<< "!" << std::endl;
			}
}

void qmt::QmtPhononFreq::print() {
	for (int i = 0; i < DynamicalMatrix->size1; i++) {
		for (int j = 0; j < DynamicalMatrix->size2; j++)
			std::cout << gsl_matrix_get(DynamicalMatrix, i, j) << " ";
		std::cout << std::endl;
	}
}
