/*
 * qmtbasisgenerator.h
 *
 *  Created on: 8 sep 2014
 *      Author: andrzej kądzielawa
 */

#ifndef QMTPHONONFREQ_H_
#define QMTPHONONFREQ_H_

#include <iostream>
#include <iomanip>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

namespace qmt{

class QmtPhononFreq{
private:
	int num_of_dimentions;
	gsl_matrix* DynamicalMatrix;
	bool IF_DynamicalMatrix;
	gsl_vector_complex* Frequencies;
	bool IF_Frequencies;
	gsl_matrix_complex* PolarizationVectors;
	bool IF_PolarizationVectors;
	gsl_eigen_nonsymmv_workspace* GslWorkspace;
	bool IF_GslWorkspace;

public:
	QmtPhononFreq();
	QmtPhononFreq(unsigned int D);
	~QmtPhononFreq();

private:
	void print();
	void solve();

public:
	void FindFrequencies(gsl_matrix* EigenMatrix, gsl_vector_complex* Frequencies, gsl_matrix_complex* Polarizations);
	void FindFrequencies(gsl_vector_complex* Frequencies, gsl_matrix_complex* Polarizations);
	void set_Matrix(gsl_matrix* EigenMatrix);
	void printFreqs();
};

} //end of namespace qmt

#endif
