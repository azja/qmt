/*
 * qmtscsgasolverstable.h
 *
 *  Created on: 27 sie 2014
 *      Author: kasia
 */

#ifndef QMTSCSGASOLVERSTABLE_H_
#define QMTSCSGASOLVERSTABLE_H_

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <cmath>

//#define const

#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
//#include <gsl/gsl_spline.h>

//#define PI 3.141592653589793

struct s_SGA_out { //a container allowing to get all of important outcome at once
    double d;
    double m;
    double lmb_m;
    double mu;
    double lmb_n;
    double EG;
};

class QmtSC_SGA_Solver_Stable {
public:
    QmtSC_SGA_Solver_Stable(double _beta, double _t, double _U, double _R, double _h, FILE *_error_file,FILE *_exit_file, bool _DCONST);
    QmtSC_SGA_Solver_Stable(double _beta, double _t, double _U, bool _DCONST);
    QmtSC_SGA_Solver_Stable(bool _DCONST=false);
    ~QmtSC_SGA_Solver_Stable();

    typedef int (QmtSC_SGA_Solver_Stable::*model)(const gsl_vector * x, void * params, gsl_vector * f);
    typedef int (*model2)(const gsl_vector * x, void * params, gsl_vector * f);

    model wsk;

    int to_opt (const gsl_vector * x, void *params, gsl_vector * f);
    double get(char c);
    void set(char c,double value);
    void set_all(double _t, double _U, double _h);
    void set_beta(double _beta);

    void print();
    double get_energy();

    s_SGA_out end();

    s_SGA_out solve(double test_d, 	double test_m, double test_lmb_m, double test_mu, double test_lmb_n);
    s_SGA_out solve(				double test_m, double test_lmb_m, double test_mu, double test_lmb_n);
    s_SGA_out solve();

    double ESG;	//the value of Energy for d,lam_m,lam_n;
    int iferr;	//error handling;
    double d;	//d - double occupancy - parameters of SGA model;
    double lmb_m, m; //m - magnetisation, lmb_m - Lagrange multiplier for magnetisation
    double lmb_n, n; //lmb_n - Lagrange multiplier for occupancy op.
    double pU, lmb_pU; //auxiliary parameter pU (pU^2 = nU - d^2), and respective lmb_pU
    double pD, lmb_pD; //auxiliary parameter pD (pD^2 = nD - d^2), and respective lmb_pD
    bool DCONST;

private:
    /********* Constructor ***********/
    void QmtSC_SGA_Solver_Stable_Constructor(double _beta, double _t, double _U, double _R, double _h, FILE *_error_file,FILE *_exit_file, bool _DCONST);


    /*********** Constants ***********/
    int L;
    int L3;

    /*********** Variables ***********/
    double t,U;	//t - hopping integral in Hubbard Model, U - interaction between electrons in one knot;
    double beta,R;	//beta = 1/(kT), R - SC latitude;
    double mu; 		//mu - chemical potential
    double h; // h - magnetic field in Ry units
    double e_k,EnD,EnU;	//e_k - dispersion relation for SCC, EnD/U - energy for pol. down/up;
    double FDSU, FDSD;			//Fermi-Dirac Statistic factor in equations for pol. up/down;
    double LogEn;		// Logarithm inside free energy expression;
    double d_d,d_m,d_lmb_m,d_n,d_lmb_n;	// d_* partial derivative of the Functional in respect to *
    double d_pU, d_pD, d_lmb_pU, d_lmb_pD;	// d_* partial derivative of the Functional in respect to *
    double F_SGA;		// free energy for SGA
    double g_up, g_down, g_up_d_d, g_down_d_d, g_up_d_n, g_down_d_n; // Gutzwiller coefficient and its derivatives
    double g_up_d_m, g_down_d_m, g_up_d_pU, g_down_d_pU, g_up_d_pD, g_down_d_pD; // Gutzwiller coefficient and its derivatives

    FILE *err,*ext;	//files pointers;

    bool CZY;
    bool Checker;
    int Checher_which_approach;


    /*********** Functions ***********/
    void epsilon(double kx, double ky, double kz); // calc. dispersion relation for inverse space vector k

    void energy_s(double *p);	// calc. energy for polarization Up or Down;
    double GutzFactor(double s); // calc. Gutzwiller Factor for double occupancy d and magnetisation m;
    void FerDirStat(double *p);	//calc. FDSU/D;

    void stepset(double mul);	//calc. step of calculating function to minimalise
    inline double der_m(double s);
    inline double der_d(double s);
    inline double der_n(double s);
    inline double der_pU(double s);
    inline double der_pD(double s);

    void energy();				//calc. the energy for d and l_n given
    void energy_step(double mul);	// calc. step of calculating the energy given on one knot
    void epsset(int x, int y, int z);	// setting value of dispersion relation

    void innerlogarithm(); // calc. log factor in free energy expression
    void derivatives(); //calc. partial derivatives of Functional in respect to d, lam_m, lam_n

};

class rparams {
public:
    rparams(double t,double beta,double U,double R,double h)
    {
        a = t;
        b = beta;
        c = U;
        d = R;
        e = h;
        pnt=NULL;
    };
    double a,b,c,d,e;
    QmtSC_SGA_Solver_Stable *pnt;
};

int optme (const gsl_vector*,void*,gsl_vector*);
#endif /* QMTSCSGASOLVERSTABLE_H_ */
