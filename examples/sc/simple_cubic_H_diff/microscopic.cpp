/*
microoscopic.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

#include "microscopic.h"

void qmt::examples::MicroscopicSC::sc_betas(double a, double alpha,
        std::vector<double>& betas, double** M) {


    std::vector<Gauss> batom1;
    std::vector<Gauss> batom2;
    std::vector<Gauss> batom3;
    std::vector<Gauss> batom4;
    std::vector<Gauss> batom5;
    std::vector<Gauss> batom6;
    std::vector<Gauss> batom7;
    std::vector<Gauss> batom8;
    std::vector<Gauss> batom9;
    std::vector<Gauss> batom10;
    std::vector<Gauss> batom11;
    std::vector<Gauss> batom12;




    for(int i=0; i < N; ++i) {
        batom1.push_back(Gauss(0,0,0,alpha*gammas[i]));
        batom2.push_back(Gauss(0,a,0,alpha*gammas[i]));
        batom3.push_back(Gauss(0,-a,0,alpha*gammas[i]));
        batom4.push_back(Gauss(-a,0,0,alpha*gammas[i]));
        batom5.push_back(Gauss(a,0,0,alpha*gammas[i]));
        batom6.push_back(Gauss(0,0,a,alpha*gammas[i]));
        batom7.push_back(Gauss(0,0,-a,alpha*gammas[i]));
        batom8.push_back(Gauss(a,a,0,alpha*gammas[i]));
        batom9.push_back(Gauss(a,-a,0,alpha*gammas[i]));
        batom10.push_back(Gauss(a,0,a,alpha*gammas[i]));
        batom11.push_back(Gauss(a,0,-a,alpha*gammas[i]));
        batom12.push_back(Gauss(2 * a,0,0,alpha*gammas[i]));

    }

    std::vector<g_omp>  bslaters;

    bslaters.push_back(g_omp(0,0,0,0));
    bslaters.push_back(g_omp(0,a,0,1));
    bslaters.push_back(g_omp(0,-a,0,2));
    bslaters.push_back(g_omp(-a,0,0,3));
    bslaters.push_back(g_omp(a,0,0,4));
    bslaters.push_back(g_omp(0,0,a,5));
    bslaters.push_back(g_omp(0,0,-a,6));


    bslaters.push_back(g_omp(a,a,0,7));
    bslaters.push_back(g_omp(a,-a,0,8));
    bslaters.push_back(g_omp(a,0,a,9));
    bslaters.push_back(g_omp(a,0,-a,10));
    bslaters.push_back(g_omp(2*a,0,0,11));


    for(int i=0; i < N; ++i) {
        bslaters[0].add_element(i, coefs[i], batom1[i]);
        bslaters[1].add_element(i, coefs[i], batom2[i]);
        bslaters[2].add_element(i, coefs[i], batom3[i]);
        bslaters[3].add_element(i, coefs[i], batom4[i]);
        bslaters[4].add_element(i, coefs[i], batom5[i]);
        bslaters[5].add_element(i, coefs[i], batom6[i]);
        bslaters[6].add_element(i, coefs[i], batom7[i]);
        bslaters[7].add_element(i, coefs[i], batom8[i]);
        bslaters[8].add_element(i, coefs[i], batom9[i]);
        bslaters[9].add_element(i, coefs[i], batom10[i]);
        bslaters[10].add_element(i, coefs[i],batom11[i]);
        bslaters[11].add_element(i, coefs[i],batom12[i]);

    }

    Wannier<g_omp> wannier1;
    Wannier<g_omp> wannier2;


    wannier1.addOrbital(bslaters[0],0);
    wannier1.addOrbital(bslaters[1],1);
    wannier1.addOrbital(bslaters[2],1);
    wannier1.addOrbital(bslaters[3],1);
    wannier1.addOrbital(bslaters[4],1);
    wannier1.addOrbital(bslaters[5],1);
    wannier1.addOrbital(bslaters[6],1);


    wannier2.addOrbital(bslaters[4],0);
    wannier2.addOrbital(bslaters[7],1);
    wannier2.addOrbital(bslaters[8],1);
    wannier2.addOrbital(bslaters[9],1);
    wannier2.addOrbital(bslaters[10],1);
    wannier2.addOrbital(bslaters[11],1);
    wannier2.addOrbital(bslaters[0],1);


<<<<<<< HEAD:examples/sc/simple_cubic_H_diff/microscopic.cpp
    std::vector<BilinearForm> forms;
=======
   std::vector<BilinearForm> forms;
>>>>>>> 27cb4c601b7726e20a3c1b462f380a959f700c57:examples/sc/simple_cubic_H_diff/microscopic.cpp

    Wannier<g_omp>::multiply(wannier1,wannier2,M,2);
    forms.push_back(BilinearForm(M,2));



    Wannier<g_omp>::multiply(wannier1,wannier1,M,2);
    forms.push_back(BilinearForm(M,2,-1));

////////////////////////////////////////////////////////////////////

//    double in[2] = { 1.058, 0.08 };
    double in[2] = { 1.0, 0.};
    double out[2] = {1.04, -0.068 };

<<<<<<< HEAD:examples/sc/simple_cubic_H_diff/microscopic.cpp
    NonLinearSystemSolve(forms,in,out,1.0e-12);
=======
   NonLinearSystemSolve(forms,in,out,1.0e-12);
>>>>>>> 27cb4c601b7726e20a3c1b462f380a959f700c57:examples/sc/simple_cubic_H_diff/microscopic.cpp
    std::cout<<"alpha = "<<alpha<<" a = "<<a<<std::endl;
    std::cout<<"beta0 = "<<out[0]<<" beta[1] = "<<out[1]<<std::endl;
    betas.clear();
    betas.push_back(out[0]);
    betas.push_back(out[1]);



}

qmt::examples::MicroscopicSC::MicroscopicSC():N(7),alfa(1.084),a(4.0),wannier(0,0,0,0) {
    M = new double* [2];
    for(int i = 0; i < 2; ++i)
        M[i] = new double [2];

    qmt::qmGtoNSlater1s(2.0,2.0 , 1.0e-8, 0.001, 0.001, coefs, gammas, N);
    set(a,alfa);

}

qmt::examples::MicroscopicSC::~MicroscopicSC() {
    for(int i = 0; i < 2; ++i)
        delete [] M[i];
    delete [] M;
}

void qmt::examples::MicroscopicSC::set(double new_a, double new_alfa) {
    a = new_a;

    alfa = new_alfa;

    atom1.clear();
    atom2.clear();
    atom3.clear();
    atom4.clear();
    atom5.clear();
    atom6.clear();
    atom7.clear();



    for(int i=0; i < N; ++i) {
        atom1.push_back(Gauss(0,0,0,alfa*gammas[i]));
        atom2.push_back(Gauss(0,a,0,alfa*gammas[i]));
        atom3.push_back(Gauss(0,-a,0,alfa*gammas[i]));
        atom4.push_back(Gauss(-a,0,0,alfa*gammas[i]));
        atom5.push_back(Gauss(a,0,0,alfa*gammas[i]));
        atom6.push_back(Gauss(0,0,a,alfa*gammas[i]));
        atom7.push_back(Gauss(0,0,-a,alfa*gammas[i]));

    }


    slaters.clear();



    slaters.push_back(g_omp(0,0,0,0));
    slaters.push_back(g_omp(0,a,0,1));
    slaters.push_back(g_omp(0,-a,0,2));
    slaters.push_back(g_omp(-a,0,0,3));
    slaters.push_back(g_omp(a,0,0,4));
    slaters.push_back(g_omp(0,0,a,5));
    slaters.push_back(g_omp(0,0,-a,6));





    for(int i=0; i < N; ++i) {
        slaters[0].add_element(i, coefs[i], atom1[i]);
        slaters[1].add_element(i, coefs[i], atom2[i]);
        slaters[2].add_element(i, coefs[i], atom3[i]);
        slaters[3].add_element(i, coefs[i], atom4[i]);
        slaters[4].add_element(i, coefs[i], atom5[i]);
        slaters[5].add_element(i, coefs[i], atom6[i]);
        slaters[6].add_element(i, coefs[i], atom7[i]);
    }




    std::vector<double> betas;

    sc_betas(a, alfa, betas, M);



    wannier.clear_elements();

    wannier.add_element(0, betas[0], slaters[0]);
    wannier.add_element(1, betas[1], slaters[1]);
    wannier.add_element(2, betas[1], slaters[2]);
    wannier.add_element(3, betas[1], slaters[3]);
    wannier.add_element(4, betas[1], slaters[4]);
    wannier.add_element(5, betas[1], slaters[5]);
    wannier.add_element(6, betas[1], slaters[6]);


    system.clear();
    system_t.clear();


    system = coord.get1zone(5,a);
    system_t = coord.get2zone(5,a,qmt::QmtVector(a,0,0));

    std::vector<std::tuple<double,int,qmt::QmtVector>> temp_pairs;
    pair_numbers.clear();
    pair_vectors.clear();
    coord.get_coordinated_zones(temp_pairs);

    for(int i=1; i < temp_pairs.size(); ++i) {
        pair_numbers.push_back(std::get<1>(temp_pairs[i]));
        pair_vectors.push_back(std::get<2>(temp_pairs[i]));
    }


    typedef qmt::QmtVector vec3d;


}


void  qmt::examples::MicroscopicSC::set_a(double new_a) {
    set(new_a,  alfa);
}



void  qmt::examples::MicroscopicSC::set_alpha(double new_alfa) {
    set(a,  new_alfa);
}




double qmt::examples::MicroscopicSC::get_one_body_integral(const qmt::QmtVector& translation) {
    typedef w_omp expansion;
    double result = 0;
    std::vector<qmt::QmtVector> *neigs;

    neigs = &system_t;

    if(qmt::QmtVector::norm(translation) != 0) {
        result += expansion::kinetic_integral(wannier,wannier, system_t[0], translation);
        for(unsigned int k = 0; k < neigs->size(); ++k) {
            result +=  expansion::attractive_integral(wannier, wannier, system_t[0], translation,
                       (*neigs)[k].get_x(), (*neigs)[k].get_y(), (*neigs)[k].get_z());
        }
    }

    else {
        result += expansion::kinetic_integral(wannier,wannier, system_t[0], system_t[0]);
        for(unsigned int k = 0; k < neigs->size(); ++k) {
            result +=  expansion::attractive_integral(wannier, wannier, system_t[0], system_t[0],
                       (*neigs)[k].get_x(), (*neigs)[k].get_y(), (*neigs)[k].get_z());
        }
    }


    return result;
}



double qmt::examples::MicroscopicSC::get_two_body_integral(const qmt::QmtVector& translation) {
    typedef w_omp expansion;
    if(qmt::QmtVector::norm(translation) != 0)
        return    expansion::v_integral(wannier, wannier, wannier, wannier, system_t[0], translation, system_t[0], translation );
    else
        return    expansion::v_integral(wannier, wannier, wannier, wannier, system_t[0], system_t[0], system_t[0], system_t[0] );
}

double qmt::examples::MicroscopicSC::get_one_body_integral_diff(uint i,uint j, const qmt::QmtVector& delta) {
    typedef w_omp expansion;
    qmt::QmtVector zero(0,0,0);
    std::vector<qmt::QmtVector> *neigs;

    if(i == j) neigs = &system;
    else       neigs = &system_t;


    double result = 0;
    result += expansion::kinetic_integral(wannier,wannier, system[i] + delta,system[j]);
    result += expansion::kinetic_integral(wannier,wannier, system[i] - delta,system[j]);
    for(int k = 0; k < neigs->size(); ++k) {
        result +=  expansion::attractive_integral(wannier, wannier, delta, zero,(*neigs)[k].get_x(), (*neigs)[k].get_y(), (*neigs)[k].get_z());
        result +=  expansion::attractive_integral(wannier, wannier, zero - delta, zero, (*neigs)[k].get_x(), (*neigs)[k].get_y(), (*neigs)[k].get_z());
    }
    return result/qmt::QmtVector::norm(2 * delta);
}

double qmt::examples::MicroscopicSC::get_two_body_integral_diff(uint i,uint j,const qmt::QmtVector& delta) {
    typedef w_omp expansion;

    double dI = expansion::v_integral(wannier, wannier, wannier, wannier, system[i] + delta , system[j], system[i] + delta, system[j]);
    dI += expansion::v_integral(wannier, wannier, wannier, wannier,system[i] - delta, system[j], system[i] - delta, system[j]);
    return  dI/qmt::QmtVector::norm(2 * delta);

}

unsigned int qmt::examples::MicroscopicSC::number_of_pairs(unsigned int zone) {
    return pair_numbers[zone];
}

const qmt::QmtVector& qmt::examples::MicroscopicSC::get_pair_vector(unsigned int id) {
    return pair_vectors[id];
}

unsigned int qmt::examples::MicroscopicSC::number_of_pairs_size() {
    return pair_numbers.size();
}

unsigned int qmt::examples::MicroscopicSC::get_system_size() {
    return system_t.size();
}

const qmt::QmtVector& qmt::examples::MicroscopicSC::get_system_vector(unsigned int id) const {
    return system_t[id];
}

void qmt::examples::MicroscopicSC::displace_center(const qmt::QmtVector& displacement) {
    if(system_t.size()>0) {
        system_t[0]=displacement;
        // wannier.set_r(displacement.get_x(), displacement.get_y(), displacement.get_z());
    }
}
