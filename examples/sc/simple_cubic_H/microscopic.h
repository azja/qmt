/*
 * microscopic.h
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */

#ifndef MICROSCOPIC_H_
#define MICROSCOPIC_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../../headers/qmtorbitals.h"
#include "../../headers/gaussexp.h"
#include <math.h>
#include "../../headers/qmtbform.h"
#include "../../headers/qmteqsolver.h"
#include "qmtsccord.h"
//#include "../../headers/qmtvars.h"
#include <omp.h>
#include <stdlib.h>

namespace qmt {
namespace examples {

class MicroscopicSC {

    int N;// = 19;
    double alfa;// = 1.19378;
    std::vector<double> coefs;
    std::vector<double> gammas;

    qmt::examples::QmtCoordination coord;
    std::vector<unsigned int> pair_numbers;
    std::vector<qmt::QmtVector> pair_vectors;

    std::vector<Gauss> atom1;
    std::vector<Gauss> atom2;
    std::vector<Gauss> atom3;
    std::vector<Gauss> atom4;
    std::vector<Gauss> atom5;
    std::vector<Gauss> atom6;
    std::vector<Gauss> atom7;



    double a;// = 1.0;


    typedef QmExpansion<Gauss, QmVariables::QmParallel::no> g_omp;
    typedef unsigned int uint;
    std::vector<g_omp>  slaters;


    double** M;


    typedef QmExpansion < g_omp, QmVariables::QmParallel::omp > w_omp;
    w_omp wannier;

    std::vector<qmt::QmtVector> system;
    std::vector<qmt::QmtVector> system_t;
    void  sc_betas(double a, double alpha, std::vector<double>& betas, double** M);

    MicroscopicSC(const MicroscopicSC&) = delete;
    MicroscopicSC operator=(const MicroscopicSC&) = delete;



public:
    MicroscopicSC();
    ~MicroscopicSC();

    void set(double a, double alpha);
    void set_a(double a);
    void set_alpha(double alpha);
    double get_alpha() {
        return alfa;
    }




    double get_one_body_integral(const qmt::QmtVector& translation);
    double get_two_body_integral(const qmt::QmtVector& translation);

    double get_one_body_integral_diff(uint i,uint j, const qmt::QmtVector& delta);
    double get_two_body_integral_diff(uint i,uint j, const  qmt::QmtVector& delta);

    unsigned int number_of_pairs(unsigned int zone);
    const qmt::QmtVector& get_pair_vector(unsigned int id);
    unsigned int number_of_pairs_size();
    unsigned int get_system_size();

};


}
}



#endif /* MICROSCOPIC_H_ */
