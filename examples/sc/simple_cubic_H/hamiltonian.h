/*
 * hamiltonian.h
 *
 *  Created on: 1 lip 2014
 *      Author: abiborski
 */

#ifndef HAMILTONIAN_H_
#define HAMILTONIAN_H_


#include <iostream>
#include "../../headers/qmtstate.h"
#define GSL_LIBRARY
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtsgen.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_eigen.h>
#include "../../headers/qmtlanczos.h"
#include "../../headers/qmtgsltools.h"
#include "../../headers/qmtbasisgenerator.h"
#include "microscopic.h"


namespace qmt {
namespace examples {
class Hamiltonian2xH2 {
    typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;

    qmt::QmtHubbard<sq_operator> h4_hamiltonian;

    enum OneBodyHubbard {
        eps, // 00, 11, 22, 33
        t12, // 01, 23
        t13, // 02, 13
        t14  // 03, 12
    };

    enum TwoBodyHubbard {
        U,   // 0000, 1111, 2222, 3333
        K12, // 0011, 2233
        K13, // 0022, 1133
        K14, // 0033, 1122
        V12, // 0011, 2233
        V13, // 0022, 1133
        V14, // 0033, 1122
        J12, // 0011, 2233
        J13, // 0022, 1133
        J14, // 0033, 1122
        P12, // 0011, 2233
        P13, // 0022, 1133
        P14  // 0033, 1122
    };

    qmt::QmtMatrixFormula<qmt::QmtHubbard<sq_operator>> *h4_formula;

    std::vector<qmt::QmtNState<qmt::uint, int> > states;
    qmt::QmtBasisGenerator generator;

    int problem_size;
    gsl_matrix *hamiltonian_M;

public:

    Hamiltonian2xH2();
    ~Hamiltonian2xH2();

    Hamiltonian2xH2(const Hamiltonian2xH2 & ) = delete;
    Hamiltonian2xH2 operator=(const Hamiltonian2xH2 & ) = delete;

    void set_integrals(const std::vector<double> integrals);
    gsl_matrix* get_matrix();

};

}
}



#endif /* HAMILTONIAN_H_ */
