#include <iostream>
#include <math.h>

class Slater1S {

    double _x;
    double _y;
    double _z;

    double _ri;
    double _alpha;
public:

    Slater1S(double x, double y, double z, double alpha ):_x(x), _y(y), _z(z), _alpha(alpha) {
        _ri = sqrt(_x*_x + _y*_y + _z*_z);
    }


    double distance(const Slater1S& slater) const {

        double dx = _x - slater._x;
        double dy = _y - slater._y;
        double dz = _z - slater._z;

        return sqrt(dx*dx + dy*dy + dz*dz);
    }

    static double overlap(const Slater1S& slater1, const Slater1S& slater2)  {
        double d = slater1.distance(slater2);
        double alpha = slater1._alpha;
        return exp(-alpha * d) * (1 + alpha * d + alpha*alpha *d *d /3);
    }

};


double Af(double a, double alpha) {
    Slater1S s0(0,0,0,alpha);
    Slater1S s2(sqrt(2)* a,0,0,alpha);
    Slater1S s4(2 * a,0,0,alpha);

    return 1 + 4 * Slater1S::overlap(s0,s2) + Slater1S::overlap(s0,s4);
}

double Bf(double a, double alpha) {
    Slater1S s0(0,0,0,alpha);
    Slater1S s3(sqrt(3) * a,0,0,alpha);
    Slater1S s5(sqrt(5) * a,0,0,alpha);
    Slater1S s8(3.0 * a,0,0,alpha);

    return 15 + 8 * Slater1S::overlap(s0,s3) + 12 * Slater1S::overlap(s0,s5) + Slater1S::overlap(s0,s8);
}

double S1f(double a, double alpha) {
    Slater1S s0(0,0,0,alpha);
    Slater1S s1( a,0,0,alpha);
    return Slater1S::overlap(s0,s1);
}

double beta(double a, double alpha) {

    double A = Af(a,alpha);
    double B = Bf(a,alpha);
    double S1 = S1f(a, alpha);

    double nom = A + sqrt(6*A*A - B * S1);
    double den = sqrt(2*A*A - B * S1 - 6 * A * S1*S1 + 2 *(A-6*S1*S1)*sqrt(6*A*A-B*S1));
    std::cout<<" A = "<<A<<" S = "<<S1<<" B ="<<B<<" A*A - B * S1 ="<<6*A*A -B * S1<< std::endl;
    return nom/den;
}

int main() {

    std::cout<<"beta = "<<beta(1.0 ,1.087)<<std::endl;

    return 0;
}


