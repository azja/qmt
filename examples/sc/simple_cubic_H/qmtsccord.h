#ifndef QMTCOORDINATIONGENERATOR_H_
#define QMTCOORDINATIONGENERATOR_H_

#include <stdio.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include "../../headers/qmtvector.h"


namespace qmt{
namespace examples {
class QmtCoordination{
	std::vector<qmt::QmtVector> vectors1zone;
	std::vector<qmt::QmtVector> vectors2zones;
	int CoordNumber;
	int MaxLinearPossiton;
	double latticeParam;


	static bool vectorComperator(const qmt::QmtVector& vec1, const qmt::QmtVector& vec2){
		return qmt::QmtVector::sq_norm(vec1) < qmt::QmtVector::sq_norm(vec2);
	}

	static bool distanceComperator(const std::tuple<double,int,qmt::QmtVector>& pair1, const std::tuple<double,int,qmt::QmtVector>& pair2){
		return std::get<0>(pair1) < std::get<0>(pair2);
	}

public:
	QmtCoordination(){
		CoordNumber = 5;
		MaxLinearPossiton = (CoordNumber);
		latticeParam=1.0;
	}

	QmtCoordination(int CoordinationNumber){
		CoordNumber = CoordinationNumber;
		MaxLinearPossiton = (CoordNumber);
		latticeParam=1.0;
	}

	~QmtCoordination(){
		vectors1zone.clear();
		vectors2zones.clear();
	}


/*	qmt::QmtVector get (int index){
		if(index>1 && index < MaxNeighbor+1)
			return *(uniqueDistances[index-1]);

		std::cerr<<"Wrong index!\n";
		return qmt::QmtVector();
	}*/

	void set_max_distance(int newdistance){
		CoordNumber = newdistance;
		MaxLinearPossiton = (CoordNumber);
	}

	void set_lattice_param(double param){
		latticeParam=param;
	}

	std::vector<qmt::QmtVector> get1zone(){
		run();
		return vectors1zone;
	}

	std::vector<qmt::QmtVector> get1zone(int newdistance){
		set_max_distance(newdistance);
		return get1zone();
	}

	std::vector<qmt::QmtVector> get1zone(double param){
		set_lattice_param(param);
		return get1zone();
	}

	std::vector<qmt::QmtVector> get1zone(int newdistance, double param){
		set_lattice_param(param);
		set_max_distance(newdistance);
		return get1zone();
	}

	std::vector<qmt::QmtVector> get2zone(qmt::QmtVector translation){
		run();
		addNewZone(translation);
		return vectors2zones;
	}

	std::vector<qmt::QmtVector> get2zone(int newdistance, qmt::QmtVector translation){
		set_max_distance(newdistance);
		get2zone(translation);
		return vectors2zones;
	}

	std::vector<qmt::QmtVector> get2zone(double param, qmt::QmtVector translation){
		set_lattice_param(param);
		get2zone(translation);
		return vectors2zones;
	}

	std::vector<qmt::QmtVector> get2zone(int newdistance, double param, qmt::QmtVector translation){
		set_lattice_param(param);
		set_max_distance(newdistance);
		get2zone(translation);
		return vectors2zones;
	}

	void get_coordinated_pairs(std::vector<std::tuple<double,int,qmt::QmtVector>>& uniquePairs){
		uniquePairs.clear();
		generate_pairs(uniquePairs);
	}

	void get_coordinated_zones(std::vector<std::tuple<double,int,qmt::QmtVector>>& uniqueSingles){
		uniqueSingles.clear();
		generate_singles(uniqueSingles);
	}



private:

	void run(){
		vectors1zone.clear();

		for(int i = -MaxLinearPossiton; i<=MaxLinearPossiton; i++)
			for(int j = -MaxLinearPossiton; j<=MaxLinearPossiton; j++)
				for(int k = -MaxLinearPossiton; k<=MaxLinearPossiton; k++){
					qmt::QmtVector tmp_vector(i*latticeParam,j*latticeParam,k*latticeParam);
					//std::cout<<tmp_vector<<std::endl;
					if(qmt::QmtVector::sq_norm(tmp_vector)<pow(latticeParam*0.5*(MaxLinearPossiton+sqrt(pow(MaxLinearPossiton,2)+1)),2)) vectors1zone.push_back(tmp_vector);
				}

		std::sort(vectors1zone.begin(), vectors1zone.end(), vectorComperator);

/*				for(std::vector<qmt::QmtVector>::iterator iter=vectors1zone.begin(); iter!=vectors1zone.end(); iter++){
					std::cout<<*iter<<" "<<qmt::QmtVector::norm(*iter)<<std::endl;
				}*/

	}

	void addNewZone(qmt::QmtVector translation){
		vectors2zones.clear();
		std::vector<qmt::QmtVector>::iterator iter;

		vectors2zones = vectors1zone;


		for(iter=vectors1zone.begin(); iter!=vectors1zone.end(); iter++){
			vectors2zones.push_back(translation+*iter);
		}


		for(iter=vectors2zones.begin(); iter!=vectors2zones.end(); iter++){
			for(std::vector<qmt::QmtVector>::iterator iter2=iter+1; iter2!=vectors2zones.end(); iter2++)
					if((*iter)==(*iter2)){
						//std::cout<<*iter<<" "<<*iter2<<std::endl;
						vectors2zones.erase(iter2);
						iter2--; // erasing element of vector moves all consecutive ones
					}
		}


		std::sort(vectors2zones.begin(), vectors2zones.end(), vectorComperator);

/*				for(iter=vectors2zones.begin(); iter!=vectors2zones.end(); iter++){
					std::cout<<*iter<<" "<<qmt::QmtVector::norm(*iter)<<std::endl;
				}

				std::cout<<std::endl;*/

	}

	void generate_singles(std::vector<std::tuple<double,int,qmt::QmtVector>>& uniqueSingles) {
		for (std::vector<qmt::QmtVector>::iterator iter = vectors2zones.begin();
						iter != vectors2zones.end(); iter++) {
					double distance = qmt::QmtVector::norm((*iter));
					double eps = 1e-14; // double accuracy
					bool IFNOTFOUND=true;

						for (std::vector<std::tuple<double, int,qmt::QmtVector>>::iterator iter3 =
								uniqueSingles.begin(); iter3 != uniqueSingles.end();
								iter3++) {
							if(fabs(std::get<0>(*iter3)-distance) < eps){
								std::get<1>(*iter3)++;
								IFNOTFOUND=false;
							}
						}
						if(IFNOTFOUND){
							uniqueSingles.push_back(std::make_tuple(distance,1,(*iter)));
						}


				}

				std::sort(uniqueSingles.begin(),uniqueSingles.end(),distanceComperator);


	}


	void generate_pairs(std::vector<std::tuple<double,int,qmt::QmtVector>>& uniquePairs) {


/*		for (std::vector<qmt::QmtVector>::iterator iter = vectors1zone.begin();
				iter != vectors1zone.end(); iter++) {
			for (std::vector<qmt::QmtVector>::iterator iter2 = iter + 1;
					iter2 != vectors1zone.end(); iter2++) {
				double distance = qmt::QmtVector::norm((*iter) - (*iter2));
				bool IFNOTFOUND=true;

				for (std::vector<std::tuple<double, int,qmt::QmtVector>>::iterator iter3 =
						uniquePairs.begin(); iter3 != uniquePairs.end();
						iter3++) {
					if(fabs(std::get<0>(*iter3)-distance) < 1e-14){
						std::get<1>(*iter3)++;
						IFNOTFOUND=false;
					}
				}

				if(IFNOTFOUND){
					uniquePairs.push_back(std::make_tuple(distance,1,(*iter) - (*iter2)));
				}
			}*/

		for (std::vector<qmt::QmtVector>::iterator iter = vectors2zones.begin();
				iter != vectors2zones.end(); iter++) {
			for (std::vector<qmt::QmtVector>::iterator iter2 = iter + 1;
					iter2 != vectors2zones.end(); iter2++) {
				double distance = qmt::QmtVector::norm((*iter) - (*iter2));
				bool IFNOTFOUND=true;

				for (std::vector<std::tuple<double, int,qmt::QmtVector>>::iterator iter3 =
						uniquePairs.begin(); iter3 != uniquePairs.end();
						iter3++) {
					if(fabs(std::get<0>(*iter3)-distance) < 1e-14){
						std::get<1>(*iter3)++;
						IFNOTFOUND=false;
					}
				}

				if(IFNOTFOUND){
					uniquePairs.push_back(std::make_tuple(distance,1,(*iter) - (*iter2)));
				}
			}

		}


		std::sort(uniquePairs.begin(),uniquePairs.end(),distanceComperator);
/*
		int how_many=0;
		for (std::vector<std::pair<double, int>>::iterator iter3 =
								uniquePairs.begin(); iter3 != uniquePairs.end();
								iter3++) {
			std::cout<<std::get<0>(*iter3)<<" "<<std::get<1>(*iter3)<<std::endl;
			how_many+=std::get<1>(*iter3);
		}
		std::cout<<how_many<<" "<<vectors1zone.size()*(vectors1zone.size()+1)/2;*/
	}


};
} //end of namespace examples
} //end of namespace qmt

#endif
