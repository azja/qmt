#include "../../headers/qmtmpipool.h"
#include <iostream>
#include "microscopic.h"
#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "qmtscsgasolverstable.h"

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {

    virtual double integral(qmt::examples::MicroscopicSC &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

    qmt::QmtVector translation;

    TwoBody(const qmt::QmtVector t):
        translation(t) {}

    double integral(qmt::examples::MicroscopicSC &model) {
        return model.get_two_body_integral(translation);
    }

};

struct OneBody:public Body {

	qmt::QmtVector translation;
    OneBody(const qmt::QmtVector t): translation(t) {}

    double integral(qmt::examples::MicroscopicSC &model) {
        return model.get_one_body_integral(translation);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    double _a;


    std::vector< Body*> bodies;
    double *integrals;
    
    qmt::examples::MicroscopicSC model;

   public:
    SimplePoolJob(double a, unsigned int size): _a(a) {
        model.set(_a, 1);

        
        typedef qmt::QmtVector vec3d;

        bodies.push_back(new OneBody(vec3d(0,0,0)));
        bodies.push_back(new OneBody(vec3d(a,0,0)));
        bodies.push_back(new TwoBody(vec3d(0,0,0))); //U

       for(int i = 0; i < model.number_of_pairs_size(); ++i)
         bodies.push_back(new TwoBody(model.get_pair_vector(i)));

        integrals = new double[size];    
        

    }
    
    ~SimplePoolJob() {
     for(int i = 0; i < bodies.size(); ++i)
	delete bodies[i];	
    delete [] integrals;
    }


    void set_a(double a) {
        _a = a;


        model.set_a(a);
    }
qmt::examples::MicroscopicSC* get_model()  {return &model;}

   private:
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_alpha(arg);
    
        result = bodies [id]->integral(model);
        return result;
   }
};


/*
 * Diagonalization stage - now performed on the single core
 */

class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {
QmtSC_SGA_Solver_Stable *solverD;
QmtSC_SGA_Solver_Stable *solver0;
s_SGA_out solutionD;
s_SGA_out solution0;
qmt::examples::MicroscopicSC* model;

public:
SimpleInternalJob(){
	solver0 = new QmtSC_SGA_Solver_Stable(true);
	solverD = new QmtSC_SGA_Solver_Stable;

	solver0->set_beta(1e+6); //3 mK
	solverD->set_beta(1e+6);
	solutionD = {0,0,0,0,0,0};
	solution0 = {0,0,0,0,0,0};
	model = NULL;
}

~SimpleInternalJob(){
	delete solver0;
	delete solverD;
}

s_SGA_out get_mean_fields(double d){
	if(fabs(d) < 1e-15)	return solution0;
	else				return solutionD;
}

double get_double_occ(){
	return solution0.EG > solutionD.EG ? solutionD.d :solution0.d;
}

void set_model(qmt::examples::MicroscopicSC* m) { model = m;}
private:
 double Calculate(double* arg,int size,void * params) {
        std::vector<double> intgs(&arg[0], &arg[0] + size);

    	solver0->set_all(intgs[1],intgs[2],0.0);
    	solverD->set_all(intgs[1],intgs[2],0.0);

    	solution0 = solver0->solve();
    	solutionD = solverD->solve();
//	for(int i = 0; i < intgs.size(); ++i)
//        	std::cout<<"i"<<i<<"="<<intgs[i]<<" ";
//	    std::cout<<"E0= "<<solution0.EG<<" Ed= "<<solutionD.EG<<" ";

 	double energy = solution0.EG > solutionD.EG ? solutionD.EG :solution0.EG;



    	//Kij
    	double K_sum = 0;
    	for(unsigned int i = 3; i < intgs.size(); ++i ){
  	 K_sum += 0.5 * (2.0/qmt::QmtVector::norm(model->get_pair_vector(i-3)) + intgs[i] )* model->number_of_pairs(i-3) ;
	}

	//K_sum = (6*intgs[3] + 12 * intgs[4] + 3 * intgs[5]) + 0.5*6+0.5*12/sqrt(2) + 0.25 * 3;
    	energy += intgs[0] + K_sum;///model->get_system_size() ;
    //	    	std::cout<<model->get_alpha()<<" "<<"EG= "<<energy<<std::endl;;
   return energy;
 }
};

/*
 * Minimization - final result
 */

class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
 double alfa;
 double minimal_alfa;
 double minimal_f;
 double Calculate(void * params) {


   int status;
    int iter = 0, max_iter = 100;
    const gsl_min_fminimizer_type *T;
    gsl_min_fminimizer *s;
    gsl_function F;

    F.function = FinalProcessJob<double>::OutFunc;
    F.params = params;

    double m = 1.0, m_expected = M_PI;
    double a = 0.5, b = 10.0;

  
//     T = gsl_min_fminimizer_brent;
      T = gsl_min_fminimizer_goldensection;
    s = gsl_min_fminimizer_alloc (T);
    gsl_min_fminimizer_set (s, &F, m, a, b);


    do
    {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        m = gsl_min_fminimizer_x_minimum (s);
        a = gsl_min_fminimizer_x_lower (s);
        b = gsl_min_fminimizer_x_upper (s);
     //   std::cout<<m<<" "<<a<<" "<<b<<std::endl;
        status
            = gsl_min_test_interval (a, b, 0.0000001, 0.0);
 
    minimal_f = gsl_min_fminimizer_f_minimum(s);
    minimal_alfa = gsl_min_fminimizer_x_minimum(s);
    }
    while (status == GSL_CONTINUE && iter < max_iter);
   
    
    gsl_min_fminimizer_free (s);
 
  return minimal_alfa;
 }
 
 public:
  void set_alfa(double a) {
   alfa = a;
  }
  
  double get_min_alfa() const { return minimal_alfa;}
  double get_min_f() const {return minimal_f;}
};






int main() {
/*
	qmt::examples::MicroscopicSC model;

std::vector<double> out;

//model.get_integrals(out);



double a;

//for(int i = 0; i < 25; ++i) {
//a = .5 + i * 0.1;
//model.set_alpha(1.084);
//model.get_integrals(out);
//std::cout<<a<<" "<<out[0]<<" "<<out[1]<<" "<<out[2]<<" "<<out[3]<<" "<<out[4]<<" "<<out[5]<<std::endl;
std::cout<<a<<model.get_one_body_integral(qmt::QmtVector(0,0,0))<<" "<<model.get_one_body_integral(qmt::QmtVector(3.8,0,0))<<" "<<model.get_two_body_integral(qmt::QmtVector(0,0,0))<<" "<<model.get_system_size()<<std::endl;
out.clear();
double K_sum = 0;
std::cout<< model.number_of_pairs_size()<<std::endl;

for(int i = 0; i < model.number_of_pairs_size(); ++i) {
      // std::cout<<model.get_two_body_integral(model.get_pair_vector(i))<<" ";
       K_sum += 0.5 * (2.0/qmt::QmtVector::norm(model.get_pair_vector(i)) + model.get_two_body_integral(model.get_pair_vector(i)) )* model.number_of_pairs(i) ;
//std::cout<<model.get_pair_vector(i)<<" "<< model.number_of_pairs(i)<<std::endl;
}

std::cout<<" comp = "<<K_sum<<std::endl;

//}

//for(int i = 0; i < out.size(); ++i)
//std::cout<<i<<"# = "<<out[i]<<std::endl;
*/

MPI_Init(NULL, NULL);

   int taskid;
   int nproc;
   MPI_Comm comm;
   MPI_Comm_dup( MPI_COMM_WORLD, &comm);
   double t_start;// = MPI_Wtime();
   MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
   MPI_Comm_size(MPI_COMM_WORLD,&nproc);


   double a = 3.01;
//	qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;
//	qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
///	qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a,nproc - 1);

//	static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());

    for(int i = 0; i < 100; ++i) {
        a = 3.5+ i * 0.1;
       t_start = MPI_Wtime();
       qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;
	qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
	qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a,nproc - 1);
	static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());

	qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer(comm, finalJob, internalJob, processJob);

	double result = gatherer.run();




	if(taskid == nproc-1) {
         double sd = static_cast<SimpleInternalJob*>(internalJob)->get_double_occ();
	 std::cout<<a<<" "<<static_cast<SimpleFinalJob*>(finalJob)->get_min_f()<<" "<<sd * sd;
         std::cout<<" "<<  static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa()<<" "<<MPI_Wtime() - t_start<<std::endl;
       }

      delete finalJob;
      delete internalJob;
      delete processJob;
 }
   MPI_Finalize();


return 0;
}
