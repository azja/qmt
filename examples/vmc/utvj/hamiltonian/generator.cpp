#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <stdlib.h>
struct Atom {

int x;


size_t id;

std::string name;


Atom(int _x,const std::string& nm,size_t _id=0): x(_x),name(nm),id(_id){

}

friend std::ostream& operator<< (std::ostream& stream, const Atom& atom) {
 stream<<atom.name<<" "<<atom.x<<" "<<atom.id;
return stream;
}

};

//////////////////////////////////////////////////////////////////////////

std::vector<Atom> createChain(size_t size) {

std::vector<Atom> atoms;

size_t cntr = 0;

for(int i=0;i < size; ++i) {
    atoms.push_back(Atom(i,"Cu",cntr));
    cntr++;
   }



return atoms;
}

////////////////////////////////////////////////////////////////////////////



void CreateHamiltonian(std::vector<Atom>& atoms, int dim) {

size_t size = atoms.size();

int max = dim - 1;

std::map<size_t,size_t> ids;

/////////////////////////////////////////////////////////////////

auto  jump=[&](Atom atom,int i){
      int dist_x = atom.x + i;

      int cx;
      cx = dist_x;

      if(dist_x<0) cx = max;
      if(dist_x>max) cx = 0;

      
      
      int integral_id = 0;

std::string position="( "+std::to_string(atom.x)+", 0, 0, 0 ), ( "+std::to_string(cx)+", 0, 0, 0 );";

{
std::string out;
 if(atom.x != cx){
  out = std::to_string(atom.x)+"cup, " + std::to_string(cx)+ "aup, 1.0, "  + std::to_string(integral_id) + ", " + position + "\n";
  out += std::to_string(atom.x)+"cdown, " + std::to_string(cx)+ "adown, 1.0, "  + std::to_string(integral_id) + ", " + position + "\n";
  }
 else {
  out +=  std::to_string(atom.x)+"cdown, " + std::to_string(atom.x)+ "adown, 1.0,"  +  " 1, " + position + "\n";
  out +=  std::to_string(atom.x)+"cup, " + std::to_string(atom.x)+ "aup, 1.0,"  +  " 1, " + position + "\n";
  }
return std::string(out);
}
};

/////////////////////////////////////////////////////////////////

auto Jinter=[&](const Atom& atom, int k) {

std::string a_position="( "+std::to_string(atom.x)+", 0, 0, 0 )";
std::string position="( "+std::to_string(atom.x)+", 0, 0, 0 )";

int i = atom.x;
int j = atom.x + k;
std::string int_id = "1";
      if(j<0) j = max;
      if(j>max) j = 0;





std::string out;
out=std::to_string(i)+"cup, " + std::to_string(j)+"cup,"+ std::to_string(i)+"aup, " + std::to_string(j)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(i)+"cdown, " + std::to_string(j)+"cdown,"+ std::to_string(i)+"adown, " + std::to_string(j)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";

out+=std::to_string(j)+"cup, " + std::to_string(i)+"cup,"+ std::to_string(j)+"aup, " + std::to_string(i)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(j)+"cdown, " + std::to_string(i)+"cdown,"+ std::to_string(j)+"adown, " + std::to_string(i)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";

out+=std::to_string(i)+"cup, " + std::to_string(j)+"cdown,"+ std::to_string(i)+"adown, " + std::to_string(j)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(i)+"cdown, " + std::to_string(j)+"cup,"+ std::to_string(i)+"aup, " + std::to_string(j)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";

out+=std::to_string(j)+"cup, " + std::to_string(i)+"cdown,"+ std::to_string(j)+"adown, " + std::to_string(i)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(j)+"cdown, " + std::to_string(i)+"cup,"+ std::to_string(j)+"aup, " + std::to_string(i)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";

return out;
};





/////////////////////////////////////////////////////////////////

auto Uinter=[&](const Atom& atom){

auto id=atom.x;
auto x = atom.x;



std::string position ="( "+std::to_string(x)+", 0, 0, 0 )";

std::string out;

std::string int_id;


int_id = "0";


out=std::to_string(atom.id)+"cup, " + std::to_string(atom.id)+"aup,"+ std::to_string(atom.id)+"cdown, " + std::to_string(atom.id)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+";\n";
out=out+std::to_string(atom.id)+"cdown, " + std::to_string(atom.id)+"adown,"+ std::to_string(atom.id)+"cup, " + std::to_string(atom.id)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+";\n";

return out;

};

/////////////////////////////////////////////////////////////////


auto Kinter=[&](const Atom& atom, int k) {

std::string a_position="( "+std::to_string(atom.x)+", 0, 0, 0 )";
std::string position="( "+std::to_string(atom.x)+", 0, 0, 0 )";

int i = atom.x;
int j = atom.x + k;
std::string int_id = "2";
      if(j<0) j = max-1;
      if(j>max) j = 1;





std::string out;
out=std::to_string(i)+"cup, " + std::to_string(j)+"cup,"+ std::to_string(j)+"aup, " + std::to_string(i)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(i)+"cdown, " + std::to_string(j)+"cdown,"+ std::to_string(j)+"adown, " + std::to_string(i)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(i)+"cup, " + std::to_string(j)+"cdown,"+ std::to_string(j)+"adown, " + std::to_string(i)+"aup, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";
out+=std::to_string(i)+"cdown, " + std::to_string(j)+"cup,"+ std::to_string(j)+"aup, " + std::to_string(i)+"adown, 0.5, "+int_id +", " +position+", "+position+",  "+ position+","+position+"; \n";


return out;
};

std::cout<<"one_body"<<std::endl;




for(const auto& atom : atoms) {
//std::cout<<atom<<std::endl;
  if(atom.name == "Cu") {

    int x = atom.x;

    
    for(int i=-1; i<=1;i++){

     std::cout<<jump(atom,i);

  }


}
}

std::cout<<"end_one_body"<<std::endl;
std::cout<<"two_body"<<std::endl;

for(const auto& atom : atoms) {
//std::cout<<atom<<std::endl;
  if(atom.name == "Cu") {

   
   std::cout<<Uinter(atom);




}
}







for(const auto& atom : atoms) {
//std::cout<<atom<<std::endl;
  if(atom.name == "Cu") {

    int x = atom.x;

    
    for(int i=-1; i<=1;i=i+2){

     std::cout<<Jinter(atom,i);

  }


}
}


for(const auto& atom : atoms) {
//std::cout<<atom<<std::endl;
  if(atom.name == "Cu") {

    int x = atom.x;

    
    for(int i=-2; i<=2;i=i+4){

     std::cout<<Kinter(atom,i);

  }


}
}



std::cout<<"end_two_body"<<std::endl;

}

////////////////////////////////////////////////////////////////////////////


int main(int argc, const char* argv[]) {

size_t n=atoi(argv[1]);



auto result =createChain(n);

CreateHamiltonian(result,n);

//for(const auto& item : result)
// std::cout<<item<<std::endl;


return 0;
}



