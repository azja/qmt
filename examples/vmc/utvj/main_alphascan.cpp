#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define SIZE 20
//#define NEL 40


int main(int argc,const char* argv[])
{

int NEL = 10;




qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE> ("ham.dat",(NEL/2),(NEL/2),40000U);

//auto &stat = (static_cast<qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE>*>(diagonalizer_vmc_gsl))->get_stat();

//if(i==1)
/*
{
std::cout<<" E_tot "<<" N";
auto avs_names = stat.get_names();
for(const auto item : avs_names)
std::cout<<" "<<item;
}
std::cout<<std::endl;
*/

    
//qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);


//original

double eps = 0;
double t = -1;

double J =0.3;
//U_d=double(start);

/*
double eps_d = 0;
double eps_p = 2;

double t_pd = 1.13;
double t_pp = 0.49;

double U_d = 7.5;
double U_p = 4.1;
*/


std::vector<double> one_body;

one_body.push_back(0);
one_body.push_back(t); //0
one_body.push_back(eps); //1

std::vector<double> two_body;
two_body.push_back(0);
two_body.push_back(J);
two_body.push_back(0);
std::cout<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)<<" "<<NEL<<" ";
//std::cout<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)<<" "<<U_d<<" ";

/*
for(const auto item : stat.get_values())
std::cout<<" "<<item;
*/
//std::cout<<std::endl;



                                                                                                                        

 	
delete diagonalizer_vmc_gsl;

return 0;
}
