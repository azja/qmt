#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define SIZE 132
#define NEL 66


int main(int argc,const char* argv[])
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1.0,1.0,1.0));

   double alpha_0=atof(argv[1]);;
   
   double a = 1.6;
   double R = 2.4;
   
  if(argc > 3) {
   a = atof(argv[2]);
   R = atof(argv[3]);
  }
qmt::QmtVector scale(a,a,R);
qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE> ("ham.dat",(NEL/2  ),(NEL/2   ),100000U);




//std::cout<<scale<<std::endl<<std::endl;
    
//qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",1);

auto &stat = (static_cast<qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE>*>(diagonalizer_vmc_gsl))->get_stat();

std::cout<<"alpha (1/a_0) E/atom (Ry)";

auto avs_names = stat.get_names();
for(const auto item : avs_names)
std::cout<<" "<<item;

std::cout<<std::endl;



for(int l = 0; l < 1; l++){
double    alpha = alpha_0 + l *0.05;
    std::vector<double> one_body;
    std::vector<double> two_body;
    system->set_parameters(std::vector<double>({alpha}),scale);
   for(auto i = 0; i < system->get_one_body_number();++i) {
    double integral = 0.0; 
         integral = system->get_one_body_integral(i);
//   std::cout<<integral<<" "<<std::endl;
//     std::cout<<integral<<std::endl;
     one_body.push_back(integral);
   }

   for(auto i = 0; i < system->get_two_body_number();++i){

    auto integral =  system->get_two_body_integral(i);

//   std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
//    std::cout<<integral<<" "<<std::endl;
    two_body.push_back(integral);
   
   }

/*double f01 =  static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer_vmc_gsl)->DimerDimerAvs(0,1);
double f02 =  static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer_vmc_gsl)->DimerDimerAvs(0,2);
double f03 =  static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer_vmc_gsl)->DimerDimerAvs(0,3);
double f04 =  static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer_vmc_gsl)->DimerDimerAvs(0,4);
double f05 =  static_cast<qmt::QmtLanczosDiagonalizer*>(diagonalizer_vmc_gsl)->DimerDimerAvs(0,5);*/
//std::cout<<"---------------------------------------------------------------------------------------------"<<std::endl;


std::cout<<alpha<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/(NEL);//<<" "<<" "<<f01<<" "<<f02<<" "<<f03<<" "<<f04<<" "<<f05<<std::endl;

for(const auto item : stat.get_values())
std::cout<<" "<<item;

std::cout<<std::endl;



                                                                                                                         

 }

 	
delete diagonalizer_vmc_gsl;
delete system;
	return 0;
}
