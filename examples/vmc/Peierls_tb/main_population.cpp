#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define SIZE 20
#define NEL 10

#define NALPHA 3
#define NPROBES 50

double SET[NALPHA];

double delta_alpha = 0.025;

int main(int argc,const char* argv[])
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1.0,1.0,1.0));

   double alpha_0=atof(argv[1]);;
   
   double a = 1.6;
   double R = 2.4;
   
  if(argc > 3) {
   a = atof(argv[2]);
   R = atof(argv[3]);
  }
qmt::QmtVector scale(a,a,R);
qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE> ("ham.dat",((NEL/2)-1),(NEL/2),25000U);




//std::cout<<scale<<std::endl<<std::endl;
    
//qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);

auto &stat = (static_cast<qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE>*>(diagonalizer_vmc_gsl))->get_stat();

std::cout<<"alpha (1/a_0) E/atom (Ry)";

auto avs_names = stat.get_names();
//for(const auto item : avs_names)
//std::cout<<" "<<item;

//std::cout<<std::endl;


for(int l = 0; l < NALPHA; l++){
std::cout<<alpha_0 + l * delta_alpha<<" ";
}

std::cout<<std::endl;


for(int k =0; k < NPROBES;k++){
for(int l = 0; l < NALPHA; l++){
double    alpha = alpha_0 + l * delta_alpha;
    std::vector<double> one_body;
    std::vector<double> two_body;
    system->set_parameters(std::vector<double>({alpha}),scale);
   for(auto i = 0; i < system->get_one_body_number();++i) {
    double integral = 0.0; 
    integral = system->get_one_body_integral(i);
//   std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }

   for(auto i = 0; i < system->get_two_body_number();++i){
    auto integral =  system->get_two_body_integral(i);
// std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
    two_body.push_back(integral);   
   }

SET[l] = diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/(NEL);
}


for(int l = 0; l < NALPHA; l++){
std::cout<<SET[l]<<" ";
}std::cout<<k<<" "<<std::endl;
}

 	
delete diagonalizer_vmc_gsl;
delete system;
	return 0;
}
