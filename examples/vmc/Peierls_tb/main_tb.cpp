#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex>
#include <gsl/gsl_integration.h>




#define SIZE 132
#define NEL 66

double a;
double R;

qmt::QmtSystem *systemtb;

#define PI 3.14159265359

//single electron energy

double energy (double k,void* params){

        auto val = *(static_cast<std::vector<double>*>(params));

	auto e1 = std::complex<double>(0,k*(R-a));
	auto e2 = std::complex<double>(0,k*(R-2*a));
	auto e3 = std::complex<double>(0,k*(R));
	auto e4 = std::complex<double>(0,k*(a+R));

	double t0=val[1];
	double t1=val[2];
	double t2=val[3];
	double t3=val[4];
	double eps=val[5];
        double t4 = val[6];
        double t5 = val[7];

        if(fabs(a/2 - R)<1.0e-4){
 //std::cout<<"t1 = "<<t1<<" t5 = "<<t5<<std::endl;         
// t1=t5;
  //        t3=t4;
         
         }

	auto z= t3*std::exp(e1) + t1*std::exp(e2)+t4*std::exp(e3)+t5*std::exp(e4);
	double real_part = eps+2*(t2*cos(k*a)+t0*cos(2*k*a));


auto k_molecular=[&](double k)->double{
       return real_part-std::abs(z);
};


auto k_atomic=[&](double k)->double{
       return real_part+std::real(z + std::conj(z))/2;
};

if(fabs(a/2 - R)<1.0e-4)
 return k_atomic(k);
else
 return k_molecular(k);

}


//compute integrals
void compute_integrals(double alph,std::vector<double>& integrals) {
   integrals.clear();

    systemtb->set_parameters(std::vector<double>({alph}),qmt::QmtVector(1,1,1));
   for(auto i = 0; i < systemtb->get_one_body_number();++i) {
    double integral = 0.0; 
         integral = systemtb->get_one_body_integral(i);
     integrals.push_back(integral);
   }
}


//total energy
double Total_Energy(double alph,void* params)
{
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (1000);
  
  double result, error;
  std::vector<double> integrals;
  compute_integrals(alph,integrals);
  gsl_function F;
  F.function = &energy;
  F.params = (void*)(&integrals);

  double k_f;

  if(fabs(a/2 - R)<1.0e-4)  {
    k_f=0.5*2*PI/a;
  }
  else
    k_f=PI/a;

  
  gsl_integration_qags (&F, 0, k_f, 0, 1e-7, 1000,
                        w, &result, &error); 


  gsl_integration_workspace_free (w);

  double brill_vol;

  if(fabs(a/2 - R)<1.0e-4)  {
    brill_vol=a/(PI);
  }
  else
    brill_vol=a/(PI);


  return result*brill_vol;
}


//optimize total energy
double Optimize_Energy() {
int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = 1.4;
  double a = 0.5, b = 10.0;
  gsl_function F;

  F.function = Total_Energy;
  F.params = 0;

  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.001, 0.0);

  /*    if (status == GSL_SUCCESS)
        printf ("Converged:\n");

      printf ("%5d [%.7f, %.7f] "
              "%.7f %+.7f %.7f\n",
              iter, a, b,
              m, m - m_expected, b - a);*/
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
//  std::cout<<"m = "<<m<<std::endl;
  return Total_Energy(m,NULL);


}


//////////////////////////////////////////////////////////

int main(int argc,const char* argv[])
{
  
   systemtb = new qmt::QmtSystemStandard("conf.dat"); 
   systemtb->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1.0,1.0,1.0));   

   a = atof(argv[1]);
   R = atof(argv[2]);
  
std::cout<<a<<" "<<R<<" "<<Optimize_Energy()+ systemtb->get_one_body_integral(0)/18<<std::endl;
delete systemtb;

	return 0;
}
