#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
//   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<128> ("ham_64_4N.dat",32U,32U,10000U);
  
//   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<16> ("ham_9_4N.dat",4U,4U,50000U);
   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);



    std::vector<double> one_body;
    std::vector<double> two_body;
   


// std::cout<<"Two body parameters#"<<system->get_one_body_number()<<std::endl;
//    std::cout<<"Policzył parametry:"<<system->get_two_body_number()<<std::endl;


  
auto F = [&](std::vector<double> v)->double {

    one_body.clear();
    two_body.clear();
    system->set_parameters(v,qmt::QmtVector(4.0,4.0,4.0));
   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
     one_body.push_back(integral);
   }


   for(auto i = 0; i < system->get_two_body_number();++i){
        auto integral = system->get_two_body_integral(i);
   two_body.push_back(integral);
//   std::cout<<"I#"<<i<<" = "<<integral<<std::endl;
   }

  double value = diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/8; 
  std::cout<<v[0]<<" "<<value<<std::endl;
  return value;
  };

/*
for(int j = 0; j < 40;j++) {

  system->set_parameters(std::vector<double>({0.8 + j *0.01}),qmt::QmtVector(4.0,4.0,4.0));
    one_body.clear();
    two_body.clear();

//std::cout<<"System set"<<std::endl;

   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
     one_body.push_back(integral);
   }


   for(auto i = 0; i < system->get_two_body_number();++i){
        auto integral = system->get_two_body_integral(i);
   two_body.push_back(integral);
//   std::cout<<"I#"<<i<<" = "<<integral<<std::endl;
   }


  double value = diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/8; 
  std::cerr<<0.8 + j *0.01<<" "<<value<<std::endl;



 }

exit(0);*/

qmt::QmtSimulatedAnnealing solver(1,1,std::function<double(std::vector<double>)>(F),std::vector<double>({1.0}),std::vector<double>({1.4}),20.0,0.1);


double min_val;
std::vector<double> out_par;
solver.run(20,0.01,min_val,out_par);


std::cout<<min_val<<" "<<out_par[0]<<std::endl;;

 delete system;	
 delete diagonalizer_vmc_gsl;

return 0;
}
