#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
 // qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<128> ("ham_64_4N.dat",32U,32U,5000U);
    
   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<16> ("ham_9_4N.dat",4U,4U,20000U);
//  qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",1);
// qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<64> ("hamN16.dat",16U,16U,50000U);
//qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<8> ("hamN2.dat",2U,2U,5000U);
 for(auto i = 0U; i < 10U; ++i) {

    std::vector<double> one_body;
    std::vector<double> two_body;
   //1.083 is equilibrium for a = 4.0
    system->set_parameters(std::vector<double>({1.08+i*0.1}),qmt::QmtVector(4.0,4.0,4.0));
// std::cout<<"One body parameters#"<<system->get_one_body_number()<<std::endl;
   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
     std::cout<<"int_one-body#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }

std::vector<double> pparams({
1.41236,
0.498262,
0.353024,
0.288395,
0.250319});


// std::cout<<"Two body parameters#"<<system->get_one_body_number()<<std::endl;
   for(auto i = 0; i < system->get_two_body_number();++i){
    auto integral =/* pparams[i];*/ system->get_two_body_integral(i);
//     std::cout<<integral<<std::endl;
   two_body.push_back(integral);
   
   }
//    std::cout<<"Policzył parametry:"<<system->get_two_body_number()<<std::endl;
//std::cout<<"Parameters computed..."<<std::endl;
  
	std::cout<<0.8 + i * 0.1<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/8<<" coulomb part = "<<one_body[0]<<std::endl;
   
}
 
 delete system;	
 delete diagonalizer_vmc_gsl;

	return 0;
}
