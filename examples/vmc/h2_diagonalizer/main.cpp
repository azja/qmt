#define GSL_LIBRARY

#include "../../../qmtvmc/qmtvmcdiag.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
	qmt::vmc::QmtVmcGslMetropolisDiagonalizer<4>  diagonalizer_vmc_gsl("ham1s.dat",1U,1U,10000U);

	
	std::vector<double> interactions ({1.65321, 0.956691});
	std::vector<double> hoppings ({2/1.43,-1.75079,-0.727647});
	
	std::cout<<"E = "<<diagonalizer_vmc_gsl.Diagonalize(hoppings,interactions)/2;
	
	return 0;
}
