#define GSL_LIBRARY

#include "../../../qmtvmc/qmtslaterstate.h"
#include "../../../qmtvmc/qmtvmcjastrow.h"
#include "../../../qmtvmc/qmtconfiguration.h"
#include "../../../qmtvmc/qmtconfbitset.h"
#include "../../../qmtvmc/qmtvmcprobabilityGSL.h"
#include "../../../qmtvmc/qmtvmchamiltonianenergy.h"
#include "../../../qmtvmc/qmtvmcdata.h"
#include "../../../qmtvmc/qmtvmcalgorithm.h"
#include "../../../qmtvmc/qmtvmcmetropolis.h"
#include "../../../qmtvmc/qmtvmcsingleparticleproblemsolver.h"
#include "../../../qmtvmc/qmtvmcgsloptimizer.h"
#include "../../../headers/qmthubbard.h"
#include "../../../headers/qmthbuilder.h"
#include "../../../headers/qmtstate.h"
#include "../../../headers/qmtbasisgenerator.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
	typedef qmt::QmtNState<std::bitset<4>, int> qmt_nstate;
	typedef qmt::QmtHubbard<qmt::SqOperator<qmt_nstate >> qmt_hamiltonian;
  // Two-site model with U and K
  
  
	/*
	 * Jastrow
	 */
	std::vector<std::tuple<size_t, size_t, size_t,size_t>> jastrow_definition = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getInteractionMap("ham1s.dat");
	
	/*jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,0,0,1)); // U on site 0
	jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(1,1,0,1)); // U on site 1
	jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,1,1,0)); // K
	jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(1,0,1,0)); // K*/
  
	qmt::vmc::QmtVmcJastrow jastrow(jastrow_definition);
	
	/*
	 *Fermi sea
	 */
	
	qmt::vmc::QmtSlaterState* uncorrelated_state = new qmt::vmc::QmtSlaterState();
	

	

	// get hamiltonian
	qmt_hamiltonian* hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian("ham1s.dat",1);
	
	// create single-particle basis
	qmt::QmtBasisGenerator generator;
	std::vector<qmt_nstate> basis;

	generator.generate<4>(1,2,basis);

	// solve single-particle problem

	qmt::vmc::QmtVmcSingleParticleProblemSolver<qmt_hamiltonian> problem_solver(hamiltonian,basis);
	std::vector<double> microscopic_parameters({-1.75079,-0.727647});

	problem_solver.get_single_particle_eigenstates(2,*uncorrelated_state,microscopic_parameters);

	
	/*
	 * probability
	 */
	
	qmt::vmc::QmtVmcProbability *probability;
	probability = new qmt::vmc::QmtVmcProbabilityGSL<4>(2,jastrow,uncorrelated_state);
	
	
	std::shared_ptr<qmt::vmc::QmtConfigurationState> x (new qmt::vmc::QmtConfigurationStateBitset<4>(1u,1u));
	
	
	/*
	 * Hoppings map
	 */
	
	std::vector<std::tuple<size_t,size_t,size_t>> hop_map_vec = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHoppingMap("ham1s.dat");
		
//	for(auto & item : hop_map_vec)
//		std::cout<<"<"<<std::get<0>(item)<<", "<<std::get<1>(item)<<", "<<std::get<2>(item)<<">"<<std::endl;

//	hop_map_vec.push_back(std::make_tuple(0,1,1));
//	hop_map_vec.push_back(std::make_tuple(1,0,1));
	
	qmt::vmc::QmtVmcHoppingsMap *map = new qmt::vmc::QmtVmcHoppingsMap(hop_map_vec);
	
	/*
	 * Interaction definitions
	 */
	
	
	std::vector<std::tuple<size_t, size_t, size_t,size_t>> interaction_definition  = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getInteractionMap("ham1s.dat");

//	for(auto & item : interaction_definition)
//		std::cout<<"<"<<std::get<0>(item)<<", "<<std::get<1>(item)<<", "<<std::get<2>(item)<<", "<<std::get<3>(item)<<">"<<std::endl;


/*	interaction_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,0,0,1)); // U on site 0
	interaction_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(1,1,0,1)); // U on site 1
	interaction_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,1,1,0)); // K
*/	
	/*
	 * Integrals
	 */
	
	std::vector<double> interactions ({1.65321, 0.956691});
	std::vector<double> hoppings ({-1.75079,-0.727647});
	std::vector<unsigned int> sp_energy = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getSPEnergyMap("ham1s.dat");
	
//	for(auto& item : sp_energy)
//	    std::cout<<item<<std::endl;
	
//	std::vector<unsigned int> sp_energy ({0,0,0,0});

	/*
	 * Energy calculator
	 */
	
	qmt::vmc::QmtVmcGetEnergy  *energy_calculator = new qmt::vmc::QmtVmcHamiltonianEnergy(
	  *uncorrelated_state,interaction_definition,interactions,sp_energy,hoppings,map,probability);
	
	/*
	 * Data gather them all
	 */
	
	qmt::vmc::QmtVmcData data ;
	
	
	data.prob_calc = probability;
	data.energy_calc = energy_calculator;
	data.map = map;
	data.slater = uncorrelated_state;
	
	
	/*
	 * Algorithm
	 */
	
	
	qmt::vmc::QmtVmcAlgorithm* metropolis = new qmt::vmc::QmtVmcMetropolis(data,x);
	
	
	/*
	 *Optimizer
	 */
	
	qmt::vmc::QmtVmcOptimizer *optimizer = new  qmt::vmc::QmtVmcGslOptimizer();
	
	
	std::vector<std::tuple<double,double,double>> start_points;
	
	start_points.push_back(std::make_tuple(-1.0,-0.1,1.0));
	start_points.push_back(std::make_tuple(-1.0,0.0,1.0));
	
	std::vector<double> output;

	std::cout<<" E = "<<0.5*(optimizer->optimize(data,*metropolis,start_points,output,10000,8000) + 2.0/1.43)<<std::endl;
	
	/*
	for(int i = 0; i  < 20;++i) {
	 double v_0 =-0.5 + i * 0.1;
	 double v_1 = 0;
	 data.prob_calc->set_parameters(std::vector<double>({v_0,v_1})); 
	 std::cout<<v_0<<" "<<(metropolis->get_local_energy(1000) + 2.0/1.43042)/2<<std::endl;
	}
	*/
	
	delete uncorrelated_state;
	delete probability;
	delete map;
	delete energy_calculator;
	delete metropolis;
	delete optimizer;
	return 0;
}
