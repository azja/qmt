#define GSL_LIBRARY

#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<4> ("ham1s.dat",1U,1U,1000U);


 for(auto i = 0U; i < 40U; ++i) {

    std::vector<double> one_body;
    std::vector<double> two_body;
   
    system->set_parameters(std::vector<double>({1.085+i*0.005}),qmt::QmtVector(1,1,1.43));
   
   for(auto i = 0; i < system->get_one_body_number();++i)
     one_body.push_back(system->get_one_body_integral(i));

   for(auto i = 0; i < system->get_two_body_number();++i)
   two_body.push_back(system->get_two_body_integral(i));

  
	std::cout<<1.08 + i * 0.005<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/2<<std::endl;
   
}
 
 delete system;	
 delete diagonalizer_vmc_gsl;
 
	return 0;
}
