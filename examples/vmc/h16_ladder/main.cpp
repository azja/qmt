#define GSL_LIBRARY

#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc,const char* argv[])
{
  
  double alpha = atof(argv[1]);
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<64> ("hamN16.dat",16U,16U,1000U);


// for(auto i = 0U; i < 40U; ++i) {

    std::vector<double> one_body;
    std::vector<double> two_body;
   
    system->set_parameters(std::vector<double>({alpha}),qmt::QmtVector(3.1,3.1,1.43));
// std::cout<<"One body parameters#"<<system->get_one_body_number()<<std::endl;
   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
//     std::cout<<integral<<std::endl;
     one_body.push_back(integral);
   }

   for(auto i = 0; i < system->get_two_body_number();++i){
    auto integral = system->get_two_body_integral(i);
//     std::cout<<integral<<std::endl;
   two_body.push_back(integral);
   
   }
//    std::cout<<"Policzył parametry:"<<system->get_two_body_number()<<std::endl;

  
	std::cout<<alpha<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/32<<std::endl;
   
//}
 
 delete system;	
 delete diagonalizer_vmc_gsl;

	return 0;
}
