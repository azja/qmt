#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc,const char* argv[])
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1.0,1.0,1.0));
   
   double alpha=atof(argv[1]);;
 qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<32> ("ham3.dat",8U,8U,50000U);
    
// qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",1);


    std::vector<double> one_body;
    std::vector<double> two_body;
    
//std::cout<<"S_01 = "<<    system->get_overlap(0,1,qmt::QmtVector(0,0,0.55),qmt::QmtVector(0,0,-0.54)<<std::endl;
//exit(0);
    system->set_parameters(std::vector<double>({alpha}),qmt::QmtVector(1.0,1.0,1.0));
   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
         std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }
/*
one_body.push_back(133.104);
one_body.push_back(-16.3309);
one_body.push_back(-1.50761);
one_body.push_back(-0.0138026);
one_body.push_back(0.00537664);
one_body.push_back(-0.000344085);
one_body.push_back(-6.71795e-05);
one_body.push_back(-0.0455446);
one_body.push_back(-0.00194426);
one_body.push_back( 1.02563e-05);
one_body.push_back( -3.30609e-05);
one_body.push_back( -0.0458526);
one_body.push_back( -0.000410956);
one_body.push_back( -0.0355115);





two_body.push_back(1.52005);
two_body.push_back(0.968561);
two_body.push_back(0.600293);
two_body.push_back( 0.402944);
two_body.push_back( 0.377016);
two_body.push_back( 0.293679);
two_body.push_back( 0.337055);
two_body.push_back( 0.283715);
two_body.push_back( 0.274526);
two_body.push_back( 0.23625);
two_body.push_back( 0.258069);
two_body.push_back(0.230826);
two_body.push_back( 0.243239);
*/
   for(auto i = 0; i < system->get_two_body_number();++i){

    auto integral =  system->get_two_body_integral(i);
    std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
   two_body.push_back(integral);
   
   }
	std::cout<<alpha<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/16<<std::endl;
   

 	
delete diagonalizer_vmc_gsl;
delete system;
	return 0;
}
