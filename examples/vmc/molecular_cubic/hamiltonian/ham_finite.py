#!/usr/bin/python

import math
import numpy as np
import sys
import copy as cp

def length(vector):
	return math.sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2])
#length

def distance(atom1,atom2):
	return length(atom1[1]-atom2[1])
#distance

NumberOrbitals=16

SuperCell=[
[0,np.array([0,0,0]),np.array([0,0,0]),100,0,1], 
[1,np.array([0,0,1.43]),np.array([0,0,1.43]),100,1,2],
[2,np.array([0,0,4]),np.array([0,0,4]),100,2,3], 
[3,np.array([0,0,5.43]),np.array([0,0,5.43]),100,3,4],
[4,np.array([0,4,0]),np.array([0,4,0]),100,4,5], 
[5,np.array([0,4,1.43]),np.array([0,4,1.43]),100,5,6],
[6,np.array([0,4,4]),np.array([0,4,4]),100,6,7], 
[7,np.array([0,4,5.43]),np.array([0,4,5.43]),100,7,8],
[8,np.array([4,0,0]),np.array([4,0,0]),100,8,9], 
[9,np.array([4,0,1.43]),np.array([4,0,1.43]),100,9,10],
[10,np.array([4,0,4]),np.array([4,0,4]),100,10,11], 
[11,np.array([4,0,5.43]),np.array([4,0,5.43]),100,11,12],
[12,np.array([4,4,0]),np.array([4,4,0]),100,12,13], 
[13,np.array([4,4,1.43]),np.array([4,4,1.43]),100,13,14],
[14,np.array([4,4,4]),np.array([4,4,4]),100,14,15], 
[15,np.array([4,4,5.43]),np.array([4,4,5.43]),100,15,16]
]


# mega cell
MegaCell=cp.copy(SuperCell)

#MegaCell=cp.copy(SuperCell)

SuperCellFile = open('supercell.dat', 'w+')
for atom in SuperCell:
	SuperCellFile.write("("+str(atom[2][0])+",  "+str(atom[2][1])+",  "+str(atom[2][2])+",  "+str(atom[0])+");\n")
SuperCellFile.close()

MegaCellFile = open('megacell.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write("("+str(atom[2][0])+",  "+str(atom[2][1])+",  "+str(atom[2][2])+",  "+str(atom[0])+");\n")
MegaCellFile.close()

MegaCellFile = open('megacell_orbital.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write(str(atom[3])+", ")
MegaCellFile.close()
################################## JMOL ###############################################

JmolFile = open('jmol_supercell.dat', 'w+')
JmolFile.write(str(len(SuperCell))+"\n\n") # for jmol
for atom in SuperCell:
 	if atom[3]==0:
 		JmolFile.write("Ca ")
 	if atom[3]==1:
 		JmolFile.write("K ")
 	if atom[3]==2:
 		JmolFile.write("Ar ")
 	if atom[3]==3:
 		JmolFile.write("Cl ")
 	if atom[3]==4:
 		JmolFile.write("S ")
 	if atom[3]==5:
 		JmolFile.write("P ")
 	if atom[3]==6:
 		JmolFile.write("Si ")
 	if atom[3]==7:
 		JmolFile.write("Al ")
 	if atom[3]==8:
 		JmolFile.write("Mg ")
 	if atom[3]==9:
 		JmolFile.write("Na ")
 	if atom[3]==10:
 		JmolFile.write("Ne ")
 	if atom[3]==11:
 		JmolFile.write("F ")
 	if atom[3]==12:
 		JmolFile.write("O ")
 	if atom[3]==13:
 		JmolFile.write("N ")
 	if atom[3]==14:
 		JmolFile.write("C ")
 	if atom[3]==15:
 		JmolFile.write("B ")
 	if atom[3]==16:
 		JmolFile.write("Be ")
 	if atom[3]==17:
 		JmolFile.write("Li ")
 	if atom[3]==18:
 		JmolFile.write("He ")
 	if atom[3]>18:
 		JmolFile.write("H ")
 	JmolFile.write(str(atom[1][0])+" "+str(atom[1][1])+" "+str(atom[1][2])+"\n")
JmolFile.close()

JmolFile = open('jmol_megacell.dat', 'w+')
JmolFile.write(str(len(MegaCell))+"\n\n") # for jmol
for atom in MegaCell:
 	if atom[3]==0:
 		JmolFile.write("Ca ")
 	if atom[3]==1:
 		JmolFile.write("K ")
 	if atom[3]==2:
 		JmolFile.write("Ar ")
 	if atom[3]==3:
 		JmolFile.write("Cl ")
 	if atom[3]==4:
 		JmolFile.write("S ")
 	if atom[3]==5:
 		JmolFile.write("P ")
 	if atom[3]==6:
 		JmolFile.write("Si ")
 	if atom[3]==7:
 		JmolFile.write("Al ")
 	if atom[3]==8:
 		JmolFile.write("Mg ")
 	if atom[3]==9:
 		JmolFile.write("Na ")
 	if atom[3]==10:
 		JmolFile.write("Ne ")
 	if atom[3]==11:
 		JmolFile.write("F ")
 	if atom[3]==12:
 		JmolFile.write("O ")
 	if atom[3]==13:
 		JmolFile.write("N ")
 	if atom[3]==14:
 		JmolFile.write("C ")
 	if atom[3]==15:
 		JmolFile.write("B ")
 	if atom[3]==16:
 		JmolFile.write("Be ")
 	if atom[3]==17:
 		JmolFile.write("Li ")
 	if atom[3]==18:
 		JmolFile.write("He ")
 	if atom[3]>18:
 		JmolFile.write("H ")
 	JmolFile.write(str(atom[1][0])+" "+str(atom[1][1])+" "+str(atom[1][2])+"\n")
JmolFile.close()


# anihilator
def an(N,s):
	return str(N)+"a"+str(s)
# creator
def cr(N,s):
	return str(N)+"c"+str(s)
# number operator
def num(N,s):
	return cr(N,s)+",	"+an(N,s)

# spins
spin = ["up","down"]

def bar(spin):
	if spin == "up":
		return "down"
	else:
		return "up"

# actual microscopic parameter flag

one_body_flag = 0
two_body_flag = 0


dict_one_body=[]
print ""
print "one_body"
for atom1 in SuperCell:
	for b in range(atom1[5]-atom1[4]):
		for atom2 in SuperCell:
			for d in range(atom2[5]-atom2[4]):
				i = atom1[4]+b
				j = atom2[4]+d
	
				dij=distance(atom1,atom2)

				IF_IN_DICT=False
				for reg in dict_one_body:
					if ( b==reg[0] and d==reg[1] and abs(dij-reg[2])<1e-6):
						IF_IN_DICT=True
						loc_body_flag=reg[3]

				if not IF_IN_DICT:
					loc_body_flag=cp.copy(one_body_flag)
					dict_one_body.append([b,d,dij,loc_body_flag])
					one_body_flag+=1

				for s in spin:
					print 	cr(i,s)+",	"+an(j,s)+",	"+" +,	"+str(loc_body_flag)+",	"\
					+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(i)+")"\
					+		",	"\
					+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(j)+")"\
					+       ";"
print "end_one_body"


dict_two_body=[]


print ""
print "two_body"
for atom1 in SuperCell:
	for b in range(atom1[5]-atom1[4]):
		for atom2 in MegaCell:
			for d in range(atom2[5]-atom2[4]):
				i = atom1[4]+b
				j = atom2[4]+d
				k = atom1[4]+b
				l = atom2[4]+d
				for s1 in spin:
					for s2 in spin:

						dij = distance(atom1,atom2)

						if i==j and i==k and i==l and s1==s2 and abs(dij)<1e-6: # U can't be for same spins
							continue


					if (i==j and j==k and k==l):
						print 	cr(i,s1)+",	"+an(k,s1)+",	"+cr(j,s2)+",	"+an(l,s2)+",	"+" 0.5,	"+str(two_body_flag)+",	"\
							+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(i)+")"\
							+		",	"\
							+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(j)+")"\
							+		",	"\
							+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(k)+")"\
							+		",	"\
							+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(l)+")"\
							+       ";"
print "end_two_body"

two_body_flag = 1

## Only U and K
print ""
print "two_body"
for atom1 in SuperCell:
	for b in range(atom1[5]-atom1[4]):
		for atom2 in MegaCell:
			for d in range(atom2[5]-atom2[4]):
				i = atom1[4]+b
				j = atom2[4]+d
				k = atom1[4]+b
				l = atom2[4]+d
				for s1 in spin:
					for s2 in spin:

						dij = distance(atom1,atom2)
					
						if i==j and i==k and i==l: # U not
							continue

						IF_IN_DICT=False
						for reg in dict_two_body:
							if ( ((b==reg[0] and d==reg[1]) or (d==reg[0] and b==reg[1])) and abs(dij-reg[2])<1e-6):
								IF_IN_DICT=True
								loc_body_flag=reg[3]
#							if (i==j and j==k and k==l):
#								IF_IN_DICT=True
#								loc_body_flag=reg[3]

						if not IF_IN_DICT:
							loc_body_flag=cp.copy(two_body_flag)
							dict_two_body.append([b,d,dij,loc_body_flag])
							two_body_flag+=1

#					if (i!=j or i!=k or i!=l):				
						print 	cr(i,s1)+",	"+an(k,s1)+",	"+cr(j,s2)+",	"+an(l,s2)+",	"+" 0.5,	"+str(loc_body_flag)+",	"\
							+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(i)+")"\
							+		",	"\
							+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(j)+")"\
							+		",	"\
							+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(k)+")"\
							+		",	"\
							+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(l)+")"\
							+       ";"
print "end_two_body"


print one_body_flag+two_body_flag

sys.exit()
