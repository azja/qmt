#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1.0,1.0,1.0));
   
   

   std::cout<<"I1 = "<<system->get_one_body_integral(0)<<std::endl;
   std::cout<<"I1 = "<<system->get_one_body_integral(2)<<std::endl;
   std::cout<<"I2 = "<<system->get_one_body_integral(3)<<std::endl;
   std::cout<<"I3 = "<<system->get_one_body_integral(4)<<std::endl;
 qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<32> ("ham3.dat",8U,8U,100000U);
    
// qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",1);

 for(auto i = 0U; i < 40U; ++i) {

    std::vector<double> one_body;
    std::vector<double> two_body;
   //1.083 is equilibrium for a = 4.0
    system->set_parameters(std::vector<double>({0.8+i*0.01}),qmt::QmtVector(1.0,1.0,1.0));
// std::cout<<"One body parameters#"<<system->get_one_body_number()<<std::endl;
   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
//     std::cout<<"int_one-body#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }

   for(auto i = 0; i < system->get_two_body_number();++i){
    auto integral =  system->get_two_body_integral(i);
//     std::cout<<integral<<std::endl;
   two_body.push_back(integral);
   
   }
//    std::cout<<"Policzył parametry:"<<system->get_two_body_number()<<std::endl;
//std::cout<<"Parameters computed..."<<std::endl;
  
	std::cout<<0.8 + i * 0.01<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/16<<std::endl;
   
}
 	
 delete diagonalizer_vmc_gsl;


delete system;
	return 0;
}
