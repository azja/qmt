#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>





int main(int argc,const char* argv[])
{
  
   double R = atof(argv[1]);
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1,1,R));
   
   double alpha0=1.19;//atof(argv[1]);;
 qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<4> ("ham.dat",1U,1U,20000);
    
qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);


for(int k = 0; k < 1;++k) {
double alpha = alpha0 + 0.01 * k;
    std::vector<double> one_body;
    std::vector<double> two_body;
    system->set_parameters(std::vector<double>({alpha}),qmt::QmtVector(1,1,R));
   for(auto i = 0; i < system->get_one_body_number();++i) {
    double integral = 0.0; 
         integral = system->get_one_body_integral(i);
//    if( i==8 || i==12)
         
    
//     std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }
   for(auto i = 0; i < system->get_two_body_number();++i){

    auto integral =  system->get_two_body_integral(i);
//    std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
   two_body.push_back(integral);
   
   }

	std::cout<<R<<" "<<alpha<<"  "<<diagonalizer_gsl->Diagonalize(one_body,two_body)/2 <<" "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/2<<std::endl;
	//	std::cout<<R<<" "<<alpha <<" "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/10<<std::endl;

}
 	
delete diagonalizer_vmc_gsl;
//delete system;
	return 0;
}
