#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"
#include "../../../headers/qmtsimulatedannealing.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
//  qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<16> ("ham_9_4N.dat",4U,4U,5000U);
   qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",1);


 for(auto i = 0U; i < 1U; ++i) {

  

auto F = [&](std::vector<double> v)->double {
    std::vector<double> one_body;
    std::vector<double> two_body;
   
    system->set_parameters(v,qmt::QmtVector(4.0,4.0,4.0));
 std::cout<<"One body parameters#"<<system->get_one_body_number()<<std::endl;
   for(auto i = 0; i < system->get_one_body_number();++i) {
     auto integral = system->get_one_body_integral(i);
     std::cout<<integral<<std::endl;
     one_body.push_back(integral);
   }
 std::cout<<"Two body parameters#"<<system->get_one_body_number()<<std::endl;
   for(auto i = 0; i < system->get_two_body_number();++i){
    auto integral = system->get_two_body_integral(i);
     std::cout<<integral<<std::endl;
   two_body.push_back(integral);
   
   }
//    std::cout<<"Policzył parametry:"<<system->get_two_body_number()<<std::endl;
std::cout<<"Parameters computed..."<<std::endl;

  double energy = diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/8; 
  
//  if(energy<-2.0) energy=0.0;

std::cout<<"E= "<<energy<<" Ry"<<std::endl;

  return energy;
  };
  
  qmt::QmtSimulatedAnnealing solver(1,1,std::function<double(std::vector<double>)>(F),std::vector<double> ({0.9}),std::vector<double>({1.5}),10.0,0.1);
  
  std::vector<double> params;
  double min_val;
  solver.run(100,0.01,min_val,params);
	std::cout<<"----------------------------------"<<std::endl;
	std::cout<<params[0]<<"  "<<min_val<<std::endl;
   
}
 
 delete system;	
 delete diagonalizer_vmc_gsl;

	return 0;
}
