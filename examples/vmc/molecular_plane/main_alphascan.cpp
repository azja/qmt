#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc,const char* argv[])
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(4.0,4.0,1.4251));
   
   double alpha=atof(argv[1]);;
//qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<16> ("ham.dat",4U,4U,15000U);
    
 qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",1);

for(int l = 0; l < 1; l++){
    alpha = alpha + l *0.025;
    std::vector<double> one_body;
    std::vector<double> two_body;
    system->set_parameters(std::vector<double>({alpha}),qmt::QmtVector(4.0,4.0,1.4251));
   for(auto i = 0; i < system->get_one_body_number();++i) {
    double integral = 0.0; 
         integral = system->get_one_body_integral(i);
//    if( i==8 || i==12)
         
    
     std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }
   for(auto i = 0; i < system->get_two_body_number();++i){

    auto integral =  system->get_two_body_integral(i);
    std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
   two_body.push_back(integral);
   
   }
	std::cout<<alpha<<"  "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/8<<std::endl;
 }

 	
delete diagonalizer_vmc_gsl;
delete system;
	return 0;
}
