#!/usr/bin/python

import math
import numpy
import sys

# lattice constant
a=5.0
theta=math.pi/2.0
R=1.43042
verX=numpy.array([1,0,0])
verY=numpy.array([0,1,0])
verZ=numpy.array([0,0,1])
vers=[verX,verY,verZ]
vers2D=[verX,verY]
latticeX=a*verX
latticeY=0
latticeZ=R*math.sin(theta)*verZ+R*math.cos(theta)*verX

# max Coordination Radius
# must be the same as in main program!
CoordRadMul=1
maxCoordRad=CoordRadMul*a

# super celllatticeX
SuperCell = [
[0,numpy.array([0,0,0]),0,0,0,0],
[1,latticeZ,0,0,1,1],
[2,latticeX,1,0,0,0],
[3,latticeX+latticeZ,1,0,1,1]
]

# how many nodes in one crystal direction
NodesInCrystalDirection=2

# Hopping cut-off
allowed_hoppings=[R,a,math.sqrt(a*a+R*R-2*a*R*math.cos(theta)),math.sqrt(a*a+R*R+2*a*R*math.cos(theta))]
hoppingCutOff=a+R

def length(vector):
	return math.sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2])
#length

def distance(atom1,atom2):
	return length(atom1[1]-atom2[1])
#	return math.sqrt(math.pow(atom1[1][0]-atom2[1][0],2)+math.pow(atom1[1][1]-atom2[1][1],2)+math.pow(atom1[1][2]-atom2[1][2],2))
#distance

def CellTrans(translation,normal_translation,Cell):
	newCell=[]
	for atom in Cell:
		newatom=[atom[0],atom[1]+translation,\
				atom[2]+normal_translation[0],atom[3]+normal_translation[1],atom[4]+normal_translation[2],atom[5]]
		newCell.append(newatom)
	return newCell
#translation

def ClearCell(Cell,referenceCell,cutoff):
	for i in range(len(Cell)-1,0-1,-1):
		IF_REMOVE = True
		for atom in referenceCell:
			if(distance(atom,Cell[i]) <= cutoff):
				IF_REMOVE = False
		if IF_REMOVE:
			Cell.pop(i)

def AllowToStayCell(Cell,referenceCell,MayStay):
	for i in range(len(Cell)-1,0-1,-1):
		IF_REMOVE = True
		for atom in referenceCell:
			for dist in MayStay:
				if(math.fabs(distance(atom,Cell[i])-dist) < 1e-05):
					IF_REMOVE = False
		if IF_REMOVE:
			Cell.pop(i)
				


# super cell with border
BorderSuperCell = []

for i in [-NodesInCrystalDirection,NodesInCrystalDirection]:
	BorderSuperCell+=CellTrans(i*latticeX,i*verX,SuperCell)

AllowToStayCell(BorderSuperCell,SuperCell,allowed_hoppings)

BorderSuperCellFile = open('jmol_bordercell.dat', 'w+')
BorderSuperCellFile.write(str(len(BorderSuperCell))+"\n\n") # for jmol
for atom in BorderSuperCell:
 	if atom[0]==0:
 		BorderSuperCellFile.write("Li ")
 	if atom[0]==1:
 		BorderSuperCellFile.write("Be ")
 	if atom[0]==2:
 		BorderSuperCellFile.write("B ")
 	if atom[0]==3:
 		BorderSuperCellFile.write("C ")
 	if atom[0]==4:
 		BorderSuperCellFile.write("N ")
 	if atom[0]==5:
 		BorderSuperCellFile.write("O ")
 	if atom[0]==6:
 		BorderSuperCellFile.write("F ")
 	if atom[0]==7:
 		BorderSuperCellFile.write("Ne ")
 	BorderSuperCellFile.write(str(atom[2])+" "+str(atom[3])+" "+str(atom[4])+"\n")
BorderSuperCellFile.close()


# mega cell
MegaCell=[] 
#MegaCell+= SuperCell

for i in range(-CoordRadMul/2-1, (CoordRadMul)/2+2):
	direction_bare=NodesInCrystalDirection*(i*verX)
	direction_dres=NodesInCrystalDirection*(i*a*verX)
	MegaCell+=CellTrans(direction_dres,direction_bare,SuperCell)

ClearCell(MegaCell,SuperCell,math.sqrt(maxCoordRad*maxCoordRad+a*a))
#ClearCell(MegaCell,[[0,numpy.array([R*0.5*math.cos(theta),0,R*0.5*math.sin(theta)]),0,0,0.5,0]],math.sqrt(maxCoordRad*maxCoordRad))
#ClearCell(MegaCell,[SuperCell[0],SuperCell[1]],math.sqrt(maxCoordRad*maxCoordRad+a*a))

SuperCellFile = open('supercell.dat', 'w+')
for atom in SuperCell:
	SuperCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
SuperCellFile.close()

MegaCellFile = open('megacell.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
MegaCellFile.close()

############################################################################################################################################################
# jmol Megacell
MegaCellFile = open('jmol_megacell.dat', 'w+')
MegaCellFile.write(str(len(MegaCell))+"\n\n") # for jmol
for atom in MegaCell:
 	if atom[0]==0:
 		MegaCellFile.write("Li ")
 	if atom[0]==1:
 		MegaCellFile.write("Be ")
 	if atom[0]==2:
 		MegaCellFile.write("B ")
 	if atom[0]==3:
 		MegaCellFile.write("C ")
 	if atom[0]==4:
 		MegaCellFile.write("N ")
 	if atom[0]==5:
 		MegaCellFile.write("O ")
 	if atom[0]==6:
 		MegaCellFile.write("F ")
 	if atom[0]==7:
 		MegaCellFile.write("Ne ")
 	MegaCellFile.write(str(atom[2])+" "+str(atom[3])+" "+str(atom[4])+"\n")
MegaCellFile.close()




############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################

# anihilator
def an(N,s):
	return str(N)+"a"+str(s)
# creator
def cr(N,s):
	return str(N)+"c"+str(s)
# number operator
def num(N,s):
	return cr(N,s)+",	"+an(N,s)

# spins
spin = ["up","down"]

# actual microscopic parameter flag

one_body_flag = 0
two_body_flag = 0

# dictionaries

one_body_dict = [ ]
two_body_dict = [ ]

# epsilon_a
print ""
print "one_body"
for itemS in SuperCell:
	for s in spin:
		print 	cr(itemS[0],s)+",	"+an(itemS[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"
print "end_one_body"


one_body_dict.append([0, 0.0])
one_body_flag+=1

# t in SuperCell
print ""
print "one_body"
for i in range(0,len(SuperCell)):
	for j in range(i+1,len(SuperCell)):
		dij=distance(SuperCell[i],SuperCell[j])
		if (i==j or dij > hoppingCutOff):
			continue

		if_exist= False
		for entry in one_body_dict:
			if (math.fabs(entry[1]-dij)<1e-5):
				if_exist= True
				local_flag = entry[0]
		
		if not if_exist:
			local_flag=one_body_flag
			one_body_dict.append([local_flag, dij])
			one_body_flag+=1


		for s in spin:
			print cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+" +,	"+str(local_flag)+",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		";"
			print cr(SuperCell[j][0],s)+",	"+an(SuperCell[i][0],s)+",	"+" +,	"+str(local_flag)+",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		";"
		
print "end_one_body"

# t with border
print ""
print "one_body"
for itemS in SuperCell:
	for itemB in BorderSuperCell:
		dij=distance(itemS,itemB)
		if (dij<1e-16 or dij > hoppingCutOff):
			continue

		if_exist= False
		for entry in one_body_dict:
			if (math.fabs(entry[1]-dij)<1e-5):
				if_exist= True
				local_flag = entry[0]
		
		if not if_exist:
			local_flag=one_body_flag
			one_body_dict.append([local_flag, dij])
			one_body_flag+=1

		for s in spin:
			print cr(itemS[0],s)+",	"+an(itemB[0],s)+",	"+" +,	"+str(local_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemB[2])+",  "+str(itemB[3])+",  "+str(itemB[4])+",  "+str(itemB[5])+")"\
		+		";"

print "end_one_body"


# U
print ""
print "two_body"
for itemS in SuperCell:
	print num(itemS[0],spin[0])+",	"+num(itemS[0],spin[1])+",	"+" +,	"+str(two_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"
print "end_two_body"


two_body_dict.append([0, 0.0])
two_body_flag+=1

# K
print ""
print "two_body"
for itemS in SuperCell:
	for itemM in MegaCell:
		dij=distance(itemS,itemM)
		if (dij<1e-16):
			continue

		if_exist= False
		for entry in two_body_dict:
			if (math.fabs(entry[1]-dij)<1e-5):
				if_exist= True
				local_flag = entry[0]
		
		if not if_exist:
			local_flag=two_body_flag
			two_body_dict.append([local_flag, dij])
			two_body_flag+=1

		for s1 in spin:
			for s2 in spin:
				print num(itemS[0],s1)+",	"+num(itemM[0],s2)+",	"+" 0.5,	"+str(local_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemM[2])+",  "+str(itemM[3])+",  "+str(itemM[4])+",  "+str(itemM[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemM[2])+",  "+str(itemM[3])+",  "+str(itemM[4])+",  "+str(itemM[5])+")"\
		+       ";"
print "end_two_body"

print two_body_flag+one_body_flag

sys.exit()
