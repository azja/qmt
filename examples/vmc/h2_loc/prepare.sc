#!/bin/bash


for i in {0..40} 
do
     x=$(echo $i | awk '{print 1 + $1 * 0.1}')
     dir=${x}
     cat fit.sc > f$dir.sc
     sed -i "s/XXX/${x}.dat/g" f$dir.sc
     sed -i "s/YYY/out_${x}.dat/g" f$dir.sc
     sed -i "s/UUU/${x}/g" f$dir.sc
     gnuplot f$dir.sc
     cat "out_${x}.dat" >> h2_R_vmc.dat
     rm *.log
     rm f$dir.sc
     rm out_${x}.dat
   done

