#!/usr/bin/python

import math
import numpy as np
import sys
import copy as cp

def length(vector):
	return math.sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2])
#length

def distance(atom1,atom2):
	return length(atom1[2]-atom2[2])
#distance


def distanceX(atom1,atom2):
	return math.fabs(atom1[2][0] - atom2[2][0])

NumberOrbitals=1

#a=4.0
#b=4.0
#R=1.43042

a=1.6
b=1.6
R=2.4

a=float(sys.argv[1])
b=float(sys.argv[2])

if(math.fabs(a-R) < 1.0e-6):
	a = a + 0.000001

verX=np.array([1,0,0])
verY=np.array([0,0,0])
verZ=np.array([0,0,0])
vers=[verX,verY,verZ]
latticeX=(a)*verX
#latticeY=b*verY
#latticeZ=R*verZ

SuperCell=[
[0,np.array([0,0,0]),np.array([0.0,0.0,0.0]),0],
[1,np.array([1,0,0]),np.array([b,0,0]),1],
[0,np.array([2,0,0]),np.array([a,0,0]),2],
[1,np.array([3,0,0]),np.array([a+b,0,0]),3],
[0,np.array([4,0,0]),np.array([2*a,0,0]),4],
[1,np.array([5,0,0]),np.array([2*a+b,0,0]),5],
[0,np.array([6,0,0]),np.array([3*a,0,0]),6],
[1,np.array([7,0,0]),np.array([3*a+b,0,0]),7],
[0,np.array([8,0,0]),np.array([4*a,0,0]),8],
[1,np.array([9,0,0]),np.array([4*a+b,0,0]),9],
[0,np.array([10,0,0]),np.array([5*a,0,0]),10],
[1,np.array([11,0,0]),np.array([5*a+b,0,0]),11],
[0,np.array([12,0,0]),np.array([6*a,0,0]),12],
[1,np.array([13,0,0]),np.array([6*a+b,0,0]),13]
]

# directions of PBC
PBCdirections=[
7*latticeX,
#8*latticeY
#latticeY*math.sin(math.pi/3)+latticeX*math.cos(math.pi/3)
]

# vers of PBC
PBCvers=[
7*verX,
#8*verY
#(latticeY*math.sin(math.pi/3)+latticeX*math.cos(math.pi/3))/length(latticeY*math.sin(math.pi/3)+latticeX*math.cos(math.pi/3))
]

# cut-off
#CutOff=math.sqrt(a*a+b*b+R*R)
#CutOff=math.sqrt(2*a*2*a+2*b*2*b+R*R)
#CutOff=math.sqrt(100*a*a+R*R)
CutOff=2*a + 0.001

#OChopping CutOff
hoppingCutOff=2*a + 0.001

PBCsizes=[]
for PBCd in PBCdirections:
	PBCsizes.append(int(CutOff/length(PBCd)))
CutOffUnits=max(PBCsizes)


def CellTrans(translation,normal_translation,Cell):
	newCell=[]
	for atom in Cell:
		if(math.fabs(normal_translation[1]) < 1.0e-6 and math.fabs(translation[1]) < 1.0e-6):
			newatom=[atom[0],atom[1]+normal_translation,atom[2]+translation,atom[3]]
			newCell.append(newatom)
	return newCell
#translation

def ClearCell(Cell,referenceCell,cutoff):
	for i in range(len(Cell)-1,0-1,-1):
		IF_REMOVE = True
		for atom in referenceCell:
			if(distance(atom,Cell[i]) <= cutoff or math.fabs(atom[2][1]) < 1.0e-6):
				IF_REMOVE = False
		if IF_REMOVE:
			Cell.pop(i)

# mega cell
MegaCell=[] 
BorderCell=[]
#MegaCell+= SuperCell


for i in range(-CutOffUnits-3, CutOffUnits+3):
		direction_bare=(i*PBCvers[0])
		direction_dres=(i*PBCdirections[0])
		BorderCell+=CellTrans(direction_dres,direction_bare,SuperCell)

ClearCell(BorderCell,SuperCell,hoppingCutOff)

for i in range(-CutOffUnits-3, CutOffUnits+3):
		direction_bare=(i*PBCvers[0])
		direction_dres=(i*PBCdirections[0])
		MegaCell+=CellTrans(direction_dres,direction_bare,SuperCell)

ClearCell(MegaCell,SuperCell,CutOff)


#MegaCell=cp.copy(SuperCell)

SuperCellFile = open('supercell.dat', 'w+')
for atom in SuperCell:
	SuperCellFile.write("("+str(atom[2][0])+",  "+str(atom[2][1])+",  "+str(atom[2][2])+",  "+str(atom[0])+");\n")
SuperCellFile.close()

#MegaCellFile = open('megacell.dat', 'w+')
#for atom in MegaCell:
#	MegaCellFile.write("("+str(atom[1][0])+",  "+str(atom[1][1])+",  "+str(atom[1][2])+",  "+str(atom[0])+");\n")
#MegaCellFile.close()

BorderCellFile = open('bordercell.dat', 'w+')
for atom in BorderCell:
	BorderCellFile.write("("+str(atom[2][0])+",  "+str(atom[2][1])+",  "+str(atom[2][2])+",  "+str(atom[0])+");\n")
BorderCellFile.close()

MegaCellFile = open('megacell_orbital.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write(str(atom[3])+", ")
MegaCellFile.close()

# anihilator
def an(N,s):
	return str(N)+"a"+str(s)
# creator
def cr(N,s):
	return str(N)+"c"+str(s)
# number operator
def num(N,s):
	return cr(N,s)+",	"+an(N,s)

# spins
spin = ["up","down"]


# actual microscopic parameter flag

one_body_flag = 0
two_body_flag = 0


dict_one_body=[]
#print ""
#print "one_body"
#for atom1 in SuperCell:
#	for b in range(NumberOrbitals):
#		for atom2 in SuperCell:
#			for d in range(NumberOrbitals):
#				i = NumberOrbitals*atom1[3]+b
#				j = NumberOrbitals*atom2[3]+d
#	
#				dij=distance(atom1,atom2)

#				if (distance(atom1,atom2) > hoppingCutOff):
#					continue

#				IF_IN_DICT=False
#				for reg in dict_one_body:
#					if ( b==reg[0] and d==reg[1] and abs(dij-reg[2])<1e-6):
#						IF_IN_DICT=True
#						loc_body_flag=reg[3]

#				if not IF_IN_DICT:
#					loc_body_flag=cp.copy(one_body_flag)
#					dict_one_body.append([b,d,dij,loc_body_flag])
#					one_body_flag+=1

#				for s in spin:
#					print 	cr(i,s)+",	"+an(j,s)+",	"+" +,	"+str(loc_body_flag)+",	"\
#					+		"("+str(atom1[1][0])+",  "+str(atom1[1][1])+",  "+str(atom1[1][2])+",  "+str(atom1[0])+")"\
#					+		",	"\
#					+		"("+str(atom2[1][0])+",  "+str(atom2[1][1])+",  "+str(atom2[1][2])+",  "+str(atom2[0])+")"\
#					+       ";"
#print "end_one_body"

#print ""
print "one_body"
for atom1 in SuperCell:
	for b in range(NumberOrbitals):
		for atom2 in BorderCell:
			for d in range(NumberOrbitals):
				i = NumberOrbitals*atom1[3]+b
				j = NumberOrbitals*atom2[3]+d

				dij=distance(atom1,atom2)

				if (distance(atom1,atom2) > hoppingCutOff):
					continue

				IF_IN_DICT=False
				for reg in dict_one_body:
					if ( b==reg[0] and d==reg[1] and abs(dij-reg[2])<1e-6):
						IF_IN_DICT=True
						loc_body_flag=reg[3]

				if not IF_IN_DICT:
					loc_body_flag=cp.copy(one_body_flag)
					dict_one_body.append([b,d,dij,loc_body_flag])
					one_body_flag+=1

				if i==j:
                                        coeff=1.0
                                else:
                                        coeff=1.0

				for s in spin:
					print 	cr(i,s)+",	"+an(j,s)+",	"+" "+str(coeff)+",	"+str(loc_body_flag)+",	"\
					+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(atom1[0])+")"\
					+		",	"\
					+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(atom2[0])+")"\
					+       ";"
print "end_one_body"


dict_two_body=[]
## Only U and K
print ""
print "two_body"
for atom1 in SuperCell:
	for b in range(NumberOrbitals):
		for atom2 in MegaCell:
			for d in range(NumberOrbitals):
				i = NumberOrbitals*atom1[3]+b
				j = NumberOrbitals*atom2[3]+d
				k = NumberOrbitals*atom1[3]+b
				l = NumberOrbitals*atom2[3]+d
				for s1 in spin:
					for s2 in spin:

						dij = distance(atom1,atom2)
		
						if (distance(atom1,atom2) > CutOff):
							continue

						if i==j and i==k and i==l and s1==s2 and abs(dij)<1e-6: # U can't be for same spins
							continue

						IF_IN_DICT=False
						for reg in dict_two_body:
							if ( b==reg[0] and d==reg[1] and abs(dij-reg[2])<1e-6):
								IF_IN_DICT=True
								loc_body_flag=reg[3]

						if not IF_IN_DICT:
							loc_body_flag=cp.copy(two_body_flag)
							dict_two_body.append([b,d,dij,loc_body_flag])
							two_body_flag+=1

										
						print 	cr(i,s1)+",	"+cr(j,s2)+",	"+an(l,s2)+",	"+an(k,s1)+",	"+" 0.5,	"+str(loc_body_flag)+",	"\
							+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(atom1[0])+")"\
							+		",	"\
							+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(atom2[0])+")"\
							+		",	"\
							+		"("+str(atom1[2][0])+",  "+str(atom1[2][1])+",  "+str(atom1[2][2])+",  "+str(atom1[0])+")"\
							+		",	"\
							+		"("+str(atom2[2][0])+",  "+str(atom2[2][1])+",  "+str(atom2[2][2])+",  "+str(atom2[0])+")"\
							+       ";"
print "end_two_body"


#print ""
print two_body_flag+one_body_flag
#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
#########################################################################################################################

#CHECKER
# jmol obital indx=indx
#indx=0
#ObitalFile = open('jmol_orbital.dat', 'w+')
#ObitalFile.write(str(len(finalBasis[indx]))+"\n\n") # for jmol
#for atom in finalBasis[indx]:
# 	ObitalFile.write("Ca ")
# 	ObitalFile.write(str(atom[2][0])+" "+str(atom[2][1])+" "+str(atom[2][2])+"\n")
#ObitalFile.close()

sys.exit()


