#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>





int main(int argc,const char* argv[])
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   double a = atof(argv[1]);;
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(a,0,0));
   
   double alpha0= 0.9;
    
 qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<84> ("ham.dat",21U,21U,60000U);
 auto &stat = (static_cast<qmt::vmc::QmtVmcGslMetropolisDiagonalizer<84>*>(diagonalizer_gsl))->get_stat();
std::cout<<" alpha "<<" E_tot ";
auto avs_names = stat.get_names();
for(const auto item : avs_names)
std::cout<<" "<<item;

std::cout<<std::endl;
//qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);


for(int k = 0; k < 20;++k) {
double alpha = alpha0 + 0.05 * k;
    std::vector<double> one_body;
    std::vector<double> two_body;
    system->set_parameters(std::vector<double>({alpha}),qmt::QmtVector(a,0,0));
   for(auto i = 0; i < system->get_one_body_number();++i) {
    double integral = 0.0; 
         integral = system->get_one_body_integral(i);
//    if( i==8 || i==12)
         
    
//     std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
     one_body.push_back(integral);
   }
   for(auto i = 0; i < system->get_two_body_number();++i){

    auto integral =  system->get_two_body_integral(i);
//    std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
   two_body.push_back(integral);
   
   }

	std::cout<<alpha<<"  "<<diagonalizer_gsl->Diagonalize(one_body,two_body)/42<<" ";
        for(const auto item : stat.get_values())
           std::cout<<" "<<item;

        std::cout<<std::endl;

}
 	
delete diagonalizer_gsl;
delete system;
	return 0;
}
