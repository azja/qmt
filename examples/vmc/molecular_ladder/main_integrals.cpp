#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define SIZE 100
#define NEL 50


int main(int argc,const char* argv[])
{
  
   qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
   system->set_parameters(std::vector<double>({1.0}),qmt::QmtVector(1.0,1.0,1.0));

qmt::QmtVector scale(1,1,1);


//std::cout<<scale<<std::endl<<std::endl;
    
//qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);


double alpha_0=atof(argv[1]);


    std::vector<double> one_body;
    std::vector<double> two_body;
    system->set_parameters(std::vector<double>({alpha_0}),scale);

//  for(auto i = 0; i < system->get_one_body_number();++i) 
//  std::cout<<"_"<<i<<" ";
  
//    for(auto i = 0; i < system->get_two_body_number();++i) 
//  std::cout<<"two_"<<i<<" ";

  
//  std::cout<<std::endl;
  
//  std::cout<<system->get_overlap(0,1,qmt::QmtVector(0,0,0),qmt::QmtVector(0.953868939201,0,0))<<std::endl;
  //exit(0);
  
   for(auto i = 0; i < system->get_one_body_number();++i) {
    double integral = 0.0; 
         integral = system->get_one_body_integral(i);
   std::cout<<integral<<" ";
//     one_body.push_back(integral);
   }
   std::cout<<std::endl;
exit(0);
   for(auto i = 0; i < system->get_two_body_number();++i){

    auto integral =  system->get_two_body_integral(i);
   std::cout<<" "<<integral;
    
//    two_body.push_back(integral);
   
   }
   
   std::cout<<std::endl;

delete system;
	return 0;
}
