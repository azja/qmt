#!/usr/bin/python
import cmath
import math
import sys
import numpy as np
from scipy import interpolate
from scipy.optimize import minimize

a=float(sys.argv[1])
R=float(sys.argv[2])



t0=float(sys.argv[3])
t1=float(sys.argv[4])
t2=float(sys.argv[5])
t3=float(sys.argv[6])
t4=float(sys.argv[7])
t5=float(sys.argv[8])

#a=complex(1,2)
#R=complex(1,3)


def Z(k):
    e1 = complex(0,k*(R-a))
    e2 = complex(0,k*(R-2*a))
    e3 = complex(0,k*(R))
    e4 = complex(0,k*(a+R))
    return t3*cmath.exp(e1) + t1*cmath.exp(e2)+t4*cmath.exp(e3)+t5*cmath.exp(e4)
    
def epsilon(k):
    return 2*(t2*math.cos(k*a)+t0*math.cos(2*k*a))


def down_f(k):
    return epsilon(k)-abs(Z(k))

def atomic_f(k):
    return  epsilon(k) + ((Z(k) + np.conj(Z(k))).real)/2


m = 100
deltak = math.pi/m;
lambda1 =[]
lambda2 = []
lambda3=[]
ks = []
for i in range(-5*m,5*m):
	k=-math.pi/(a) + i * deltak
	ks.append(k)
	lambda1.append(epsilon(k) + abs(Z(k)))

	lambda2.append(epsilon(k) - abs(Z(k)))
	lambda3.append( epsilon(k) + ((Z(k) + np.conj(Z(k))).real)/2)


#minimum of lower band

#k0=k1-2*deltak
#k2=k2+2*deltak


if abs(a/2-R) > 1.0e-2:
    k1=ks[np.argmin(lambda2)]
else:
    k1=ks[np.argmin(lambda3)]


if abs(a/2-R) > 1.0e-2:
    res_min = minimize(down_f, k1, tol=1e-6)
else:
   res_min = minimize(atomic_f, k1, tol=1e-6)
#rint np.argmin(lambda2),min(lambda2),res.x[0],down_f(res_min.x[0])

#maksimu of lower band


if abs(a/2-R) > 1.0e-2:
    k1=ks[np.argmax(lambda2)]
else:
    k1=ks[np.argmax(lambda3)]

if abs(a/2-R) > 1.0e-2:
    res_max = minimize(lambda k: -down_f(k), k1,tol=1e-6)
else:
    res_max = minimize(lambda k: -atomic_f(k), k1,tol=1e-6)
    
if abs(a/2-R) > 1.0e-2:
    w_min = down_f(res_min.x[0])
    w_max = down_f(res_max.x[0])
else:
    w_min = atomic_f(res_min.x[0])
    w_max = atomic_f(res_max.x[0])

#print min(lambda2),max(lambda2),res_min.x[0],w_min,res_max.x[0], w_max, abs(w_max - w_min)

print a,R,abs(w_max - w_min)