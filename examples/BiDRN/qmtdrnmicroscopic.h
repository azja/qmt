#ifndef QMT_DRN_MICROSCOPIC_H_INCLUDED
#define QMT_DRN_MICROSCOPIC_H_INCLUDED

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "../../headers/qmthbuilder.h"
#include "../../headers/qmtparsertools.h"
#include "qmtprimedict.h"



/*********************************************************************/

#define NDIM 6
#define NCOMP 1

#define NVEC 1
#define EPSREL 1e-4
#define EPSABS 1e-4
#define VERBOSE 0
#define LAST 4
#define SEED 1
#define MINEVAL 1000000
#define MAXEVAL 2000000

#define NSTART 10000
#define NINCREASE 500
#define NBATCH 1000
#define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

#define NNEW 40000
#define NMIN 32000
#define FLATNESS 5

#define KEY1 3
#define KEY2 3
#define KEY3 1
#define MAXPASS 200000
#define BORDER 0.
#define MAXCHISQ 0.5
#define MINDEVIATION .25
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 9

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif
/*********************************************************************/

namespace qmt {

  const double nm_a0 = 18.8971616463;
  
template < typename Hamiltonian, typename WaveFunction, typename Potential>
class QmtDrnMicroscopic{
    std::string hamiltonian_fn;
    std::string potential_fn;
    std::vector<qmt::one_body_integral_v> one_body_intgs;
    std::vector<double> one_body_table;
    std::vector<qmt::two_body_integral_v> two_body_intgs;
    std::vector<std::tuple<bool,double>> computed_tb;
//    std::vector<std::tuple<double**,int,int>> wave_functions;
    Potential* potential;
    std::vector<WaveFunction> wave_functions;
    qmt::QmtVector max;
    qmt::QmtVector min;
    std::vector<int> integrals_mapped;    
    int ida,idb,idc,idd;
    bool read_integral_flag;
    std::vector<double> integrals_list;
    std::vector<double> variance_list;
    
  
    void LoadDataFromFile(const std::string& configuration_file) {
      std::ifstream cfile;      
#ifdef _QMT_VERBOSE
      std::cout<<"QmtDRNMicroscopic::LoadDataFromFile: Opening configuration file:"<<configuration_file<<" ...";
#endif

      cfile.open(configuration_file.c_str());
      
if(!cfile.fail()) { 
#ifdef _QMT_VERBOSE
     std::cout<<"OK"<<std::endl;
#endif
      std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
   
     hamiltonian_fn =  qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
     potential_fn =  qmt::parser::get_bracketed_words(content,"potential_file","potential_file_end")[0];
     auto wfs_def_strings =  qmt::parser::get_bracketed_words(content,"wfn","wfn_end");
  
     auto dimmax_strings =  qmt::parser::get_bracketed_words(content,"dim_max","dim_max_end");
     auto dimmin_strings =  qmt::parser::get_bracketed_words(content,"dim_min","dim_min_end");
     
     auto dimmax_string_numbers = qmt::parser::get_delimited_words(",",dimmax_strings[0]);
     auto dimmin_string_numbers = qmt::parser::get_delimited_words(",",dimmin_strings[0]);
     
     max = qmt::QmtVector(std::stod(dimmax_string_numbers[0]),std::stod(dimmax_string_numbers[1]),std::stod(dimmax_string_numbers[2]));
     min = qmt::QmtVector(std::stod(dimmin_string_numbers[0]),std::stod(dimmin_string_numbers[1]),std::stod(dimmin_string_numbers[2]));
     
#ifdef _QMT_VERBOSE
     std::cout<<"MAX dimensions:"<<max<<std::endl;
     std::cout<<"MIN dimensions:"<<min<<std::endl;
#endif      
     
     
     std::vector<std::tuple<std::string,int,int,int>> wfs_defs;
     
     std::string n_oc = qmt::parser::get_bracketed_words(content,"states_number","states_number_end")[0];
     int number_of_centers = std::stoi(n_oc);
     int cntr = 0;
     for(const auto & def : wfs_def_strings){
      std::vector<std::string> info = qmt::parser::get_delimited_words(",",def);
      if(cntr < number_of_centers)
       wfs_defs.push_back(std::make_tuple(info[0],std::stoi(info[1]),std::stoi(info[2]),std::stoi(info[3])));
      cntr++;
     }

#ifdef _QMT_VERBOSE
     double  memory = 0;
     for(const auto &def : wfs_defs)
        memory += std::get<1>(def)*std::get<2>(def)*sizeof(double)/(1024 * 1024);
     if(wfs_defs.size() > 0)
       std::cout<<"Single particle wave-function(s) number: "<<wfs_defs.size()<<std::endl;//<<" Estimated total memory consumption:"<<memory<<"MB"<<std::endl;
     
     int wfs_cntr = 1;
#endif
     
     for(const auto &def : wfs_defs) {

#ifdef _QMT_VERBOSE     
      std::cout<<"Memory allocation and reading for file:"<<std::get<0>(def)<<" "<<wfs_cntr<<"|"<<wfs_defs.size()<<"...";
      wfs_cntr++;
#endif       
       /*
      double** M = new double * [std::get<1>(def)];
       for(int i = 0; i < std::get<1>(def); ++i)
	 M[i] = new double [std::get<2>(def)];

      std::ifstream wf(std::get<0>(def).c_str());
       if (wf.is_open())
       {
        for(int i = 0; i <std::get<1>(def);++i){
	  for(int j = 0; j < std::get<2>(def);++j){
	    double x,y,v;
	    wf>>x>>y>>v;
	    M[i][j] = v;
	    
	  }	    
	}
	wf.close();
#ifdef _QMT_VERBOSE     
      std::cout<<"OK"<<std::endl;
#endif
       }*/
//	wave_functions.push_back(std::make_tuple(M,std::get<1>(def),std::get<2>(def)));
        std::cerr<<"size:"<<std::get<1>(def)<<" l ="<<std::get<3>(def)<<" d_z = "<<max.get_z() - min.get_z()<<std::endl;
       	wave_functions.push_back(WaveFunction(std::get<0>(def).c_str(),std::get<1>(def),std::get<2>(def),std::get<3>(def),max.get_z() - min.get_z()));
     }

    std::string eps_string =  qmt::parser::get_bracketed_words(content,"epsilon_file","epsilon_file_end")[0];
    std::ifstream eps_file;      
    eps_file.open(eps_string.c_str());
    while(!eps_file.eof()){
      int n,l;
      double eps;
      eps_file>>n>>l>>eps;
      one_body_table.push_back(eps);
    }
 
    auto intg_load_file = qmt::parser::get_bracketed_words(content,"integrals_file","integrals_file_end");
    if(intg_load_file.size() > 0) {
     read_integral_flag= true;
     std::ifstream integrals_file;      
     integrals_file.open(intg_load_file[0].c_str());
    std::string content_load( (std::istreambuf_iterator<char>(integrals_file) ),
                       (std::istreambuf_iterator<char>()    ) );
     auto integrals_strings = qmt::parser::get_bracketed_words(content_load,"]#","#}");
     //std::cout<<"Content load:"<<content_load<<std::endl;
     for(const auto &v : integrals_strings){
      integrals_list.push_back(std::stod(v));
    }
     integrals_file.close();
     std::cout<<"Read "<<integrals_list.size()<<" from "<< intg_load_file[0]<<std::endl;
    }
    else 
     read_integral_flag = false;

}
#ifdef _QMT_VERBOSE
 //    else 
    //     std::cout<<"failed"<<std::endl;
#endif     

    
     }
    

    

public: 
	QmtDrnMicroscopic(const std::string& configuration_file){
		
	  LoadDataFromFile(configuration_file);

#ifdef _QMT_VERBOSE
          std::cout<<"Getting microscopic parameter definitions...";
#endif     
	  
	  one_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getOneBodyIntegralsV(
					hamiltonian_fn);                
	   two_body_intgs =
			qmt::QmtHamiltonianBuilder<Hamiltonian>::getTwoBodyIntegralsV(
					 hamiltonian_fn);
#ifdef _QMT_VERBOSE
          std::cout<<"done"<<std::endl;
#endif     


    potential = new Potential(potential_fn.c_str(),397,max.get_z() - min.get_z());
	  
 std::vector<std::tuple<qmt::QmtIntegralCode,int>> coded_intgs;

int i_index = 0;

variance_list = std::vector<double>(two_body_intgs.size(),0.0);

for(const auto& tb : two_body_intgs) {
 computed_tb.push_back(std::make_tuple(false,0.0));
 int a = (std::get < 0 > (tb)).id;
 int b = (std::get < 1 > (tb)).id;
 int c = (std::get < 2 > (tb)).id;
 int d = (std::get < 3 > (tb)).id;
 auto code = qmt::QmtIntegralCode(wave_functions[a]._n,wave_functions[b]._n,wave_functions[c]._n,wave_functions[d]._n,
 wave_functions[a]._l,wave_functions[b]._l,wave_functions[c]._l,wave_functions[d]._l);
 auto code_intg = std::make_tuple(code,i_index);
 int find_index = -1;
 

std::vector<int> tst_v({a,b,c,d });//
std::sort(tst_v.begin(),tst_v.end());//
auto it = std::unique(tst_v.begin(),tst_v.end());//
if(int(it - tst_v.begin()) <= 0)//
  for(int i = 0; i < coded_intgs.size(); ++i) {
    if(code == std::get<0>(coded_intgs[i])) {
      find_index = i; break;
    }
  }
 
 if(find_index < 0){
   coded_intgs.push_back(code_intg);
   integrals_mapped.push_back(find_index);
//    std::cout<<" index = "<<find_index<<std::endl;
 }
 else {
   integrals_mapped.push_back(std::get<1>(coded_intgs[find_index]));
//   std::cout<<" index = "<</*find_index*/std::get<1>(coded_intgs[find_index])<<std::endl;
  }
  i_index++;
}  
		
        }


private:
	

public:
	void show_two_body_code(int index) const {
		std::cout<<"["<<std::get < 0 > (two_body_intgs[index]).id<<", "<< std::get < 1 > (two_body_intgs[index]).id
		<<", "<<std::get < 2 > (two_body_intgs[index]).id<<", "<< std::get < 3 > (two_body_intgs[index]).id<<"]";
	}


	void show_one_body_code(int index) const {
		std::cout<<"["<<std::get < 0 > (one_body_intgs[index]).id<<", "<< std::get < 1 > (one_body_intgs[index]).id<<"]";
	}

	/*
 	/ ONE BODY INTEGRALS
	*/

	double get_one_body(size_t index) const {
          
		if (index >= one_body_intgs.size()) {
		std::cerr
				<< "QmtDRNMicroscopic::get_one_body: index exceeding the number of one-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 2 > (one_body_intgs[index])) {
		std::cerr
				<< "QmtDRNMicroscopic::get_one_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

	
         return one_body_table[index];
        }
 	
	/*
 	/ TWO BODY INTEGRALS
	*/

	double get_two_body(size_t index)  { 
        if (index >= two_body_intgs.size()) {
		std::cerr
				<< "QmtDRNMicroscopic::get_two_body: index exceeding the number of two-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 4 > (two_body_intgs[index])) {
		std::cerr
				<< "QmtDRNMicroscopic::get_two_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

//       if(read_integral_flag)
//       return integrals_list[index];

	if(std::get<0>(computed_tb[index])) return std::get<1>(computed_tb[index]);
	
	if(integrals_mapped[index] >= 0) {	  
	  std::cout<<"Takin:"<<integrals_mapped[index]<<" ";
	 return get_two_body(integrals_mapped[index]);          	 
	}
	
	ida = std::get < 0 > (two_body_intgs[index]).id;
	idb = std::get < 1 > (two_body_intgs[index]).id;
	idc = std::get < 2 > (two_body_intgs[index]).id;
	idd = std::get < 3 > (two_body_intgs[index]).id;
	

std::vector<int> v({ida,idb,idc,idd });
std::sort(v.begin(),v.end());
auto it = std::unique(v.begin(),v.end());
//if(int(it - v.begin()) > 2 ) return 0;
//if(int(it - v.begin()) == 3) return 0;

       if(read_integral_flag)
       return integrals_list[index];



  int  nregions, neval, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

double val = 0;
const int N =  1;
/*
for(int i = 0; i < N;++i) {
 Vegas(NDIM, NCOMP, Integrand, this, NVEC,
     EPSREL, EPSABS, VERBOSE, time(NULL),
     MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH,
     GRIDNO, STATEFILE, SPIN,
    &neval, &fail, integral, error, prob);
    val += (double)integral[NCOMP-1];
}
*/

std::vector<double> to_calculate_variance;

for(int i = 0; i < N;++i) {
	 Suave(NDIM, NCOMP, Integrand, this, NVEC,
    EPSREL, EPSABS, VERBOSE | LAST,time(NULL),
    MINEVAL, MAXEVAL, NNEW, NMIN, FLATNESS,
    STATEFILE, SPIN,
    &nregions, &neval, &fail, integral, error, prob);
    val += (double)integral[NCOMP-1]; 
    to_calculate_variance.push_back((double)integral[NCOMP-1]);
}


    /*
    Divonne(NDIM, NCOMP, Integrand, this, NVEC,
    EPSREL, EPSABS, VERBOSE, SEED,
    MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
    BORDER, MAXCHISQ, MINDEVIATION,
    NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
    STATEFILE, SPIN,
        &nregions, &neval, &fail, integral, error, prob);*/
                              
/*
  Cuhre(NDIM, NCOMP, Integrand,this, NVEC,
    EPSREL, EPSABS, VERBOSE | LAST,
    MINEVAL, MAXEVAL, KEY,
    STATEFILE, SPIN,
    &nregions, &neval, &fail, integral, error, prob);*/
//    val += (double)integral[NCOMP-1]; 
	 double result = val/N;//(double)integral[NCOMP-1];
/*
	double variance=0.0;
	for(const auto& elem : to_calculate_variance){

		variance+=pow(result-elem,2);
	}

	variance /= N*(N-1);

	to_calculate_variance.clear();

	variance_list[index]=sqrt(variance);*/
 /* printf("SUAVE RESULT:\tnregions %d\tneval %d\tfail %d\n",
    nregions, neval, fail);
  for( comp = 0; comp < NCOMP; ++comp )
    printf("SUAVE RESULT:\t%.8f +- %.8f\tp = %.3f\n",
      (double)integral[comp], (double)error[comp], (double)prob[comp]); */

std::cout<<"Integrand calculation fails:"<<fail<<std::endl;    	
         std::get<0>(computed_tb[index]) = true;
	 std::get<1>(computed_tb[index]) = result;
 
	return result;// in meV;
        }
       
	double get_coulomb_repulsion() const {
        	return 0;
	}

	size_t number_of_one_body() const {
		return one_body_intgs.size();
        }

	size_t number_of_two_body() const {
		return two_body_intgs.size();
        }

	void set_parameters(const std::vector<double>& params, const  qmt::QmtVector& v = qmt::QmtVector(1,1,1)) {
	  
	}

	void set_parameters(double param, const  qmt::QmtVector& v = qmt::QmtVector(1,1,1)) {
	  
	}

	double get_variance(size_t index){
        if (index >= two_body_intgs.size()) {
		std::cerr
				<< "QmtDRNMicroscopic::get_variance: index exceeding the number of two-body parameters"
				<< std::endl;
		return 0;
	}	
	if(!std::get<0>(computed_tb[index])){
		std::cerr
				<< "QmtDRNMicroscopic::get_variance: integral not yet calculated"
				<< std::endl;
		return 0;
	}
		return variance_list[index];
	}
	


	~QmtDrnMicroscopic() {
/*       
   for(auto &wf : wave_functions) {
	       for(int i = 0; i < std::get<1>(wf); ++i)
		 delete [] std::get<0>(wf)[i];
	       delete [] std::get<0>(wf);
	  }
*/	    
        }
        
private:
  
   double func(double x, double y, double z, double xp, double yp, double zp,int a,int b,int c, int d) {
    
/*Cartesian      
*/
     
     double x1 = (-280.0 + x * (140.0 + 280.0));//*nm_a0;
     double x2 = (-280.0 + xp * (140.0 + 280.0));//*nm_a0;
     
     double y1 = (min.get_y() + y * (max.get_y() - min.get_y()));//*nm_a0;
     double y2 = (min.get_y() + yp * (max.get_y() - min.get_y()));//*nm_a0;
     
     double z1 = (min.get_z() + z * (max.get_z() - min.get_z()));//*nm_a0;
     double z2 = (min.get_z() + zp * (max.get_z() - min.get_z()));//*nm_a0;
     
     
     double distance = sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2));
     
     const double threshold = 1.0e-9;
     
     if( distance < threshold) distance = threshold;
     
     double eV = 1.43996449;

     double h2_over_2m = 568.654082104881451324780815006798745991541892172339438208; // meV nm^2

     double d2_z_dz = -M_PI*M_PI/25.0;

     double p1 = potential->get_value(x1,y1,z1);
     double p2 = potential->get_value(x1+140,y1,z1);

     double potential_value = 0.0;

	if (p1<p2) potential_value=p1;
	else potential_value=p2;
    
     
      double potential_integral = wave_functions[c].get_value(x1,y1,z1) * wave_functions[d].get_value(x1+140,y1,z1) * potential_value * (140.0 + 280.0)*(max.get_y() - min.get_y())*(max.get_z() - min.get_z());
      double kinetic_integral = h2_over_2m *(/* d2_z_dz* wave_functions[c].get_value(x1,y1) *wave_functions[d].get_value(x1,y1) +*/ wave_functions[c].get_value(x1,y1)* wave_functions[d].get_laplacian(x1+140,y1)) * (140.0 + 280.0)*(max.get_y() - min.get_y());

	return potential_integral - kinetic_integral;

 /*     return wave_functions[a].get_value(x1,y1,z1) * wave_functions[b].get_value(x2,y2,z2) * wave_functions[c].get_value(x1,y1,z1) * wave_functions[d].get_value(x2,y2,z2) * eV/(distance) *
     (max.get_x() - min.get_x())*(max.get_y() - min.get_y())*(max.get_z() - min.get_z())*
            (max.get_x() - min.get_x())*(max.get_y() - min.get_y())*(max.get_z() - min.get_z())/12.9;

*/

/*
*Cylindircal
*/
/*
     double x1 =  x * (max.get_x());
     double x2 =  xp * (max.get_x());
          
     double y1 = y * 2 * M_PI;
     double y2 = yp * 2 * M_PI;
     
     double z1 = (min.get_z() + z * (max.get_z() - min.get_z()));//*nm_a0;
     double z2 = (min.get_z() + zp * (max.get_z() - min.get_z()));//*nm_a0;
     
     
     double distance = sqrt(x1*x1 + x2*x2 - 2*x1*x2*cos(y1-y2) + (z1-z2)*(z1-z2));
     
     const double threshold = 1.0e-9;
     
     if( distance < threshold) distance = threshold;
     
     double eV = 1.43996449;
     
      return wave_functions[a].get_value(x1,y1,z1) * wave_functions[b].get_value(x2,y2,z2) * wave_functions[c].get_value(x1,y1,z1) * wave_functions[d].get_value(x2,y2,z2) *eV/(distance) *
     (max.get_x())*2 * M_PI*(max.get_z() - min.get_z())*
            (max.get_x())*2 * M_PI*(max.get_z() - min.get_z())*x1 * x2/12.9;*/

  }

public:
  void print_density(const std::vector<double>& M,const char* file_name = "density.dat")  {

     int lmax =  500;
      std::ofstream dfile(file_name);   
      
      std::cout<<"wfs = "<<wave_functions.size()<<std::endl;
     for(int i = 0; i < lmax; ++i) {
	for(int j = 0; j < lmax; ++j) {
         double value = 0;
	 double lx = min.get_x() + i * (max.get_x() - min.get_x())/lmax;
         double ly = min.get_y() + j * (max.get_y() - min.get_y())/lmax;
         for(int a = 0;a < wave_functions.size(); ++a) {
          for(int b = 0;b < wave_functions.size(); ++b) {
            value += M[a *  wave_functions.size() +b]*wave_functions[a].get_value(lx,ly,0.0) * wave_functions[b].get_value(lx,ly,0.0);
            value += M[wave_functions.size() * wave_functions.size() + a * wave_functions.size() + b]*wave_functions[a].get_value(lx,ly,0.0) * wave_functions[b].get_value(lx,ly,0.0);

         }
        }
      dfile<<lx<<" "<<ly<<" "<<value<<std::endl; 
       }
      dfile<<std::endl;
     }
       
  
  }
private:
static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *userdata) {

  
  QmtDrnMicroscopic<Hamiltonian,WaveFunction,Potential> *f = static_cast<QmtDrnMicroscopic<Hamiltonian,WaveFunction,Potential>*>(userdata);
  
  ff[0] = f->func(xx[0],xx[1],xx[2],xx[3],xx[4],xx[5],f->ida,f->idb,f->idc,f->idd);


  return 0;
}
 
 
 
};

}

#endif // QMT_DRN_MICROSCOPIC_H_INCLUDED
