#include "../../headers/qmtspline.h"
//#include "../../headers/qmtparsertools"
#include <fstream>
#include <math.h>
#include <iostream>
template <typename Radial>
struct QmtDrnWaveFunction {

Radial *radial;
int _n;
int _l;
double _d;
size_t size;
double max_xy,max_z;

QmtDrnWaveFunction(const char* radial_file, size_t size,int n, int l, double d):_n(n),_l(l), _d(d){

std::cout<<"radial file:"<<radial_file<<" l = "<<_l<<std::endl;
max_xy = 140 * 140;

max_z = _d/2;

double *X = new double [size];
double *Y = new double [size];

std::ifstream wf(radial_file);
       if (wf.is_open())
       {
        for(int i = 0; i <size;++i) {
	    double x,y;
	    wf>>x>>y;
	    X[i] = x;
            Y[i] = y;	    
	  }	    
	}
	wf.close();
 radial = new Radial(X,Y, size);
 delete [] X;
 delete [] Y;
}

double  get_value(double x, double y, double z) {

double distance2 = x*x + y*y ;

double r = get_radial(sqrt(x*x + y*y));

if(z <= -2.5 || z >= 2.5 ) return 0;

double angular = 1.0;
if(_l < 0)
  angular= sin(fabs(_l)* atan2(y,x))*sqrt(2);
if(_l > 0)
  angular= cos(fabs(_l)* atan2(y,x))*sqrt(2);

return angular * r * cos(M_PI*z/_d) * sqrt(2/_d);
}

double  get_value(double x, double y) {

double distance2 = x*x + y*y ;

double r = get_radial(sqrt(x*x + y*y));

double angular = 1.0;
if(_l < 0)
  angular= sin(fabs(_l)* atan2(y,x))*sqrt(2);
if(_l > 0)
  angular= cos(fabs(_l)* atan2(y,x))*sqrt(2);

return angular * r;
}


double get_radial(double r) {
double distance2 = r*r ;

if(distance2 < 0.028*0.028){
    double x1 = 0.0281;
    double x2 = 0.0561;
    double y1 = radial->get_value(x1);
    double y2 = radial->get_value(x2);
     
    return ((y1-y2)/(x1*x1-x2*x2))*distance2 + y1 - ((y1-y2)/(x1*x1-x2*x2))*x1*x1;

}

if(distance2 >= max_xy){
    double x1 = 0.98*sqrt(max_xy);
    double x2 = 0.99*sqrt(max_xy);
    double y1 = radial->get_value(x1);
    double y2 = radial->get_value(x2);

    double B = (log(y1)-log(y2))/(x2-x1); // y = A*exp(-B*x)
    double A = y1*exp(B*x1);

    return A*exp(-B*r);

}

return radial->get_value(r);

}

double  get_laplacian(double x, double y, double z) {
double distance2 = x*x + y*y ;

double r = -1.0;
double zet =  cos(M_PI*z/_d) * sqrt(2/_d);

double d2_r = 0.0;
double r2d2_phi = 0.0;
double rd_r = 0.0;
double d2_z = 0.0;

r= get_radial(sqrt(x*x + y*y));


/*
// o(h^2)
std::vector<double> coeffs_der1 ({-1.0/2.0,0.0,1.0/2.0});
std::vector<double> coeffs_der2 ({1.0,-2.0,1.0});



	for(int i=-1; i<=1; ++i){
		double radius = sqrt(distance2) + i* 0.028; // because function is symmetrical!
		if (radius<0.0) radius*=-1; // because function is symmetrical!

		d2_r += coeffs_der2[i+1]*get_radial(radius);
		rd_r += coeffs_der1[i+1]*get_radial(radius);
	}

	d2_r/=0.028*0.028;
	rd_r/=0.028;
	rd_r/=sqrt(distance2);
*/
/*
// o(h^6)
std::vector<double> coeffs_der1 ({-1.0/60.0,3.0/20.0,-3.0/4.0,0.0,3.0/4.0,-3.0/20.0,1.0/60.0});
std::vector<double> coeffs_der2 ({1.0/90.0,-3.0/20.0,3.0/2.0,-49.0/18.0,3.0/2.0,-3.0/20.0,1.0/90.0});



	for(int i=-3; i<=3; ++i){
		double radius = sqrt(distance2) + i* 0.028; // because function is symmetrical!
		if (radius<0.0) radius*=-1; // because function is symmetrical!

		d2_r += coeffs_der2[i+3]*get_radial(radius);
		rd_r += coeffs_der1[i+3]*get_radial(radius);
	}

	d2_r/=0.028*0.028;
	rd_r/=0.028;
	rd_r/=sqrt(distance2);
*/

// o(h^8)
std::vector<double> coeffs_der1 ({-1.0/280.0,-4.0/105.0,1.0/5.0,-4.0/5.0,0.0,4.0/5.0,-1.0/5.0,4.0/105.0,-1.0/280.0});
std::vector<double> coeffs_der2 ({-1.0/560.0,8.0/315.0,-1.0/5.0,8.0/5.0,-205.0/72.0,8.0/5.0,-1.0/5.0,8.0/315.0,-1.0/560.0});



	for(int i=-4; i<=4; ++i){
		double radius = sqrt(distance2) + i* 0.028; 
		if (radius<0.0) radius*=-1; // because function is symmetrical!

		d2_r += coeffs_der2[i+4]*get_radial(radius);
		rd_r += coeffs_der1[i+4]*get_radial(radius);
	}

	d2_r/=0.028*0.028;
	rd_r/=0.028;
	rd_r/=sqrt(distance2);

//std::cout<<d2_r<<" "<<rd_r<<std::endl;


if(z <= -2.5 || z >= 2.5) return 0;

double angular = 1.0;
double d2_angular = 0.0;
if(_l < 0){
  angular= sin(fabs(_l)* atan2(y,x))*sqrt(2);
  d2_angular= -_l*_l* sin(fabs(_l)* atan2(y,x))*sqrt(2);
}
if(_l > 0){
  angular=  cos(fabs(_l)* atan2(y,x))*sqrt(2);
  d2_angular=  -_l*_l* cos(fabs(_l)* atan2(y,x))*sqrt(2);
}

d2_r *= angular * zet ;
rd_r *= angular * zet ;
r2d2_phi =  r * d2_angular * zet/distance2;
d2_z = - r * angular * pow(M_PI/_d,2) * zet;

return d2_r + rd_r + r2d2_phi ;//+ d2_z;
}


double  get_laplacian(double x, double y) {
double distance2 = x*x + y*y ;

double r = -1.0;

double d2_r = 0.0;
double r2d2_phi = 0.0;
double rd_r = 0.0;

r= get_radial(sqrt(x*x + y*y));


/*
// o(h^2)
std::vector<double> coeffs_der1 ({-1.0/2.0,0.0,1.0/2.0});
std::vector<double> coeffs_der2 ({1.0,-2.0,1.0});



	for(int i=-1; i<=1; ++i){
		double radius = sqrt(distance2) + i* 0.028; // because function is symmetrical!
		if (radius<0.0) radius*=-1; // because function is symmetrical!

		d2_r += coeffs_der2[i+1]*get_radial(radius);
		rd_r += coeffs_der1[i+1]*get_radial(radius);
	}

	d2_r/=0.028*0.028;
	rd_r/=0.028;
	rd_r/=sqrt(distance2);
*/
/*
// o(h^6)
std::vector<double> coeffs_der1 ({-1.0/60.0,3.0/20.0,-3.0/4.0,0.0,3.0/4.0,-3.0/20.0,1.0/60.0});
std::vector<double> coeffs_der2 ({1.0/90.0,-3.0/20.0,3.0/2.0,-49.0/18.0,3.0/2.0,-3.0/20.0,1.0/90.0});



	for(int i=-3; i<=3; ++i){
		double radius = sqrt(distance2) + i* 0.028; // because function is symmetrical!
		if (radius<0.0) radius*=-1; // because function is symmetrical!

		d2_r += coeffs_der2[i+3]*get_radial(radius);
		rd_r += coeffs_der1[i+3]*get_radial(radius);
	}

	d2_r/=0.028*0.028;
	rd_r/=0.028;
	rd_r/=sqrt(distance2);
*/

// o(h^8)
std::vector<double> coeffs_der1 ({-1.0/280.0,-4.0/105.0,1.0/5.0,-4.0/5.0,0.0,4.0/5.0,-1.0/5.0,4.0/105.0,-1.0/280.0});
std::vector<double> coeffs_der2 ({-1.0/560.0,8.0/315.0,-1.0/5.0,8.0/5.0,-205.0/72.0,8.0/5.0,-1.0/5.0,8.0/315.0,-1.0/560.0});



	for(int i=-4; i<=4; ++i){
		double radius = sqrt(distance2) + i* 0.028; 
		if (radius<0.0) radius*=-1; // because function is symmetrical!

		d2_r += coeffs_der2[i+4]*get_radial(radius);
		rd_r += coeffs_der1[i+4]*get_radial(radius);
	}

	d2_r/=0.028*0.028;
	rd_r/=0.028;
	rd_r/=sqrt(distance2);

//std::cout<<d2_r<<" "<<rd_r<<std::endl;


double angular = 1.0;
double d2_angular = 0.0;
if(_l < 0){
  angular= sin(fabs(_l)* atan2(y,x))*sqrt(2);
  d2_angular= -_l*_l* sin(fabs(_l)* atan2(y,x))*sqrt(2);
}
if(_l > 0){
  angular=  cos(fabs(_l)* atan2(y,x))*sqrt(2);
  d2_angular=  -_l*_l* cos(fabs(_l)* atan2(y,x))*sqrt(2);
}

d2_r *= angular;
rd_r *= angular;
r2d2_phi =  r * d2_angular/distance2;

return d2_r + rd_r + r2d2_phi;
}

};
