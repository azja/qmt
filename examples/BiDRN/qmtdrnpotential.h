#ifndef QMT_DRN_POTENTIAL_INCLUDED
#define QMT_DRN_POTENTIAL_INCLUDED

#include <iostream>
#include <fstream>
#include <math.h>
template <typename Radial>
struct QmtDrnPotential {
Radial *radial;

double _d;
size_t size;
double max_xy,max_z;
double potential_wall;

QmtDrnPotential(const char* radial_file, size_t size, double d):_d(d){

std::cout<<"potential file:"<<radial_file<<std::endl;
max_xy = 80 * 80;
potential_wall = 90.0;

max_z = _d/2;

double *X = new double [size];
double *Y = new double [size];

std::ifstream wf(radial_file);
       if (wf.is_open())
       {
        for(int i = 0; i <size;++i) {
	    double x,y;
	    wf>>x>>y;
	    X[i] = x;
            Y[i] = y;	    
	  }	    
	}
	wf.close();
 radial = new Radial(X,Y, size);
 delete [] X;
 delete [] Y;
}

double  get_value(double x, double y, double z) {

double distance2 = x*x + y*y ;
//std::cout<<"z = "<<z<<std::endl;

double r = -1.0;


if(distance2 >= max_xy){
return potential_wall;
}

if(z <= -max_z || z >= max_z ) return 1e16;

r= radial->get_value(sqrt(x*x + y*y));

return r;
}

};




#endif //QMT_DRN_POTENTIAL_INCLUDED
