#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtmicroscopic.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>


#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include <stdio.h>



const size_t NVAR = 4;




typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 

struct Optimizer {
  
   typedef double(*internal_func_two)(int, double*,void*);
   internal_func_two Function;
   void* params;
   double args[3];
   
   double result() {
   args[0] = 1;
   args[1] =.5;
   args[2] =.5;
     return Function(3,args,params);
          
  }
   std::tuple<double,double>  getResult(){
     
     return std::make_tuple(1.0,1.0);
     
  }
};



struct System {

qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> microscopic;

    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<qmt::QmtNState <qmt::uint, int> > states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula
         <qmt_hamiltonian,LocalAlgebras::SparseAlgebra> 
                                                                              h2_formula; //Tranlate Hamiltonian to matrix
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    unsigned int problem_size;
    unsigned int number_of_centers;							       //Number of atomic centers - spin obitals						
   
   
 



System(std::vector<double>& alphas):microscopic("ham3sfull.dat","defs3s.dat","supercell.dat", "megacell.dat",alphas),number_of_centers(6),
                                                                                          _lanczos_steps(25)  ,
									                  _lanczos_eps(1.0e-9)  {
   
    hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian("ham3sfull.dat");

    microscopic.set_parameters(alphas,qmt::QmtVector(1,1,1));

    generator.generate(6,number_of_centers*2,states);
    

    h2_formula.set_hamiltonian(*hamiltonian);
    
    h2_formula.set_States(states);
    
    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);

    LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);

 }
 double getEnergy(qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian>& microscopic, double* intgs){
   
 for(unsigned int i = 0; i < microscopic.number_of_one_body();++i) {
   
   auto integral = intgs[i];
    
   h2_formula.set_microscopic_parameter(1,i,integral);
  }
 
 for(unsigned int i = microscopic.number_of_one_body(); i < microscopic.number_of_two_body();++i) {

   auto integral = intgs[i];
    h2_formula.set_microscopic_parameter(2,i,integral);
   
 }

h2_formula.get_Hamiltonian_Matrix(hamiltonian_M);

      int s = states.size();
      gsl_vector* eigenvalues=gsl_vector_calloc(s);
      gsl_matrix* eigenvectors = gsl_matrix_calloc(s,s);
      gsl_matrix* matrix = gsl_matrix_calloc(s,s);
gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
					s);
for(int i = 0; i <s; ++i) {
  for(int j = 0; j <s; ++j)
    gsl_matrix_set(matrix,i,j,hamiltonian_M->get_value(i,j));
}
			
			gsl_eigen_symmv(matrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);
	
			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
double  energy = gsl_vector_get(eigenvalues,0);
	                gsl_matrix_free(matrix);
			gsl_matrix_free(eigenvectors);
			gsl_vector_free(eigenvalues);
			
  //std::cout<<"S size = "<<states.size()<<std::endl;
  
return  energy;

}

~System() {

    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	
}


};

double my_f (const gsl_vector *xvec_ptr, void *params) {
 System *s = static_cast<System*>(params);
 std::vector<double> alphas;
 alphas.push_back(gsl_vector_get(xvec_ptr,0));
 alphas.push_back(gsl_vector_get(xvec_ptr,1));
 alphas.push_back(gsl_vector_get(xvec_ptr,2));
 alphas.push_back(gsl_vector_get(xvec_ptr,3));
 alphas.push_back(gsl_vector_get(xvec_ptr,2));
// alphas.push_back(0.4287);
// alphas.push_back(0.5788);

//  alphas.push_back(gsl_vector_get(xvec_ptr,2));
if(alphas[0] < 0.1 || alphas[1] < 0.1 || alphas[2] < 0.1  /*|| alphas[3] < 0.1*/ ) return 0.0;
  s->set(alphas);
  double energy =  s->get_energy();
  
//  if(alphas[0] < 0.35 || alphas[1] < 0.35 || alphas[2] < 0.35) return -0.5;
//  if(fabs(energy) > 1.2) return 0;
std::cout<<"E = "<<energy<<" "<<alphas[0]<<" "<<alphas[1]<<std::endl;
  return energy;
}



int main() {



//qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> microscopic("ham_px.dat","defs.dat","supercell.dat", "megacell.dat",1.19);
std::vector<double> alphs;

alphs.push_back(0.4287);
alphs.push_back(0.5788);
//alphs.push_back(1.19/2);
//System h2_system(alphs);

System h2_system(alphs);



void*  sys = static_cast<void*>(&h2_system);

  const gsl_multimin_fminimizer_type *T = 
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  x = gsl_vector_alloc (NVAR);
  gsl_vector_set (x, 0, 5.8);
  gsl_vector_set (x, 1,2.2);
 gsl_vector_set (x, 2, 2.8);
 gsl_vector_set (x, 3, 0.5);
 
  ss = gsl_vector_alloc(NVAR);
  gsl_vector_set_all (ss, 0.05);

  minex_func.n = NVAR;
  minex_func.f = my_f;
  minex_func.params = sys;

  s = gsl_multimin_fminimizer_alloc (T, NVAR);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      
      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-4);

      if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }

      printf ("%5d %7.3f %7.3f  %7.3f %7.3f f() = %7.3f size = %.3f\n", 
              iter,
              gsl_vector_get (s->x, 0), 
              gsl_vector_get (s->x, 1), 
              gsl_vector_get (s->x, 2), 
              gsl_vector_get (s->x, 3),               

              s->fval, size);
    }
  while (status == GSL_CONTINUE && iter < 1000);
  
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return status;









return 0;
}
