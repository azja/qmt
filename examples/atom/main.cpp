#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtmicroscopic.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"
#include "../../headers/qmtparsertools.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>


#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include <stdio.h>





typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 

struct System {

qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> *microscopic;

    

    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<qmt::QmtNState <qmt::uint, int> > states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula
         <qmt_hamiltonian,LocalAlgebras::SparseAlgebra> 
                                                                              h2_formula; //Tranlate Hamiltonian to matrix
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    unsigned int number_of_centers;							       //Number of atomic centers - spin obitals						
    std::vector<double> intgs;                                                                 //Integrals - microscopic parameters
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;                //Eigen problem solver
    typedef qmt::QmtNState<qmt::uint, int> NState;                                             //Output - eigen state
    int _lanczos_steps;
    int _lanczos_eps;
    double w_vs_u;
    int problem_size;
    int electron_number;
    int alphas_number;


System(std::vector<double>& alphas, const char* file_name): _lanczos_steps(500), _lanczos_eps(1.0e-9)  {
  
   std::ifstream cfile;       
   cfile.open(file_name); 
  std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
  
   std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
   std::string definitions_fn = qmt::parser::get_bracketed_words(content,"wfs_definitions","wfs_definitions_end")[0];
   std::string megacell_fn = qmt::parser::get_bracketed_words(content,"megacell","megacell_end")[0];;
   std::string supercell_fn =qmt::parser::get_bracketed_words(content,"supercell","supercell_end")[0];
   problem_size =std::stoi(qmt::parser::get_bracketed_words(content,"problem_size","problem_size_end")[0]);
   electron_number =std::stoi(qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0]);
   alphas_number  =std::stoi(qmt::parser::get_bracketed_words(content,"alphas_number","alphas_number_end")[0]);
   microscopic = new  qmt::QmtMicroscopic<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian>
		(hamiltonian_fn,definitions_fn,supercell_fn, megacell_fn,alphas);

    hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(hamiltonian_fn);

    microscopic->set_parameters(alphas,qmt::QmtVector(1,1,1));
    //generating all posible electronic states
    generator.generate(electron_number,problem_size*2,states);
    
    // preparing hamiltonian formula engine
    h2_formula.set_hamiltonian(*hamiltonian);
    
    h2_formula.set_States(states);
    
     for (auto state : states){
      std::cout<<state<<std::endl; 
     }

    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);

    LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size,_lanczos_steps, _lanczos_eps); 
 }

double get_energy() {
 std::vector<double> one_body;

 for(unsigned int i = 0; i < microscopic->number_of_one_body();++i) {
   
   auto integral = microscopic->get_one_body(i);

   h2_formula.set_microscopic_parameter(1,i,integral);
   one_body.push_back(integral);

 }
 
  std::vector<double> two_body;

 for(unsigned int i = 0; i < microscopic->number_of_two_body();++i) {

   auto integral = microscopic->get_two_body(i);

    h2_formula.set_microscopic_parameter(2,i,integral);

   two_body.push_back(integral);

 }

h2_formula.get_Hamiltonian_Matrix(hamiltonian_M);
//std::cout<<*hamiltonian_M;
//LanczosSolver->solve(hamiltonian_M);
//double energy = LanczosSolver->get_minimal_eigenvalue();


int s = states.size();
                        gsl_vector* eigenvalues=gsl_vector_calloc(s);
			gsl_matrix* eigenvectors = gsl_matrix_calloc(s,s);

                        gsl_matrix* matrix = gsl_matrix_calloc(s,s);
			gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
					s);
for(int i = 0; i <s; ++i) {
  for(int j = 0; j <s; ++j)
    gsl_matrix_set(matrix,i,j,hamiltonian_M->get_value(i,j));
}
			
			gsl_eigen_symmv(matrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);
	
			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
double  energy = gsl_vector_get(eigenvalues,0);
	                gsl_matrix_free(matrix);
			gsl_matrix_free(eigenvectors);
			gsl_vector_free(eigenvalues);
			
  //std::cout<<"S size = "<<states.size()<<std::endl;
  
return  energy;


  
}

~System() {

    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	delete LanczosSolver;
	delete microscopic;
}

void set(const std::vector<double> params) {
      microscopic->set_parameters(params,qmt::QmtVector(1,1,1));
}

};

double my_f (const gsl_vector *xvec_ptr, void *params) {
 System *s = static_cast<System*>(params);
 std::vector<double> alphas;
 
 for(int i = 0; i < s->alphas_number; ++i)
   alphas.push_back(gsl_vector_get(xvec_ptr,i));
 
 for(int i = 0; i < s->alphas_number; ++i)
       if (alphas[i] < 0.1) return 0;
 
  s->set(alphas);
  double energy =  s->get_energy();
  
  std::cout<<"E = "<<energy<<" "<<alphas[0]<<" "<<alphas[1]<<std::endl;
  return energy;
}



int main() {

std::vector<double> alphs;

alphs.push_back(0.4287);
alphs.push_back(0.5788);



System h2_system(alphs, "config.cfg");
int NVAR = h2_system.alphas_number;


void*  sys = static_cast<void*>(&h2_system);

  const gsl_multimin_fminimizer_type *T = 
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  
  
  x = gsl_vector_alloc (NVAR);
  
  for(int i = 0; i < NVAR;++i)
   gsl_vector_set (x, i, 1.0);
  
  ss = gsl_vector_alloc(NVAR);
  gsl_vector_set_all (ss, 0.05);

  minex_func.n = NVAR;
  minex_func.f = my_f;
  minex_func.params = sys;

  s = gsl_multimin_fminimizer_alloc (T, NVAR);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      
      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-4);

      if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }

        std::cout<<iter<<" ";
        for(int i = 0; i < NVAR;++i)
	  std::cout<<gsl_vector_get(s->x,i)<<" ";
	std::cout<<s->fval<<" "<<size<<std::endl;;
	
    }
  while (status == GSL_CONTINUE && iter < 1000);
  
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return status;


}
