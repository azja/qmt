#include "../include/microscopic.h"

#define MICROSCOPIC_H_DEBUG false

qmt::examples::MicroscopicH2chain::MicroscopicH2chain() :
		Num_Gaussians(13), alpha(1.19378), a(2.0), R(1.43042), theta(
				 0.1), dx(0), dy(0), dz(0), reference_point(0, 0, 0), wannier1(
				0, 0, 0, 0), wannier2(0, 0, 0, 0) {

	M = new double*[num_of_betas];
	for (int i = 0; i < num_of_betas; ++i)
		M[i] = new double[num_of_betas];

	qmt::qmGtoNSlater1s(2.0, 2.0, 1.0e-12, 1.0e-06, 1.0e-06, coefs, gammas,
			Num_Gaussians);

	one_body_intgs =
			qmt::QmtHamiltonianBuilder<qmt::QmtHubbard<sq_operator> >::getOneBodyIntegralsV(
					"chainHam.dat");
	two_body_intgs =
			qmt::QmtHamiltonianBuilder<qmt::QmtHubbard<sq_operator> >::getTwoBodyIntegralsV(
					"chainHam.dat");
	megacell =
			qmt::QmtHamiltonianBuilder<qmt::QmtHubbard<sq_operator> >::getAllAtoms(
					"megacell.dat");
	supercell =
			qmt::QmtHamiltonianBuilder<qmt::QmtHubbard<sq_operator> >::getAllAtoms(
					"supercell.dat");
					

					_betas.push_back(1.0);
					_betas.push_back(-0.5);
					_betas.push_back(0.0);
					_betas.push_back(0.0);
					_betas.push_back(0.0);

	prepare_wannier();

}

qmt::examples::MicroscopicH2chain::~MicroscopicH2chain() {
	for (int i = 0; i < num_of_betas; ++i)
		delete[] M[i];
	delete[] M;
}

void qmt::examples::MicroscopicH2chain::calculate_betas(double a, 
		double R, double alpha, std::vector<double>& betas, double** M) {
	coefs.clear();
	gammas.clear();
	qmt::qmGtoNSlater1s(2.0, 2.0, 1.0e-12, 0.0001, 0.0001, coefs, gammas,
			Num_Gaussians);


	//************************************************************************************************************************
	// What must be set for orthonolaization to the Nth nearest neighbor
	// Note that one must change num_of_betas in microscopic.h as well (num_of_betas=N+1)

	// Nodes taken into account
	// Be careful to take as many as it is needed
	int max_node = 3 + 1; //+1 for algebra
	int min_node = -2;

	double in[5] =  { 1.0, 0.1, 0.1, 0.0, 0.0 }; //as many as betas (N+1)
/*	in[0] = _betas[0];
	in[1] = _betas[1];
	in[2] = _betas[2];
	in[3] = _betas[3];
	in[4] = _betas[4];*/
	std::vector<qmt::QmtVector> main_nodes_coords(
			{ qmt::QmtVector(0, 0, 0), qmt::QmtVector(R*cos(theta), 0, R*sin(theta)), qmt::QmtVector(
					a, 0, 0), qmt::QmtVector(a+R*cos(theta), 0,  R*sin(theta)),
					qmt::QmtVector(-a+R*cos(theta), 0,  R*sin(theta))}); // how many Wannier functions we desire,qmt::QmtVector(a,a,a)

	std::vector<std::pair<double, int> > nodal_symmetry_conditions; //as many as neighbors (N)
	nodal_symmetry_conditions.push_back(std::make_pair(R, 1));
	nodal_symmetry_conditions.push_back(std::make_pair(a, 2));
	nodal_symmetry_conditions.push_back(std::make_pair(sqrt(a * a + R * R - 2*a*R*cos(theta)), 3));
	nodal_symmetry_conditions.push_back(std::make_pair(sqrt(a * a + R * R + 2*a*R*cos(theta)), 4));

	//************************************************************************************************************************
	/*       // What must be set for orthonolaization to the Nth nearest neighbor
	 // Note that one must change num_of_betas in microscopic.h as well (num_of_betas=N+1)

	 // Nodes taken into account
	 // Be careful to take as many as it is needed
	 int max_node=5+1; //+1 for algebra
	 int min_node=-2;

	 double in[] = {2.5, -0.5 ,0.01,0.01,0.0,0.0,0.0}; //as many as betas (N+1)
	 std::vector<qmt::QmtVector>	main_nodes_coords({qmt::QmtVector(0,0,0),qmt::QmtVector(a,0,0),qmt::QmtVector(a,a,0),qmt::QmtVector(a,a,a),qmt::QmtVector(2*a,0,0),qmt::QmtVector(2*a,a,0),qmt::QmtVector(2*a,a,a)}); // how many Wannier functions we desire

	 std::vector<std::pair<double,int> > nodal_symmetry_conditions; //as many as neighbors (N)
	 nodal_symmetry_conditions.push_back(std::make_pair(a,1));
	 nodal_symmetry_conditions.push_back(std::make_pair(a*sqrt(2),2));
	 nodal_symmetry_conditions.push_back(std::make_pair(a*sqrt(3),3));
	 nodal_symmetry_conditions.push_back(std::make_pair(a*2,4));
	 nodal_symmetry_conditions.push_back(std::make_pair(a*sqrt(6),6));
	 */
	//************************************************************************************************************************

	int number_of_nodes = (max_node - min_node) * 2;

	std::vector<int> main_nodes(main_nodes_coords.size(), 0);

	double eps = 1e-8; // double equality tolerance for comparator below

	//comparator of two vectors
	auto compare_vectors =
			[ ](qmt::QmtVector &vec1, qmt::QmtVector vec2, double eps)-> bool {
				double len=qmt::QmtVector::norm(vec1-vec2);

				if (len<eps) return true;
				else return false;
			};

	// must be clean
	brick_coord1.clear();
	brick_coord2.clear();

// Coordinates of nodes taken into account
	std::vector<qmt::QmtVector> coordinates;
	coordinates.reserve(number_of_nodes);

	for (int i = min_node; i < max_node; i++) {
			for (int k = 0; k < 2; k++) {
				coordinates.push_back(qmt::QmtVector(i * a + k*R*cos(theta), 0, k*R*sin(theta)));
			}
	}

// Gaussian expansions to work on
	std::vector<std::vector<Gauss> > batom;
	batom.reserve(number_of_nodes);

	for (int i = 0; i < number_of_nodes; i++) {
		std::vector<Gauss> quick;
		batom.push_back(quick);
	}

	for (int j = 0; j < number_of_nodes; ++j) {
		for (int i = 0; i < Num_Gaussians; ++i) {
			batom[j].push_back(
					Gauss(coordinates[j].get_x(), coordinates[j].get_y(),
							coordinates[j].get_z(), alpha * gammas[i]));

		}
	}

	std::vector<g_omp> bslaters;

	for (int j = 0; j < number_of_nodes; ++j) {
		bslaters.push_back(
				g_omp(coordinates[j].get_x(), coordinates[j].get_y(),
						coordinates[j].get_z(), j));

		for (unsigned int i = 0; i < main_nodes_coords.size(); i++) {
			if (compare_vectors(coordinates[j], main_nodes_coords[i], eps))
				main_nodes[i] = j;
		}

	}

	for (int j = 0; j < number_of_nodes; ++j) {
		for (int i = 0; i < Num_Gaussians; ++i) {
			bslaters[j].add_element(i, coefs[i], batom[j][i]);
		}
	}

	std::vector<Wannier<g_omp> > wanniers(num_of_betas, Wannier<g_omp>());

	brick_coord1.push_back(std::make_pair(0, reference_point)); // reference_point is always the center of wannier function
	brick_coord2.push_back(std::make_pair(0, reference_point)); // reference_point is always the center of wannier function

	std::vector<std::vector<int> > count_betas(8, std::vector<int>(8, 0));

	for (unsigned int i = 0; i < main_nodes.size(); i++) {
		wanniers[i].addOrbital(bslaters[main_nodes[i]], 0);
		for (int j = 0; j < number_of_nodes; ++j) {
			for (auto iter = nodal_symmetry_conditions.begin();
					iter != nodal_symmetry_conditions.end(); iter++)
				if (fabs(
						qmt::QmtVector::norm(
								coordinates[j] - main_nodes_coords[i])
								- std::get < 0 > (*iter)) < eps) {
					wanniers[i].addOrbital(bslaters[j], std::get < 1 > (*iter));
					count_betas[i][std::get < 1 > (*iter)] += 1;
					if (i == 0) {
						brick_coord1.push_back(
								std::make_pair(std::get < 1 > (*iter),
										coordinates[j]));
//				std::cout<<std::get<1>(*iter)<<" "<<coordinates[j]<<std::endl;
					}
					if (i == 1) {
						brick_coord2.push_back(
								std::make_pair(std::get < 1 > (*iter),
										(coordinates[j]
												- qmt::QmtVector(R*cos(theta), 0, R*sin(theta)))));
//					std::cout<<std::get<1>(*iter)<<" "<<coordinates[j]<<std::endl;
					}
				}
		}
	}

	/*    for(unsigned int i = 0; i<count_betas.size(); i++){
	 std::cout<<"w_"<<i<<": "<<std::endl;
	 for(unsigned int j = 0; j<count_betas[i].size(); j++){
	 std::cout<<"b"<<j<<"= "<<count_betas[i][j]<<", ";
	 }
	 std::cout<<std::endl;
	 }*/

	std::vector<BilinearForm> forms;

	for (int i = 1; i < num_of_betas; i++) {
		Wannier<g_omp>::multiply(wanniers[0], wanniers[i], M, num_of_betas);
		forms.push_back(BilinearForm(M, num_of_betas));
	}

	Wannier<g_omp>::multiply(wanniers[0], wanniers[0], M, num_of_betas);
	forms.push_back(BilinearForm(M, num_of_betas, -1));

////////////////////////////////////////////////////////////////////

//	for (int i = 0; i < 1; i++) {

		double *out = new double[num_of_betas];
		/*
		 if (i==0){   double in2[]= {1,-0.5,0.0,0.0};
		 NonLinearSystemSolve(forms,in2,out,1.0e-10);
		 }
		 else if (i==2){   double in2[]= {10,-1,0.1,0.1};
		 NonLinearSystemSolve(forms,in2,out,1.0e-10);
		 }
		 else if (i==3){   double in2[]= {1,-0.5,0.1,-0.1};
		 NonLinearSystemSolve(forms,in2,out,1.0e-10);
		 }
		 else if (i==4){   double in2[]= {1,-0.5,-0.1,0.1};
		 NonLinearSystemSolve(forms,in2,out,1.0e-10);
		 }
		 else     {   double in2[]= {0,0,0,0};
		 NonLinearSystemSolve(forms,in2,out,1.0e-10);
		 }
		 */

		NonLinearSystemSolve(forms, in, out, 1.0e-8);

		betas.clear();
		_betas.clear();
		for (int i = 0; i < num_of_betas; i++) {
			betas.push_back(out[i]);
			_betas.push_back(out[i]);
//	std::cout<<" b"<<i<<" = "<<out[i];
		}
delete [] out;
//std::cout<<std::endl;
//	}

}

void qmt::examples::MicroscopicH2chain::prepare_wannier() {

	std::vector<double> betas;

	calculate_betas(a, R, alpha, betas, M);

	num_of_bricks1 = brick_coord1.size();
	wannier_bricks1.clear();
	wannier_bricks1 = std::vector<std::vector<Gauss> >(num_of_bricks1,
			std::vector<Gauss>());
	num_of_bricks2 = brick_coord2.size();
	wannier_bricks2.clear();
	wannier_bricks2 = std::vector<std::vector<Gauss> >(num_of_bricks2,
			std::vector<Gauss>());

	for (unsigned int j = 0; j < brick_coord1.size(); j++) {
		for (int i = 0; i < Num_Gaussians; ++i) {
			wannier_bricks1[j].push_back(
					Gauss(std::get < 1 > (brick_coord1[j]).get_x(),
							std::get < 1 > (brick_coord1[j]).get_y(),
							std::get < 1 > (brick_coord1[j]).get_z(),
							alpha * gammas[i]));
		}
	}
	for (unsigned int j = 0; j < brick_coord2.size(); j++) {
		for (int i = 0; i < Num_Gaussians; ++i) {
			wannier_bricks2[j].push_back(
					Gauss(std::get < 1 > (brick_coord2[j]).get_x(),
							std::get < 1 > (brick_coord2[j]).get_y(),
							std::get < 1 > (brick_coord2[j]).get_z(),
							alpha * gammas[i]));
		}
	}

	slaters1.clear();
	slaters2.clear();

	for (unsigned int j = 0; j < brick_coord1.size(); j++) {
		slaters1.push_back(
				g_omp(std::get < 1 > (brick_coord1[j]).get_x(),
						std::get < 1 > (brick_coord1[j]).get_y(),
						std::get < 1 > (brick_coord1[j]).get_z(), j));
	}
	for (unsigned int j = 0; j < brick_coord2.size(); j++) {
		slaters2.push_back(
				g_omp(std::get < 1 > (brick_coord2[j]).get_x(),
						std::get < 1 > (brick_coord2[j]).get_y(),
						std::get < 1 > (brick_coord2[j]).get_z(), j));
	}

	for (unsigned int j = 0; j < brick_coord1.size(); j++) {
		for (int i = 0; i < Num_Gaussians; ++i) {
			slaters1[j].add_element(i, coefs[i], wannier_bricks1[j][i]);
		}
	}
	for (unsigned int j = 0; j < brick_coord2.size(); j++) {
		for (int i = 0; i < Num_Gaussians; ++i) {
			slaters2[j].add_element(i, coefs[i], wannier_bricks2[j][i]);
		}
	}

	wannier1.clear_elements();
	wannier2.clear_elements();

	for (unsigned int j = 0; j < brick_coord1.size(); j++) {
		wannier1.add_element(j, betas[std::get < 0 > (brick_coord1[j])],
				slaters1[j]);
		wannier2.add_element(j, betas[std::get < 0 > (brick_coord2[j])],
				slaters2[j]);
	}

//******************************************************************************************

	local_megacell.clear();

	for (auto iter = megacell.begin(); iter != megacell.end(); iter++) {
		local_megacell.push_back(apply_basis(*iter));
	}

	local_supercell.clear();

	for (auto iter = supercell.begin(); iter != supercell.end(); iter++) {
		local_supercell.push_back(apply_basis(*iter));
	}

	local_one_body_intgs.clear();
	local_two_body_intgs.clear();

	for (auto iter = one_body_intgs.begin(); iter != one_body_intgs.end();
			iter++) {
		local_one_body_intgs.push_back(
				std::make_tuple(apply_basis(std::get < 0 > (*iter)),
						apply_basis(std::get < 1 > (*iter)),
						std::get < 2 > (*iter)));
	}

	for (auto iter = two_body_intgs.begin(); iter != two_body_intgs.end();
			iter++) {
		local_two_body_intgs.push_back(
				std::make_tuple(apply_basis(std::get < 0 > (*iter)),
						apply_basis(std::get < 1 > (*iter)),
						apply_basis(std::get < 2 > (*iter)),
						apply_basis(std::get < 3 > (*iter)),
						std::get < 4 > (*iter)));
	}
	// std::cout<<"____________________________________________________"<<std::endl;
	//std::cout<<std::get<0>(local_two_body_intgs[1])<<std::endl;
	//std::cout<<"____________________________________________________"<<std::endl;
}

qmt::QmtVectorizedAtom qmt::examples::MicroscopicH2chain::apply_basis(
		qmt::QmtVectorizedAtom in) {
	qmt::QmtVectorizedAtom out(
			qmt::QmtVector(in.position.get_x() * a + in.position.get_z() * R*cos(theta), 0.0,
					in.position.get_z() * R*sin(theta)), in.id);

	return out;

}

double qmt::examples::MicroscopicH2chain::get_one_body(unsigned int index) {

	if (index >= local_one_body_intgs.size()) {
		std::cerr
				<< "MicroscopicH2chain::get_one_body: index exceeding the number of one-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 2 > (local_one_body_intgs[index])) {
		std::cerr
				<< "MicroscopicH2chain::get_one_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

	qmt::QmtVector trans1 =
			(std::get < 0 > (local_one_body_intgs[index])).position
					- reference_point;
	qmt::QmtVector trans2 =
			(std::get < 1 > (local_one_body_intgs[index])).position
					- reference_point;

//std::cout<<trans1<<" "<<trans2<<std::endl;

	if (std::get < 0 > (local_one_body_intgs[index]).id < 0
			&& std::get < 1 > (local_one_body_intgs[index]).id > 0) {
		trans1 += qmt::QmtVector(dx, dy, dz);
	}

	if (std::get < 1 > (local_one_body_intgs[index]).id < 0
			&& std::get < 0 > (local_one_body_intgs[index]).id > 0) {
		trans2 += qmt::QmtVector(dx, dy, dz);
	}

	w_omp* p_w1; //pointer on wannier
	w_omp* p_w2;

	if (std::get < 0 > (local_one_body_intgs[index]).id % 2 == 0)
		p_w1 = &wannier2;
	else
		p_w1 = &wannier1;

	if (std::get < 1 > (local_one_body_intgs[index]).id % 2 == 0)
		p_w2 = &wannier2;
	else
		p_w2 = &wannier1;

	double result = 0;

	 result += w_omp::kinetic_integral(*p_w1,*p_w2,trans1,trans2);
	 for(auto iter=local_megacell.begin(); iter!=local_megacell.end(); iter++) {
	 //       std::cout<<qmt::QmtVector::norm(iter->position)<<" "<<iter->position<<std::endl;
	 result +=  w_omp::attractive_integral(*p_w1,*p_w2,trans1,trans2, iter->position.get_x(), iter->position.get_y(), iter->position.get_z());
	 }

	return result;
}

double qmt::examples::MicroscopicH2chain::get_two_body(unsigned int index) {

	if (index >= local_two_body_intgs.size()) {
		std::cerr
				<< "MicroscopicH2chain::get_two_body: index exceeding the number of two-body parameters"
				<< std::endl;
		return 0;
	}
	if (index != std::get < 4 > (local_two_body_intgs[index])) {
		std::cerr
				<< "MicroscopicH2chain::get_two_body: vector of microscopic parameters not sorted!"
				<< std::endl;
		return 0;
	}

	qmt::QmtVector trans1 =
			(std::get < 0 > (local_two_body_intgs[index])).position
					- reference_point;
	qmt::QmtVector trans2 =
			(std::get < 1 > (local_two_body_intgs[index])).position
					- reference_point;
	qmt::QmtVector trans3 =
			(std::get < 2 > (local_two_body_intgs[index])).position
					- reference_point;
	qmt::QmtVector trans4 =
			(std::get < 3 > (local_two_body_intgs[index])).position
					- reference_point;

	/*Phonons*/

	if (std::get < 1 > (local_two_body_intgs[index]).id < 0
			&& std::get < 2 > (local_two_body_intgs[index]).id > 0) {
		trans1 += qmt::QmtVector(dx, dy, dz);
		trans2 += qmt::QmtVector(dx, dy, dz);
	}

	if (std::get < 2 > (local_two_body_intgs[index]).id < 0
			&& std::get < 1 > (local_two_body_intgs[index]).id > 0) {
		trans3 += qmt::QmtVector(dx, dy, dz);
		trans4 += qmt::QmtVector(dx, dy, dz);
	}

	w_omp* p_w1; //pointer on wannier
	w_omp* p_w2;
	w_omp* p_w3; //pointer on wannier
	w_omp* p_w4;

	if ((std::get < 0 > (local_two_body_intgs[index]).id) % 2 == 0)
		p_w1 = &wannier2;
	else
		p_w1 = &wannier1;

	if ((std::get < 1 > (local_two_body_intgs[index]).id) % 2 == 0)
		p_w2 = &wannier2;
	else
		p_w2 = &wannier1;

	if ((std::get < 2 > (local_two_body_intgs[index]).id) % 2 == 0)
		p_w3 = &wannier2;
	else
		p_w3 = &wannier1;

	if ((std::get < 3 > (local_two_body_intgs[index]).id) % 2 == 0)
		p_w4 = &wannier2;
	else
		p_w4 = &wannier1;


	return w_omp::v_integral(*p_w1, *p_w3, *p_w2, *p_w4, trans1, trans3, trans2,
			trans4);

}

double qmt::examples::MicroscopicH2chain::get_ion_repulsion() {
	double CoulombEnergy = 0.0;
	for (auto i = local_supercell.begin(); i != local_supercell.end(); i++) {
		for (auto j = local_megacell.begin(); j != local_megacell.end(); j++) {
			double distance = qmt::QmtVector::norm(i->position - j->position);
			if (distance > 1e-5) {
				CoulombEnergy += 2.0 / distance;
			}
		}
	}
	return CoulombEnergy / 2;

}

double qmt::examples::MicroscopicH2chain::get_1st_ion_repulsion() {
	double CoulombEnergy = 0.0;

	auto i = local_supercell.begin();
	for (auto j = local_megacell.begin(); j != local_megacell.end(); j++) {
		double distance = qmt::QmtVector::norm(i->position - j->position);
		if (distance > 1e-5) {
			CoulombEnergy += 2.0 / distance;
		}
	}

	return CoulombEnergy / 2;

}

const std::vector<double> qmt::examples::MicroscopicH2chain::get_betas() const {
   return _betas;
}


unsigned int qmt::examples::MicroscopicH2chain::number_of_one_body() const {
	return one_body_intgs.size();
}

unsigned int qmt::examples::MicroscopicH2chain::number_of_two_body() const {
	return two_body_intgs.size();
}

void qmt::examples::MicroscopicH2chain::set_all(double _a, double _R,
		double _alpha, double _theta) {
	a = _a;
	R = _R;
	alpha = _alpha;
	theta = _theta;
	prepare_wannier();
}

void qmt::examples::MicroscopicH2chain::set_all(double _a, double _R,
		double _alpha, double _theta, unsigned ng) {
	Num_Gaussians = ng;
	a = _a;
	R = _R;
	alpha = _alpha;
	theta = _theta;
	prepare_wannier();
}

void qmt::examples::MicroscopicH2chain::set(double _a, double _alpha) {
	set_all(_a, R, _alpha, theta);
}

void qmt::examples::MicroscopicH2chain::set_a(double _a) {
	set_all(_a, R, alpha, theta);
}

void qmt::examples::MicroscopicH2chain::set_R(double _R) {
	set_all(a, _R, alpha, theta);
}

void qmt::examples::MicroscopicH2chain::set_alpha(double _alpha) {
	set_all(a, R, _alpha, theta);
}

void qmt::examples::MicroscopicH2chain::set_theta(double _theta) {
	set_all(a, R, alpha, _theta);
}

void qmt::examples::MicroscopicH2chain::set_ng(unsigned int ng) {
	Num_Gaussians = ng;
	set_all(a, R, alpha, theta);
}

void qmt::examples::MicroscopicH2chain::set_dx(double x) {
	dx = x;
	prepare_wannier();
}

void qmt::examples::MicroscopicH2chain::set_dy(double y) {
	dy = y;
	prepare_wannier();
}

void qmt::examples::MicroscopicH2chain::set_dz(double z) {
	dy = z;
	prepare_wannier();
}

void qmt::examples::MicroscopicH2chain::set_dxdy(double x, double y) {
	dx = x;
	dy = y;
	prepare_wannier();
}

void qmt::examples::MicroscopicH2chain::set_dxdydz(double x, double y, double z) {
	dx = x;
	dy = y;
	dz = z;
	prepare_wannier();
}

double qmt::examples::MicroscopicH2chain::get_overlap() {
 return w_omp::overlap(wannier1,wannier1);
}
