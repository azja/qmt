#define GSL_LIBRARY
#include "../../../headers/qmtmpipool.h"
#include <iostream>
#include <iomanip>
#include <iterator>
#include "../include/microscopic.h"
//#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "../../../headers/qmtbasisgenerator.h"
#include <gsl/gsl_matrix.h>
#include "../include/hamiltonianH2sc.h"
#include "../../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../../headers/qmtmatrixformula.h"
#include "../../../headers/qmtreallanczos.h"
#include <stdlib.h>
#include <fstream>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


unsigned int  NSIZE;
typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {

    virtual double integral(qmt::examples::MicroscopicH2chain &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

    uint id;

    TwoBody(uint t):
        id(t) {}

    double integral(qmt::examples::MicroscopicH2chain &model) {
        return model.get_two_body(id);
    }

};

struct OneBody:public Body {

    uint id;

    OneBody(uint t):  id(t) {}

    double integral(qmt::examples::MicroscopicH2chain &model) {
        return model.get_one_body(id);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    
    std::vector< Body*> bodies;
    double *integrals;
    int mode; //0-alpha 1-R
    qmt::examples::MicroscopicH2chain model;

public:
    SimplePoolJob(double a, double R, double theta, unsigned int size):mode(0) {
            model.set_all(a, R, 1, theta);
        for(unsigned int i = 0; i < model.number_of_one_body(); ++i)
           bodies.push_back(new OneBody(i));

        for(unsigned int i = 0; i < model.number_of_two_body(); ++i)
            bodies.push_back(new TwoBody(i));

        integrals = new double[size];

        std::fill(integrals, integrals + size, 0.0);

    }

    SimplePoolJob(double a, double R, double theta, unsigned int size, unsigned int num_gauss):mode(0) {
            model.set_all(a, R, 1, theta, num_gauss);
            
        for(unsigned int i = 0; i < model.number_of_one_body(); ++i)
            bodies.push_back(new OneBody(i));
        for(unsigned int i = 0; i < model.number_of_two_body(); ++i)
           bodies.push_back(new TwoBody(i));

        integrals = new double[size];

        std::fill(integrals, integrals + size, 0.0);

    }



    ~SimplePoolJob() {
        for(unsigned int i = 0; i < bodies.size(); ++i)
            delete bodies[i];
        delete [] integrals;
    }

    void set_alpha(double alpha){
      model.set_alpha(alpha);
    }


    void set_R(double R){
      model.set_R(R);
    }

    void set_alpha_active() {
      mode = 0;
    }
    
    void set_R_active() {
      mode = 1;
    } 
    
    void set(double a, double R, double theta) {
      model.set_all(a, R, 1, theta);
    }
    
    void set(double a, double R, double theta, int ng) {
      model.set_all(a, R, 1, theta, ng);
    }

    qmt::examples::MicroscopicH2chain* get_model()  {
        return &model;
    }

private:  //Here result is returned by params!!! result = 0 is ok it can by anything
    double Calculate(double arg,int id,void * params)  {
     
        double result  = 0;
       if( 0 == mode)
        model.set_alpha(arg);
       if( 1 == mode)
        model.set_R(arg);

       double *array = static_cast<double*>(params);

        for(unsigned int i = 0; i < NSIZE; ++i) {
            if(i + id *NSIZE < bodies.size()) {
                array[i] =  bodies [id * NSIZE +i]->integral(model);
            }
            else
                array[i] = 0.0;
            if(i + id * NSIZE == bodies.size() )
              array[i] = model.get_ion_repulsion();
        }

        return result;
    }
};


/*
 * Diagonalization stage - now performed on the single core
 */

class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {

   qmt::examples::MicroscopicH2chain* model;  					               //microscopic model					
    qmt::QmtHamiltonianH2Sc hamiltonian;                                                       //Hamiltonian
    std::vector<qmt::QmtNState <qmt::uint, int> > states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula
         <qmt::QmtHubbard<sq_operator>,LocalAlgebras::SparseAlgebraMklTridiag> 
                                                                              h2chain_formula; //Tranlate Hamiltonian to matrix
    LocalAlgebras::SparseAlgebraMklTridiag::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebraMklTridiag::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    unsigned int problem_size;
    unsigned int number_of_centers;							       //Number of atomic centers - spin obitals						
    std::vector<double> intgs;                                                                 //Integrals - microscopic parameters
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebraMklTridiag> *LanczosSolver;                //Eigen problem solver
    typedef qmt::QmtNState<qmt::uint, int> NState;                                             //Output - eigen state
    double w_vs_u;


public:
    SimpleInternalJob(int ncenters):number_of_centers(ncenters) {

     //uploading hamiltonian from the file
    hamiltonian.read_hamiltonian((char*)"chainHam.dat");

    //generating all posible electronic states
    generator.generate(number_of_centers,number_of_centers*2,states);

    // preparing hamiltonian formula engine
    h2chain_formula.set_hamiltonian(*hamiltonian.Hubbard);
    
    h2chain_formula.set_StatesOptional(states, 1);

    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebraMklTridiag::InitializeMatrix(hamiltonian_M,problem_size,problem_size);

    LocalAlgebras::SparseAlgebraMklTridiag::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebraMklTridiag>(problem_size,1000,1.0e-4);
    }

    ~SimpleInternalJob() {
    LocalAlgebras::SparseAlgebraMklTridiag::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebraMklTridiag::DeinitializeVector(eigenVector);
	delete LanczosSolver;
    }




    void set_model(qmt::examples::MicroscopicH2chain* m) {
        model = m;
    }

/* Get Hubbard criterion*/   

    double getHubbardCriterion() const { return w_vs_u;}
/* End Get Hubbar criterion*/

/* Get N-Wave density */   
    std::vector<double> NWaveFunctionEssentials(){
 
    LanczosSolver->get_minimal_eigenvector(eigenVector, hamiltonian_M);
    std::vector<double> result;


    for(unsigned int i=0; i<number_of_centers; i++){
        for(unsigned int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebraMklTridiag::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l])));
                }
            }
            result.push_back(average);
        }
    }
    for(unsigned int i=0; i<number_of_centers; i++){
        for(unsigned int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebraMklTridiag::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l])));
                }
            }
            result.push_back(average);
        }
    }
    return result;
}

/* End Get N-Wave density */   

/* Get N-Wave density */   
    const std::vector<double>  getIntegrals() const {
     return intgs;
    }
/* End Get N-Wave density */   


double AverageS2(){

    LanczosSolver->get_minimal_eigenvector(eigenVector, hamiltonian_M);
#ifdef _QMT_DEBUG
    LocalAlgebras::SparseAlgebra::PrintVector(eigenVector);
#endif
            double average=0.0;
double coord_vector = 0;
    for(unsigned int i=0; i<number_of_centers; i++){
    unsigned int k=0;
    unsigned int l=0;
  #pragma omp parallel for  private(k,l, coord_vector) shared(i) reduction(+ : average)  collapse(2)
            for( k = 0; k < states.size(); k++ ){
                for( l = 0; l <states.size(); l++ ){
			coord_vector=LocalAlgebras::SparseAlgebraMklTridiag::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l);
			if(fabs(coord_vector)>1e-16){
                    average+=coord_vector*(
			(NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_up_anihilator(i)(states[l]))))) +
			NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(i)(states[l])))))) * 0.5 +
			0.25*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(states[l]))))) +
			0.25*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l]))))) -
			0.5*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l]))))));
                }
		}
            }
    }

    return average/number_of_centers;
}


private:
    double Calculate(double* arg,int size,void * params) {

        intgs.clear();
        std::copy(&arg[0], &arg[0] + model->number_of_one_body() + model->number_of_two_body(),
               std::back_inserter(intgs));
               
        double CoulombEnergy = arg[model->number_of_one_body() + model->number_of_two_body()];
        for(unsigned int i=0; i<model->number_of_one_body(); i++) {
            h2chain_formula.set_microscopic_parameter(1,i,intgs[i]);
        }
        
        
        for(unsigned int i=0; i<model->number_of_two_body(); i++) {
            if (i < 1) h2chain_formula.set_microscopic_parameter(2,i,intgs[model->number_of_one_body()+i]);
            else     h2chain_formula.set_microscopic_parameter(2,i,0.5 * intgs[model->number_of_one_body()+i] );
        }

     
    w_vs_u = intgs[4]/fabs(4*intgs[2] - fabs(intgs[1] + 2 * intgs[3]) + fabs(intgs[1] - 2 * intgs[3]) );
    
    

        // Lanczos algorith of finding minimal eigenvalue
        double ts = MPI_Wtime();
        h2chain_formula.get_Hamiltonian_Matrix(hamiltonian_M);

        ts = MPI_Wtime() - ts;
        double td = MPI_Wtime();
        LanczosSolver->solve(hamiltonian_M);
        td = MPI_Wtime() - td;
        double energy = LanczosSolver->get_minimal_eigenvalue()/number_of_centers;
        return energy + CoulombEnergy/number_of_centers;
    }

};

/*
 * Minimization - final result
 */

class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
    double alfa;
    double minimal_alfa;
    double minimal_f;
    double a,b;
    double Calculate(void * params) {
       
                   
           int status;
            int iter = 0, max_iter = 100;
            const gsl_min_fminimizer_type *T;
            gsl_min_fminimizer *s;
            gsl_function F;

            F.function = FinalProcessJob<double>::OutFunc;
            F.params = params;

            double m = a + (b-a)/2;//, m_expected = M_PI;
            


            T = gsl_min_fminimizer_brent;
        //      T = gsl_min_fminimizer_goldensection;
            s = gsl_min_fminimizer_alloc (T);
            gsl_min_fminimizer_set (s, &F, m, a, b);


            do
            {
                iter++;
                status = gsl_min_fminimizer_iterate (s);

                m = gsl_min_fminimizer_x_minimum (s);
                a = gsl_min_fminimizer_x_lower (s);
                b = gsl_min_fminimizer_x_upper (s);
                status
                    = gsl_min_test_interval (a, b, 0.0005, 0.0);

            minimal_f = gsl_min_fminimizer_f_minimum(s);
            minimal_alfa = gsl_min_fminimizer_x_minimum(s);
            }
            while (status == GSL_CONTINUE && iter < max_iter);


            gsl_min_fminimizer_free (s);

          return minimal_alfa;
          
    }

public:
    void set_alfa(double a) {
        //alfa = a;
    }
    double get_min_v() const { return minimal_alfa;}
    double get_min_f() const {return minimal_f;}
    void set_boundaries(double x0, double x1) {
       a = x0;
       b = x1;
    }
};


double minimizer(qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE>* gatherer,
		SimpleFinalJob* finalJob, SimplePoolJob *poolJob,
		     double x0, double x1, double y0, double y1,double &x_v, double &y_v ) {

    int taskid;
    int nproc;
    
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    poolJob->set_alpha_active();
    finalJob->set_boundaries(x0,x1);
    gatherer->run();
    double x_out = finalJob->get_min_v();


    auto model_optimal  = static_cast<SimplePoolJob*>(poolJob)->get_model();

	if(taskid == nproc - 1) {
            model_optimal->set_alpha(static_cast<SimpleFinalJob*>(finalJob)->get_min_v());
        }
     
    poolJob->set_R_active();
    finalJob->set_boundaries(y0,y1);
    gatherer->run();
    double y_out = finalJob->get_min_v();




    double x = 0, y = 0;
    int cntr = 0;
    int cntr_max = 10;
    double eps_x = 0.001;
    double eps_y = 0.001;
    
    double delta_x = 0.0;
    double delta_y = 0.0;

    do {
 
    
    poolJob->set_alpha_active();
    finalJob->set_boundaries(x0,x1);
    gatherer->run();

    if( nproc - 1 == taskid) {
      x = finalJob->get_min_v();
      delta_x = fabs(x - x_out);
      x_out = x;
    }
    poolJob->set_R_active();
    finalJob->set_boundaries(y0,y1);
    gatherer->run();

    if( nproc - 1 == taskid) {
      y = finalJob->get_min_v();
      delta_y = fabs(y - y_out);
      y_out = y;
      cntr++;
    }
    MPI_Bcast(&delta_x,1,MPI_DOUBLE, nproc - 1, MPI_COMM_WORLD);
    MPI_Bcast(&delta_y,1,MPI_DOUBLE, nproc - 1, MPI_COMM_WORLD);
    MPI_Bcast(&cntr,1,MPI_INT, nproc - 1, MPI_COMM_WORLD);
    
    }
    while( (delta_x >= eps_x || delta_y >= eps_y ) && cntr < cntr_max);

   
    x_v = x;
    y_v = y;

   return finalJob->get_min_f();
}



class SimpleFinalJobEmpty:public qmt::FinalProcessJob<double, MPI_DOUBLE/*, NSIZE*/> {
    double result;
    double _alpha;
    double Calculate(void * params) {
          result = FinalProcessJob<double>::OutFunc(_alpha, params);
     return result;
    }

public:

    double get_result() const { return result;}
    void set_alpha(double alpha) { _alpha = alpha; }
};








int main(int argc, char* argv[]) {
   

    MPI_Init(NULL, NULL);

    int taskid;					//Task id
    int nproc;					//Number of processes - total - usually number of nodes if -pernode flag is used
    MPI_Comm comm;				//MPI Communicator - here global (world)
    MPI_Comm_dup( MPI_COMM_WORLD, &comm);
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);


    double a = 4.1;
    double R = 1.43042;
    double theta =  M_PI/1.999999;

    unsigned int ng;
    unsigned int n_centers;
    double R_start, R_end, R_gran;
    double a_start, a_end, a_gran;
    
//    int R_steps = 0;
//    int a_steps = 0;
    std::string config_file;
    if(argc > 1) {
       config_file = argv[1];
       std::fstream inputf;
       inputf.open(config_file.c_str(), std::ios::in);
       std::string field_name;
       inputf>>field_name>>n_centers;
       inputf>>field_name>>R_start>>R_end>>R_gran;
       inputf>>field_name>>a_start>>a_end>>a_gran;
       inputf>>field_name>>ng;
       inputf>>field_name>>theta;
//       a_steps = 1 +  (a_end - a_start) / a_gran;
//       R_steps = 1 +  (R_end - R_start) / R_gran;


#ifdef _QMT_DEBUG
       std::cout<<"n_centers = "<<n_centers<<std::endl;
       std::cout<<"R_start = "<<R_start<<" R_end = "<<R_end<<" R_gran = "<<R_gran<<std::endl;
       std::cout<<"a_start = "<<a_start<<" a_end = "<<a_end<<" a_gran = "<<a_gran<<std::endl;
       std::cout<<" ng = "<<ng<<std::endl;
       std::cout<<field_name>>theta;
//       std::cout<<" a_steps = "<<a_steps<<std::endl;
//       std::cout<<" R_steps = "<<R_steps<<std::endl;
#endif
    }
    else  {
      std::cerr<<"Give configuration filename according to scheme:"
      <<"number_of_centers value [integer]"<<std::endl
      <<"bond_length start_value end_value increment [float, float, float] "<<std::endl
      <<"intermolecular_distance start_value end_value increment [float, float, float] "<<std::endl
      <<"number_of_gaussians value [unsigned integer]"<<std::endl
      <<"Decision: exiting..."<<std::endl;
      return 0;
    }
    


 

        qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;					//Diagonalization
        qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob(n_centers);				//number of centers  -> n_centers = n_centers/2  x H2 
        qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a, R, theta,nproc - 1, ng);			
      
        static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());
        NSIZE =  (static_cast<SimplePoolJob*>(processJob)->get_model()->number_of_one_body() +
                   static_cast<SimplePoolJob*>(processJob)->get_model()->number_of_two_body())/(nproc - 1) +2;
        
        qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer(comm, finalJob, internalJob, processJob, NSIZE);


double x = 0;
double y = 0;
R = R_start;

static_cast<SimplePoolJob*>(processJob)->set(a_start, R_start, theta,ng);

double E = minimizer(&gatherer, static_cast<SimpleFinalJob*>(finalJob),static_cast<SimplePoolJob*>(processJob), 0.8, 1.5, R_start , R_end, x, y);
if(taskid == nproc - 1)
std::cout<<a_start<<" "<<y<<" "<<E<<" "<<x<<std::endl;

    delete finalJob;
    delete internalJob;
    delete processJob;

    MPI_Finalize();


    return 0;
}
