#define GSL_LIBRARY
#include <mpi.h>
#include <iostream>
#include "../microscopic.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "../hamiltonianH2sc.h"
#include "../../../headers/qmtbasisgenerator.h"
#include <gsl/gsl_matrix.h>
#include "../../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../../headers/qmtmatrixformula.h"
#include "../../../headers/qmtreallanczos.h"
#include <stdlib.h>

typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;

class SimpleInternalJob{

    qmt::examples::MicroscopicH2Sc* model;
    qmt::QmtHamiltonianH2Sc hamiltonian;
    std::vector<qmt::QmtNState <qmt::uint, int> > states;
    qmt:: QmtBasisGenerator generator;
    qmt::QmtGeneralMatrixFormula<qmt::QmtHubbard<sq_operator>,LocalAlgebras::SparseAlgebra> h2chain_formula;
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;
    LocalAlgebras::SparseAlgebra::Vector eigenVector;
    unsigned int problem_size;
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;



public:
    SimpleInternalJob() {

     //uploading hamiltonian from the file
    hamiltonian.read_hamiltonian((char*)"hamHsc.dat");
 //   std::cout<<"Hamiltonian file uploaded"<<std::endl;
    //generating all posible electronic states
    generator.generate(8,8*2,states);
  //  std::cout<<"States generated"<<std::endl;
    // preparing hamiltonian formula engine
    h2chain_formula.set_hamiltonian(*hamiltonian.Hubbard);
  //  std::cout<<"Hamiltonian formula engine prepared"<<std::endl;
    h2chain_formula.set_StatesMPI(states);
 //   std::cout<<" states have been set "<<std::endl;
    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);
    
    LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size);
    }

    ~SimpleInternalJob() {
    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	delete LanczosSolver;
    }

};

int main() {

MPI_Init(0,0);
int taskid;
int nproc;
MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
MPI_Comm_size(MPI_COMM_WORLD,&nproc);
double t_start = MPI_Wtime();
SimpleInternalJob j;
MPI_Barrier(MPI_COMM_WORLD);
if(0 == taskid) std::cout<<nproc<<" "<<MPI_Wtime() - t_start<<std::endl;
MPI_Finalize();
return 0;
}
