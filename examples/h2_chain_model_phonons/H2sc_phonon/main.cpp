#define GSL_LIBRARY
#include "../../headers/qmtmpipool.h"
#include <iostream>
#include "microscopic.h"
//#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "hamiltonianH2sc.h"
#include "../../headers/qmtbasisgenerator.h"
#include <gsl/gsl_matrix.h>
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"
#include <stdlib.h>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


unsigned int  NSIZE;
typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;

//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {

    virtual double integral(qmt::examples::MicroscopicH2Sc &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

    uint id;

    TwoBody(uint t):
        id(t) {}

    double integral(qmt::examples::MicroscopicH2Sc &model) {
        return model.get_two_body(id);
    }

};

struct OneBody:public Body {

    uint id;

    OneBody(uint t):  id(t) {}

    double integral(qmt::examples::MicroscopicH2Sc &model) {
        return model.get_one_body(id);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE/*, NSIZE*/> {

    double _a;


    std::vector< Body*> bodies;
    double *integrals;

    qmt::examples::MicroscopicH2Sc model;

public:
    SimplePoolJob(double a,double R, double theta, unsigned int size): _a(a) {
            model.set_all(a, R, 1, theta);
        for(unsigned int i = 0; i < model.number_of_one_body(); ++i)
           bodies.push_back(new OneBody(i));

        for(unsigned int i = 0; i < model.number_of_two_body(); ++i)
            bodies.push_back(new TwoBody(i));

        integrals = new double[size];

        std::fill(integrals, integrals + size, 0.0);

    }

    SimplePoolJob(double a,double R, double theta, unsigned int size, unsigned int num_gauss): _a(a) {
            model.set_all(a, R, 1, theta, num_gauss);
            
        for(unsigned int i = 0; i < model.number_of_one_body(); ++i)
            bodies.push_back(new OneBody(i));
        for(unsigned int i = 0; i < model.number_of_two_body(); ++i)
           bodies.push_back(new TwoBody(i));

        integrals = new double[size];

        std::fill(integrals, integrals + size, 0.0);

    }



    ~SimplePoolJob() {
        for(unsigned int i = 0; i < bodies.size(); ++i)
            delete bodies[i];
        delete [] integrals;
    }


    void set(double a, double R, double theta) {
      model.set_all(a, R, 1, theta);
    }

    qmt::examples::MicroscopicH2Sc* get_model()  {
        return &model;
    }

private:  //Here result is returned by params!!! result = 0 is ok it can by anything
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_alpha(arg);

        double *array = static_cast<double*>(params);

        for(unsigned int i = 0; i < NSIZE; ++i) {
            if(i + id *NSIZE < bodies.size()) {
                array[i] =  bodies [id * NSIZE +i]->integral(model);
//             std::cout<<"I#"<<id*NSIZE + i<<" = "<<array[i]<<std::endl;
            }
            else
                array[i] = 0.0;
        }

        return result;
    }
};


/*
 * Diagonalization stage - now performed on the single core
 */

class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE/*, NSIZE*/> {

   qmt::examples::MicroscopicH2Sc* model;
    qmt::QmtHamiltonianH2Sc hamiltonian;
    std::vector<qmt::QmtNState <qmt::uint, int> > states;
    qmt:: QmtBasisGenerator generator;
    qmt::QmtGeneralMatrixFormula<qmt::QmtHubbard<sq_operator>,LocalAlgebras::SparseAlgebraMklTridiag> h2chain_formula;
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;
    LocalAlgebras::SparseAlgebra::Vector eigenVector;
    unsigned int problem_size;
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebraMklTridiag> *LanczosSolver;



public:
    SimpleInternalJob() {

     //uploading hamiltonian from the file
    hamiltonian.read_hamiltonian((char*)"hamHsc.dat");

    //generating all posible electronic states
    generator.generate(8,8*2,states);

    // preparing hamiltonian formula engine
    h2chain_formula.set_hamiltonian(*hamiltonian.Hubbard);
    h2chain_formula.set_StatesMPI(states);

    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebraMklTridiag::InitializeMatrix(hamiltonian_M,problem_size,problem_size);
    LocalAlgebras::SparseAlgebraMklTridiag::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebraMklTridiag>(problem_size);
    }

    ~SimpleInternalJob() {
    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	delete LanczosSolver;
    }




    void set_model(qmt::examples::MicroscopicH2Sc* m) {
        model = m;
    }
private:
    double Calculate(double* arg,int size,void * params) {

        std::vector<double> intgs(&arg[0], &arg[0] + model->number_of_one_body() + model->number_of_two_body());

double t = omp_get_wtime();
//       std::cout<<"I0 = "<<intgs[0]<<std::endl;
        double CoulombEnergy = model->get_ion_repulsion();
	// calculating microscopic parameters and inserting them into hamiltonian formula
        
        for(int i = 0; i < 8;++i) {
         std::cout<<"Itb#"<<i<<" = "<<intgs[i + model->number_of_one_body()]<< "| ";
        }
        std::cout<<std::endl;
/*        
        for(int i = 9; i < model->number_of_one_body(); ++i) {
         intgs[i] = intgs[8];
        }*/
        for(unsigned int i=0; i<model->number_of_one_body(); i++) {
            h2chain_formula.set_microscopic_parameter(1,i,intgs[i]);
        }
        
        double local_K = 0;
        
        for(unsigned int i=0; i<model->number_of_two_body(); i++) {
            if (i<8) h2chain_formula.set_microscopic_parameter(2,i,intgs[model->number_of_one_body()+i]);
            else     h2chain_formula.set_microscopic_parameter(2,i,0.5 * intgs[model->number_of_one_body()+i] );
        }
       

        // Lanczos algorith of finding minimal eigenvalue
        h2chain_formula.get_Hamiltonian_Matrix(hamiltonian_M);
        LanczosSolver->solve(hamiltonian_M);
	// getting electronic and ionic energy per site
        double energy = LanczosSolver->get_minimal_eigenvalue()/8.0;
//        CoulombEnergy=model->get_ion_repulsion()/8.0;
//        hamiltonian_M->show_sparse();
//        std::cout<<"two_body[0]"<<intgs[model->number_of_one_body()]<<" two_body[8]= "<<intgs[model->number_of_one_body() + 8]<<" two_body[313] = "<<intgs[model->number_of_one_body() + 313]<<std::endl;
//        std::cout<<"one_body[0] = "<<intgs[0]<<" one_body[7] = "<<intgs[7]<<" one_body[8] = "<<intgs[8]<<" one_body[15] = "<<intgs[15]<<std::endl;
//        std::cout<<model->get_alpha()<<" "<<energy<<" "<<CoulombEnergy<<" "<<energy+CoulombEnergy<<std::endl;

//std::cout<<" Local K = "<<local_K<<" I11 = "<<intgs[11]<<"Coulomb = "<<model->get_1st_ion_repulsion()<<" Effective_1st = "<<intgs[0] + model->get_1st_ion_repulsion() + local_K<<std::endl;
std::cout<<"Diagonalization step time:"<< omp_get_wtime() - t<<"s Energy:"<<energy +  CoulombEnergy/8<<" Min Eigen = "<<energy * 8<<std::endl;
std::cout<<" Eff = "<<intgs[0] +0.5* std::accumulate(intgs.begin() + model->number_of_one_body() + 8,intgs.end(),0.0)/8 + CoulombEnergy/8<<std::endl;
        return energy + CoulombEnergy/8;
    }

};

/*
 * Minimization - final result
 */

class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE/*, NSIZE*/> {
    double alfa;
    double minimal_alfa;
    double minimal_f;
    double Calculate(void * params) {
      //  return FinalProcessJob<double>::OutFunc(1.0, params);
       

           int status;
            int iter = 0, max_iter = 100;
            const gsl_min_fminimizer_type *T;
            gsl_min_fminimizer *s;
            gsl_function F;

            F.function = FinalProcessJob<double>::OutFunc;
            F.params = params;

            double m = 1.0;//, m_expected = M_PI;
            double a = 1.087, b = 10.0;


            T = gsl_min_fminimizer_brent;
        //      T = gsl_min_fminimizer_goldensection;
            s = gsl_min_fminimizer_alloc (T);
            gsl_min_fminimizer_set (s, &F, m, a, b);


            do
            {
                iter++;
                status = gsl_min_fminimizer_iterate (s);

                m = gsl_min_fminimizer_x_minimum (s);
                a = gsl_min_fminimizer_x_lower (s);
                b = gsl_min_fminimizer_x_upper (s);
             //   std::cout<<m<<" "<<a<<" "<<b<<std::endl;
                status
                    = gsl_min_test_interval (a, b, 0.001, 0.0);

            minimal_f = gsl_min_fminimizer_f_minimum(s);
            minimal_alfa = gsl_min_fminimizer_x_minimum(s);
            }
            while (status == GSL_CONTINUE && iter < max_iter);


            gsl_min_fminimizer_free (s);

          return minimal_alfa;
          
    }

public:
    void set_alfa(double a) {
        //alfa = a;
    }

    double get_min_alfa() const { return minimal_alfa;}
    double get_min_f() const {return minimal_f;}
};






class SimpleFinalJobEmpty:public qmt::FinalProcessJob<double, MPI_DOUBLE/*, NSIZE*/> {
    double result;
    double _alpha;
    double Calculate(void * params) {
          result = FinalProcessJob<double>::OutFunc(_alpha, params);
     return result;
    }

public:

    double get_result() const { return result;}
    void set_alpha(double alpha) { _alpha = alpha; }
};








int main(int argc, char* argv[]) {
   

    MPI_Init(NULL, NULL);

    int taskid;					//Task id
    int nproc;					//Number of processes - total - usually number of nodes if -pernode flag is used
    MPI_Comm comm;				//MPI Communicator - here global (world)
    MPI_Comm_dup( MPI_COMM_WORLD, &comm);
    double t_start = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);


    double a = 3.0;
    double R = 1000000000000.43042;
    double theta = M_PI/2;

    unsigned int ng;
    if(argc > 1)
     ng = atoi(argv[1]);
    else 
     ng = 5;
    
    
    double Exx;
    double Eyy;
    double Ezz;

    double Exy;
    double Exz;
    double Eyz;



    double eeps = 0.001;

        NSIZE = 7340 / (nproc - 1) + 1; //SIZE OF MPI POOL CHUNK


    for(int i = 0; i < 1; ++i) {

 	a = 4.0 + i * 0.1;
    	R = 10000000000000000000.43042;
    	//theta = M_PI_2 - i * M_PI_2/30;
	theta = M_PI_2 * i/160  ;//  - i * M_PI_4/30;

        qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;					//Diagonalization
        qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
        qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a, R, theta,nproc - 1, ng);			
        qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJobEmpty = new ::SimpleFinalJobEmpty;
        static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());
        qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer(comm, finalJob, internalJob, processJob, NSIZE);
        qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE> gatherer_diff(comm, finalJobEmpty, internalJob, processJob, NSIZE);
//        a = 15.0 + i * 0.1;
        


      static_cast<SimplePoolJob*>(processJob)->get_model()->set_theta(theta);
        t_start = MPI_Wtime();

        gatherer.run();

	static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->set_alpha(static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa());	

        double E = static_cast<SimpleFinalJob*>(finalJob)->get_min_f();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_alpha(static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa());
	double E_plus;
        double E_minus;

       auto E_bis = [&]()->double{ return (E_plus - 2*E + E_minus) /(eeps * eeps); };
   //Exx
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dx(eeps);
        gatherer_diff.run();
        E_plus  = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dx(-eeps);
        gatherer_diff.run();
        E_minus = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        
        Exx = E_bis();



   //Eyy
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(0,eeps,0);
        gatherer_diff.run();
        E_plus  = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(0,-eeps,0);
        gatherer_diff.run();
        E_minus = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        
        Eyy = E_bis();

   //Ezz
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(0,0,eeps);
        gatherer_diff.run();
        E_plus  = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(0,0,-eeps);
        gatherer_diff.run();
        E_minus = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        
        Ezz = E_bis();


   //Exy
       static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(eeps,eeps,0);
        
        gatherer_diff.run();
        E_plus  = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(-eeps, -eeps,0);
        gatherer_diff.run();
        E_minus = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        
        Exy = E_bis();

   //Exz
       static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(eeps,0,eeps);
        
        gatherer_diff.run();
        E_plus  = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(-eeps, 0, -eeps);
        gatherer_diff.run();
        E_minus = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        
        Exz = E_bis();



   //Eyz
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(eeps,0,eeps);
        
        gatherer_diff.run();
        E_plus  = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        static_cast<SimplePoolJob*>(processJob)->get_model()->set_dxdydz(-eeps, 0, -eeps);
        gatherer_diff.run();
        E_minus = static_cast<SimpleFinalJobEmpty*>(finalJobEmpty)->get_result();
        
        Eyz = E_bis();


//       gatherer.run();
	if(taskid == nproc - 1) {
    	    std::cout<<a<<" "<<" "<<R<<" "<<theta<<"  "<<E<<" "<<Exx<<" "<<Exy<<" "<<Exz<<" "<<Eyy<<" "<<Eyz<<" "<<Ezz
            <<" "<<static_cast<SimpleFinalJob*>(finalJob)->get_min_alfa()<<"     "<<nproc -1<<"  "<<ng<<"  "<<MPI_Wtime() - t_start<<std::endl;
            t_start = 0;
        }

     
    delete finalJob;
    delete internalJob;
    delete processJob;
    delete finalJobEmpty;
        
    }
    MPI_Finalize();


    return 0;
}
