#include "microscopic.h"


#define MICROSCOPIC_H_DEBUG false



qmt::examples::MicroscopicH2Sc::MicroscopicH2Sc():Num_Gaussians(7),alpha(1.087/*1.19378*/),a(4.0),R(1.43042),theta(M_PI/3.0),reference_point(0,0,0),wannier(0,0,0,0),dx(0),dy(0),dz(0) {

    M = new double* [num_of_betas];
    for(int i = 0; i < num_of_betas; ++i)
        M[i] = new double [num_of_betas];

    qmt::qmGtoNSlater1s(2.0,2.0 , 1.0e-15, 0.0001, 0.0001, coefs, gammas, Num_Gaussians);


    one_body_intgs = qmt::QmtHamiltonianBuilder< qmt::QmtHubbard<sq_operator> > :: getOneBodyIntegralsV("hamHsc.dat");
    two_body_intgs = qmt::QmtHamiltonianBuilder< qmt::QmtHubbard<sq_operator> > :: getTwoBodyIntegralsV("hamHsc.dat");
    megacell  = qmt::QmtHamiltonianBuilder< qmt::QmtHubbard<sq_operator> > :: getAllAtoms("megacell.dat");
    supercell  = qmt::QmtHamiltonianBuilder< qmt::QmtHubbard<sq_operator> > :: getAllAtoms("supercell.dat");

    prepare_wannier();

}

qmt::examples::MicroscopicH2Sc::~MicroscopicH2Sc() {
    for(int i = 0; i < num_of_betas; ++i)
        delete [] M[i];
    delete [] M;
}

void  qmt::examples::MicroscopicH2Sc::calculate_betas(double a, double R, double alpha, std::vector<double>& betas, double** M) {

   

    std::vector<Gauss> batom1;
    std::vector<Gauss> batom2;
    std::vector<Gauss> batom3;
    std::vector<Gauss> batom4;
    std::vector<Gauss> batom5;
    std::vector<Gauss> batom6;
    std::vector<Gauss> batom7;
    std::vector<Gauss> batom8;
    std::vector<Gauss> batom9;
    std::vector<Gauss> batom10;
    std::vector<Gauss> batom11;
    std::vector<Gauss> batom12;




for(int i=0; i < Num_Gaussians; ++i) {
        batom1.push_back(Gauss(0,0,0,alpha*gammas[i]));
        batom2.push_back(Gauss(0,a,0,alpha*gammas[i]));
        batom3.push_back(Gauss(0,-a,0,alpha*gammas[i]));
        batom4.push_back(Gauss(-a,0,0,alpha*gammas[i]));
        batom5.push_back(Gauss(a,0,0,alpha*gammas[i]));
        batom6.push_back(Gauss(0,0,a,alpha*gammas[i]));
        batom7.push_back(Gauss(0,0,-a,alpha*gammas[i]));
        batom8.push_back(Gauss(a,a,0,alpha*gammas[i]));
        batom9.push_back(Gauss(a,-a,0,alpha*gammas[i]));
        batom10.push_back(Gauss(a,0,a,alpha*gammas[i]));
        batom11.push_back(Gauss(a,0,-a,alpha*gammas[i]));
        batom12.push_back(Gauss(2 * a,0,0,alpha*gammas[i]));

    }

    std::vector<g_omp>  bslaters;

    bslaters.push_back(g_omp(0,0,0,0));
    bslaters.push_back(g_omp(0,a,0,1));
    bslaters.push_back(g_omp(0,-a,0,2));
    bslaters.push_back(g_omp(-a,0,0,3));
    bslaters.push_back(g_omp(a,0,0,4));
    bslaters.push_back(g_omp(0,0,a,5));
    bslaters.push_back(g_omp(0,0,-a,6));


    bslaters.push_back(g_omp(a,a,0,7));
    bslaters.push_back(g_omp(a,-a,0,8));
    bslaters.push_back(g_omp(a,0,a,9));
    bslaters.push_back(g_omp(a,0,-a,10));
    bslaters.push_back(g_omp(2*a,0,0,11));


  for(int i=0; i < Num_Gaussians; ++i) {
        bslaters[0].add_element(i, coefs[i], batom1[i]);
        bslaters[1].add_element(i, coefs[i], batom2[i]);
        bslaters[2].add_element(i, coefs[i], batom3[i]);
        bslaters[3].add_element(i, coefs[i], batom4[i]);
        bslaters[4].add_element(i, coefs[i], batom5[i]);
        bslaters[5].add_element(i, coefs[i], batom6[i]);
        bslaters[6].add_element(i, coefs[i], batom7[i]);
        bslaters[7].add_element(i, coefs[i], batom8[i]);
        bslaters[8].add_element(i, coefs[i], batom9[i]);
        bslaters[9].add_element(i, coefs[i], batom10[i]);
        bslaters[10].add_element(i, coefs[i],batom11[i]);
        bslaters[11].add_element(i, coefs[i],batom12[i]);
 
    }

    Wannier<g_omp> wannier1;
    Wannier<g_omp> wannier2;


    wannier1.addOrbital(bslaters[0],0);
    wannier1.addOrbital(bslaters[1],1);
    wannier1.addOrbital(bslaters[2],1);
    wannier1.addOrbital(bslaters[3],1);
    wannier1.addOrbital(bslaters[4],1);
    wannier1.addOrbital(bslaters[5],1);
    wannier1.addOrbital(bslaters[6],1);


    wannier2.addOrbital(bslaters[4],0);
    wannier2.addOrbital(bslaters[7],1);
    wannier2.addOrbital(bslaters[8],1);
    wannier2.addOrbital(bslaters[9],1);
    wannier2.addOrbital(bslaters[10],1);
    wannier2.addOrbital(bslaters[11],1);
    wannier2.addOrbital(bslaters[0],1);

   std::vector<BilinearForm> forms;

    Wannier<g_omp>::multiply(wannier1,wannier2,M,2);
    forms.push_back(BilinearForm(M,2));



    Wannier<g_omp>::multiply(wannier1,wannier1,M,2);
   forms.push_back(BilinearForm(M,2,-1));

////////////////////////////////////////////////////////////////////

    double in[2] = { 1.041, -0.068 };
    double out[2] = {1.04, -0.068 };

   NonLinearSystemSolve(forms,in,out,1.0e-12);

    betas.clear();
    betas.push_back(out[0]);
    betas.push_back(out[1]);
    
    
    betas.clear();

    for (int i=0; i<num_of_betas; i++) {
        betas.push_back(out[i]);
    }
    
    
    

}

void qmt::examples::MicroscopicH2Sc::prepare_wannier() {

    std::vector<double> betas;

    calculate_betas(a,R,alpha, betas,M);


    atom1.clear();
    atom2.clear();
    atom3.clear();
    atom4.clear();
    atom5.clear();
    atom6.clear();
    atom7.clear();



    for(int i=0; i < Num_Gaussians; ++i) {
           atom1.push_back(Gauss(0,0,0,alpha*gammas[i]));
           atom2.push_back(Gauss(0,a,0,alpha*gammas[i]));
           atom3.push_back(Gauss(0,-a,0,alpha*gammas[i]));
           atom4.push_back(Gauss(-a,0,0,alpha*gammas[i]));
           atom5.push_back(Gauss(a,0,0,alpha*gammas[i]));
           atom6.push_back(Gauss(0,0,a,alpha*gammas[i]));
           atom7.push_back(Gauss(0,0,-a,alpha*gammas[i]));

       }


    slaters.clear();



    slaters.push_back(g_omp(0,0,0,0));
    slaters.push_back(g_omp(0,a,0,1));
    slaters.push_back(g_omp(0,-a,0,2));
    slaters.push_back(g_omp(-a,0,0,3));
    slaters.push_back(g_omp(a,0,0,4));
    slaters.push_back(g_omp(0,0,a,5));
    slaters.push_back(g_omp(0,0,-a,6));





    for(int i=0; i < Num_Gaussians; ++i) {
        slaters[0].add_element(i, coefs[i], atom1[i]);
        slaters[1].add_element(i, coefs[i], atom2[i]);
        slaters[2].add_element(i, coefs[i], atom3[i]);
        slaters[3].add_element(i, coefs[i], atom4[i]);
        slaters[4].add_element(i, coefs[i], atom5[i]);
        slaters[5].add_element(i, coefs[i], atom6[i]);
        slaters[6].add_element(i, coefs[i], atom7[i]);
    }



     wannier.clear_elements();

       wannier.add_element(0, betas[0], slaters[0]);
       wannier.add_element(1, betas[1], slaters[1]);
       wannier.add_element(2, betas[1], slaters[2]);
       wannier.add_element(3, betas[1], slaters[3]);
       wannier.add_element(4, betas[1], slaters[4]);
       wannier.add_element(5, betas[1], slaters[5]);
       wannier.add_element(6, betas[1], slaters[6]);

    local_megacell.clear();

    for(auto iter=megacell.begin(); iter!=megacell.end(); iter++) {
        local_megacell.push_back(apply_basis(*iter));
    }

    local_supercell.clear();

    for(auto iter=supercell.begin(); iter!=supercell.end(); iter++) {
        local_supercell.push_back(apply_basis(*iter));
    }

    local_one_body_intgs.clear();
    local_two_body_intgs.clear();

    for(auto iter=one_body_intgs.begin(); iter!=one_body_intgs.end(); iter++) {
        local_one_body_intgs.push_back(std:: make_tuple( apply_basis(std::get<0>(*iter)),
                                       apply_basis(std::get<1>(*iter)),std::get<2>(*iter)));
    }


    for(auto iter=two_body_intgs.begin(); iter!=two_body_intgs.end(); iter++) {
        local_two_body_intgs.push_back(std:: make_tuple( apply_basis(std::get<0>(*iter)),
                                       apply_basis(std::get<1>(*iter)), apply_basis(std::get<2>(*iter)),
                                       apply_basis(std::get<3>(*iter)),std::get<4>(*iter)));
    }
   // std::cout<<"____________________________________________________"<<std::endl;
    //std::cout<<std::get<0>(local_two_body_intgs[1])<<std::endl;
    //std::cout<<"____________________________________________________"<<std::endl;
}



qmt::QmtVectorizedAtom  qmt::examples::MicroscopicH2Sc::apply_basis(qmt::QmtVectorizedAtom in) {
    qmt::QmtVectorizedAtom out(qmt::QmtVector(in.position.get_x() * a, in.position.get_y() * a, in.position.get_z() * a),in.id);

    return out;

}


double qmt::examples::MicroscopicH2Sc::get_one_body(unsigned int index) {

    if (index >= local_one_body_intgs.size()) {
        std::cerr<<"MicroscopicH2Sc::get_one_body: index exceeding the number of one-body parameters"<<std::endl;
        return 0;
    }
    if (index != std::get<2>(local_one_body_intgs[index])) {
        std::cerr<<"MicroscopicH2Sc::get_one_body: vector of microscopic parameters not sorted!"<<std::endl;
        return 0;
    }

    qmt::QmtVector trans1 = (std::get<0>(local_one_body_intgs[index])).position-reference_point;
    qmt::QmtVector trans2 = (std::get<1>(local_one_body_intgs[index])).position-reference_point;

     if(std::get<0>(local_one_body_intgs[index]).id < 0 &&  std::get<1>(local_one_body_intgs[index]).id > 0) {
     trans1 += qmt::QmtVector(dx, dy, dz);
     }

     if(std::get<1>(local_one_body_intgs[index]).id < 0 &&  std::get<0>(local_one_body_intgs[index]).id > 0) {
      trans2 += qmt::QmtVector(dx, dy, dz);
     }

    
    

    double result=0;

    result += w_omp::kinetic_integral(wannier,wannier,trans1,trans2);
    for(auto iter=local_megacell.begin(); iter!=local_megacell.end(); iter++) {
       
        result +=  w_omp::attractive_integral(wannier,wannier,trans1,trans2, iter->position.get_x(), iter->position.get_y(), iter->position.get_z());
    }
  
    return result;
}



double qmt::examples::MicroscopicH2Sc::get_two_body(unsigned int index) {

    if (index >= local_two_body_intgs.size()) {
        std::cerr<<"MicroscopicH2Sc::get_two_body: index exceeding the number of two-body parameters"<<std::endl;
        return 0;
    }
    if (index != std::get<4>(local_two_body_intgs[index])) {
        std::cerr<<"MicroscopicH2Sc::get_two_body: vector of microscopic parameters not sorted!"<<std::endl;
        return 0;
    }


    qmt::QmtVector trans1 = (std::get<0>(local_two_body_intgs[index])).position-reference_point;
    qmt::QmtVector trans2 = (std::get<1>(local_two_body_intgs[index])).position-reference_point;
    qmt::QmtVector trans3 = (std::get<2>(local_two_body_intgs[index])).position-reference_point;
    qmt::QmtVector trans4 = (std::get<3>(local_two_body_intgs[index])).position-reference_point;


/*Phonons*/

    if(std::get<1>(local_two_body_intgs[index]).id < 0 &&  std::get<2>(local_two_body_intgs[index]).id > 0) {
     trans1 += qmt::QmtVector(dx, dy, dz);
     trans2 += qmt::QmtVector(dx, dy, dz);
     }

     if(std::get<2>(local_two_body_intgs[index]).id < 0 &&  std::get<1>(local_two_body_intgs[index]).id > 0) {
      trans3 += qmt::QmtVector(dx, dy, dz);
      trans4 += qmt::QmtVector(dx, dy, dz);
     }
    
    return w_omp::v_integral(wannier,wannier,wannier,wannier,trans1,trans3,trans2,trans4);
    
}

double qmt::examples::MicroscopicH2Sc::get_ion_repulsion() {
double local_c = 0.0;
    double CoulombEnergy=0.0;
    for(auto i=local_supercell.begin(); i!=local_supercell.end(); i++) {
        for(auto j=local_megacell.begin(); j!=local_megacell.end(); j++) {
            double distance = qmt::QmtVector::norm(i->position - j->position);
            if (distance > 1e-5){ CoulombEnergy+=2.0/distance; local_c += 2.0/distance; }
        }
        local_c = 0;
    }
    return CoulombEnergy/2;

}

double qmt::examples::MicroscopicH2Sc::get_1st_ion_repulsion() {
    double CoulombEnergy=0.0;
	
    auto i=local_supercell.begin(); 
    	for(auto j=local_megacell.begin(); j!=local_megacell.end(); j++) {
            double distance = qmt::QmtVector::norm(i->position - j->position);
            if (distance > 1e-5){ CoulombEnergy+=2.0/distance; }
        }

    return CoulombEnergy/2;

}


unsigned int qmt::examples::MicroscopicH2Sc::number_of_one_body() const {
    return one_body_intgs.size();
}

unsigned int qmt::examples::MicroscopicH2Sc::number_of_two_body() const {
    return two_body_intgs.size();
}

void qmt::examples::MicroscopicH2Sc::set_all(double _a, double _R, double _alpha, double _theta) {
    a=_a;
    R=_R;
    alpha=_alpha;
    theta=_theta;
    prepare_wannier();
}

void qmt::examples::MicroscopicH2Sc::set_all(double _a, double _R, double _alpha, double _theta, unsigned ng) {
    Num_Gaussians = ng;
    a=_a;
    R=_R;
    alpha=_alpha;
    theta=_theta;
    prepare_wannier();
}


void qmt::examples::MicroscopicH2Sc::set(double _a, double _alpha) {
    set_all(_a, R, _alpha, theta);
}

void qmt::examples::MicroscopicH2Sc::set_a(double _a) {
    set_all(_a, R, alpha, theta);
}

void qmt::examples::MicroscopicH2Sc::set_R(double _R) {
    set_all(a, _R, alpha, theta);
}

void qmt::examples::MicroscopicH2Sc::set_alpha(double _alpha) {
    set_all(a, R, _alpha, theta);
}

void qmt::examples::MicroscopicH2Sc::set_theta(double _theta) {
    set_all(a, R, alpha, _theta);
}


void qmt::examples::MicroscopicH2Sc::set_ng(unsigned int  ng) {
    Num_Gaussians = ng;
    set_all(a, R, alpha, theta);
}

void qmt::examples::MicroscopicH2Sc::set_dx(double x) {
    dx = x;
    prepare_wannier();
}

void qmt::examples::MicroscopicH2Sc::set_dy(double y) {
    dy = y;
    prepare_wannier();
}

void qmt::examples::MicroscopicH2Sc::set_dz(double z) {
    dy = z;
    prepare_wannier();
}

void qmt::examples::MicroscopicH2Sc::set_dxdy(double x, double y) {
    dx = x;
    dy = y;
    prepare_wannier();
}

void qmt::examples::MicroscopicH2Sc::set_dxdydz(double x, double y, double z) {
    dx = x;
    dy = y;
    dz = z;
    prepare_wannier();
}


