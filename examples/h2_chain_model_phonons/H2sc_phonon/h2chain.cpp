#include "../../headers/qmtmpipool.h"
#include <iostream>
#include "microscopic.h"
//#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>

/*
 * main.cpp
 *
 *  Created on: 30 cze 2014
 *      Author: abiborski
 */


unsigned int  NSIZE=20;


//////////////////////////////////////////////////////////////////////////

typedef unsigned int uint;

struct Body {

    virtual double integral(qmt::examples::MicroscopicH2Chain &model) = 0;

    virtual ~Body() {}

};

struct TwoBody:public Body {

    uint id;

    TwoBody(uint t):
        id(t) {}

    double integral(qmt::examples::MicroscopicH2Chain &model) {
        return model.get_two_body(id);
    }

};

struct OneBody:public Body {

    uint id;

    OneBody(uint t):  id(t) {}

    double integral(qmt::examples::MicroscopicH2Chain &model) {
        return model.get_one_body(id);
    }

};

/*
 * Calculation of microscopic parameters - each integral will be proceed on the different node
 */

class SimplePoolJob:public qmt::PoolProcessJob<double, MPI_DOUBLE> {

    double _a;


    std::vector< Body*> bodies;
    double *integrals;

    qmt::examples::MicroscopicH2Chain model;

public:
    SimplePoolJob(double a, unsigned int size): _a(a) {
        model.set(_a, 1);


        typedef qmt::QmtVector vec3d;

        for(auto i = 0; i < model.number_of_one_body(); ++i)
            bodies.push_back(OneBody(i));


        for(auto i = 0; i < model.number_of_two_body(); ++i)
            bodies.push_back(TwoBody(i));

        integrals = new double[size];

        std::fill(integrals, integrals + size, 0.0);

    }

    ~SimplePoolJob() {
        for(int i = 0; i < bodies.size(); ++i)
            delete bodies[i];
        delete [] integrals;
    }


    void set_a(double a) {
        _a = a;


        model.set_a(a);
    }
    qmt::examples::MicroscopicH2Chain* get_model()  {
        return &model;
    }

private:  //Here result is returned by params!!! result = 0 is ok it can by anything
    double Calculate(double arg,int id,void * params)  {
        double result  = 0;
        model.set_alpha(arg);

        double *array = static_cast<double*>(params);

        for(auto i = 0; i < NSIZE; ++i) {
            if(i + id *NSIZE < bodies.size())
                array[i] =  bodies [id * NSIZE +i]->integral(model);
            else
                array[i] = 0.0;
        }

        return result;
    }
};


/*
 * Diagonalization stage - now performed on the single core
 */

class SimpleInternalJob:public qmt::InternalProcessJob<double, MPI_DOUBLE> {

    qmt::examples::MicroscopicH2Chain* model;

public:
    SimpleInternalJob() {

    }

    ~SimpleInternalJob() {

    }




    void set_model(qmt::examples::MicroscopicH2Chain* m) {
        model = m;
    }
private:
    double Calculate(double* arg,int size,void * params) {

        std::vector<double> intgs(&arg[0], &arg[0] + model->number_of_one_body() + model->number_of_two_body());

        for(int i = 0; i < intgs.size(); ++i)
            std::cout<<"i"<<i<<"="<<intgs[i]<<std::endl;

        return 0;
    }
};

/*
 * Minimization - final result
 */

class SimpleFinalJob:public qmt::FinalProcessJob<double, MPI_DOUBLE> {
    double alfa;
    double minimal_alfa;
    double minimal_f;
    double Calculate(void * params) {
        return FinalProcessJob<double>::OutFunc(1.0, params);
        /*

           int status;
            int iter = 0, max_iter = 100;
            const gsl_min_fminimizer_type *T;
            gsl_min_fminimizer *s;
            gsl_function F;

            F.function = FinalProcessJob<double>::OutFunc;
            F.params = params;

            double m = 1.0, m_expected = M_PI;
            double a = 0.5, b = 10.0;


        //     T = gsl_min_fminimizer_brent;
              T = gsl_min_fminimizer_goldensection;
            s = gsl_min_fminimizer_alloc (T);
            gsl_min_fminimizer_set (s, &F, m, a, b);


            do
            {
                iter++;
                status = gsl_min_fminimizer_iterate (s);

                m = gsl_min_fminimizer_x_minimum (s);
                a = gsl_min_fminimizer_x_lower (s);
                b = gsl_min_fminimizer_x_upper (s);
             //   std::cout<<m<<" "<<a<<" "<<b<<std::endl;
                status
                    = gsl_min_test_interval (a, b, 0.0000001, 0.0);

            minimal_f = gsl_min_fminimizer_f_minimum(s);
            minimal_alfa = gsl_min_fminimizer_x_minimum(s);
            }
            while (status == GSL_CONTINUE && iter < max_iter);


            gsl_min_fminimizer_free (s);

          return minimal_alfa;
          */
    }

public:
    void set_alfa(double a) {
        //alfa = a;
    }

    //double get_min_alfa() const { return minimal_alfa;}
    //double get_min_f() const {return minimal_f;}
};






int main() {
    /*
    	qmt::examples::MicroscopicH2Chain model;

    std::vector<double> out;

    //model.get_integrals(out);



    double a;

    //for(int i = 0; i < 25; ++i) {
    //a = .5 + i * 0.1;
    //model.set_alpha(1.084);
    //model.get_integrals(out);
    //std::cout<<a<<" "<<out[0]<<" "<<out[1]<<" "<<out[2]<<" "<<out[3]<<" "<<out[4]<<" "<<out[5]<<std::endl;
    std::cout<<a<<model.get_one_body_integral(qmt::QmtVector(0,0,0))<<" "<<model.get_one_body_integral(qmt::QmtVector(3.8,0,0))<<" "<<model.get_two_body_integral(qmt::QmtVector(0,0,0))<<" "<<model.get_system_size()<<std::endl;
    out.clear();
    double K_sum = 0;
    std::cout<< model.number_of_pairs_size()<<std::endl;

    for(int i = 0; i < model.number_of_pairs_size(); ++i) {
          // std::cout<<model.get_two_body_integral(model.get_pair_vector(i))<<" ";
           K_sum += 0.5 * (2.0/qmt::QmtVector::norm(model.get_pair_vector(i)) + model.get_two_body_integral(model.get_pair_vector(i)) )* model.number_of_pairs(i) ;
    //std::cout<<model.get_pair_vector(i)<<" "<< model.number_of_pairs(i)<<std::endl;
    }

    std::cout<<" comp = "<<K_sum<<std::endl;

    //}

    //for(int i = 0; i < out.size(); ++i)
    //std::cout<<i<<"# = "<<out[i]<<std::endl;
    */

    MPI_Init(NULL, NULL);

    int taskid;
    int nproc;
    MPI_Comm comm;
    MPI_Comm_dup( MPI_COMM_WORLD, &comm);
    double t_start;// = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);


    double a = 3.01;
//	qmt::FinalProcessJob<double, MPI_DOUBLE>* finalJob = new ::SimpleFinalJob;
//	qmt::InternalProcessJob<double, MPI_DOUBLE>* internalJob = new ::SimpleInternalJob;
///	qmt::PoolProcessJob<double, MPI_DOUBLE>* processJob = new ::SimplePoolJob(a,nproc - 1);

//	static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());

    for(int i = 0; i < 100; ++i) {
        a = 3.5+ i * 0.1;
        t_start = MPI_Wtime();

        qmt::FinalProcessJob<double, MPI_DOUBLE, 8>* finalJob = new ::SimpleFinalJob;
        qmt::InternalProcessJob<double, MPI_DOUBLE, 8>* internalJob = new ::SimpleInternalJob;
        qmt::PoolProcessJob<double, MPI_DOUBLE, 8>* processJob = new ::SimplePoolJob(a,nproc - 1);
        static_cast<SimpleInternalJob*>(internalJob)->set_model(static_cast<SimplePoolJob*>(processJob)->get_model());

        qmt::QmtMPIGatheredFunction<double, MPI_DOUBLE, 8> gatherer(comm, finalJob, internalJob, processJob);

        double result = gatherer.run();



        delete finalJob;
        delete internalJob;
        delete processJob;
    }
    MPI_Finalize();


    return 0;
}
