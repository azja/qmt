#!/usr/bin/python

import math
import numpy
import sys

# lattice constant
a=4.0
verX=numpy.array([1,0,0])
verY=numpy.array([0,1,0])
verZ=numpy.array([0,0,1])
vers=[verX,verY,verZ]
latticeX=a*verX
latticeY=a*verY
latticeZ=a*verZ
lattice=[latticeX,latticeY,latticeZ]

# displacement
delta = numpy.array([0.1,0.1,0.1])

# max Coordination Radius
# must be the same as in main program!
CoordRadMul=2
maxCoordRad=CoordRadMul*a

# super celllatticeX
SuperCell = [
[0,numpy.array([0,0,0]),0,0,0,-1],
[1,latticeX,1,0,0,0],
[2,latticeY,0,1,0,0],
[3,latticeZ,0,0,1,0],
[4,latticeX+latticeY,1,1,0,0],
[5,latticeY+latticeZ,0,1,1,0],
[6,latticeZ+latticeX,1,0,1,0],
[7,latticeX+latticeY+latticeZ,1,1,1,0]
]


def distance(atom1,atom2):
	return math.sqrt(math.pow(atom1[1][0]-atom2[1][0],2)+math.pow(atom1[1][1]-atom2[1][1],2)+math.pow(atom1[1][2]-atom2[1][2],2))
#distance

def CellTrans(translation,normal_translation,Cell):
	newCell=[]
	for atom in Cell:
		newatom=[atom[0],atom[1]+translation,\
				atom[2]+normal_translation[0],atom[3]+normal_translation[1],atom[4]+normal_translation[2],atom[5]]
		newCell.append(newatom)
	return newCell
#translation

def ClearCell(Cell,referenceCell,cutoff):
	for i in range(len(Cell)-1,0-1,-1):
		IF_REMOVE = True
		for atom in referenceCell:
			if(distance(atom,Cell[i]) < cutoff):
				IF_REMOVE = False
		if IF_REMOVE:
			Cell.pop(i)
				


# super cell with border
BorderSuperCell = []

for i in [-2,2]:
	for direction in vers:
			BorderSuperCell+=CellTrans(i*direction*a,i*direction,SuperCell)

ClearCell(BorderSuperCell,SuperCell,a*math.sqrt(2.0))



# mega cell
MegaCell=[] 
#MegaCell+= SuperCell

for i in range(-CoordRadMul/2, (CoordRadMul+1)/2+1):
	for j in range(-CoordRadMul/2, (CoordRadMul+1)/2+1):
		for k in range(-CoordRadMul/2, (CoordRadMul+1)/2+1):
			direction=2*(i*verX+j*verY+k*verZ)
			MegaCell+=CellTrans(direction*a,direction,SuperCell)

ClearCell(MegaCell,[SuperCell[0],SuperCell[1]],math.sqrt(maxCoordRad*maxCoordRad+a*a))


MegaCellFile = open('sc_megacell.dat', 'w+')
for atom in MegaCell:
	MegaCellFile.write("("+str(atom[2])+",  "+str(atom[3])+",  "+str(atom[4])+",  "+str(atom[5])+");\n")
MegaCellFile.close()
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################
############################################################################################################################################################

# anihilator
def an(N,s):
	return str(N)+"a"+str(s)
# creator
def cr(N,s):
	return str(N)+"c"+str(s)
# number operator
def num(N,s):
	return cr(N,s)+",	"+an(N,s)

# spins
spin = ["up","down"]

# actual microscopic parameter flag

one_body_flag = 0
two_body_flag = 0

# epsilon_a
print ""
print "one_body"
for itemS in SuperCell:
	for s in spin:
		print 	cr(itemS[0],s)+",	"+an(itemS[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"
	one_body_flag+=1
print "end_one_body"



# t in SuperCell
print ""
print "one_body"
for i in range(0,len(SuperCell)):
	for j in range(i+1,len(SuperCell)):
		dij=distance(SuperCell[i],SuperCell[j])
		if (i==j or dij >= a*math.sqrt(2)):
			continue
		for s in spin:
			print cr(SuperCell[i][0],s)+",	"+an(SuperCell[j][0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		";"
			print cr(SuperCell[j][0],s)+",	"+an(SuperCell[i][0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(SuperCell[j][2])+",  "+str(SuperCell[j][3])+",  "+str(SuperCell[j][4])+",  "+str(SuperCell[j][5])+")"\
		+		",	"\
		+		"("+str(SuperCell[i][2])+",  "+str(SuperCell[i][3])+",  "+str(SuperCell[i][4])+",  "+str(SuperCell[i][5])+")"\
		+		";"
		one_body_flag+=1
print "end_one_body"


# t with border
print ""
print "one_body"
for itemS in SuperCell:
	for itemB in BorderSuperCell:
		dij=distance(itemS,itemB)
		if (dij<1e-16 or dij >= a*math.sqrt(2)):
			continue
		for s in spin:
			print cr(itemS[0],s)+",	"+an(itemB[0],s)+",	"+" +,	"+str(one_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemB[2])+",  "+str(itemB[3])+",  "+str(itemB[4])+",  "+str(itemB[5])+")"\
		+		";"
		one_body_flag+=1
print "end_one_body"


# U
print ""
print "two_body"
for itemS in SuperCell:
	print num(itemS[0],spin[0])+",	"+num(itemS[0],spin[1])+",	"+" +,	"+str(two_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+       ";"
	two_body_flag+=1
print "end_two_body"


# K
print ""
print "two_body"
for itemS in SuperCell:
	for itemM in MegaCell:
		dij=distance(itemS,itemM)
		if (dij<1e-16):
			continue
		for s1 in spin:
			for s2 in spin:
				print num(itemS[0],s1)+",	"+num(itemM[0],s2)+",	"+" +,	"+str(two_body_flag)+",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemS[2])+",  "+str(itemS[3])+",  "+str(itemS[4])+",  "+str(itemS[5])+")"\
		+		",	"\
		+		"("+str(itemM[2])+",  "+str(itemM[3])+",  "+str(itemM[4])+",  "+str(itemM[5])+")"\
		+		",	"\
		+		"("+str(itemM[2])+",  "+str(itemM[3])+",  "+str(itemM[4])+",  "+str(itemM[5])+")"\
		+       ";"
		two_body_flag+=1
print "end_two_body"

print two_body_flag+one_body_flag

sys.exit()
