#include "hamiltonianH2.h"

    qmt::QmtHamiltonianH2chain:: QmtHamiltonianH2chain(){
	Hubbard=NULL;
}

    qmt::QmtHamiltonianH2chain::~QmtHamiltonianH2chain(){
	delete Hubbard;
}


void qmt::QmtHamiltonianH2chain::read_hamiltonian(char* filename) {

    Hubbard = qmt::QmtHamiltonianBuilder< qmt::QmtHubbard<sq_operator> > :: getHamiltonian(filename);

}


