#include "hamiltonianH2sc.h"

    qmt::QmtHamiltonianH2Sc :: QmtHamiltonianH2Sc(){
	Hubbard=NULL;
}

    qmt::QmtHamiltonianH2Sc ::~QmtHamiltonianH2Sc(){
	delete Hubbard;
}


void qmt::QmtHamiltonianH2Sc ::read_hamiltonian(char* filename) {

    Hubbard = qmt::QmtHamiltonianBuilder< qmt::QmtHubbard<sq_operator> > :: getHamiltonian(filename);

}


