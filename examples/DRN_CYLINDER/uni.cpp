#include <algorithm>
#include <iostream>
#include <vector>


int main(){

std::vector<int> v({1,2,1,3,1,3,5});
std::sort(v.begin(),v.end());
auto it = std::unique(v.begin(),v.end());

std::cout<<"Last is:"<< int(it - v.begin())<<std::endl;

for(const auto &x : v)
 std::cout<<"x = "<<x<<std::endl;

return 0;
}
