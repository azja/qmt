#ifndef QMT_PRIME_DICT_INCLUDED
#define QMT_PRIME_DICT_INCLUDED 

#include <tuple>
#include <cmath>

namespace qmt {

//typename std::tuple<int,int> CodePair;
int prime(int n);


struct QmtIntegralCode {

std::tuple<int,int> outer;
std::tuple<int,int> inner;
int total_momentum;

QmtIntegralCode(int i, int j, int k, int l, int sgn1,int sgn2,int sgn3,int sgn4);

bool operator==(const QmtIntegralCode& code) const;

};

}

#endif //QMT_PRIME_DICT_INCLUDED
