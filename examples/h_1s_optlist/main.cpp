#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <sstream>
#include <algorithm>
#include "../../headers/qmtlncdiag.h"
#include "../../headers/qmtsystemstandard.h"
#include "../../headers/qmtparsertools.h"

struct Data{
 qmt::QmtSystem *system;
 qmt::QmtDiagonalizer *diagonalizer;
 qmt::QmtVector *scale;
 double *Z0;
 double *Zm;
 double *Zp;
};

double energy(double x, void* p){
 std::vector<double> one_body;
 std::vector<double> two_body;

 Data *sol = (Data*) p;

 sol->system->set_parameters(std::vector<double>({x}),*(sol->scale));

 for(auto i = 0U; i < sol->system->get_one_body_number();++i)
   one_body.push_back(sol->system->get_one_body_integral(i));

 for(auto i = 0U; i < sol->system->get_two_body_number();++i)
   two_body.push_back(sol->system->get_two_body_integral(i));

return sol->diagonalizer->Diagonalize(one_body, two_body);
}

double minimize(void* solver, double& Zm, double& Z0, double& Zp, bool ALPHA_SCAN=false){

  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;

  gsl_function F;
  F.function = &energy;
  F.params = solver;

  double E;

  if(!ALPHA_SCAN){
    T = gsl_min_fminimizer_brent;
    s = gsl_min_fminimizer_alloc (T);
//    std::cout<<Zeta0<<",\t"<<ZetaMin<<",\t"<<ZetaMax<<std::endl;
//    std::cout<<energy(Zeta0,solver)<<",\t"<<energy(ZetaMin,solver)<<",\t"<<energy(ZetaMax,solver)<<std::endl;
//    std::cout<<"##################################################################"<<std::endl;
//
    double F0=energy(Z0, solver);
    double Fm=energy(Zm, solver);
    double Fp=energy(Zp, solver);
    unsigned int fuse = 0;
    while(F0>Fm || F0>Fp){
      if(Fm<Fp){
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
        Zp=Z0;
        Fp=F0;
        Z0=Zm;
        F0=F0;
        Zm-=(Zp-Z0);
        Fm=energy(Zm, solver);
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
      }
      else{
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
        Zm=Z0;
        Fm=F0;
        Z0=Zp;
        F0=Fp;
        Zp+=(Z0-Zm);
        Fp=energy(Zp, solver);
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
      }
      fuse++;
      if(fuse>100){
        std::cerr<<"Can't find a minimum!"<<std::endl;
	exit(-1);
      }
    }

    gsl_min_fminimizer_set_with_values(s, &F, Z0, F0, Zm, Fm, Zp, Fp); 

    do
      {
        iter++;
        status = gsl_min_fminimizer_iterate (s);

        Z0 = gsl_min_fminimizer_x_minimum (s);
        Zm = gsl_min_fminimizer_x_lower (s);
        Zp = gsl_min_fminimizer_x_upper (s);

        status = gsl_min_test_interval (Zm, Zp, 0.00001, 0.0);
      }
    while (status == GSL_CONTINUE && iter < max_iter);

    E = gsl_min_fminimizer_f_minimum(s);

  }
  else{
    double a=Zm;
    E=1000000.0;
    while(a<Zp){
	double tE = energy(a,solver);
	if(tE<E) E=tE;
  	std::cout<<a<<" "<<tE<<std::endl;
  	a+=0.02;
    }
    gsl_min_fminimizer_free (s);
  }

  return E;
}

double tominimizeR(double x, void* p){
  Data *sol = (Data*) p;

  sol->scale->set_z(x);

//  std::cout<<*(sol->scale)<<std::endl;

  return minimize((void*)sol, *(sol->Zm), *(sol->Z0), *(sol->Zp));
} 

double minimizeR(void* solver, double& R0, double& Rm, double& Rp, bool R_SCAN=false){
  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;

  gsl_function F;
  F.function = &tominimizeR;
  F.params = solver;

  Data* sol = (Data*) solver;

  double Z0 = *(sol->Z0);
  double Zm = *(sol->Zm);
  double Zp = *(sol->Zp);

  double E;

  if(!R_SCAN){
    T = gsl_min_fminimizer_brent;
    s = gsl_min_fminimizer_alloc (T);
 
    double F0=tominimizeR(R0, solver);
    double Fm=tominimizeR(Rm, solver);
    double Fp=tominimizeR(Rp, solver);
    unsigned int fuse = 0;
    while(F0>Fm || F0>Fp){
      if(Fm<Fp){
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
        Rp=R0;
        Fp=F0;
        R0=Rm;
        F0=F0;
        Rm-=(Rp-R0);
        Fm=tominimizeR(Rm, solver);
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
      }
      else{
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
        Rm=R0;
        Fm=F0;
        R0=Rp;
        F0=Fp;
        Rp+=(R0-Rm);
        Fp=tominimizeR(Rp, solver);
      	//std::cout<<F0<<" ["<<Fm<<","<<Fp<<"];\t"<<Z0<<" ["<<Zm<<","<<Zp<<"];"<<std::endl;
      }
      fuse++;
      if(fuse>100){
        std::cerr<<"Can't find a minimum!"<<std::endl;
	exit(-1);
      }
    }

    gsl_min_fminimizer_set_with_values(s, &F, R0, F0, Rm, Fm, Rp, Fp);

   do
      {
	*(sol->Z0) = Z0;
	*(sol->Zm) = Zm;
	*(sol->Zp) = Zp;

        iter++;
        status = gsl_min_fminimizer_iterate (s);

        R0   = gsl_min_fminimizer_x_minimum (s);
        Rm = gsl_min_fminimizer_x_lower (s);
        Rp = gsl_min_fminimizer_x_upper (s);

        status = gsl_min_test_interval (Rm, Rp, 0.001, 0.0);
      }
    while (status == GSL_CONTINUE && iter < max_iter);

    E = gsl_min_fminimizer_f_minimum(s);
  }
  else{
    double R=Rm;
    E=1000000.0;
    while(R<=Rp){
        double tE = energy(R,solver);
        if(tE<E) E=tE;
        std::cout<<R<<" "<<tE<<std::endl;
        R+=0.02;
    }
    gsl_min_fminimizer_free (s);
  }

  return E;
}

void setme (std::string& input, std::vector<double>& XY,
                                       std::vector<double>& Z,
                                      std::vector<double>& dz,
                                    std::vector<double>& zeta,
                                  std::vector<double>& dzeta);

int main(int argc, const char* argv[]) {

 gsl_set_error_handler_off(); // turns off the GSL error handler

 std::string config("conf.dat");

 double Z0 = 1.0;
 double Zp = 2.0;
 double Zm = 0.6;

 double R0 = 1.4;
 double Rp = 1.5;
 double Rm = 0.9;

 std::vector<double> XY({4.0});
 std::vector<double> Z({1.4});
 std::vector<double> dZ({0.1});
 std::vector<double> zeta({1.16});
 std::vector<double> dzeta({0.1});

 if(argc>1){
   std::ifstream file(argv[1]);
   std::string input((std::istreambuf_iterator<char>(file)),
                      std::istreambuf_iterator<char>());
	
   setme (input, XY, Z, dZ, zeta, dzeta);
 }

 if(argc>2)
  config=std::string(argv[2]);


 for(unsigned int i=0; i<XY.size(); i++){
	Z0=zeta[i];
	Zm=zeta[i]-dzeta[i];
	Zp=zeta[i]+dzeta[i];

	R0=Z[i];
	Rm=Z[i]-dZ[i];
	Rp=Z[i]+dZ[i];

	Data solver; 
	solver.system = new qmt::QmtSystemStandard(config.c_str()); 
	solver.diagonalizer = new qmt::QmtLanczosDiagonalizer(config.c_str(),1);
	solver.Z0 = &Z0;
	solver.Zm = &Zm;
	solver.Zp = &Zp;

	solver.scale = new qmt::QmtVector(XY[i],XY[i],R0);

        std::cout<<XY[i]<<" "<<minimizeR((void*) &solver,R0,Rm,Rp);
        std::cout<<" at ("<<R0<<","<<Z0<<")"<<std::endl;
  }

 return 0; 
}

void setme (std::string& input, std::vector<double>& XY,
                                 std::vector<double>& Z,
                                std::vector<double>& dz,
                              std::vector<double>& zeta,
                             std::vector<double>& dzeta){
  XY.clear();
  Z.clear();
  dz.clear();
  zeta.clear();
  dzeta.clear();

  //remove comments
  std::size_t found = 0;
  while((found = input.find("#",found))!=std::string::npos){
        std::size_t f2 = input.find("\n",found);
	input.erase(found,f2-found+1); // +1 to erase "\n" as well!
  }

  std::istringstream iss(input);
  std::string line;
  std::string::size_type ptr;
  while (std::getline(iss, line)){
    std::vector<std::string> nums = qmt::parser::get_delimited_words(" ",line);

//    std::cout<<"line: "<<line<<std::endl;
    if(nums.size()>4){
      XY.push_back(std::stod(nums[0],&ptr));
      Z.push_back(std::stod(nums[1],&ptr));
      dz.push_back(std::stod(nums[2],&ptr));
      zeta.push_back(std::stod(nums[3],&ptr));
      dzeta.push_back(std::stod(nums[4],&ptr));
    }

  }
}

