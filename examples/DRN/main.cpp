#include "qmtdrnmicroscopic.h"
#include "qmtdrnwf.h"
#include "../../headers/qmtspline.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../headers/qmtmatrixformula.h"
#include "../../headers/qmtreallanczos.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>



#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include <stdio.h>

const size_t NVAR = 4;
typedef qmt::SqOperator<qmt::QmtNState<unsigned long, int> > sq_operator;
typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<unsigned long, int> >> qmt_hamiltonian; 

struct System {

qmt::QmtDrnMicroscopic<qmt_hamiltonian,QmtDrnWaveFunction<qmt::QmtSpline1dGsl>> microscopic;
//qmt::QmtDrnMicroscopic<qmt_hamiltonian,QmtDrnWaveFunction<qmt::QmtTableFunction1d>> microscopic;
    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<qmt::QmtNState <unsigned long, int> > states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula
         <qmt_hamiltonian,LocalAlgebras::SparseAlgebra> 
                                                                              h2_formula; //Tranlate Hamiltonian to matrix
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    unsigned int problem_size;
    unsigned int number_of_centers;							       //Number of atomic centers - spin obitals						
    std::vector<double> intgs;                                                                 //Integrals - microscopic parameters
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;                //Eigen problem solver
    typedef qmt::QmtNState<unsigned long, int> NState;                                             //Output - eigen state
    int _lanczos_steps;
    int _lanczos_eps;
    double w_vs_u;



System(const std::string &file_name):microscopic(file_name),_lanczos_steps(40), _lanczos_eps(1.0e-9)  {

      std::ifstream cfile;      
      
      cfile.open(file_name.c_str());
      
      std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
   
   std::string  hamiltonian_fn =  qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
    hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(hamiltonian_fn);
  
    std::string n_oc = qmt::parser::get_bracketed_words(content,"states_number","states_number_end")[0];
     number_of_centers = std::stoi(n_oc);
     
      std::string n_el_str = qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0];
     int n_el = std::stoi(n_el_str);

#ifdef _QMT_VERBOSE
std::cerr<<"Generating basis...";
#endif

    generator.generate(n_el,number_of_centers*2,states);

#ifdef _QMT_VERBOSE
std::cout<<"done"<<std::endl;
#endif

    
    // preparing hamiltonian formula engine
#ifdef _QMT_VERBOSE
std::cout<<"Setting Hamiltonian formula...";
#endif
    h2_formula.set_hamiltonian(*hamiltonian);

#ifdef _QMT_VERBOSE
std::cout<<"done"<<std::endl;
#endif

#ifdef _QMT_VERBOSE
std::cout<<"Setting states for Hamiltonian formula...";
#endif
    
    h2_formula.set_StatesOptional(states);

#ifdef _QMT_VERBOSE
std::cout<<"done..."<<std::endl;
#endif

    
     for (auto state : states){
      std::cout<<state<<std::endl; 
     }

    // Hamiltonian Matrix
    problem_size=states.size();
    LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);

    LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);

    LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size,_lanczos_steps, _lanczos_eps); 
 }

double get_energy() {
 std::vector<double> one_body;


 for(unsigned int i = 0; i < microscopic.number_of_one_body();++i) {
      auto integral = microscopic.get_one_body(i);
       std::cout<<"Getting one particle energy  #"<<i+1<<"|"<<microscopic.number_of_one_body()<<":="<<integral<<std::endl;
    if(fabs(integral) < 1.0e-3) integral = 0.0;
     h2_formula.set_microscopic_parameter(1,i,integral);
     one_body.push_back(integral);
   
}
 

std::vector<std::tuple<bool,double>> two_bd;

/*
std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
double avIntegral=0.0;
for(int i=0; i<20; i++){
double localInt=microscopic.get_two_body(16);
avIntegral+=localInt;
std::cout<<"I"<<i<<" ";
microscopic.show_two_body_code(16);
std::cout<<"= "<<localInt<<std::endl;
}
std::cout<<"Average ";
microscopic.show_two_body_code(16);
std::cout<<"= "<<avIntegral/20.0<<std::endl;
std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
*/
 for(unsigned int i = 0; i < microscopic.number_of_two_body();++i) {
   
     auto integral = microscopic.get_two_body(i);
     std::cout<<"{";
     microscopic.show_two_body_code(i);
     std::cout<<"#"<<integral<<"#}"/* << "error_est = "<<microscopic.get_variance(i)*/<<" "<<i+1<<"|"<<microscopic.number_of_two_body()<<std::endl;
     
        if(fabs(integral) < 5.0e-3) integral = 0.0;
     two_bd.push_back(std::make_tuple(false,(integral)));
//    h2_formula.set_microscopic_parameter(2,i,integral);
 }


unsigned int cntr = 0;
const double eps = 5.0e-3;
 for(auto& bd : two_bd) {
   if(!std::get<0>(bd)) {
     for(auto i = cntr + 1; i < two_bd.size();++i) {
      if(fabs( std::get<1>(bd) - std::get<1>(two_bd[i])) < eps) two_bd[i] = std::make_tuple(true,std::get<1>(bd));
     }
   }
   cntr++;
   std::get<0>(bd) = true;
}

cntr = 0;
for(const auto& bd : two_bd){
 h2_formula.set_microscopic_parameter(2,cntr,std::get<1>(bd));
 cntr++;
 }

h2_formula.get_Hamiltonian_Matrix(hamiltonian_M);
std::cout<<*hamiltonian_M;
//LanczosSolver->solve(hamiltonian_M);



//double energy = LanczosSolver->get_minimal_eigenvalue();

std::vector<std::tuple<int,int,double>> h_matrix;
 std::vector<double> eig_vals;
std::vector<std::vector<double>> eig_vecs;

  for(auto i = 0U; i < problem_size;++i) {
    for(auto j = 0U; j < problem_size;++j) {
       double value = LocalAlgebras::SparseAlgebra::GetMatrixE(hamiltonian_M,i,j);
      if(fabs(value) > 1.0e-10)
       h_matrix.push_back(std::make_tuple(i,j,value));
    }
  }



int s = states.size();
                        gsl_vector* eigenvalues=gsl_vector_calloc(s);
			gsl_matrix* eigenvectors = gsl_matrix_calloc(s,s);

                        gsl_matrix* matrix = gsl_matrix_calloc(s,s);
			gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(s);
for(int i = 0; i <s; ++i) {
  for(int j = 0; j <s; ++j)
    gsl_matrix_set(matrix,i,j,hamiltonian_M->get_value(i,j));
}



			
			gsl_eigen_symmv(matrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);
	
			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);

for(auto i = 0U; i < states.size();++i) {
std::cout<<"GSL eigen#"<<i<<" = "<<gsl_vector_get(eigenvalues,i)<<"  | ";
  for(auto j = 0U; j < states.size();++j)
    std::cout<<"  "<<gsl_matrix_get(eigenvectors,j,i)<<"  ";
  std::cout<<std::endl;
}


for(auto i = 0U; i < 16;++i) {
  std::string lfnm = "eigen_vec" + std::to_string(i) + ".dat";
  std::ofstream output(lfnm.c_str());
   for(auto j = 0U; j < states.size();++j)
     output<<gsl_matrix_get(eigenvectors,j,i)<<" "<<states[j]<<std::endl;
  output.close();
}


    std::vector<double> density;
    std::vector<double> SiSj;
    std::vector<double> occupancy;
for(int index=0; index<16;++index){
  density.clear();
  SiSj.clear();
  occupancy.clear();
  
    for(unsigned int i=0; i<number_of_centers; i++){
        double avO=0.0;
        for(unsigned int j=0; j<number_of_centers; j++){
            double avN=0.0;
            double avS=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    avN+=gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l])));
                    if (i==j) avO+=gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l])));

                    avS+=0.25*gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(states[l])))));
                    avS-=0.25*gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(states[l])))));
                    avS-=0.25*gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(states[l])))));
                    avS+=0.25*gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(states[l])))));
                    avS+=0.5*gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(j)(sq_operator::get_up_anihilator(j)(states[l])))));
                    avS+=0.5*gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(j)(sq_operator::get_down_anihilator(j)(states[l])))));
                }
            }
            density.push_back(avN);
            SiSj.push_back(avS);
        }
	occupancy.push_back(avO);
    }
    for(unsigned int i=0; i<number_of_centers; i++){
        double avO=0.0;
        for(unsigned int j=0; j<number_of_centers; j++){  
            double avN=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    avN+=gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l])));
                    if (i==j) avO+=gsl_matrix_get(eigenvectors,k,index)*gsl_matrix_get(eigenvectors,l,index)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l])));
                }
            }
            density.push_back(avN);
        }
	occupancy.push_back(avO);
    }
    
/*    if (index==0){
	std::cout<<"++++++++++++++++++++++++++++++"<<std::endl;
	for(auto i=0U; i<number_of_centers; i++){
	    for(auto j=0U; j<number_of_centers; j++){
		std::cout<<density[number_of_centers*i+j]<<" ";
	    }
	    std::cout<<std::endl;
	}
	std::cout<<"------------------------------"<<std::endl;
	for(auto i=0U; i<number_of_centers; i++){
	    for(auto j=0U; j<number_of_centers; j++){
		std::cout<<density[number_of_centers*number_of_centers+number_of_centers*i+j]<<" ";
	    }
	    std::cout<<std::endl;
	}
    
    }
*/
    
    double Stot=0.0;
    for (const auto& iter : SiSj)
	Stot+=iter;
   	 microscopic.print_density(density,(std::string("density")+std::to_string(index)+std::string(".dat")).c_str());
	save_vector(std::vector<double>({Stot}),std::string("totalspin")+std::to_string(index)+std::string(".dat"));
	save_vector(SiSj,std::string("spin")+std::to_string(index)+std::string(".dat"));
	save_vector(occupancy,std::string("occupancy")+std::to_string(index)+std::string(".dat"));
	save_vector(density,std::string("wall_occupancy")+std::to_string(index)+std::string(".dat"));
}


 double  energy = gsl_vector_get(eigenvalues,0);
	                gsl_matrix_free(matrix);
			gsl_matrix_free(eigenvectors);
			gsl_vector_free(eigenvalues);
return  energy;  
}



/* Get N-Wave density */   
    std::vector<double> NWaveFunctionEssentials(){
 
   // LanczosSolver->get_minimal_eigenvector(eigenVector, hamiltonian_M);
    std::vector<double> result;


    for(unsigned int i=0; i<number_of_centers; i++){
        for(unsigned int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l])));
                }
            }
            result.push_back(average);
        }
    }
    for(unsigned int i=0; i<number_of_centers; i++){
        for(unsigned int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l])));
                }
            }
            result.push_back(average);
        }
    }
    return result;
}

/* Get Occupation Correlation <n_i> */   
    std::vector<double> Occupation(){
 
   // LanczosSolver->get_minimal_eigenvector(eigenVector, hamiltonian_M);
    std::vector<double> result;


    for(unsigned int i=0; i<number_of_centers; i++){
            double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(states[l])));
                }
            }
            result.push_back(average);
    }
    for(unsigned int i=0; i<number_of_centers; i++){
            double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l])));
                }
            }
            result.push_back(average);
    }
    return result;
}

/* Get Spin Correlation <S_i^2>*/
    std::vector<double> SiSquare(){
 
    //LanczosSolver->get_minimal_eigenvector(eigenVector, hamiltonian_M);
    std::vector<double> result;


    for(unsigned int i=0; i<number_of_centers; i++){
        double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=0.75*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(states[l])));
                    average+=0.75*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l])));
                    average-=1.5*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l])))));
                }
            }
            result.push_back(average);
    }

    return result;
}

/* Get Spin Correlation <S_i S_j>*/
    std::vector<double> SiSj(){
 
    //LanczosSolver->get_minimal_eigenvector(eigenVector, hamiltonian_M);
    std::vector<double> result;


    for(unsigned int i=0; i<number_of_centers; i++){
    for(unsigned int j=0; j<number_of_centers; j++){
        double average=0.0;
            for(unsigned int k=0; k<states.size(); k++){
                for(unsigned int l=0; l<states.size(); l++){
                    average+=0.25*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(states[l])))));
                    average-=0.25*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(states[l])))));
                    average-=0.25*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(states[l])))));
                    average+=0.25*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(states[l])))));
                    average+=0.5*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(i)(sq_operator::get_up_anihilator(j)(states[l])))));
                    average+=0.5*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],
                sq_operator::get_down_creator(i)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(i)(sq_operator::get_down_anihilator(j)(states[l])))));
                }
            }
            result.push_back(average);
    }
    }

    return result;
}

void save_vector(std::vector<double> my_vector, std::string file_name){
	std::ofstream output(file_name);

	for(const auto &iter : my_vector)
		output<<iter<<" ";

	output.close(); 

}

~System() {

    LocalAlgebras::SparseAlgebra::DeinitializeMatrix(hamiltonian_M);
    LocalAlgebras::SparseAlgebra::DeinitializeVector(eigenVector);
	delete LanczosSolver;
}
};



int main() {
  
  System system("conf.dat");
 std::cout<<"Energy = "<<system.get_energy()<<std::endl;;
/* for(const auto x : system.NWaveFunctionEssentials())
   std::cout<<x<<" ";
 std::cout<<std::endl;
  system.microscopic.print_density(system.NWaveFunctionEssentials());*/
  return 0; 
}
