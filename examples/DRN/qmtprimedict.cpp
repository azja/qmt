#include "qmtprimedict.h"


int qmt::prime(int n){
        const int primes[32] = {2, 3, 5, 7, 11, 13, 17, 19, 
                                23, 29, 31, 37, 41, 43, 47, 
                                53, 59, 61, 67, 71, 73, 79, 
                                83, 89, 97, 101, 103, 107, 
                                       109, 113, 127, 131};

        if(n>=32)       return -1;
        else            return primes[n];
}


qmt::QmtIntegralCode::QmtIntegralCode(int i, int j, int k, int l, int sgn1,int sgn2,int sgn3,int sgn4):
    outer(prime(i) * prime(k), sgn1 * sgn3), inner(prime(j) * prime(l), sgn2 * sgn4), total_momentum(fabs(sgn1+sgn2+sgn3+sgn4)) {

}

bool qmt::QmtIntegralCode::operator==(const QmtIntegralCode& code) const {
   return ((outer == code.outer && inner == code.inner) || (outer == code.inner && inner ==code.outer)) && total_momentum == code.total_momentum;
}
