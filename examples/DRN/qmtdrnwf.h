#include "../../headers/qmtspline.h"
//#include "../../headers/qmtparsertools"
#include <fstream>
#include <math.h>
#include <iostream>
template <typename Radial>
struct QmtDrnWaveFunction {

Radial *radial;
int _n;
int _l;
double _d;
size_t size;
double max_xy,max_z;

QmtDrnWaveFunction(const char* radial_file, size_t size,int n, int l, double d):_n(n),_l(l), _d(d){

std::cout<<"radial file:"<<radial_file<<" l = "<<_l<<std::endl;
max_xy = 173 * 173;

max_z = _d/2;

double *X = new double [size];
double *Y = new double [size];

std::ifstream wf(radial_file);
       if (wf.is_open())
       {
        for(int i = 0; i <size;++i) {
	    double x,y;
	    wf>>x>>y;
	    X[i] = x;
            Y[i] = y;	    
	  }	    
	}
	wf.close();
 radial = new Radial(X,Y, size);
 delete [] X;
 delete [] Y;
}

double  get_value(double x, double y, double z) {

double distance2 = x*x + y*y ;
//std::cout<<"z = "<<z<<std::endl;

double r = -1.0;

bool was_run = false;

if(distance2 < 0.028*0.028){
    double x1 = 0.028;
    double x2 = 0.056;
    double y1 = radial->get_value(x1);
    double y2 = radial->get_value(x2);
     
    r = ((y1-y2)/(x1*x1-x2*x2))*distance2 + y1 - ((y1-y2)/(x1*x1-x2*x2))*x1*x1;

    was_run = true;
}

if(distance2 >= max_xy ||  z <= -2.5 || z >= 2.5 /*|| distance2 < 1.0e-1*/) return 0;
//std::cout<<"_d = "<<_d<<std::endl;
double angular = 1.0;
if(_l < 0)
  angular= sin(fabs(_l)* atan2(y,x))*sqrt(2);
if(_l > 0)
  angular= cos(fabs(_l)* atan2(y,x))*sqrt(2);
// std::cout<<"angular = "<<angular<<std::endl;

if(!was_run)
 r= radial->get_value(sqrt(x*x + y*y));

return angular * r * cos(M_PI*z/_d) * sqrt(2/_d);
}



};
