#ifndef HAMILTONIANH2SC_H
#define HAMILTONIANH2SC_H

#include <iostream>
#include <iomanip>
#include "../../../headers/qmthbuilder.h"
#include "../../../headers/qmtstate.h"
#include "../../../headers/qmthubbard.h"
#include <math.h>
#include <algorithm>
#include <string>
#define ELECTRONS 6

namespace qmt {

class QmtHamiltonianH2Sc {

    typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;

public:
    qmt::QmtHubbard<sq_operator>* Hubbard;

    void read_hamiltonian(char* filename);

     QmtHamiltonianH2Sc();
    ~QmtHamiltonianH2Sc();

};
}// end of namespace qmt
#endif
