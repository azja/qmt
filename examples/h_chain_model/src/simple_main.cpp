#define GSL_LIBRARY
#include "../../../headers/qmtmpipool.h"
#include <iostream>
#include <iomanip>
#include <iterator>
#include "../include/microscopic.h"
//#include "hamiltonian.h"
#include <math.h>
#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
#include <algorithm>
#include "../../../headers/qmtbasisgenerator.h"
#include <gsl/gsl_matrix.h>
#include "../include/hamiltonianH2sc.h"
#include "../../../headers/MatrixAlgebras/SparseAlgebra.h"
#include "../../../headers/qmtmatrixformula.h"
#include "../../../headers/qmtreallanczos.h"
#include <stdlib.h>
#include <fstream>

/*
 * main.cpp
 *
 *  Created on: 01 kwi 2015
 *      Author: akadzielawa
 */

 unsigned int  NSIZE;
 typedef qmt::SqOperator<qmt::QmtNState<qmt::uint, int> > sq_operator;


int main(int argc, char* argv[]) {
   

    double a = 4.0;
    double R = 0.5*a;
    double theta =  0.0001;

    unsigned int ng=9;
 
    qmt::examples::MicroscopicH2chain model;

     model.set_all(a, R, 1, theta,ng);
     std::cout<<model.get_one_body(1)<<std::endl;

    return 0;
}
